# Getting Started

Welcome to GudHub! This article is a complete guide for developers to get started with GudHub Lib.

## Installation

### NPM

Before initialization GudHub lib you must install it on your device. Run npm command:

```js
npm install @gudhub/core
```

If you have problems with installing, make sure you have Node.js and npm installed on your device.

>Your Node.js version must be above 14 and above 16.18.0 for interpreted values.

After successful installation, you can initialize the library in the project.

```js
import Gudhub from "@gudhub/core"

const gudhub = new Gudhub();
```

### CDN

In addition, the GudHub library is available via **CDN**. It eliminates the need to install a library.

```html
<script src="https://unpkg.com/@gudhub/core@1.1.30/umd/library.min.js"></script>
```

## Get Application Name

Once the GudHub library is installed, all its methods will be available to you. The best way to get acquainted with the library practical example.

So let's start with one of the simplest thing - getting the name of the application. In this tutorial we will create an HTML page with header and button.

![Example of the page](getting-started-images/getting-started-example-page.jpg "Example page")

After clicking on that button the name of the certain application will be displayed instead of the header.

>The application used in the example is called **Celebrities** and the application ID is **28837**.

![Gotten name of the application](getting-started-images/getting-started-gotten-app-name.jpg "Application name")

### Step 1

First of all, we must initialize lib in the project as we did in the [previous section](#cdn).

```html
<script src="https://unpkg.com/@gudhub/core/umd/library.min.js"></script>
```

### Step 2

In addition to the main script, using the following code we will make a header and a button.

```html
<div class="wrapper">
    <h1>Get APP name</h1>
    <div class="input-box">
        <button class="button">GET APP NAME</button>
    </div>
    <pre class="output"></pre>
</div>
```

### Step 3

The next step is the main script. This tag can be used without any attributes.

Let's start by getting the GudHub class from lib to freely use the method we need.

```js
const { GudHub } = GudHubLibrary;
```

Then create an instance of the class using your authentication key.
>Please note that you will not have access to the application data without your authorization key.

```js
const auth_key = 'NWfWIMActkcc2Z3p4dkjflsjnsncsakjdalkjdsl/acs3487kncKJKGxc';

const gudhub = new GudHub(auth_key);
```

### Step 4

After that, let's set the selectors for button and H1 where the app name will be displayed.

```js
const button = document.querySelector('.button');
const output = document.querySelector('h1');
```

And let's write an event for our button.

```js
button.addEventListener('click', async () => {
});
```

The function for it is described in the following steps.

### Step 5

Every application has property with its name. So first we have to get the complete application object. The method called ***`getApp`*** is used for that.

```js
const app = await gudhub.getApp(app_id);
console.log(app);
```

It returns the whole application by its identifier.The answer will be similar to this one:

```json
{
    "app_id": 28837,
    "app_name": "Celebrities",
    "icon": {
        "id": 27343,
        "icon_id": "user",
        "icon_color": "ffffff",
        "gradient_up": "ffc3a0",
        "gradient_down": "ffafbd"
    },
    "group_id": 28808,
    "trash": false,
    "last_update": 1659341227775,
    "view_init": 1530891,
    "privacy": 0,
    "views_list": [],
    "items_list": [],
    "field_list": []
}
```

### Step 6

Then from the resulting object we can get its the name of the application and display it as a header.

```js
const beautifiedApp = JSON.stringify(app.app_name, null, 2);

output.innerHTML = beautifiedApp;
```

### Full Code

```html
<body>
    <script src="https://unpkg.com/@gudhub/core/umd/library.min.js"></script>

    <div class="wrapper">
        <h1>Get APP name</h1>
        <div class="input-box">
            <button class="button">GET APP NAME</button>
        </div>
    </div>
    
    <script>
        const { GudHub } = GudHubLibrary;

        const auth_key = 'NWfWIMsdsfdsf43sd8jvKh/aFN4V5LG2//5ixx7TYnjABS3ky6U1tH0MKC+Ya/zNSGF4VKUq25\n1JqIf/mZXA==';
        const app_id = '28837';


        const gudhub = new GudHub(auth_key);

        const button = document.querySelector('.button');
        const output = document.querySelector('h1');

        button.addEventListener('click', async () => {
            const app = await gudhub.getApp(app_id);
            console.log(app);
            const beautifiedApp = JSON.stringify(app.app_name, null, 2);

            output.innerHTML = beautifiedApp;
        });

    </script>
</body>
```
