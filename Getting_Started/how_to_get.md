# How To Get Credentials

GubHub consists of lots of elements like applications, items and fields. Each of them has its own ID that in turn is used in different methods and functions. During programming with the help of [GudHub Lib](../Core_API/lib.md) you need to use certain IDs all the time.

It is also important that access to the [GudHub library](../Core_API/lib.md) can be implemented using a user authentication key. This leads to the fact that you need to know where to get all this data.
So, in this chapter, we will tell you where and how you can get the ID you need.

Each of elements have at least two ways how to get it. Here we will talk about ways to get a specific ID. But you can also get IDs using methods like `getFieldList`.

## Application ID

### App ID with the Help of Link

This is the first and the easiest way to get ID. As you may have gathered, app ID is hiding in the browser link. Open your application. It will have a link that looks like this:

        https://gudhub.com/act/open_app/25904/1442266

The first number after the base url is exactly app ID. In this case, it is 25904.

### App ID with the Help of DevTools

DevTools is probably well-know for you and will help you to get an application ID.

1\. Open *DevTools* in page of your application and switch to *Network*.

![App ID example of active DevTools ](how-to-get-credentials/app-item-field-id-devtools-step-1.jpg "Active Network tab in DevTools. App ID")

2\. In filter choose *Fetch/XHR*.

![App ID Fetch/XHR filtering example](how-to-get-credentials/app-item-field-id-devtools-step-2.jpg "Fetch/XHR filtering. App ID")

3\. Find *get?app_id* in the table below and click on it. Then the *Preview* will appeared.

![App ID example of opened Preview](how-to-get-credentials/app-item-field-id-devtools-step-3.jpg "Open application preview. App ID")

4\. The first property in *Preview* is *app_id*.

![Example of application and application ID](how-to-get-credentials/app-id-devtools-step-4.jpg "Application ID in DevTools")

### App ID with the Help of GudHub

1\. Open the desired application and right-click anywhere. Then choose *Edit field* in appeared list.

![Example of settings bar](how-to-get-credentials/app-id-gudhub-step-1.jpg "Edit Field")

2\. Choose *EMBED* in pop-up box at the bottom of the page.

![Open settings of example field](how-to-get-credentials/app-id-gudhub-step-2.jpg "Field settings")

3\. Find the *app_id* in the appeared code.

![EMBED of the example field](how-to-get-credentials/app-id-gudhub-step-3.jpg "EMBED of the field")

>This method will not works if you do not have editing access to the application

## Item ID

### Item ID with the Help of link

Open needed item in GudHub. It will have a link looks like:

        https://gudhub.com/act/open_item/25904/1442279/2996764

The last number in it is an item ID. In our case, it is 2996764.

### Item ID with the Help of DevTools

1\. Open *DevTools* in page of your application and switch to *Network*.

![Item ID example of active DevTools](how-to-get-credentials/app-item-field-id-devtools-step-1.jpg "Active Network tab in DevTools. Item ID")

2\. In filter choose *Fetch/XHR*.

![Item ID Fetch/XHR filtering example](how-to-get-credentials/app-item-field-id-devtools-step-2.jpg "Fetch/XHR filtering. Item ID")

3\. Find *get?app_id* in the table below and click on it. Then the *Preview* will appeared.

![Item ID example of opened Preview](how-to-get-credentials/app-item-field-id-devtools-step-3.jpg "Application Preview. Item ID")

4\. Find the items_list and open it. Then select needed item, there will be *item_id*.

![Example of application and item ID](how-to-get-credentials/item-id-devtools-step-4.jpg "Item ID in DevTools")

## Field ID

### Field ID with the Help of DevTools

1\. Open *DevTools* in page of your application and switch to *Network*.

![Field ID example of active DevTools](how-to-get-credentials/app-item-field-id-devtools-step-1.jpg "Active Network tab in DevTools. Field ID")

2\. In filter choose *Fetch/XHR*.

![Field ID Fetch/XHR filtering example](how-to-get-credentials/app-item-field-id-devtools-step-2.jpg "Fetch/XHR filtering. Field ID")

3\. Find *get?app_id* in the table below and click on it. Then the *Preview* will appeared.

![Field ID example of opened Preview](how-to-get-credentials/app-item-field-id-devtools-step-3.jpg "Application Preview. Item ID")

4\. Now you can get field ID from:

- field_list. Just open it. Find the needed field by its name.

 ![Example of getting field ID from field list](how-to-get-credentials/field-id-devtools-step-4-field-list.jpg "Field ID from  the field list")

- items_list. Find the certain item and needed field in it

 ![Example of getting field ID from items list](how-to-get-credentials/field-id-devtools-step-4-items-list.jpg "Field ID from  the items list")

### Field ID with the Help of GudHub

1\. Open the application and right-click at the chosen field. Then choose *Edit field* in appeared list.

![Example of edit chosen field](how-to-get-credentials/field-id-gudhub-step-1.jpg "Edit field")

2\. Choose *EMBED* in pop-up box at the bottom of the page.

![Settings of the chosen field from example table](how-to-get-credentials/field-id-gudhub-step-2.jpg "Settings of the chosen field")

3\. Find the *field_id* in the appeared code.

![Code of the example field from EMBED](how-to-get-credentials/field-id-gudhub-step-3.jpg "Field ID in EMBED")

>This method will not works if you do not have editing access to the application

## Authentication Key

### Authentication Key with the Help of DevTools

1\. Log in to your GudHub account with *DevTools* open.

![Login with DevTools](how-to-get-credentials/auth-key-devtools.jpg "DevTools")

2\. Switch to the Network tab and select the *Fetch/XHR* filter.

![Required tab](how-to-get-credentials/auth-key-network.jpg "Network tab")

3\. Find the *login request*, and open its preview.

![Login request preview](how-to-get-credentials/auth-key-preview.jpg "Request preview")

There is property called `auth_key` where you can find your authentication key.

### Authentication Key with the Help of GudHub

1\. To get your authorization key in GudHub, you need to open application you need or its item.

>You must have **Admin** rights to this application to perform the following steps..

2\. Right-click on any element and select *Edit Field*.

![Opening element settings](how-to-get-credentials/edit-field-get-auth-key.jpg "Edit Field")

3\. Then select *tab EMBED* where you can find your auth_key in the body.

![Auth key in EMBED](how-to-get-credentials/embed-auth-key.jpg "EMBED")
