# Nested List Method

**makeNestedList** is a method that allows to create the list of fields with infinite nesting. This method is used in [Nested List gh-element](../Gh_Elements/nested_list_element.md). It takes an array of fields and depends of their IDs makes attachments for each of them.

The current method can accept five arguments but only three of them are mandatory. More details in the [next chapter](#arguments).

Argument|Type|Description
:---|:---|:---
arr|`array`|*array of all fields that will be used to make nested list*
id|`string`, `number`|*the actual ID of the field*
parent_id|`string`, `number`|*ID of the field parent*
children_property|`string`|*name of the array of children fields*
priority_property|`string`|*property by which fields will be sorted*

The calling of the method is looks like this:

```js
import GudHub from '@gudhub/core';
const gudhub = new GudHub();
let input = [{
    name: "Article 1",
    id: 1,
    parent_id: 0
},{
    name: "Article 2",
    id: 2,
    parent_id: 0
}];

gudhub.makeNestedList(arr, 'id', 'parent_id', 'children', 'priority')
```

As a result, the method returns an array of objects that have no parent. If they have children, the objects get a new property "children". Objects that have a parent are in the child array of the corresponding object.

```json
[{
    "name": "Article 1",
    "id": 1,
    "parent_id": 0,
    "children": [{
        "name": "Article 2",
        "id": 2,
        "parent_id": 0
    }]
}]
```

## Arguments

As mentioned above, three method arguments are mandatory, the rest are not.

Mandatory:

- *arr* - array of fields which have at least three properties: name, ID, and parent ID.
- *id* - self ID of the field
- *parent_id* - ID of the field that is the parent of the current field

Optional ones:

- *children_property* - a name that will be given to the array of child objects.
- *priority_property* - a name of the property by which fields will be sorted.

> All arguments except `arr` can be called anything, as long as it is a string type. And `id` and `parent_id` can also be any numbers.

## Using makeNestedList

As mentioned above, the method takes at least three arguments. Therefore, three examples are given below:

- [For a set of mandatory arguments only](#three-arguments)
- [For a set of arguments with the property children](#four-arguments)
- [For the full set of mandatory and optional arguments](#five-arguments)

### Three Arguments

The first example shows the method call with only three mandatory arguments:

```js
import GudHub from '@gudhub/core';
const gudhub = new GudHub();
let input = [
    {
        name: "Article 1",
        id: 1,
        parent_id: 0
    },
    {
        name: "Article 2",
        id: 2,
        parent_id: 0
    },
    {
        name: "Article 3",
        id: 3,
        parent_id: 1
    },
    {
        name: "Article 4",
        id: 4,
        parent_id: 2
    },
    {
        name: "Article 5",
        id: 5,
        parent_id: 2
    },
    {
        name: "Article 6",
        id: 6,
        parent_id: 1
    },
    {
        name: "Article 7",
        id: 7,
        parent_id: 3
    }
];

let nestedList = gudhub.makeNestedList(input, 'id', 'parent_id');

nestedList.forEach(field => {
    console.log("name:", field.name);
    console.log("id:", field.id);
    console.log("parent_id:", field.parent_id);
    console.log("children:", field.children);
});
```

It returns such an array:

```json
[{
    "name": "Article 1",
    "id": 1,
    "parent_id": 0,
    "children": [{ 
        "name": "Article 3", 
        "id": 3, 
        "parent_id": 1, 
        "children": [{
            "name": "Article 7",
            "id": 7,
            "parent_id": 3
        }] 
    },{ 
        "name": "Article 6", 
        "id": 6, 
        "parent_id": 1 
    }]
},{
    "name": "Article 2",
    "id": 2,
    "parent_id": 0,
    "children": [{ 
        "name": "Article 4", 
        "id": 4, 
        "parent_id": 2 
    },{ 
        "name": "Article 5", 
        "id": 5, 
        "parent_id": 2 
    }]
}]
```

### Four Arguments

The second example is calling the method with mandatory arguments and the new name of the children array.

```js
import GudHub from '@gudhub/core';
const gudhub = new GudHub();
let input = [
    {
        name: "Article 1",
        id: 1,
        parent_id: 0
    },
    {
        name: "Article 2",
        id: 2,
        parent_id: 0
    },
    {
        name: "Article 3",
        id: 3,
        parent_id: 1
    },
    {
        name: "Article 4",
        id: 4,
        parent_id: 2
    },
    {
        name: "Article 5",
        id: 5,
        parent_id: 2
    },
    {
        name: "Article 6",
        id: 6,
        parent_id: 1
    },
    {
        name: "Article 7",
        id: 7,
        parent_id: 3
    }
];

let nestedList = gudhub.makeNestedList(input, 'id', 'parent_id', 'children_new');

nestedList.forEach(field => {
    console.log("name:", field.name);
    console.log("id:", field.id);
    console.log("parent_id:", field.parent_id);
    console.log("children_new:", field.children_new);
});
```

The following result is returned:

```json
[{
    "name": "Article 1",
    "id": 1,
    "parent_id": 0,
    "children_new": [{ 
        "name": "Article 3", 
        "id": 3, 
        "parent_id": 1, 
        "children_new": [{
            "name": "Article 7",
            "id": 7,
            "parent_id": 3
        }] 
    },{ 
        "name": "Article 6", 
        "id": 6, 
        "parent_id": 1 
    }]
},{
    "name": "Article 2",
    "id": 2,
    "parent_id": 0,
    "children_new": [{ 
        "name": "Article 4", 
        "id": 4, 
        "parent_id": 2 
    },{ 
        "name": "Article 5", 
        "id": 5, 
        "parent_id": 2 
    }]
}]
```

### Five Arguments

The last example shows how a method is called with all its arguments.

```js
import GudHub from '@gudhub/core';
const gudhub = new GudHub();
let input = [{
        name: "Article 1",
        id: 1,
        parent_id: 0,
        sort_position: 7
    },
    {
        name: "Article 2",
        id: 2,
        parent_id: 0,
        sort_position: 6
    },
    {
        name: "Article 3",
        id: 3,
        parent_id: 1,
        sort_position: 5
    },
    {
        name: "Article 4",
        id: 4,
        parent_id: 2,
        sort_position: 4
    },
    {
        name: "Article 5",
        id: 5,
        parent_id: 2,
        sort_position: 3
    },
    {
        name: "Article 6",
        id: 6,
        parent_id: 1,
        sort_position: 2
    },
    {
        name: "Article 7",
        id: 7,
        parent_id: 3,
        sort_position: 1
}];

let nestedList = gudhub.makeNestedList(input, 'id', 'parent_id', 'children_test', 'sort_position');

nestedList.forEach(field => {
    console.log("name:", field.name);
    console.log("id:", field.id);
    console.log("parent_id:", field.parent_id);
    console.log("sort_position:", field.sort_position);
    console.log("children_test:", field.children_test);
});
```

It returns an array sorted by the selected property:

```json
[{
    "name": "Article 2",
    "id": 2,
    "parent_id": 0,
    "sort_position": 6,
    "children_test": [{ 
        "name": "Article 5", 
        "id": 5, 
        "parent_id": 2, 
        "sort_position": 3 
    },{ 
        "name": "Article 4", 
        "id": 4, 
        "parent_id": 2, 
        "sort_position": 4 
    }]
  },{
    "name": "Article 1",
    "id": 1,
    "parent_id": 0,
    "sort_position": 7,
    "children_test": [{ 
        "name": "Article 6", 
        "id": 6, 
        "parent_id": 1, 
        "sort_position": 2
    },{
        "name": "Article 3",
        "id": 3,
        "parent_id": 1,
        "sort_position": 5,
        "children_test": [{
            "name": "Article 7",
            "id": 7,
            "parent_id": 3,
            "sort_position": 1
        }]
    }]
}]
```
