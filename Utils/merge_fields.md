# Merge Fields

**Merge Fields** is

1. [mergeFieldLists](#mergefieldlists)

2. [createFieldsListToView](#createfieldslisttoview)

3. [createFieldsListToViewWithDataType](#createFieldsListToViewWithDataType)

## mergeFieldLists

Name|Type|Description
:---|:---|:---
fieldsToView|``|
fieldList|``|

```js
gudhub.util.mergeFieldLists(fieldsToView, fieldList)
```

## createFieldsListToView

Name|Type|Description
:---|:---|:---
appId|`string`|
tableSettingsFieldListToView|``|

```js
gudhub.util.createFieldsListToView(appId, tableSettingsFieldListToView)
```

## createFieldsListToViewWithDataType

Name|Type|Description
:---|:---|:---
appId|`string`|
tableSettingsFieldListToView|``|

```js
gudhub.util.createFieldsListToViewWithDataType(appId, tableSettingsFieldListToView)
```

## Example

```js

```
