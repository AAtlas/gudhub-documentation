# Utils Overview

Utils are the supporting methods which help to work with objects. They allows to do trivial routine work much more faster and easier. Utils are mostly used in other methods. So, it also is easy to insert them in different part of code.

List of GudHub utils:

- [JSON Constructor](#json-constructor)

- [jsonToItem](#jsontoitem)

- [getDate](#getdate)

- [mergeItems](#mergeitems)

- [compareItems](#compareitems)

- [populateItems](#populateitems)

- [mergeObjects](#mergeobjects)

- [populateWithItemRef](#populatewithitemref)

## JSON Constructor

Handy tool for easy and fast JSON code generation based on existing elements. [Read more about JSON Constructor.](JSON_Conctructor/overview.md)

## jsonToItem

This method allows to generate item based on existing JSON and use it for other operations, for example, creating new item. [Read more about jsonToItem.](jsonToItem/json_to_item.md)

## getDate

This method helps to get required date. [Read more about getDate.](get_date.md)

## mergeItems

Handy method for merging several items and updating a big number of fields by field or item ID. [Read more about mergeItems.](merge_items.md)

## compareItems

This method compared items by item or field ID. [Read more about mergeItems.](compare_items.md)

## populateItems

This is the method that allows to update the item by populating it with the data from another item. [Read more about populateItems.](populate_items.md)

## mergeObjects

This method allows to merge objects such as *mergeItems* does it with items. [Read more about mergeObjects.](merge_object.md)

## populateWithItemRef

Method that allows to connect items from different applications. [Read more about populateWithItemRef.](populate_with_item_ref.md)
