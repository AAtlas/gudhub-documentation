# Date

This article describes methods that work with the date.

## Populate with Date

This method allows to automatically fill date fields in items. If it do not find no field to fill, it will add a new one.

It  accepts an array of two properties: items and model. The both are arrays of objects. **Items** is an array of items whose fields must be filled. All **model's** objects contain two properties. The first one is a date type about that you can read below and the second one is the ID of the field that will be filled.

```js
import GudHub from '@gudhub/core';
const gudhub = new GudHub();
let model = [{
    date_type: "next_day", 
    field_id:"123",
}];

let items = [{
    fields: [],
    index_number: 1,
    item_id: 2918286,
    last_update: 1638969490135,
    },
    {
    fields: [],
    index_number: 2,
    item_id: 2919988,
    last_update: 1638969479184,
}]

gudhub.populateWithDate(items, model);
```

## Get Date

This method returns requested date in milliseconds. It takes the date type by that calculates the relevant date.

```js
import GudHub from '@gudhub/core';
const gudhub = new GudHub();

let date_type = 'next_week';

gudhub.getDate(date_type);
```

## Date Types

In the following table, you can find all possible data types that can be accepted by this method.

Name|Description
:---|:---
now|will return current date
next_day|will return current day + 24hours
two_days_after|adds two days(24hours) to current date
three_days_after|adds three days(24hours) to current date
four_days_after|adds four days(24hours) to current date
next_week|adds one week(7 days) to current date
two_weeks_after|adds two weeks(7 days) to current date
three_weeks_after|adds three weeks(7 days) to current date
this_sunday|will return date for sunday, with current utc time if it's a monday then it will return sunday from the next week
this_monday|will return date for monday, with current utc time if it's a saturday then it will return monday from the next week
this_tuesday|will return date for tuesday, with current utc time if it's a saturday then it will return tuesday from the next week
this_wednesday|will return date for wednesday, with current utc time if it's a saturday then it will return wednesday from the next week
this_thursday|will return date for thursday, with current utc time if it's a saturday then it will return thursday from the next week
this_friday|will return date for friday, with current utc time if it's a saturday then it will return friday from the next week
this_saturday|will return date for monday, with current utc time if it's a sunday then it will return saturday from the next week
