# Item Selection

**Item Selection** is a service that works with items selection. It is used in lots of gh-elements. It makes no difference where the element is displayed. Items can be selected in different elements or even in different applications. If an item is selected in one place, it remains selected everywhere.

The Item Selection has four methods.

1. [selectItems](#selectItems)

2. [getSelectedItems](#getSelectedItems)

3. [clearSelectedItems](#clearSelectedItems)

4. [isItemSelected](#isItemSelected)

Different elements use different methods:

- selectItems, isItemSelected:
  - [Table](../Gh_Elements/table.md)
  - [Cards](../Gh_Elements/cards.md)
- getSelectedItems:
  - [Delete](../Gh_Elements/delete_item.md)
  - [Update Items](../Gh_Elements/update_items.md)
  - [Clone Items](../gh_elements/clone_item.md)
  - [Export CSV](../gh_elements/export_csv.md)
  - [Print Document](../Gh_Elements/print_document.md)
  - Quote Tool
  - Send Email
  - [Smart Input](../Gh_Elements/smart_input.md)
  - Twilio Autodialer
- clearSelectedItems
  - [Delete](../Gh_Elements/delete_item.md)
  - [Update Items](../Gh_Elements/update_items.md)
  - [Clone Item](../Gh_Elements/clone_item.md)
  - [Print Document](../Gh_Elements/print_document.md)
  - Quote Tool
  - [Smart Input](../Gh_Elements/smart_input.md)
  - [Table](../gh_elements/table.md)

The selection works only as long as the browser tab is active. When the page reloads, all selections are reset.

## selectItems

This is the method of the items selection that allows to select items.

It accepts two arguments:

Name|Type|Description
:---|:---|:---
appId|`string`|*contains ID of the application where the items is located*
items|`array`|*contains data of items which will be selected*

```js
gudhub.utils.selectItems(appId, itemId)
```

## getSelectedItems

This method allows to get an array of items which are selected in certain application.

It accepts only the ID of the application as an argument.

Name|Type|Description
:---|:---|:---
appId|`string`|*contains ID of the application from which selected items will be gotten*

This method can be simply called:

```js
gudhub.util.getSelectedItems(appId)
```

## clearSelectedItems

The current method allows to clear the selection of all items which are already selected in application.

Name|Type|Description
:---|:---|:---
appId|`string`|*contains ID of the application where the selected items will be cleared*

For calling this method use:

```js
gudhub.util.clearSelectedItems(appId)
```

## isItemSelected

This method is designed for checking whether the item is selected or not.

It accepts two arguments:

Name|Type|Description
:---|:---|:---
appId|`string`|*contains ID of the application where the items is located*
itemId|`string`|*contains ID of the item that will be checked*

The function call is looks like this:

```js
gudhub.util.isItemSelected(appId, itemId)
```

## Example

Here is an illustrative example. All methods of the Item Selection service are used here one by one.

```js
import GudHub from '@gudhub/core';
const gudhub = new GudHub();

// example app ID
const APPID = "23457";
// example array of items
let items = [{
    "item_id": 1,
    "fields": []
    },{
    "item_id": 2,
    "fields": []
    },{
    "item_id": 3,
    "fields": []
}];

// at first we select the second item
await gudhub.util.selectItems(APPID, [items[1]]);

// after that, output to the console all selected items in application
let selectedItems = gudhub.util.getSelectedItems(APPID);
console.log(selectedItems);

// then check whether the second item is selected
let isItemSelected = gudhub.util.isItemSelected(APPID, 2);
console.log(isItemSelected);

// clear the selection
gudhub.util.clearSelectedItems(APPID);

// and check the selection of the second item again
isItemSelected = gudhub.util.isItemSelected(APPID, 2);
console.log(isItemSelected);
```

The example code returns:

```json
// getSelectedItems for the application
{ items: [ { item_id: 2, fields: [] } ], app: null }

// isItemSelected for the items 
true
false
```
