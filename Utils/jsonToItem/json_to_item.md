# jsonToItems

**jsonToItems** is a comfortable instrument for generating items. Namely, this method converts json to gudhub items. Created items can be used for generating new ones.

```js
import GudHub from '@gudhub/core';
const AUTHKEY =  "KJGJsbjaSADAfskajlkmoiwuehyugT//ssefiooq0kd79uiohdwg2y81y";
const gudhub = await new GudHub(AUTHKEY);
const APPID = "27781";
const FIELDID = "645634";
let json = {
        "book": [
        {
                "category": "reference",
                "author": "Nigel Rees",
                "title": "Sayings of the Century",
                "price": 8.95
        }],
};

let fieldsMap = [{
        field_id: FIELDID,
        json_path : "$..author",
}];

//item generation
let items = gudhub.jsonToItems(json, fieldsMap);

// add new generated items in application
gudhub.addNewItems(APPID, items);
```

**jsonToItem** takes two objects as arguments. The first one is a source code from that the data will be taken. The second, roughly speaking, is a model by which items are generated.

Name|Type|Description
:---|:---|:---
json|`object`|*json that is going to be converted to gudhub items*
fieldsMap|`object`|*object in json format which contains instructions of how to convert to items*

The method returns items list of objects with required data:

```json
[{
    "fields":{
            "field_id": 12356,
            "field_value": "test value"
            }
    },
    {
    "fields":{
            "field_id": 12356,
            "field_value": "test value2"
            }
}, ...]
```

We suggest you to get to know more about jsonToItem method in tutorial.

## fieldsMap

The process of the conversation is based on fieldsMap. It is an array of objects. Each of them contains two properties with such values as field ID and [JSONPath query](jsonpath.md). Due to this information method gets required data from relevant field. It uses JSONPath to parse JSON.

```js
[{
        field_id: 431,
        json_path : "$..item_id"
   },
   {
        field_id: 431,
        json_path : "$..item_id"
},...]
```
