# JSONPath

>This chapter describes JSONPath only within GudHub. Accordingly, it contains partial information about that.

**JSONPath** is a query language for JSON with features that allows to get required data from JSON. It is used for selecting and extracting a sub-section from the JSON document. JSONPath expressions, including property names and values, are case-sensitive.

JsonPath expressions always refer to a JSON structure in the same way as XPath expressions are used in combination with an XML document.  It has several special rules for writing:

- Every query starts from `$` character:

        $.app_id.item_id
        $..item_id
        $[app_id].item_id

- JSONPath expressions can use the dot notation, the bracket notation or a mix of dot and bracket notations:

        $.store.book[0].title

        $['store']['book'][0]['title']

        $['store'].book[0].title

- Dots are only used before property names not in brackets.
- Within JSONPath expressions, text values are enclose in double quotes.

Other syntax elements are described in following table.

Name|Description
:---|:---
`$`|represents the root object or array and can be omitted
`.property`|selects the specified property in a parent object
`['property']`|selects the specified property in a parent object; is used for property names with special characters
`[n]`|allows to select the n-th element from the array
`[index1,index2,…]`|allows to select an array of elements with the specifies indexes
`..property`|recursively searches for property name and returns array of all values with it
`*`|wildcard selects all elements in an object or an array, regardless of their names or indexes
`[start:end]`|selects elements from the start index to the end one
`[:n]`|selects the first elements of the array
`[-n:]`|selects the last elements of the array
`[?(expression)]`|filter expression
`[(expression)]`|allows to use script expressions instead of explicit property names or indexes
`@`|used in filter expressions to refer to the current node being processed

JSONPath queries can return a single element and a list of matching elements. For example, we have JSON:

```json
{
  "name": "Rose Kolodny",
  "phoneNumbers": [
    {
      "type": "home",
      "number": "954-555-1234"
    },
    {
      "type": "work",
      "number": "754-555-5678"
    },
  ],
}
```

Enter JSON expressions:

    $.phoneNumbers[0].type
    $.phoneNumbers[*].number

As the result we will get:

    home
    [954-555-1234, 754-555-5678]

> This is not a JSON array, it is just a comma-separated list of items where [ ] indicates the beginning and end of the list.

## Filter

Filters are logical expressions used to filter arrays. A typical filter would be `[?(@.age != 18)]` where ***@*** represents the current item being processed. More complex filters can be created with logical operators ***&&*** and ***||***. String literals must be enclosed by single or double quotes `([?(@.color == 'blue')]` or `[?(@.color == "blue")])`.

### Filter operators

Operators that can be used for filters in GudHub:

№|Operator|Priority|Description
:---:|:---:|:---:|:---
1.|==|1|shows fields whose value are *equal to* the one in filter
2.|!=|1|shows fields whose value are *not equal to* the one in filter
3.|&&|2|logical AND, used to combine multiple filter expressions
4.|\| \||2|logical OR, used to combine multiple filter expressions

Following is a table of relevant examples:

№|Operator|Example expression
:---:|:---:|:---
1.|==|`[?(@.color == 'red')]`
2.|!=|`[?(@.color != 'red')]`
3.|&&|`[?(@.color=='red' && @.number < 12)]`
4.|\| \||`[?(@.color=='red' \|\| @.number < 12)]`

You can find examples of using filters in jsonToItems tutorial.
