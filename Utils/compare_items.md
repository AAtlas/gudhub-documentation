# Compare Items

**compareItems** is the method that compares items by their fields and item IDs. Namely, it compares source items or fields with destination ones. Then it adds source items to three different arrays based on the comparison results.

While invoking, compareItems takes three arguments:

Name|Type|Description
:---|:---|:---
sorceItems|`array`|*array of items that is going to be used as a source for comparison*
destinationItems|`array`|*array of items that is gong to be compared with sorceItems*
fieldToCompare|`number`|*ID of the field by which items can be compared*

>The `fieldToCompare` argument is **not a mandatory one**. It can be used when you need to compare only certain fields of different items.

This method can compares items in two ways:

- by items IDs
- by field ID

**Comparison by field ID** will be performed by values from the entered field ID. **In other case**, the comparison will be performed for all fields that contain both items

The method returns the object that contains all those arrays:

```json
{
    "is_items_diff": false,
    "new_src_items":[],
    "diff_src_items":[],
    "same_items":[]
}
```

Name|Type|Description
:---|:---|:---
is_items_diff|`boolean`|*shows is there different items or not*
new_src_items|`array`|*contains items with different IDs; [example](#different-ids)*
diff_src_items|`array`|*contains items with same IDs but different fields; [example](#same-ids-but-different-fields-values)*
same_items|`array`|*contains items with same IDs and same fields; [example](#perfect-match)*

>If source items have the same fields with the same values as destination items they will be described as same ones, even if `destinationItems` have additional fields those `sorceItems` does not have.

## Using fieldToCompare

When we pass field ID into `fieldToCompare`, the method will compare items by it, not by the whole item:

```js
import GudHub from '@gudhub/core';
const gudhub = await new GudHub();
const FIELDID = 645887;

let sorceItems = [{
    "item_id": 2987986,
    "fields": [{
      "field_id": 645887,
      "field_value": "Dow",
      },
      {
        "field_id": 645888,
        "field_value": "Johana",
        }]
}
];

let destinationItems = [{
  "item_id": 2987996,
  "status": 3,
  "fields": [{
      "field_id": 645887,
      "field_value": "Dow",
      },
      {
        "field_id": 645888,
        "field_value": "John",
        }]
  }
];

let comparedItems = gudhub.compareItems(sorceItems, destinationItems, FIELDID);

console.log(comparedItems);
```

Apart from replacement of item ID to field value, operating logic of comparison does not change:

```json
{
  "is_items_diff": true,
  "new_src_items": [],
  "diff_src_items": [{
      "item_id": 2987986,
      "fields": [{
          "field_id": 645887,
          "field_value": "Dow",
          },
          {
          "field_id": 645888,
          "field_value": "Johana",
      }] 
  }],
  "same_items": []
}
```

## Examples

We have prepared the examples for four comparison cases:

### Perfect Match

This is a situation when 'sorceItems' contains the same item with the one from 'destinationItems':

```js
import GudHub from '@gudhub/core';
const gudhub = await new GudHub();

let sorceItems = [{
    "item_id": 2987996,
    "fields": [{
      "field_id": 645887,
      "field_value": "Dow",
      },
      {
        "field_id": 645888,
        "field_value": "John",
        }]
  }
]

let destinationItems = [{
  "item_id": 2987996,
  "fields": [{
      "field_id": 645887,
      "field_value": "Dow",
      },
      {
        "field_id": 645888,
        "field_value": "John",
        }]
}]


let comparedItems = gudhub.compareItems(sorceItems, destinationItems)

console.log(comparedItems)
```

It will return:

```json
{
    "is_items_diff": false,
    "new_src_items": [],
    "diff_src_items": [],
    "same_items": [{ 
        "item_id": 2987996, 
        "fields": [{
            "field_id": 645887,
            "field_value": "Dow",
            },
            {
            "field_id": 645888,
            "field_value": "John",
            }]
        }]
}
```

### Different Number of Fields

The 'sorceItems' item has less fields, but all of them are the same with the destinationItems item:

```js
import GudHub from '@gudhub/core';
const gudhub = await new GudHub();

let sorceItems = [{
    "item_id": 2987996,
    "fields": [{
      "field_id": 645887,
      "field_value": "Dow",
    }]
}]

let destinationItems = [{
  "item_id": 2987996,
  "fields": [{
      "field_id": 645887,
      "field_value": "Dow",
      },
      {
        "field_id": 645888,
        "field_value": "John",
        }]
}]


let comparedItems = gudhub.compareItems(sorceItems, destinationItems)

console.log(comparedItems)
```

As a result, we get:

```json
{
  "is_items_diff": false,
  "new_src_items": [],
  "diff_src_items": [],
  "same_items": [{ 
    "item_id": 2987996, 
    "fields": [{
        "field_id": 645887,
        "field_value": "Dow",
    }]
  }]
}
```

### Different IDs

In this case, items have different IDs:

```js
import GudHub from '@gudhub/core';
const gudhub = await new GudHub();

let sorceItems = [{
    "item_id": 2987986,
    "fields": [{
        "field_id": 645887,
        "field_value": "Mikelson",
        },
        {
        "field_id": 645889,
        "field_value": "Johana",
    }]
}]

let destinationItems = [{
  "item_id": 2987996,
  "fields": [{
      "field_id": 645887,
      "field_value": "Dow",
      },
      {
        "field_id": 645888,
        "field_value": "John",
        }]
}]


let comparedItems = gudhub.compareItems(sorceItems, destinationItems)

console.log(comparedItems)
```

As a result, source item is added to new_src_items array:

```json
{
    "is_items_diff": true,
    "new_src_items": [{
        "item_id": 2987986, 
        "fields": [{
            "field_id": 645887,
            "field_value": "Mikelson",
            },
            {
            "field_id": 645889,
            "field_value": "Johana",
            }]
    }],
    "diff_src_items": [],
    "same_items": []
}
```

### Same IDs but Different Fields Values

In this example, items of both arrays have same IDs:

```js
import GudHub from '@gudhub/core';
const gudhub = await new GudHub();

let sorceItems = [{
    "item_id": 2987996,
    "status": 1,
    "fields": [{
        "field_id": 645887,
        "field_value": "Dow",
        },
        {
          "field_id": 645888,
          "field_value": "Johana",
          }]
}]

let destinationItems = [{
  "item_id": 2987996,
  "fields": [{
      "field_id": 645887,
      "field_value": "Dow",
      },
      {
        "field_id": 645888,
        "field_value": "John",
        }]
}]


let comparedItems = gudhub.compareItems(sorceItems, destinationItems)

console.log(comparedItems)
```

The following object will be a result:

```json
{
    "is_items_diff": true,
    "new_src_items": [],
    "diff_src_items": [{
        "item_id": 2987996, 
        "fields": [{
            "field_id": 645887,
            "field_value": "Dow",
            },
            {
            "field_id": 645888,
            "field_value": "Johana",
            }]
        }],
    "same_items": []
}
```
