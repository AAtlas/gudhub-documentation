# Populate Items

This is the method that allows to populate items and to update the item with data from another one.  In addition, it significantly optimize the work with the server.

Name|Type|Description
:---|:---|:---
items|`array`|*array of items that will be upgraded*
model|`array`|*item model by that items are updated*
keep_data|`boolean`|*optional argument that shows whether fields, that will not be updated, will be erased*

## model

**model** is an array of objects that contains source data for updating item fields. It has such view:

```json
model = [{      
    "field_id" : 228464,
    "field_value" : "example 1"
    },
    {      
    "field_id" : 228464,
    "field_value" : "example 2"
    }]
```

Each object contains two properties:

Name|Type|Description
:---|:---|:---
field_id|`number`|*ID by that the fields will be updated*
field_value|`string`|*value to be added to a certain field instead of the existing one*

## keep_data

Thanks to **keep_data** we can adjust the request for the faster and optimal work. The default value is false.

If its value is true, all fields, even those without changes, will be sent to the server. As opposite, if value is false, unchanged fields will be removed from the request.
Server will not process extra operations. So, we can regulate the amount of work that we send to the server.

## Example

Depending on the value of keep_data, the method will have different objects in the result array.

### keep_data is false

In cases when the keep_data is false or have no value:

```js
import GudHub from '@gudhub/core';
const gudhub = await new GudHub();

let items = [{
    "item_id": 2987986,
    "fields": [{
        "field_id": 645884,
        "field_value": "Dow",
        },
        {
        "field_id": 645888,
        "field_value": "John",
        },
        {
        "field_id": 645889,
        "field_value": "384595933",
        }],
    },
    {
    "item_id": 2987996,
    "fields": [{
        "field_id": 645887,
        "field_value": "Morison",
        },
        {
            "field_id": 645888,
            "field_value": "Johana",
        },
        {
            "field_id": 645889,
            "field_value": "2343588",
        }]
    }
]

let model = [{
  "field_id": 645887,
  "field_value": "Mikelson",
  },
  {
  "field_id": 645888,
  "field_value": "Jonny",
  }
]


// populate items
let populatedItems = gudhub.populateItems(items, model)

// displayed full object
populatedItems.forEach(item => {
  console.log("item_id:", item.item_id)
  console.log(item.fields)
});
```

Then all fields that have not been changed are deleted or replaced.

```json
[{
    "item_id": 2987986,
    "fields":[{ 
        "field_id": 645887, 
        "field_value": "Mikelson" 
        },
        { 
        "field_id": 645888, 
        "field_value": "Jonny" 
        }
    ]
},
{
    "item_id": 2987996,
    "fields":[{ 
        "field_id": 645887, 
        "field_value": "Mikelson" 
        },
        { 
        "field_id": 645888, 
        "field_value": "Jonny" 
        }
    ]
}]
```

### keep_data is true

If keep_data is true, then all unchanged data is kept in the objects, and all fields that were not in the original element are added to it:

```js
import GudHub from '@gudhub/core';
const gudhub = await new GudHub();

let items = [{
    "item_id": 2987986,
    "fields": [{
        "field_id": 645884,
        "field_value": "Dow",
        },
        {
        "field_id": 645888,
        "field_value": "John",
        },
        {
        "field_id": 645889,
        "field_value": "384595933",
        }],
    },
    {
    "item_id": 2987996,
    "fields": [{
        "field_id": 645887,
        "field_value": "Morison",
        },
        {
            "field_id": 645888,
            "field_value": "Johana",
        },
        {
            "field_id": 645889,
            "field_value": "2343588",
        }]
    }
]

let model = [{
  "field_id": 645887,
  "field_value": "Mikelson",
  },
  {
  "field_id": 645888,
  "field_value": "Jonny",
  }
]


// populate items
let populatedItems = gudhub.populateItems(items, model, true)

// displayed full object
populatedItems.forEach(item => {
  console.log("item_id:", item.item_id)
  console.log(item.fields)
});
```

As you can see, the matching fields are updated and the new field is added to the first item, but all source fields are retained:

```json
[{
    "item_id": 2987986,
    "fields":[{ 
        "field_id": 645884, 
        "field_value": "Dow" 
        },
        { 
        "field_id": 645888, 
        "field_value": "Jonny" 
        },
        {
        "field_id": 645889, 
        "field_value": "384595933" 
        },
        { 
        "field_id": 645887, 
        "field_value": "Mikelson"
        }
    ]
},
{
    "item_id": 2987996,
    "fields":[{ 
        "field_id": 645887, 
        "field_value": "Mikelson" 
        },
        { 
        "field_id": 645888, 
        "field_value": "Jonny" 
        },
        { 
        "field_id": 645889, 
        "field_value": "2343588" 
        }
    ]
}]
```
