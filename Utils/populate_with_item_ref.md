# Populate With Item Reference

This method allows to populate items with item reference. That means you can connect items from different applications by ite reference. It allows to take data from certain field and populate another item with it. **populatedWithItemRef** can make such connections between a big number of applications.

This method accepts six arguments:

Name|Type|Description
:---|:---|:---
sorceItemsRef|`array`|*items that are used as reference for item_ref*
srcFieldIdToCompare|`number`|*field ID that is used to find similar items in source and destination arrays*
destinationItems|`array`|*items that are used to save item_ref to sorseItemsRef*
destFieldIdToCompare|`number`|*field ID that is used to find similar items in destination array*
destFieldForRef|`number`|*contains the ID of field where item_ref will be saved*
app_id|`number`|*application ID that is used to generate item_ref value*

At first **populateWithItemRef** checks connection between *sorceItemsRef* and *srcFieldIdToCompare*. It does it with the help of *srcFieldIdToCompare* and *destFieldIdToCompare*. If the connection is detected, *destFieldForRef* will be updated with source item reference.

## Item Reference

The destination item will have the object reference as a field_value. It consists of source application ID and source item ID:

```json
{
    "field_value": "27823.2987934"
}
```

More detail about item reference you can read in [Item Reference](../Gh_Elements/item_reference.md).

## Example

```js
import GudHub from '@gudhub/core';
const gudhub = await new GudHub();
const APPID = "27823";

let srcFieldIdToCompare = 645888;

let destFieldIdToCompare = 645900;

let destFieldForRef = 648741;

let sorceItemsRef = [{
  "item_id": 2987934,
  "fields": [{
      "field_id": 645888,
      "field_value": "GudHub",
      },
      {
        "field_id": 645887,
        "field_value": "John",
      }]
  }
];

let destinationItems = [{
    "item_id": 2993230,
    "fields": [{
        "field_id": 645900,
        "field_value": "GudHub",
        },
        {
          "field_id": 648741,
          "field_value": "Johana",
          }]
  }
];


// populate items with item reference
let populatedWithItemRef = gudhub.populateWithItemRef(sorceItemsRef, srcFieldIdToCompare, destinationItems, destFieldIdToCompare, destFieldForRef, APPID)
 
populatedWithItemRef.forEach(item => {
  console.log("item_id:", item.item_id)
  console.log(item.fields)
});
```

The method returns an array of objects which in turn contain of two properties:

```json
[
    { 
    "field_id": 645900, 
    "field_value": "GudHub" 
    },
    { 
    "field_id": 648741, 
    "field_value": "27823.2987934" 
    }
]
```
