# Scheme

Scheme displayed the object scheme that is the representation of the final object structure.

In this chapter we will talk about logic of the scheme building.

Roughly speaking, scheme is a set of different properties. Depends of the settings configured by user, entities  with varied properties.

By default the scheme returns the empty array. And here is an example of the completed scheme:

```json
{
    "type":"array",
    "id":1,
    "childs":[{
        "type":"property",
        "id":4,
        "property_name":"last_name",
        "property_type":"field_value",
        "field_id":"645887",
        "interpretation":1
        },
        {
        "type":"object",
        "id":2,
        "childs":[{
            "type":"property",
            "id":3,
            "property_name":"member","property_type":"field_value",
            "field_id":"645888",
            "interpretation":1
            },
            {
            "type":"property",
            "id":6,
            "property_name":"phone",
            "property_type":"field_value",
            "field_id":"645891",
            "interpretation":1
            },
            {
            "type":"property",
            "id":5,
            "property_name":"email",
            "property_type":"field_value",
            "field_id":"645882",
            "interpretation":1
            }],
        }],
    "property_name":"family",
    "app_id":"27823",
    "filter":[]
}
```

The JSON scheme has many different properties that relate to the main object or other properties.

The core set of the properties:
Name|Type|Description
:---|:---|:---
type|`string`|*shows type of the property*
id|`number`|*shows the property ID*
childs|`array`|*contains its entities*
property_name|`string`|*property name that is given by user*
app_id|`number`|*contains the ID of application where*

## type

In Scheme, all entities are the objects that have one of three types: array, object, property.

```json
{
    "type": "array",
...}

{
    "type": "object",
...}

{
    "type": "property",
...}
```

These entities are same to ones in JSON. Every scheme consists of them.

## childs

This array deserves special attention. It contains the objects that are located into current object or array. That means, if object has type "array" or "object", it necessarily will have **childs**. It has similar properties as the main one (type, id, property_name, property_type, childs). But, depends of their type and a few other attributes, they can have different properties.

### Array

Example array:

```json
{
    "type":"array",
    "id":1,
    "childs":[],
    "property_name":"family",
    "app_id":"27823",
    "filter":[]
}
```

Name|Type|Description
:---|:---|:---
is_static|`number`|s*hows is there filter or not (1 - true value, 0 - false value)*
filter|`array`|*contains all the filters that are applied to the current object*

**is_static** will be always shown after the first applying.

**Filter`s** objects have their own properties:

```json
{
    "filter":[{
        "field_id":645888,
        "data_type":"text",
        "valuesArray":["a"],
        "search_type":"not_contain_or",
        "selected_search_option_variable":"Value"
        }]
}
```

Name|Type|Description
:---|:---|:---
field_id|`number`|*field ID in which the filter is located*
data_type|`string`|*type of searched data*
valueArray|`array`|*values searching keys*
search_type|`string`|*type of the searching*
selected_search_option_variable|`string`|*contains one of three search options*

More details about all filter properties you can find in [Filter](../Filter/filter_overview.md).

### Object

Example object:

```json
{
    "type":"object",
    "id":2,
    "childs":[]
}
```

Name|Type|Description
:---|:---|:---
current_item|`number`|*allows to return the object instead of the array (1 - true value; 0 - false value)*

Objects contains the core set of properties and **current_item**. But it has one peculiarity. Current_item and app_id  will only appear after the current_item gets true value for the first time.

### Property

Besides the core properties it has:

Name|Type|Description
:---|:---|:---
property_type|`string`|*the type of the property*
field_id|`string`|*ID of the field from which the data will be taken*
interpretation|`number`|*shows interpretation is applied or not (1 - true value, 0 - false value)*
static_field_value|`string`|*contains the static value*
variable_type|`string`|*contains one of three variable types*

Example:

```json
{
    "type":"property",
    "id":4,
    "property_name":"last_name",
    "property_type":"field_value",
    "field_id":"645887",
    "interpretation":1
}
```

Some of these properties depends of the property_type value. More about them you can read in [Editor chapter](editor.md).
