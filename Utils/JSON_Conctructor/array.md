# Array

**Array** is a base entity of the JSON scheme. It allows to create the set of similar objects.

## Interface

The current entity has its own settings. They allows to configure the source of data.

![Settings of array in JSON constructor](json-constructor/array-settings.jpg "Array settings")

Name|Description
:---|:---
Property Name|allows to set the name of the array; [details...](#property-name)
App ID|allows to select the source application for the array; [details...](#app-id)
Sortable|allows to make the array sorted; [details...](#sortable)
Field ID to sort|allows to select by which field an array will be sorted; [details...](#field-id-to-sort)
Sort type|allows to select the type of sorting; [details...](#sort-type)
Use variables for limit and offset|allows you to set variables for the limit and offset instead of custom values; [details...](#use-variables-for-limit-and-offset)
Limit|allows to enter or select the limit for array; [details...](#limit)
Offset|allows to enter or select the offset for the current array; [details...](#offset)
Add Filter|allows to filter which data will be contained in array; [details...](#add-filter)

### Property Name

This is the name that is assigned to the array. It is stored in one of the array properties called `property_name`.

### App ID

The source application is used as a data repository. Elements, fields and their values from this application will be taken to generate objects and properties of the JSON Scheme.

### Sortable

This is an easy-to-understand function that determines whether an array is sortable or not.

### Field ID to sort

Here the user can select the field by which the application will be sorted, if it is [sortable](#sortable).

### Sort type

There are two possible sort types:

- Descending
- Ascending

They determines the order of objects and properties in the array. The sort type selection function is available only if the array is [sortable](#sortable).

### Use Variables For Limit and Offset

Since the next two parameters take static values by default, the current setting exists. It allows you to change the type of value that the following settings take. That is, you use it to determine whether they need a **static numeric value** or a **variable**.

So, if this option is enabled, [Offset](#offset) and [Limit](#limit) offer a list of variables for their values instead of numbers.

### Limit

The current setting allows you to set a limit on the number of items in your array. That is, you enter the maximum number of items and the array cannot exceed this number, but it can be less.

### Offset

The next setting lets you choose the offset for your array. Here you have to enter the number of elements by which the array will be shifted. Thus, if the offset is three, the array will start from the fourth element in order.

>Using the **offset** in conjunction with the [limit](#limit), you can select a certain part of the array.

### Add Filter

The last function allows to filter an application items. That is, the user can define from which elements the data will not be taken.

## Scheme

All that settings will be contained in the array scheme:

```json
{
    "type": "array",
    "id": 1,
    "childs": [],
    "app_id": "28561",
    "filter":[{
        "field_id": 677360,
        "data_type": "radio_icon",
        "valuesArray": ["0"],
        "search_type": "equal_or",
        "selected_search_option_variable": "Value"
    }],
    "property_name": "Array",
    "isSortable": 1,
    "field_id_to_sort": "671865",
    "sortType": "desc"
}
```

Property Name|Type|Description
:---|:---|:---
type|`string`|*contains the type of the entity; in this case type is array*
id|`number`|*contains ID of the array in the scheme*
childs|`array`|*contains all entities which are stored in this array*
app_id|`string`|*contains the ID of the application from which the current array takes data*
filter|`array`|*contains all filters that are applied to the current array*
field_id|`number`|*contains ID of the field by which items in array will be filtered*
data_type|`string`|*contains the data type of the field by which app items will be filtered*
valuesArray|`array`|*contains all values by which app items are filtered*
search_type|`string`|*contains the type of the filtration*
selected_search_option_variable|`string`|*contains the search variable type*
property_name|`string`|*contains the name assigned to the array by the user*
isSortable|`boolean`|*shows whether the entities in the array are sortable or not*
field_id_to_sort|`string`|*contains ID of the field by which array elements will be sorted*
sortType|`string`|*contains type of element sorting*
