# JSON Constructor Overview

JSON Constructor is a special method that is called for generating JSON objects from existing items. It take a scheme editor as an argument. [**Scheme Editor**](#scheme-editor) allows you to generate JSON scheme and see the method result. In brief, this editor does such a chore as writing dozens of similar JSON objects much more faster and easier by converting data.

>JSON Constructor cannot edit the source data.

## Interface

JSON Constructor also has a comfortable for users visual editor. It consists of four parts:

Name|Description
:---|:---
Types|contains basic code entities
Editor|a space where you gather together elements and fill them in; [details...](editor.md)
Scheme|shows a JSON scheme; [details...](scheme.md)
Result|shows a result of the converting

The user can interact only with the first two items. The last ones only shows the result of converting.

### Scheme Editor

Scheme editor is a functional part of the **JSON constructor**. It takes chosen items, converts it and gather them in JSON format that can be edited. Also, constructor can save the result in the application and allows to copy it.

### Types

Types is the first and the smallest item in the view. It contains four elements: three blocks, that you can drag-and-drop into editor, and delete field.

#### **Blocks**

As mentioned above, you can drag-and-drop block to the editor. They contains general elements of JSON code: [array](./array.md), [object](./object.md), and [property](./property.md). In order to build your code, you have to use them.

![Example of dragging and dropping entities](json-constructor/utils-json-constuctor-gif-types-entities.gif "Drag and drop entities from Types")

#### **Delete field**

This is a field for deleting elements from editor. In order to remove extra blocks, drag-and-drop them to this field.

![Example of deleting entities from Editor](json-constructor/utils-json-constuctor-gif-types-del.gif "Deleting entities from Editor")

### Editor

This is an instrument that helps you to create a scheme and immediately see the result. Namely, it helps to gather together all elements, edit and fill them.

When you add elements into editor, they change their look. Also, the edit button appears. Click it and you open the small edit window. [Read more about Editor.](editor.md)

![Examples of entities settings](json-constructor/utils-json-constuctor-gif-editor.gif "Settings of entities")

### Scheme

This is an item that displays the code schematic converted from the editor. This scheme is just outline. We can say that it shows the basic object structure of the final JSON code. [Read more about Scheme.](scheme.md)

![Example of JSON scheme](json-constructor/utils-json-constuctor-scheme.png "Scheme")

Also, Scheme has a button that allows you to copy received code scheme.

![Example of the copy button of the scheme](json-constructor/utils-json-constuctor-copy-but.png "Copy button")

### Result

This is the item where the final JSON object is displayed.

![Final JSON of the example item](json-constructor/utils-json-constuctor-result.png "Final JSON")

## jsonConstructor

The **jsonConstructor** is the main method of the *JSON Constructor*. Exactly it allows to generate the JSON Schemes.

```js
import GudHub from '@gudhub/core';
const AUTHKEY = "NKJHIUHknjcnknkjkk9w92ehds78/7T7GYfz67w2eu+dsbddf2365kjh82kjkcJIOHBJ";
const gudhub = new GudHub(AUTHKEY);

let scheme = {
    "type": "array",
    "id": 2,
    "childs": [{
        "type": "object",
        "id": 3,
        "childs": [{
            "type": "prperty",
            "id": 4,
            "childs": [],
        }],
    }]
};

let item = [{
    "fields": [{
        "data_id": 2323153,
        "element_id": 68091,
        "field_id": 68091,
        "field_value": "Christine"
    }],
    "index_number": 1,
    "item_id": 312227,
    "last_update": 165675955000
},{
    "fields": [{
        "data_id": 2324290,
        "element_id": 68487,
        "field_id": 68487,
        "field_value": "945176"
    }],
    "index_number": 2,
    "item_id": 312228,
    "last_update": 165657901000
}];

let variables = ["variable1","variable2"];

let jsonScheme = await gudhub.jsonConstructor(scheme, item, variables, appId)
console.log(jsonScheme)
```

Argument|Type|Description
:---|:---|:---
scheme|`object`|*transmits scheme of the JSON that will be created*
item|`array`|*contains items from which data for new JSON object will be taken*
variables|`array`|*contains variables for the array filter*
appId|`number`|*current application id for the array filter
