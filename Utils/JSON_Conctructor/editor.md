# Editor

Editor is an instrument that helps to create a scheme. Right here you can gather all needed properties for generating JSON object. The main object properties are called entities.

The types of the entities are displayed in different ways in the different items:

1. Types

    ![Example of Types](json-constructor/utils-json-constuctor-types-entities.png "Types")

2. Editor

    ![Example of Editor](json-constructor/utils-json-constuctor-entities-editor.png "Editor")

3. Scheme

```json
{
    "type":"array",
    "id":1,
    "childs":[{
        "type":"property",
        "id":4,
        "property_name":"last_name",
        "property_type":"field_value",
        "field_id":"645887",
        "interpretation":1
        },
        {
        "type":"object",
        "id":2,
        "childs":[{
            "type":"property",
            "id":3,
            "property_name":"member","property_type":"field_value",
            "field_id":"645888",
            "interpretation":1
            },
            {
            "type":"property",
            "id":6,
            "property_name":"phone",
            "property_type":"field_value",
            "field_id":"645891",
            "interpretation":1
            },
            {
            "type":"property",
            "id":5,
            "property_name":"email",
            "property_type":"field_value",
            "field_id":"645882",
            "interpretation":1
            }],
        }],
    "property_name":"family",
    "app_id":"27823",
    "filter":[]
}
```

4. Result

```json
 Array [3]
 0: Object
last_name: "Dow"
 undefined: Object
member: "John"
phone: "0493495342"
email: "john@dow.com"
 1: Object
last_name: "Dow"
 undefined: Object
member: "Johana"
phone: "9032443892"
email: "johana@dow.com"
 2: Object
last_name: "Dow"
 undefined: Object
member: "Jeremy"
phone: "8374734329"
email: "jeremy@dow.com"
```

Each of them has both its own unique ones and the same for all one.

## Property name

This is the single property that presents in each entity. Property name just contains the given name. By default, array and object have appropriate names. When you enter a property name, a default value is added to it.

![Example of the property name](json-constructor/utils-json-constuctor-prop-name.png "Property name")

Also, there are properties that appears after choosing specific values, but about them later. Now let`s talk about each entity property.

## Array

Array also contains such standard properties:

Name|Description
:---|:---
Property Name|array name; [details...](#property-name)
App ID|application ID
Filter|contains settings for filtration
Is static|shows is there filter or not

in the scheme it looks like this:

```json
{
    "type":"array",
    "id":4,
    "childs":[]
}
```

### Is static

The property is_static determines array has filter or not. That means, if is_static has a value of 0, the current array will allow filtering; in turn, if its value is 1, the filter will not even be present in the schema. present in scheme.

>The main array does not has this property.

### App ID

About name you can read above, so now we pay attention the second one.

For every property, you have to choose from which application the data will be taken. App ID contains the ID of the chosen application.

![Example of App ID values](json-constructor/utils-json-constuctor-array-app-id.png "App ID values")

The property app_id in the scheme will get a value with ID of the selected application.

```json
    app_id: "26506"
```

### Filter

This is the special part of array settings. Due to filter you can filter out extra fields and get necessary values.

So, filtration takes place in three stages:

1. Choosing the application. This point is closely linked with [App ID](#app-id). After this stage in filter will appears a list of the application fields.
2. Choosing the field. In this stage you have to select the field which will be used to choose the value.

![Example of adding filters to the array](json-constructor/utils-json-constuctor-gif-field.gif "Adding filters to the array")

3. Choosing the value. For this stage you have to configure the filter. For understanding how this stage works recommend you to read about [Filter](../Filter/filter_overview.md).

## Object

As well as every JSON object, this one has its own properties:

Name|Description
:---|:---
Property Name|object name; [details...](#property-name)
Current Item|shows which ID will be taken
App ID|applications ID

But there are more of his properties in the scheme:

```json
{
    "type":"object",
    "id":6,
    "childs":[]
}
```

The details about them you can read in [Scheme chapter.](scheme.md)

### Current Item

This property allows to return the object instead of the array. Due to it you can select from which application the data for its properties will be taken.

![Example of using Current Item property](json-constructor/utils-json-constuctor-gif-editor-current-item.gif "Current Item")

If current item has false value, we will not be able to select a field for the object properties.

![Example of false Current Item value](json-constructor/utils-json-constuctor-current-item-false.png "False value of Current Item")

If current item has true value, then the [App ID](#app-id) field will appears.

![Example of true Current Item value](json-constructor/utils-json-constuctor-current-item-true.png "True value of Current Item")

## Property

Property has many own properties. The general and permanent ones:

Name|Description
:---|:---
Property Name|property name; [details...](#property-name)
Property Type|the type of the property

In the scheme it will have more properties. This code part looks like this:

```json
{
    "type":"property",
    "id":6,
    "property_name":"phone",
    "property_type":"field_value",
    "field_id":"645891",
    "interpretation":1
}
```

You can read about them in more details in [Scheme chapter](scheme.md).

### Property Type

There are four types of properties:

Name|Description
:---|:---
Field ID|selected field ID
Field Value|value from selected field
Static|contains value that will not be changed
Variable|contains ID of the current item environment

Depending on the type, the next properties vary:

**Field ID:**

*Field ID* contains the field which ID will be used.

![Example of Field ID property type](json-constructor/utils-json-constuctor-ed-prop-field-id.png "Field ID type")

The code scheme will not remain:

```json
{
    "type": "property",
    "id": 3,
    "property_name": "member",
    "property_type": "field_id",
    "field_id": "645888",
}
```

**Field Value:**

- *Field ID* contains the field which value will be used.
- *Interpretation* determines in which format the value will be displayed.

![Example of Field Value property type](json-constructor/utils-json-constuctor-ed-prop-field-value.png "Field Value Type")

As a result, the code scheme will get a new property:

```json
{
    "type": "property",
    "id": 3,
    "property_name": "member",
    "property_type": "field_value",
    "field_id": "645888",
    "interpretation": 1,
}
```

**Static:**

*Static field value* contains the entered static value.

![Example of Static property type](json-constructor/utils-json-constuctor-ed-prop-static.png "Static type")

Exit code in the scheme:

```json
{
    "type": "property",
    "id": 3,
    "property_name": "member",
    "property_type": "static",
    "field_id": "645888",
    "interpretation": 1,
    "static_field_value": "family member",
}
```

As you can see, when we choose Static as a Type_Value, a new properties appears. **interpretation** is a number that shows interpretation applied or not (1 - true value; 0 - false value).

> If you apply a **static_field_value**, it will remain in the code schema even if it has an empty value or changes property_type.

**Variable:**

*Variable field value* is the field where user can select one of the three values: *App ID*, *Current Item*, *User ID*.

![Example of Variable property type](json-constructor/utils-json-constuctor-ed-prop-variable.png "Variable type")

In the code we will get:

```json
{
    "type": "property",
    "id": 3,
    "property_name": "member",
    "property_type": "variable",
    "field_id": "645888",
    "interpretation": 1,
    "static_field_value": "family member",
    "variable_type": "user_id",
}
```

The field **variable_type** appears. It can have one of three values: "app_id", "current_item", "user_id".
