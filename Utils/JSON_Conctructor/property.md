# Property

**Property** is a base entity of the JSON scheme. It allows to add any number of properties to an [object](object.md) and an [array](array.md).

## Interface

The **property** has two main settings Property Name and Property Type. The last one determines the other settings.

- Field ID

    ![Settings of field ID type property](json-constructor/property-field-id.jpg "Property settings by field ID")

- Field Value

    ![Settings of field value type property](json-constructor/property-field-value.jpg "Property settings by field value")

- Static

    ![Settings of static type property](json-constructor/property-static.jpg "Property settings by static")

- Variable

    ![Settings of variable type property](json-constructor/property-variable.jpg "Property settings by variable")

- Function

    ![Settings of function type property](json-constructor/property-function.jpg "Property settings by function")

Name|Description
:---|:---
Property Name|allows to set the name of the property; [details...](#property-name)
Property Type|allows to select the type of the property and its settings; [details...](#property-type)
Field ID|allows to select the field whose ID or value will be stored in the current property; [details...](#field-id)
Interpretation|allows to interpret the value of the property; [details...](#interpretation)
Static field value|allows to enter the static value; [details...](#static-field-value)
Variable field value|allows to select one of three IDs which will be stored in the property; [details...](#variable-field-value)
Editor|allows to enter the function for the property; [details...](#editor)

### Property Name

This is the entered name of the property that will be displayed in the resulting model.

### Property Type

The type of the property determines which values will be stored in it. There five types:

- Field ID
- Field Value
- Static
- Variable
- Function

Each of them has its own unique settings.

### Field ID

This is the selected field whose data will be stored in the current property. It is available only for **Field ID** and **Field Value**. But the data that is taken from the field is different for each type. That type of data is clear from the name.

### Interpretation

This function defines the format of the field value that will be stored in the current property. Namely, it allows to set whether a value is interpreted or not. This function is available only for the **Field Value** [property type](#property-type).

### Static Field Value

This is the field where you can enter a static value for the current property. As you might understand, it is available only for one [property type](#property-type) called **Static**.

### Variable Field Value

The current function allows you to select one of three types of values:

- App ID
- Current Item
- Use ID

This function is available for **Variable** [property type](#property-type).

### Editor

The unique function of the last [property type](#property-type) called **Function** allows you to enter some function for the current property.

## Scheme

The current entity has lots of properties in its models. And the set of these properties varies depending on the [type of property](#property-type).

```json
// Field Value
{
    "type": "property",
    "id": 3,
    "property_type": "field_value",
    "field_id": "671865",
    "interpretation": 1,
    "property_name": "Property"
}

// Field ID
{
    "type":"property",
    "id":3,
    "property_name":"Property",
    "property_type":"field_id",
    "field_id":"648759"
}

// Static
{
    "type":"property",
    "id":3,
    "property_type":"static",
    "field_id":"671865",
    "interpretation":1,
    "property_name":"Property",
    "static_field_value":"Value"
}

// Variable
{
    "type":"property",
    "id":3,
    "property_type":"variable",
    "field_id":"671865",
    "interpretation":1,
    "property_name":"Property",
    "variable_type":"current_item"
}

// Function
{
    "type":"property",
    "id":3,
    "property_type":"function",
    "field_id":"671865",
    "interpretation":1,
    "property_name":"Property",
    "function":"function(item, app) {\n  return 'text';\n}"
}
```

Property Name|Type|Description
:---|:---|:---
type|`string`|*contains the type of the entity; in the current case, type is property*
id|`number`|*contains ID of the current property*
property_type|`string`|*contains the selected type of the property*
property_name|`string`|*contains the entered name of the property*
interpretation|`boolean`|*shows whether the value of the field is interpreted or not*
field_id|`string`|*contains ID from which data will be taken*
static_field_value|`string`|*contains data entered in settings*
variable_type|`string`|*contains selected variable type that determines which data will be taken from the application*
function|`string`|*contains entered function*
