# Object

**Object** is a base entity which is used to create JSON schemes. It allows to create a multi-tiered structure objects and to reproduce the structure of different elements.

## Interface

The object has an user-friendly interface for configuring the entity.

![Settings of the object in JSON constructor](json-constructor/object-settings.jpg "Object settings")

Name|Description
:---|:---
Property Name|allows to set the name of the object; [details...](#property-name)
Current item|allows to take data from current items of the selected application; [details...](#current-item)
App ID|allows to select the application from whose items the data for the object will be taken; [details...](#app-id)

### Property Name

This is the name of the property that will be used in the resulting schema. If this name is not entered, the property will be called *undefined*.

### Current Item

This property is entered when the object is out of the array. It allows you to select any application and take data from there. If it is disabled, the data from the application of the array selected in the settings will be used.

### App ID

This is the source application that was selected solely for the current object. It can only be used if [Current Item](#current-item) is enabled.

## Scheme

In the result scheme the object has such features:

```json
{
    "type": "object",
    "id": 3,
    "childs": [], 
    "property_name": "Object",
    "current_item": 1,
    "app_id": "28561"
}
```

Property Name|Type|Description
:---|:---|:---
type|`string`|*contains type of the entity; in this case type is object*
id|`number`|*contains ID of the current entity*
childs|`array`|*contains all object properties*
property_name|`string`|*contains the entered name of the array*
current_item|`boolean`|*shows whether the new application will be selected for the current object*
app_id|`string`|*contains ID of the selected application*
