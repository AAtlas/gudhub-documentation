# Merge Items

**mergeItems** is the method that is called to merge items by field or item ID. It allows to connect required fields with source one so that they can be changed from the source item.
You can merge items from the same application or different ones.

This method accepts three arguments:

Name|Type|Description
:---|:---|:---
sorceItems|`array`|*array of items that will be used to update items from destinationItems items array*
destinationItems|`array`|*array of items that is going to be updated*
mergeByFieldId|`string`|*field id by that items will be merged*

>Unlike the other arguments, `mergeByFieldId` is **not a mandatory argument**.

It replaces all fields with values in `destinationItems` by values from `sorceItems`. Necessary fields are defined by The required fields are determined by the ID of the field itself. If `destinationItems` does not have fields that `sorceItems` then they will be created there.

>Items from chunks have a history property in a field.

As a result, it returns an array of updated items.

## sorceItems

Source items array contains the prepared objects with data that will be used for update and IDs by which the needed fields will be found.

```json
[{
    "item_id": 2985285,
    "fields": [{
        "field_id": 645887,
        "field_value": "Dow",
        },{
        "field_id": 645888,
        "field_value": "Johana",
    }],
}]
```

Unlike others, `item_id` is optional property. It is necessary when `mergeByFieldId` has not been passed into the method. Because it will be used only in this situation.

## destinationItems

This is an object array of application items. It can contain more than just the ones you plan to update. That is possible because of using field and item IDs.

```json
[{
    "item_id": 2985284,
    "fields": [{
        "field_id": 645887,
        "field_value": "Dow",
        },{
        "field_id": 645888,
        "field_value": "John",
    }],
},
{
    "item_id": 2985285,
    "fields": [{
        "field_id": 645887,
        "field_value": "Owl",
        },{
        "field_id": 645888,
        "field_value": "Jonny",
    }],
}];
```

Unlike the previous case, here **item_id** is a mandatory property.

## mergeByFieldId

As you already know, **mergeByFieldId** is an optional argument. It is used for comparing two items. If their values in the entered field are the same, the items will be merged.

```js
gudhub.mergeItems(sorceItems, destinationItems, mergeByFieldId)
```

Instead, if you want to merge items regardless of their fields, do not pass `mergeByFieldId`.

```js
gudhub.mergeItems(sorceItems, destinationItems)
```

## Examples

Followings are the examples of code for two situations:

- Merge [by field ID](#by-field-id)
- Merge [by item ID](#by-item-id)

### By Field ID

In this example the item with ID ***2985284*** will be updated:

```js
import GudHub from '@gudhub/core';
const AUTHKEY =  "JKBSCBkjncvskldksjaksMoiu98TYUV//teek/34//oeffdgse";
const gudhub = await new GudHub(AUTHKEY);
const APPID = "27823";
const mergeByFieldId = "645887";

let destinationItems = [{
    "item_id": 2985284,
    "fields": [{
        "field_id": 645887,
        "field_value": "Dow",
        },{
        "field_id": 645888,
        "field_value": "John",
        }],
    },
    {
    "item_id": 2985285,
    "fields": [{
        "field_id": 645887,
        "field_value": "Dow",
        },{
        "field_id": 645888,
        "field_value": "Jonny",
    }],
}];

let sorceItems = [{
    "fields": [{
        "field_id": 645887,
        "field_value": "Dow",
        },{
        "field_id": 645888,
        "field_value": "Johana",
    }],
}];

let mergeByFieldId = 645887;

let mergedItems = gudhub.mergeItems(sorceItems, destinationItems, mergeByFieldId);

gudhub.updateItems(APPID, mergedItems, mergeByFieldId)
```

The result items:

```json
[{
    "item_id": 2985284,
    "fields": [{ 
        "field_id": 645887, 
        "field_value": "Dow" 
        },{ 
        "field_id": 645888, 
        "field_value": "Johana" 
    }],
}]
```

### By Item ID

In this example the item with ID ***2985285*** will be updated:

```js
import GudHub from '@gudhub/core';
const AUTHKEY =  "JKBSCBkjncvskldksjaksMoiu98TYUV//teek/34//oefghxfgh";
const gudhub = await new GudHub(AUTHKEY);
const APPID = "27823";

let destinationItems = [{
    "item_id": 2985284,
    "fields": [{
        "field_id": 645887,
        "field_value": "Dow",
        },{
        "field_id": 645888,
        "field_value": "John",
        }],
    },
    {
    "item_id": 2985285,
    "fields": [{
        "field_id": 645887,
        "field_value": "Owl",
        },{
        "field_id": 645888,
        "field_value": "Jonny",
    }],
}];

let sorceItems = [{
    "item_id": 2985285,
    "fields": [{
        "field_id": 645887,
        "field_value": "Dow",
        },{
        "field_id": 645888,
        "field_value": "Johana",
    }],
}];

let mergedItems = gudhub.mergeItems(sorceItems, destinationItems);

gudhub.updateItems(APPID, mergedItems)
```

The result items:

```json
[{
    "item_id": 2985284,
    "fields": [{
        "field_id": 645887,
        "field_value": "Dow",
        },{
        "field_id": 645888,
        "field_value": "John",
    }],
},{
    "item_id": 2985285,
    "fields": [{ 
        "field_id": 645887, 
        "field_value": "Dow" 
        },{ 
        "field_id": 645888, 
        "field_value": "Johana" 
    }],
}]
```
