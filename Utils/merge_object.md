# Merge Objects

This is a method that is called to merge different objects. It allows to connect the object with the source one so that it can be changed from the source.

**mergeObjects** accepts two arguments. They can pass either the single object or array of object:

Name|Type|Description
:---|:---|:---
target|`object`|*object that will be updated*
source|`object`|*object from which the data will be taken to update*

As a result, the target object is updated, but the source object is not changed.
Properties from source object that do not exist in target one will be created there.

## Merge the Single Objects

In this situation we pass two objects as arguments target and source:

```js
import GudHub from '@gudhub/core';
const gudhub = await new GudHub();

let target = {
  name: {
    first_name: "John",
    last_name: "Dow",
  },
  gender: "Male",
  shopping_list: ["pants", "shirt"],
};

let source = {
  name: {
    first_name: "Jeremy",
    last_name: "Gordon",
  },
  gender: "Male",
  shopping_list: ["shoes", "pants"],
};


let mergedObjects = gudhub.mergeObjects(target, source)

console.log(mergedObjects)
```

As a result, we get one updated object:

```json
{
  "name": {
    "first_name": "Jeremy",
    "last_name": "Gordon",
  },
  "gender": "Male",
  "shopping_list": ["shoes", "shirt", "pants"],
}
```

## Merge Arrays of Objects

In case, when we pass arrays as arguments, we can take equal arrays or not.

### Equal Number of Objects

In this example we have arrays with two objects in each of them. Target objects will be merge with source object by their index in array:

```js
import GudHub from '@gudhub/core';
const gudhub = await new GudHub();

let target = [{
    // index: 0
    name: {
        first_name: "John",
        last_name: "Dow",
    },
    gender: "Male",
    shopping_list: ["pants", "shirt"],
},
{
    // index: 1
    name: {
        first_name: "Jeremy",
        last_name: "Gordon",
    },
    gender: "Male",
    shopping_list: ["shoes", "pants"],
}];

let source = [{
    // index: 0
    name:{
        last_name: "Owl",
    },
    shopping_list: ["hat", "coat"],
    orders_number: 3,
},
{
    // index: 1
    name: {
        first_name: "Johana",
        last_name: "Dow",
    },
    gender: "Female",
    shopping_list: ["skirt"]
}];


let mergedObjects = gudhub.mergeObjects(target, source)

console.log(mergedObjects)
```

In result we get :

```json
[
  {
    "name": { 
        "first_name": "John", 
        "last_name": "Owl" },
    "gender": "Male",
    "shopping_list": [ "pants", "shirt", "hat", "coat" ],
    "orders_number": 3
  },
  {
    "name": { 
        "first_name": "Johana", 
        "last_name": "Dow"
        },
    "gender": "Female",
    "shopping_list": [ "shoes", "pants", "skirt" ]
  }
]
```

### Source Has More Objects

In this case, the first target object will be merge with the first one from source array and the second one from source will be added to target:

```js
import GudHub from '@gudhub/core';
const gudhub = await new GudHub();

let target = [{
    name: {
        first_name: "Jeremy",
        last_name: "Gordon",
    },
    gender: "Male",
    shopping_list: ["shoes", "pants"],
}];

let source = [{
    name:{
        last_name: "Owl",
    },
    shopping_list: ["hat", "coat"],
    orders_number: 3,
},
{
    name: {
        first_name: "Johana",
        last_name: "Dow",
    },
    gender: "Female",
    shopping_list: ["skirt"]
}];


let mergedObjects = gudhub.mergeObjects(target, source)

console.log(mergedObjects)
```

As a result we will get target array with two objects:

```json
[
  {
    "name": { 
        "first_name": "Jeremy", 
        "last_name": "Owl" 
        },
    "gender": "Male",
    "shopping_list": [ "shoes", "pants", "hat", "coat" ],
    "orders_number": 3
  },
  {
    "name": { 
        "first_name": "Johana", 
        "last_name": "Dow" 
        },
    "gender": "Female",
    "shopping_list": [ "skirt" ]
  }
]
```

> The result is the same if you pass an object instead of an array to target.

### Target Has More Objects

In following example, the first target object is merged with the object from source array. The second will remain unchanged:

```js
import GudHub from '@gudhub/core';
const gudhub = await new GudHub();

let target = [{
    name: {
        first_name: "John",
        last_name: "Dow",
    },
    gender: "Male",
    shopping_list: ["pants", "shirt"],
},
{
    name: {
        first_name: "Jeremy",
        last_name: "Gordon",
    },
    gender: "Male",
    shopping_list: ["shoes", "pants"],
}];

let source = [{
    name: {
        first_name: "Johana",
        last_name: "Dow",
    },
    gender: "Female",
    shopping_list: ["skirt"]
}];


let mergedObjects = gudhub.mergeObjects(target, source)

console.log(mergedObjects)
```

Predictably, we get an array:

```json
[
  {
    "name": { 
        "first_name": "Johana", 
        "last_name": "Dow"
        },
    "gender": "Female",
    "shopping_list": [ "pants", "shirt", "skirt" ]
  },
  {
    "name": { 
        "first_name": "Jeremy", 
        "last_name": "Gordon" 
        },
    "gender": "Male",
    "shopping_list": [ "shoes", "pants" ]
  }
]
```

## Merge Different Argument Types

As mentioned above, when we have a source array and a target object, the result is an array sam to the [case with arrays](#source-has-more-objects).

The opposite situation (target array and source object) returns the object whose first properties are a target array objects. Others are properties of the source object:

```js
import GudHub from '@gudhub/core';
const gudhub = await new GudHub();

let target = [{
  name: {
    first_name: "John",
    last_name: "Dow",
  },
  gender: "Male",
  shopping_list: ["pants", "shirt"],
},
{
  name: {
    first_name: "Jeremy",
    last_name: "Gordon",
  },
  gender: "Male",
  shopping_list: ["shoes", "pants"],
}];

let source = {
  name: {
      first_name: "Johana",
      last_name: "Dow",
  },
  gender: "Female",
  shopping_list: ["skirt"]
};


let mergedObjects = gudhub.mergeObjects(target, source)

console.log(mergedObjects)
```

```json
{
  "0": {
    "name": { 
        "first_name": "John", 
        "last_name": "Dow" 
        },
    "gender": "Male",
    "shopping_list": [ "pants", "shirt" ]
  },
  "1": {
    "name": { 
        "first_name": "Jeremy", 
        "last_name": "Gordon" 
        },
    "gender": "Male",
    "shopping_list": [ "shoes", "pants" ]
  },
  "name": { 
      "first_name": "Johana", 
      "last_name": "Dow" 
      },
  "gender": "Female",
  "shopping_list": [ "skirt" ]
}
```

## Merge By ID

As we know, utils usually are used with another methods. For example, it is used for updating items in application. In this case we can pass only the item ID to target:

```js
import GudHub from '@gudhub/core';
const AUTHKEY =  "KJGJsbjafskajlkmoiwuehyugT//ssefiooqw938u9jiuiOIsasdHIBSpwi20kd79uiohdwg2y81y";
const gudhub = await new GudHub(AUTHKEY);
const APPID = "27823";

let source = {
  "fields": [{
      "field_id": 645887,
      "field_value": "Dow",
      },
      {
      "field_id": 645888,
      "field_value": "John",
      }
    ],
};

let target = {
  "item_id": 2987986,
};


let mergedObjects = gudhub.mergeObjects(target, source)

// add object as an item in the application
gudhub.updateItems(APPID, mergedObjects)

console.log(mergedObjects)
```

And as a result, we get an updated item:

```json
{
  "item_id": 2987986,
  "fields": [{ 
        "field_id": 645887, 
        "field_value": "Dow" 
        },
    { 
        "field_id": 645888, 
        "field_value": "John" 
        }]
}
```
