# Pipe

Roughly speaking, pipe is data layer that ensures data exchange.

All methods that allow to pull data are contained in **Pipe Server**. It is a set of events that allows to listen to the events and deliver data.It is based on Node.js Event Emitters & Observer Pattern.

The PipeServer works due to the subscriptions. They help to exchange data. Every field can subscribes to the events.

PipeService has tree methods:

- [`gudhub.on(event, address, fn)`](#on)
- [`gudhub.emit(event, destination, address, params)`](#emit)
- [`gudhub.destroy(event, address, fn)`](#destroy)

Name|Type|Description
:---|:---|:---
event|`string`|*accepts the name of the event*
address|`object`|*accepts address of the field where the event located*
fn|`function`|*accepts the function*
destination|`object`|*event recipient*
params|`-`|*parameters that are passed to the function*

> Any type of data can be passed as a *parameter*.

## On()

This is the method that is called for getting subscription.

It accepts three arguments:

- event
- address
- function

```js
import GudHub from '@gudhub/core';
const gudhub = new GudHub();
let address = {
    app_id: newApp.app_id;
};


// the function will be executed when event happens 
function myFunction(event, value){
    console.log('new app name', value.app_name);

    //if you do not want listen the event anymore, you can destroy it  
    gudhub.destroy('gh_app_info_update', address, myFunction);
};

gudhub.on('gh_app_info_update', address, myFunction);
```

Also, you can subscribe to several events at the same time:

```js
import GudHub from '@GudHub/core';
const gudhub = new GudHub();
let address = {
    app_id: 12345;
};


// function for passing two values of the argument
function myFunction(event, value){
    switch(event) {

        case 'gh_item_update':
            alert( 'Updated Item Event!' );
            break;

        case 'gh_model_update': 
            alert( 'Updated Model Event!' );
            break
    }
    //if you do not want listen the event anymore, you can destroy it  
    gudhub.destroy('gh_item_update gh_model_update', address, myFunction);
};

gudhub.on('gh_item_update gh_model_update', model_address, myFunction)
```

## Emit()

This is the method that is used by GudHub itself. It is called for getting existing subscription. Emit can work without recipient. That means it is called even if no one has subscribed.

It takes four arguments:

- event
- destination
- address
- params

In addition to existing events, you can devise your own event and emit it.

Event name|Address|Description
:---|:---|:---
gh_apps_list_update|-|allows to update the application list
gh_apps_list_refreshed|-|allows to refresh app list
gh_app_info_update|`app_id`|updates application information
gh_app_views_update|`app_id`|updates application views
gh_document_insert_one|`app_id`, `item_id`, `element_id`|adds a document or updates a document if it exists
gh_filter_items|-|allows to filter items in the application
gh_items_add|`app_id`|adds items to the item list
gh_item_update|`app_id`, `item_id`|updates the value of any item field
gh_items_update|`app_id`|updates the value of items field
gh_model_update|`app_id`, `field_id`|updates field model in field_list
gh_model_delete|-|deletes field model from field_list and from items
gh_file_upload|`app_id`, `item_id`, `file_id`|allows to add file to the file list
gh_file_update|`app_id`, `file_id`|updates file data in the file list
gh_file_delete|`app_id`, `file_id`|deletes file from the file list
gh_value_update|`app_id`, `item_id`, `field_id`|updates field value in item

The example of using the emit method:

```js
this.pipeService.emit(
    "gh_app_views_update",
    { app_id: newApp.app_id },
    newApp.views_list
);
```

## Destroy()

For some time, all of subscriptions are accumulated in memory. And they do not know that they must be destroyed for optimum work. So, the destroy method exists for ruining extra subscriptions and for optimization the process.

If the destroy method is contained in a function, it means that the event is a one-time event.

Destroy takes the same arguments as the [on](#on) method: event, address, function.

```js
import GudHub from '@gudhub/core';
const gudhub = new GudHub();
let address = {
    app_id: newApp.app_id;
};


// the function will be executed when event happens 
function myFunction(event, value){
    console.log('new app name', value.app_name);

    //if you do not want listen the event anymore, you can destroy it  
    gudhub.destroy('gh_app_views_update', address, myFunction);
};

gudhub.on('gh_app_views_update', address, myFunction);
```
