# Document Manager

Document manager is responsible for creating, receiving, and deleting documents in the application.

All file manipulation is provided by these methods:

1. [createDocument](#createdocument)

2. [getDocument](#getdocument)

3. [getDocuments](#getdocuments)

4. [deleteDocument](#deletedocument)

## createDocument()

This method is called for creating a new document in the certain application. It takes object of general document data.

```js
async gudhub.createDocument(documentObject)
```

Argument Name|Type|Description
:---|:---|:---
documentObject|`object`|*contains app_id, field_id, item_id, and data*

```js
import GudHub from '@gudhub/core';

const authkey = 'kLKKSlfjMmslk;eks;ldfspfk,v';
let documentObject = {
    app_id: "24343",
    element_id: "324534",
    item_id: "2324234",
    data: {
        name: "John",
        last_name: "Dow"
    },
}

(async ()=>{
    const gudhub = await new GudHub(authkey);
    let createdDocument = await gudhub.createDocument(documentObject);
    console.log(createdDocument)
})();
```

In response we get the object of the created document:

```json
{
    "_id": "62b9a14990084b1d7bea8dc3",
    "app_id": 24343,
    "element_id": 324534,
    "item_id": 2324234,
    "last_update": 1656332617160,
    "data": "{\"text\":\"John\",\"level\":\"Dow\"}"
}
```

## getDocument()

The method for getting needed document from the applications documents list. It takes a desired document location object as an argument.

```js
async gudhub.getDocument(documentAddress)
```

Argument Name|Type|Description
:---|:---|:---
documentAddress|`object`|*contains app_id, field_id, and item_id*

```js
import GudHub from '@gudhub/core';

const authkey = 'kLKKSlfjMmslk;eks;ldfspfk,v';
let documentAddress = {
    app_id: "24343",
    element_id: "324534",
    item_id: "2324234",
};

(async ()=>{
    const gudhub = await new GudHub(authkey);
    let gottenDoc = await gudhub.getDocument(documentAddress);
    console.log(gottenDoc)
})();
```

This method returns such object as response:

```json
{
    "_id": "62b9a14990084b1d7bea8dc3",
    "app_id": 24343,
    "element_id": 324534,
    "item_id": 2324234,
    "last_update": 1656332617160,
    "data": "{\"text\":\"John\",\"level\":\"Dow\"}"
}
```

## getDocuments()

This method is called to get array of documents from the application. As an argument it takes a array of document locations objects.

```js
async gudhub.getDocuments(documentsAddresses)
```

Argument Name|Type|Description
:---|:---|:---
documentsAddresses|`object`|*contains app_id, field_id, and item_id*

```js
import GudHub from '@gudhub/core';

const authkey = 'kLKKSlfjMmslk;eks;ldfspfk,v';
let documentsAddresses = [{
    app_id: "24343",
    element_id: "324534",
    item_id: "2324234"
},{
    app_id: "24344",
    element_id: "324534",
    item_id: "2324678"
}]

(async ()=>{
    const gudhub = await new GudHub(authkey);
    let gottenDocs = await gudhub.getDocuments(documentsAddresses);
    console.log(gottenDocs)
})();
```

This method returns an array of document objects:

```json
[{
    "_id": "62b9a14990084b1d7bea8dc3",
    "app_id": 24343,
    "element_id": 324534,
    "item_id": 2324234,
    "last_update": 1656332617160,
    "data": "{\"text\":\"John\",\"level\":\"Dow\"}"
},{
    "_id": "62b9a14990084bjdkfnneer3243",
    "app_id": 24344,
    "element_id": 324534,
    "item_id": 2324678,
    "last_update": 1656332617160,
    "data": "{\"text\":\"John\",\"level\":\"Dow\"}"
}]
```

## deleteDocument()

Due to this method you can delete document from the application.

```js
async gudhub.deleteDocument(documentAddress)
```

Argument Name|Type|Description
:---|:---|:---
documentAddress|`object`|*contains app_id, field_id, and item_id*

```js
import GudHub from '@gudhub/core';

const authkey = 'kLKKSlfjMmslk;eks;ldfspfk,v';
let documentAddress = {
    app_id: "24343",
    element_id: "324534",
    item_id: "2324234",
};

(async ()=>{
    const gudhub = await new GudHub(authkey);
    let deletedDoc = await gudhub.deleteDocument(documentAddress);
    console.log(deletedDoc)
})();
```

Despite the method deletes document, it returns an object of the deleted document:

```json
{
    "_id": "62b9a14990084b1d7bea8dc3",
    "app_id": 24343,
    "element_id": 324534,
    "item_id": 2324234,
    "last_update": 1656332617160,
    "data": "{\"text\":\"John\",\"level\":\"Dow\"}"
}
```
