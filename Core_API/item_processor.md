# Item Processor

Item Processor is responsible for creating, receiving, updating and deleting items. Please note that you can not delete items from an application. When you delete the items they just marked as deleted.

Item Processor provides such methods for working with items:

1. [getItems](#getitems)

2. [addNewItems](#addnewitems)

3. [updateItems](#updateitems)

4. [deleteItems](#deleteitems)

## getItems()

Method returns items from the application. It takes an application ID as an argument.

```js
async gudhub.getItems(app_id, trash)
```

Argument Name|Type|Description
:------------|:-----------|:------------
app_id|`number`|*application ID where we want to get the field model*
trash|`boolean`|*if it's true then it returns deleted items only*

```js
import GudHub from '@gudhub/core';

const AUTHKEY = "NKJHIUHknjcnkhios9w92ehds78/7";
let app_id = 343;
let trash = false;

(async ()=>{
    const gudhub = await new GudHub(AUTHKEY);
    let gottenItems = await gudhub.getItems(app_id, trash);
    console.log(gottenItems);
})();
```

This method returns items from a particular application:

```json
[{
    "item_id": 2987986,
    "fields": [{
        "field_id": 645888,
        "field_value": "John",
    }],
    },{
    "item_id": 2987996,
    "fields": [{
        "field_id": 645888,
        "field_value": "Johana",
    }]
}]
```

## addNewItems()

The method adds a new item in the items list. It takes application ID and an item list as arguments.

```js
async gudhub.addNewItems(app_id, itemsList)
```

Argument Name|Type|Description
:---------|:-----------|:-----------
app_id|`number`|*application ID where we want to get the field model*
itemsList|`array`|*array of application items*

```js
import GudHub from '@gudhub/core';

const AUTHKEY = "NKJHIUHknjcnkhios9w92ehds78/7";
let app_id = 345;
let itemsList = [{
  "fields": [{
        "field_id": 645888,
        "field_value": "John",
      }],
  },
  {
  "fields": [{
        "field_id": 645888,
        "field_value": "Johana",
      }]
  }
];

(async ()=>{
    const gudhub = await new GudHub(AUTHKEY);
    let newItem = await gudhub.addNewItems(app_id, itemsList);
    console.log(newItem)
})();
```

In response, we will get an array of items:

```json
[{
    "item_id": 2987986,
    "fields": [{
        "field_id": 645888,
        "field_value": "John",
    }],
    },{
    "item_id": 2987996,
    "fields": [{
        "field_id": 645888,
        "field_value": "Johana",
    }]
}]
```

## updateItems()

The method allows you to update an existing item. It takes application ID and items list as arguments.

```js
async gudhub.updateItems(app_id, itemsList)
```

Argument Name|Type|Description
:---------|:-----------|:-----------
app_id|`number`|*application ID where we want to get the field model*
itemsList|`array`|*array of application items*

```js
import GudHub from '@gudhub/core';

const AUTHKEY = "NKJHIUHknjcnkhios9w92ehds78/7";
let itemsList = [{
  "item_id": 2987986,
  "fields": [{
        "field_id": 645888,
        "field_value": "John",
      }],
  },
  {
  "item_id": 2987996,
  "fields": [{
        "field_id": 645888,
        "field_value": "Johana",
      }]
  }
];

(async ()=>{
    const gudhub = await new GudHub(AUTHKEY);
    let updatedItems = await gudhub.updateItems(app_id, itemsList);
    console.log(updatedItems);
})();
```

It returns an updated item object:

```json
[{
    "item_id": 2987986,
    "fields": [{
        "field_id": 645888,
        "field_value": "John",
    }],
},{
    "item_id": 2987996,
    "fields": [{
        "field_id": 645888,
        "field_value": "Johana",
    }]
}]
```

## deleteItems()

This method is called for deleting existed items. As arguments, it takes application ID and items list.

```js
async gudhub.deleteItems(app_id, itemsIds)
```

Argument Name|Type|Description
:---------|:-----------|:-----------
app_id|`number`|*application ID where we want to get the field model*
itemsList|`array`|*array of application items*

```js
import GudHub from '@gudhub/core';

const AUTHKEY = "NKJHIUHknjcnkhios9w92ehds78/7";
let app_id = 454;
let itemsIds = [3129206, 3129207];

(async ()=>{
    const gudhub = await new GudHub(AUTHKEY);
    let deletedItems = await gudhub.deleteItems(app_id, itemsIds);
    console.log(deletedItems);
})();
```

As a result we get an array of deleted items:

```json
[{
    "item_id": 3129206,
    "fields": [{
        "field_id": 645888,
        "field_value": "John",
    }],
},{
    "item_id": 3129207,
    "fields": [{
        "field_id": 645888,
        "field_value": "Johana",
    }]
}]
```
