# Interpretation

Interface is an integral part of every program, especially for low &no-code platforms. For easy and convenient using of the application made there, it must be user-friendly. Therefore, the type of displaying and its customizing are incredibly important. For this task, the interpretation exists. Its settings allows you to configure in which part of application and how the elements will be displayed.

In this article you can read about:

1. [Interpretation Types](#interpretation-types)

2. [Interpretation Places](#interpretation-places)

3. [Interpretation of GH-elements](#interpretation-of-gh-elements)

## Interpretation Types

Interpretation type is a model on which the platform understand how to display the data. The programmer writes them himself, so he can edit and delete it or add a new one at any time.

Interpretation types are available in gh-elements settings. You can fluently change them during the application existing. There is the one that is used by default. It will be displayed, if the other interpretation types do not exist or are not selected.

## Interpretation Places

Interpretation places are the places where the element with the selected type of interpretation will be displayed.

There are many types of such places. Each of them is set as an elem-src var.

Place|Where they will be assigned
:---|:---
Form|assigned in the view
Table|assigned in the table
Input|assigned in the form
Input list|assigned in the input list
Dropdown|assigned in the dropdown
Document|assigned in the document

Container deserves particular attention. This interpretation type which will be used first, if it exists.

## Interpretation of GH-elements

Every gh-element has a variety of different interpretation types.
In addition, developers can add their own one.

```json
{
    "interpretation":[{
        "id": "default",
        "settings": {
            "editable": 0,
            "show_field": 1,
            "show_field_name": 0
        },
        "src": "table"
    }]
}
```

In fact, all differences between interpretation types are values of *id* and *src* properties. All other properties can be configured regardless of the source place.

Name|Type|Description
:---|:---|:---
id|`string`|*shows the type of value; [details...](#interpretation-types)*
settings|`object`|*contains basic value settings*
editable|`boolean`|*shows is field value editable or not*
show_field|`boolean`|*shows the field is displayed or not*
show_field_name|`boolean`|*shows the field name is displayed or not*
src|`string`|*shows where current interpretation type is displayed; [details...](#interpretation-places)*

## Methods

There two available methods in GudHub which allows you to get the interpretation type of the certain gh-element. Since each element usually has several types of interpretation, the returns of this method will be different each time. So, we have identified the main groups of interpretations in a [separate chapter](#returns).

### getInterpretation

The first method is created to get the type of interpretation of a certain element in a certain container.

```js
import GudHub from '@gudhub/core';

const AUTHKEY = "NKJHIUHknjcnkhios9w92ehds78/7T7GYfz67w2eu+dsidhfnc2365k";
let value = "#ffffff";
let field = 697462;
let dataType = {
    field_id: 697462,
    field_name: 'Color',
    field_value: '#ffffff',
    data_id: 'color',
    data_type: 'color',
    file_name: 0,
    data_model: {
        advanced_mode_by_default: true,
        interpretation : [
            {
            src: 'table',
            id: 'input',
            settings:{
                editable: 0,
                show_field_name: 0,
                show_field: 1
            }
            },{
            src: 'form',
            id: 'input',
            settings:{
                editable: 1,
                show_field_name: 1,
                show_field: 1
            }
            },{
            src: 'dropdown',
            id: 'default',
            settings:{
                editable: 0,
                show_field_name: 0,
                show_field: 1
            }
            },{
            src: 'input',
            id: 'default',
            settings:{
                editable: 0,
                show_field_name: 0,
                show_field: 1
            }
            },{
            src: 'input_list',
            id: 'default',
            settings:{
                editable: 0,
                show_field_name: 0,
                show_field: 1
            }
        }]
    }
};
let source = "form";
let itemId = 3205038;
let appId = 29860;
let containerId = 1575926;

(async ()=>{
    const gudhub = await new GudHub(AUTHKEY);
    let gottenInterpretation = await gudhub.getInterpretation(value, field, dataType, source, itemId, appId, containerId);
    console.log(gottenInterpretation);
})();
```

Argument Name|Type|Description
:---|:---|:---
value|`string`|*transmits value of the selected field*
field|`object`|*transmits model of the selected field*
dataType|`string`|*transmits type of field data*
source|`string`|*transmits place of the field location where the interpretation type is used*
itemId|`number`|*transmits ID of the item where the field is located*
appId|`number`|*transmits ID of the application where the element is located*
containerId|`number`|*transmits ID of the container where the element is located; is used instead of **source***

The current example will return:

```json
'<gh-color gh-model="field_model.field_value" editable="field_model.settings.editable" mode="field_model.data_model.advanced_mode_by_default"/>'
```

### getInterpretationById

The current method allows to get one of the interpretation type of the certain element by its location data and ID of the element interpretation type.

```js
import GudHub from '@gudhub/core';

const AUTHKEY = "NKJHIUHknjcnkhios9w92ehds78/7T7GYfz67w2eu+dsidhfnc2365k";
let appId = 29674;
let itemId = 3186265;
let fieldId = 695215;
let interpretationId = "input";

(async ()=>{
    const gudhub = await new GudHub(AUTHKEY);
    let gottenInterpretation = await gudhub.getInterpretationById(appId, itemId, fieldId, interpretationId);
    console.log(gottenInterpretation);
})();
```

Four IDs are accepted as arguments:

Argument Name|Type|Description
:---|:---|:---
appId|`number`|*transmits ID of the application where the selected element is located*
itemId|`number`|*transmits ID of the item where selected element is located*
fieldId|`number`|*transmits the identifier of the selected field, the type of interpretation of which will be received*
interpretationId|`string`|*transmits ID of the element interpretation type*
value|`string`|*transmits field value*

In this case, the method will return:

```json
'<gh-color gh-model="field_model.field_value" editable="field_model.settings.editable" mode="field_model.data_model.advanced_mode_by_default"/>'
```

### Returns

Each of the data type has one and more interpretation types. That's why, the returned data is depends on the element and its type of interpretation.

#### **Default**

In most cases, these methods returns the **string with HTML markup**. That is because the content of most types of interpretation contains exactly this data.

```json
'<gh-color gh-model="field_model.field_value" editable="field_model.settings.editable" mode="field_model.data_model.advanced_mode_by_default"/>'
```

#### **HTML**

With this interpretation type methods above will returns ready **formatted valid HTML code**.

#### **Value**

The interpretation types called *Value* returns the **raw value** of the certain field.

```json
'#ffffff'
```

#### **Functions**

Types of interpretation of several elements contain functions in their *Content*. The get interpretation methods allow you to get the return of such functions.

```js
{
    id: 'value',
    name: 'Value',
    content: () => {
        return 
        ...
    }
}
```
