# Field Processor

Field processor is responsible for receiving, updating fields models. Also with field processor you can get and update field values.

All field methods are collected and described in this article:

1. [getField](#getfield)

2. [updateField](#updatefield)

3. [deleteField](#deletefield)

4. [getFieldModels](#getfieldmodels)

5. [getFieldValue](#getfieldvalue)

6. [setFieldValue](#setfieldvalue)

## getField()

Due to this method you can get field from the relevant application. It takes application and field IDs as arguments.

```js
async gudhub.getField(app_id, field_id)
```

Argument Name|Type|Description
:---|:---|:---
app_id|`number`|*application ID where we want to get the field model*
field_id|`number`|*field ID that we want to receive*

```js
import GudHub from '@gudhub/core';

const AUTHKEY = 'kLKKSlfjMmslk;eks;ldfspfk,v';
let app_id = 23;
let field_id = 34212;

(async ()=>{
    const gudhub = await new GudHub(AUTHKEY);
    let gottenField = await gudhub.getField(app_id, field_id);
    console.log(gottenField)
})();
```

Method returns field model from specific application:

```json
{
    "app_id": 23,
    "field_id": 34212,
    "data_type": "image",
    "name_space": "thumbnail",
    "field_priority": 0,
    "field_name": "Thumbnail",
    "data_model": {...}
}
```

## updateField()

This method allows you to update field model of the current application. It take two arguments:

```js
async gudhub.updateField(app_id, fieldModel);
```

Argument Name|Type|Description
:---|:---|:---
app_id|`number`|*application ID where we want to update the field model*
fieldModel|`object`|*contains existing field data*

```js
import GudHub from '@gudhub/core';

const AUTHKEY = 'kLKKSlfjMmslk;eks;ldfspfk,v';
let app_id = 23;
let fieldModel = {
    "app_id": 23,
    "field_id": 505247,
    "data_type": "image",
    "name_space": "thumbnail",
    "field_priority": 0,
    "field_name": "Thumbnail",
    "data_model": {...}
};

(async ()=>{
    const gudhub = await new GudHub(AUTHKEY);
    let updatedField = await gudhub.updateField(app_id, fieldModel);
    console.log(updatedField)
})();
```

The method returns an updated field model:

```json
{
    "app_id": 23,
    "field_id": 505247,
    "data_type": "image",
    "name_space": "thumbnail",
    "field_priority": 0,
    "field_name": "Thumbnail",
    "data_model": {...}
}
```

## deleteField()

The method is responsible for deleting field from the application. Its arguments are application and field IDs.

```js
gudhub.deleteField(app_id, field_id)
```

Argument Name|Type|Description
:---|:---|:---
app_id|`number`|*application ID where we want to delete the field model*
field_id|`number`|*unique field ID*

```js
import GudHub from '@gudhub/core';
const AUTHKEY = 'kLKKSlfjMmslk;eks;ldfspfk,v';

let app_id = 23;
let field_id = 34545;

(async ()=>{
    const gudhub = await new GudHub(AUTHKEY);
    let deletedField = await gudhub.deleteField(app_id, field_id);
    console.log(deletedField)
})();
```

In response, we will get the model of the deleted field.

```json
{
    "app_id": 23,
    "field_id": 34545,
    "data_type": "text",
    "name_space": "first-name",
    "field_priority": 0,
    "field_name": "First Name",
    "data_model": {...}
}
```

## getFieldModels()

This method is used for getting a list of field models. It takes applications ID as an argument.

```js
async gudhub.getFieldModels(app_id)
```

Argument Name|Type|Description
:---|:---|:---
app_id|`number`|*application ID where we want to get the field models*

```js
import GudHub from '@gudhub/core';

const AUTHKEY = 'kLKKSlfjMmslk;eks;ldfspfk,v';
let app_id = 23;

(async ()=>{
    const gudhub = await new GudHub(AUTHKEY);
    let fieldModels = await gudhub.getFieldModels(app_id);
    console.log(fieldModels)
})();
```

It returns a response with an array of application field models.

```json
[{
    "app_id": 23,
    "field_id": 34545,
    "data_type": "text",
    "name_space": "first-name",
    "field_priority": 0,
    "field_name": "First Name",
    "data_model": {...}
},…]
```

## getFieldValue()

This method is responsible for getting the value from the desired field. It takes application, item and field IDs as arguments.

```js
async gudhub.getFieldValue(app_id, item_id, field_id)
```

Argument Name|Type|Description
:---|:---|:---
app_id|`number`|*application ID where we want to get the field model*
item_id|`number`|*item ID where we want to get the field model*
field_id|`number`|*unique field ID*

```js
import GudHub from '@gudhub/core';

const AUTHKEY = 'kLKKSlfjMmslk;eks;ldfspfk,v';
let app_id = 23;
let item_id = 22432;
let field_id = 111212;

(async ()=>{
    const gudhub = await new GudHub(AUTHKEY);
    let fieldValue = await gudhub.getFieldValue(app_id, item_id, field_id);
    console.log(fieldValue)
})();
```

The method returns value from desired field:

```json
"value"
```

## setFieldValue()

This method is called to set value in the desired field. It takes application, item, and field IDs and, of course, the very value as arguments.

```js
async gudhub.setFieldValue(app_id, item_id, field_id, field_value)
```

Argument Name|Type|Description
:---|:---|:---
app_id|`number`|*application ID where we want to set field value*
item_id|`number`|*item ID where we want to set field value*
field_id|`number`|*field ID where we want to set field value*
field_value|`string`|*value to be set in the field*

```js
import GudHub from '@gudhub/core';

const AUTHKEY = 'kLKKSlfjMmslk;eks;ldfspfk,v';
let app_id = 23;
let item_id = 22432;
let field_id = 111212;
let field_value = "John Dow";

(async ()=>{
    const gudhub = await new GudHub(AUTHKEY);
    let setFieldValue = await gudhub.setFieldValue(app_id, item_id, field_id, field_value);
    console.log(setFieldValue)
})();
```

The method returns the set value.

```json
"John Dow"
```
