# Auth Core API

Auth is responsible for all processes connected with user accounts, including creating and updating them, authentication, receiving tokens, logging in and out.

It has many methods that provides different types of access:

1. [Login with Username and Password](#login-with-username-and-password)

2. [Login with auth_key](#login-with-auth-key)

3. [Login with access token](#login-with-access-token)

4. [logout](#logout)

5. [signup](#signup)

6. [getUsersList](#getuserslist)

7. [updateUser](#updateuser)

8. [updateToken](#updatetoken)

9. [updateAvatar](#updateavatar)

10. [getUserFromStorage](#getuserfromstorage)

11. [saveUserToStorage](#saveusertostorage)

12. [getUserById](#getuserbyid)

13. [getToken](#gettoken)

13. [getVersion](#getversion)

## Login with Username and Password

This method is called for getting access to the account with the help of username and password. Here we have an object credentials that contains user login and password.

```js
async gudhub.login(credentials)
```

Argument Name|Type|Description
:---|:---|:---
credentials|`object`|*contains login and password*

```js
import GudHub from '@gudhub/core';

let credentials = {
    username : "john@dow.com",
    password : "hdsJJ493"
}

const gudhub = new GudHub();
let login = await gudhub.login(credentials);
console.log(login)
```

In response, we get an object with access information:

```json
{
    "accesstoken": "juirlqoyndjcxkljgcadsldvag_hnvzieubb.eio",
    "app_init": 26749,
    "auth_key": "bMfVNw8yvJHGjJVHJghkgye3dkuy89dgZIzvXLn3KjqbMQ+B5wOA3gKssFzPPDMhhnVk",
    "expirydate": 1633974987485,
    "fullname": "John Dow",
    "user_id": 1354,
    "username": "john@dow.com",
}
```

## Login with auth_key

This is the second way that allows to get access. It takes the authentication key as an argument. Due to this method you automatically use most of the other methods.

```js
import {GudHub} from '@gudhub/gudhub';

let auth_key = "bMfVNw8yvJHGjJVHJghkgye3dkuy89dgZIzvXLn3KjqbMQ+B5wOA3gKssFzPPDMhhnVk";

const gudhub = new GudHub(auth_key);
```

In response it returns a JSON object:

```json
{
    "accesstoken": "jikwkolpe_ba.fgergwed-n-_wjm.thstngje_-_v",
    "app_init": 26502,
    "auth_key": "NWfWIMAcjdskKJNKJB;,DLAKNkjhsiouoiiugtyRUTIU8967rgsja82jjKC+Ya/zNSGF4VKUq25\n1JqIf/mZXA==",
    "avatar_128": "https://gudhub.com/avatars/1627_8451_128.jpg",
    "avatar_512": "https://gudhub.com/avatars/1627_8451_512.jpg",
    "expirydate": 1643872865040,
    "fullname": "John Dow",
    "user_id": 1000,
    "username": "john@dow.com"
}
```

Also, you can use additional parameters. Due to them you can send requests to another server. It is useful for development.

```js
import {GudHub} from '@gudhub/gudhub';


const gudhub = new GudHub(auth_key,{
    server_url : "https://gudhub.com/GudHub", 
    wss_url : "wss://gudhub.com/GudHub/ws/app/", 
    initWebsocket : false
});
```

Name|Type|Description
:--|:--|:--
server_url|`string`|*contains server url*
wss_url|`string`|*contains web socket url that is request endpoint*
initWebsocket|`boolean`|*shows whether a connection to the web socket is allowed*

## Login with access token

This is the third way that allows to get access. It takes object as a second parameter. Due to this method you automatically use most of the other methods.

```js
import {GudHub} from '@gudhub/gudhub';

const accesstoken = "accesstoken";

const gudhub = new GudHub(null, {
    accesstoken: accesstoken,
    expirydate: new Date().getTime() + 300000 // 300000ms is a 5 min
});
```

Also, you can use additional parameters. Due to them you can send requests to another server. It is useful for development.

```js
import {GudHub} from '@gudhub/gudhub';


const gudhub = new GudHub(null, {
    accesstoken: accesstoken,
    expirydate: new Date().getTime() + 300000 // 300000ms is a 5 min
    server_url : "https://gudhub.com/GudHub", 
    wss_url : "wss://gudhub.com/GudHub/ws/app/", 
    initWebsocket : false
});
```

Name|Type|Description
:--|:--|:--
accesstoken|`string`|*access token*
expirydate|`number`|*Date in milliseconds when token will be outdated*
server_url|`string`|*contains server url*
wss_url|`string`|*contains web socket url that is request endpoint*
initWebsocket|`boolean`|*shows whether a connection to the web socket is allowed*

## logout()

This method is used when the user wants to log out. It takes token as an argument.

```js
await gudhub.logout(token)
```

Argument Name|Type|Description
:---|:---|:---
token|`string`|*using for getting access*

```js
import GudHub from '@gudhub/core';

const authkey = 'kjzkjdkdjsklsljdlskv';

(async ()=>{
    const gudhub = new GudHub(authkey);
    await gudhub.logout(token);
})();
```

As a result, we get a success operation for this token:

```json
Logout OK for token: bd.wqffmjipbbyuif-rhclprjrsbcqsndqxfcbeq_jd
```

## signup()

This method is called when a user register a new account. The data of the new user will be added to the data base and the unique authentication key will be given to the user. There is an user object as an argument.

```js
await gudhub.signup(user)
```

Argument Name|Type|Description
:---|:---|:---
user|`object`|*contains new user`s data*

```js
import GudHub from '@gudhub/core';

const authkey = 'kjzkjdkdjsklsljdlskv';
let user = {
    "fullname":"Johana Dow",
    "password":"sjdjj6544",
    "username":"johana@dow.com"
}

(async ()=>{
    const gudhub = new GudHub(authkey);
    await gudhub.signup(user);
})();
```

In response, we will get an object with user access data:

```json
{
    "accesstoken": "eqe-ylcdcbjcvdddgahi_lba_hi.bdnsdadsaspxfvmt",
    "app_init": 26749,
    "auth_key": "bMfVNasjfgj36442ksq82aWs6gZIzvXLn3KjqbMQ+B5wOA3gKssFBKJGJgmnggkjFGG",
    "expirydate": 163843545366,
    "fullname": "Johana Dow",
    "user_id": 1578,
    "username": "johana@dow.com",
}
```

## getUsersList()

This method is called during the search for users to share. Namely, when you enter a keyword into the search box, GudHub displays all users whose names match that word.

```js
await gudhub.getUsersList(keyword)
```

Argument Name|Type|Description
:---|:---|:---
keyword|`string`|*word entered as a search request*

```js
import GudHub from '@gudhub/core';

const authkey = 'KJKHCksdkjslkdlsklk/csoka';
let keyword= "Dow";

(async ()=>{
    const gudhub = new GudHub(authkey);
    await gudhub.getUsersList(keyword);
})();
```

In response will be an array of matched users.

```json
[{
    "expirydate": 0,
    "fullname": "John Dow",
    "user_id": 1577,
    "username": "john@dow.com",
},
{
    "expirydate": 1,
    "fullname": "Johana Dow",
    "user_id": 1571,
    "username": "johana@dow.com",
},...]
```

## updateUser()

This method is used for updating user data. It take an existing user data.

```js
await gudhub.updateUser(userData)
```

Argument Name|Type|Description
:---|:---|:---
userData|`object`|*contains user object with user`s overview*

```js
import GudHub from '@gudhub/core';

const authkey = 'kjzkjdkdjsklsljdlskv';
let userData = {
    user: {...}
}

(async ()=>{
    const gudhub = new GudHub(authkey);
    let updatedUser = await gudhub.updateUser(userData);
    console.log(updatedUser)
})();
```

In response, it returns updating user data.

```json
{
    "avatar_128": "https://gudhub.com/avatars/1578_925_128.jpg",
    "avatar_512": "https://gudhub.com/avatars/1578_925_512.jpg",
    "expirydate": 0,
    "fullname": "Johna Dow",
    "user_id": 1578,
    "username": "john@dow.com",
}
```

## updateToken()

This method updates current token. As an argument, it takes user`s authentication key.

```js
await gudhub.updateToken(auth_key)
```

Argument Name|Type|Description
:---|:---|:---
auth_key|`string`|*personal authentication key*

```js
import GudHub from '@gudhub/core';

const authkey = 'kjzkjdkdjsklsljdlskv';

(async ()=>{
    const gudhub = new GudHub(authkey);
    await gudhub.updateToken(authkey);
})();
```

Token automatically updates every 9 hours.

```json
token: wqkuzkiswsvitfqjl-onbbbnbbjhhaetwbpsbenmmv
```

## updateAvatar()

This method is used when user changes his photo in account. It takes data of the image as an argument.

```js
await gudhub.updateAvatar(imageData)
```

Argument Name|Type|Description
:---|:---|:---
imageData|`base64`|*encrypted image; accepts .jpg, .png, .gif*

```js
import GudHub from '@gudhub/core';

const authkey = 'kjzkjdkdjsklsljdlskv';
let imageData = 'QWxhZGRpbjpvcGVuIHNlc2FtZQ=...';

(async ()=>{
    const gudhub =  new GudHub(authkey);
    let updatedAvatar = await gudhub.updateAvatar(imageData);
    console.log(updatedAvatar)
})();
```

In response will be an updated user data:

```json
{
    "avatar_128": "https://gudhub.com/avatars/1578_6824_128.jpg",
    "avatar_512": "https://gudhub.com/avatars/1578_6824_512.jpg",
    "expirydate": 0,
    "fullname": "John Dow",
    "user_id": 1578,
    "username": "john@dow.com",
}
```

## getUserFromStorage()

This method is used for getting needed user during sharing. It takes user ID.

```js
await gudhub.getUserFromStorage(id)
```

Argument Name|Type|Description
:---|:---|:---
id|`number`|*unique user ID*

```js
import GudHub from '@gudhub/core';

const authkey = 'kjzkjdkdjsklsljdlskv';
let id = 97765;

(async ()=>{
    const gudhub = new GudHub(authkey);
    let userFromStorage = await gudhub.getUserFromStorage(id);
    console.log(userFromStorage)
})();
```

And returns object of needed user data:

```json
{
    "avatar_128": "https://gudhub.com/avatars/1176_4665_128.jpg",
    "avatar_512": "https://gudhub.com/avatars/1176_4665_512.jpg",
    "expirydate": 0,
    "fullname": "John Dow",
    "user_id": 97765,
    "username": "john@dow.com",
}
```

## saveUserToStorage()

Due to this method all users searched for will be added to the *storage*.

```js
await gudhub.saveUserToStorage(saveUser)
```

>Storage is a list of users that allows to do not upload user`s data from server for the second time. This process optimize a work of the server. Users in sharing are taken from this list.

*saveUserToStorage* takes an user object as an argument.

Argument Name|Type|Description
:---|:---|:---
saveUser|`object`|*contains user`s object*

```js
import GudHub from '@gudhub/core';

const authkey = 'kjzkjdkdjsklsljdlskv';
let saveUser = {
    avatar_128: "https://gudhub.com/avatars/1176_4665_128.jpg",
    avatar_512: "https://gudhub.com/avatars/1176_4665_512.jpg",
    expirydate: 0,
    fullname: "John Dow",
    user_id: 1145,
    username: "john@dow.com",
}

(async ()=>{
    const gudhub = new GudHub(authkey);
    let saveUserToStorage = await gudhub.saveUserToStorage(saveUser);
    console.log(saveUserToStorage)
})();
```

Returns the object of user overview:

```json
{
    "avatar_128": "https://gudhub.com/avatars/1176_4665_128.jpg",
    "avatar_512": "https://gudhub.com/avatars/1176_4665_512.jpg",
    "expirydate": 0,
    "fullname": "John Dow",
    "user_id": 1145,
    "username": "john@dow.com",
}
```

## getUserById()

This method gets user from list of user that have access to the application. As an argument, it takes user`s ID.

```js
await gudhub.getUserById(userId)
```

Argument Name|Type|Description
:---|:---|:---
userId|`number`|*unique user ID*

```js
import GudHub from '@gudhub/core';

const authkey = 'kjzkjdkdjsklsljdlskv';
let userId = 64774;

(async ()=>{
    const gudhub = new GudHub(authkey);
    let gottenUserById = await gudhub.getUserById(userId);
    console.log(gottenUserById)
})();
```

It returns object with user`s data:

```json
{
    "expirydate": 0,
    "fullname": "John Dow",
    "user_id": 64774,
    "username": "john@dow.com",
}
```

## getToken()

This method is called for getting current token of the user. It has no arguments.

```js
await gudhub.getToken()
```

```js
import GudHub from '@gudhub/core';

const authkey = 'kjzkjdkdjsklsljdlskv';

(async ()=>{
    const gudhub = new GudHub(authkey);
    await gudhub.getToken();
})();
```

*getToken()* returns current token:

```json
token: wqkuzkiswsvitfqjl-onbbbnbbjhhaetwbpsbenmmv
```

## getVersion()

As well as getToken, getVersion has no argument. It is called to get version of JS.

```js
await gudhub.getVersion()
```

```js
import GudHub from '@gudhub/core';

const authkey = 'kjzkjdkdjsklsljdlskv';

(async ()=>{
    const gudhub = new GudHub(authkey);
    gudhub.getVersion();
})();
```

In response we will get a new HTML page with a number of JS version.

```json
2.7.8.7
```
