# File Manager

File Manager consists of methods that responsible for different functions:

1. [getFile](#getfile)

2. [getFiles](#getfiles)

3. [uploadFile](#uploadfile)

4. [uploadFileFromString](#uploadfilefromstring)

5. [isFileExists](#isfileexists)

6. [updateFileFromString](#updatefilefromstring)

7. [duplicateFile](#duplicatefile)

8. [downloadFileFromString](#downloadfilefromstring)

9. [deleteFile](#deletefile)

## getFile()

This method is called to get file from the application. It takes application and file IDs as arguments.

```js
async gudhub.getFile(app_id, file_id)
```

Pass the method the application ID and the ID of the file you need.

Argument Name|Type|Description
:---|:---|:---
app_id|`number`|*application ID where we want to get the file*
file_id|`number`|*unique file ID*

```js
import GudHub from '@gudhub/core';

const AUTHKEY = "NKJHIUHknjcnkhios9w92ehds78/7T7GYfz67w2eu+dsidhfnc2364kjh322kjkcJIOHBJ";
let app_id = 23456;
let file_id = 92810029;

(async ()=>{
    const gudhub = await new GudHub(AUTHKEY);
    let gottenFile = await gudhub.getFile(app_id, file_id);
    console.log(gottenFile);
})();
```

It returns the file object:

```json
{
    "app_id": 26699,
    "extension": "jpg",
    "file_id": 885173,
    "file_name": "my_picture",
    "item_id": 2917052,
    "last_update": 0,
    "url": "https://gudhub.com/userdata/26699/885173.jpg",
}
```

## getFiles()

This method is called for getting a list of files from current application. It takes application ID and array of file IDs as arguments.

```js
async gudhub.getFiles(app_id, file_id)
```

Current method accepts two arguments:

Argument Name|Type|Description
:---|:---|:---
app_id|`number`|*application ID where we want to get the file*
filesId|`array`|*array of application`s file IDs*

```js
import GudHub from '@gudhub/core';

const AUTHKEY = "NKJHIUHknjcnkhios9w92ehds78/7T7GYfz67w2eu+dsidhfnc2364kjh322kjkcJIOHBJ";
let app_id = 23456;
let filesId = ["2343", "2354"];

(async ()=>{
    const gudhub = await new GudHub(AUTHKEY);
    let gottenFiles = await gudhub.getFiles(app_id, filesId);
    console.log(gottenFiles);
})();
```

It returns an array of file objects:

```json
[{
    "app_id": 26699,
    "extension": "jpg",
    "file_id": 885173,
    "file_name": "8skovoroda",
    "item_id": 2917052,
    "last_update": 234455567,
    "url": "https://gudhub.com/userdata/26699/885173.jpg",
    },
    {
    "app_id": 26709,
    "extension": "png",
    "file_id": 885384,
    "file_name": "34",
    "item_id": 2917121,
    "last_update": 2244555677,
    "url": "https://gudhub.com/userdata/26699/885173.jpg",
}]
```

## uploadFile()

The current method expectedly allows to upload files. It is used only to transfer files to the form.

```js
async gudhub.uploadFile(fileData, app_id, item_id)
```

It accepts data of the file, application and item IDs of the destination location.

Argument Name|Type|Description
:---|:---|:---
fileData|`object`|*contains form data of the file*
app_id|`number`|*contains ID of the destination application where the file will be upload*
item_id|`number`|*contains ID of the destination item where the file will be loaded*

```js
import GudHub from '@gudhub/core';

const AUTHKEY = "NKJHIUHknjcnkhios9w92ehds78/7T7GYfz67w2eu+dsidhfnc2365k";
let fileData = {
    lastModified: 1662377594650,
    lastModifiedDate: Mon Sep 05 2022 14:33:14 GMT+0300,
    name: "_72192176_thewolfofwallstreet001.jpg",
    size: 33503,
    type: "image/jpeg",
    webkitRelativePath: ""
};
let app_id = 23445;
let item_id = 1234343;

(async ()=>{
    const gudhub = new GudHub(AUTHKEY);
    let uploadedFile = await gudhub.uploadFile(fileData, app_id, item_id);
    console.log(uploadedFile);
})();
```

The method returns an object of the uploaded file.

```json
{
    "app_id": 28877,
    "extension": "jpg",
    "file_id": 953393,
    "file_name": "alignment_just",
    "item_id": 3118314,
    "last_update": 0,
    "url": "https://gudhub.com/userdata/28877/953393.jpg"
}
```

## uploadFileFromString()

This is the method that allows to upload different files in base64 format to the application. It accepts object with seven properties:

```js
async gudhub.uploadFileFromString(fileObject)
```

As mentioned above, the method has only one argument.

Name|Type|Description
:---|:---|:---
fileObject|`object`|*object that contains all data of the file*

```js
import GudHub from '@gudhub/core';

const AUTHKEY = "NKJHIUHknjcnkhios9w92ehds78/7T7GYfz67w2eu+dsidhfnc2365k";
let fileObject = {
    "format":"html",
    "source":"test",
    "file_name":"document",
    "extension":"html",
    "app_id":"28818",
    "item_id":"3103065",
    "element_id":"679784"
}

(async ()=>{
    const gudhub = await new GudHub(AUTHKEY);
    let uploadedFile = await gudhub.uploadFileFromString(fileObject);
    let uploadedFile = await gudhub.uploadFileFromString({ "format":"html", "source":"test", "file_name":"document", "extension":"html", "app_id":"28818", "item_id":"3103065", "element_id":"679784"});
    console.log(uploadedFile);
})();
```

In response it returns object like this:

```json
{
    "file_id": 929765,
    "app_id": 26290,
    "item_id": 3041132,
    "file_name": "image",
    "url": "https://gudhub.com/userdata/22390/323765.jpg",
    "extension": "jpg",
    "last_update": 0
}
```

### source

As you already know, we pass base64 code of the file to *source*. Namely, in this method we use only signs after the comma.

Full Base64:

```json
data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAKCAYAAACNMs+9AAAABmJLR0QA/wD/AP+gvaeTAGJWNkMEJADEIBEcbwjyEQXSFECYII=
```

The part that is passed to source:

```json
iVBORw0KGgoAAAANSUhEUgAKCAYAAACNMs+9AAAABmJLR0QA/wD/AP+gvaeTAGJWNkMEJADEIBEcbwjyEQXSFECYII=
```

## isFileExists()

The current method allows you to check the existence of a certain file. It accepts two arguments that determine the file itself and its location. Namely, they allow you to find a file in the [File List](../Understanding_Gudhub/Application_Data_Structure/file_list.md).

```js
async gudhub.fileManager.isFileExists(app_id, file_id)
```

This method transfers the application and file IDs

Argument Name|Type|Description
:---|:---|:---
app_id|`number`|*contains the application ID of the file, which is checked*
file_id|`number`|*contains ID of the file that is checked*

```js
import GudHub from '@gudhub/core';

const AUTHKEY =  "NWfWIMActkcc2Z3p4uJ8jvKh/aF...";
let app_id = 23457;
let file_id = 33409209;

(async ()=>{
    const gudhub = await new GudHub(AUTHKEY);
    let isFileExists = await gudhub.fileManager.isFileExists(app_id, file_id);
    console.log(isFileExists);
})();
```

After checking if the file exists, the method returns *true*, if not - *false*.

```json
true
```

## updateFileFromString()

The current method is created to update the data or name of the certain file.

```js
async gudhub.updateFileFromString(data, file_id, file_name, extension, format)
```

It takes as arguments the file name, ID and data, its extension and format.

Argument Name|Type|Description
:---|:---|:---
data|`string`|*contains new data of the file that will be updated*
file_id|`string`|*contains ID of the file that will be updated*
file_name|`string`|*transmits the updated name of the file that will be updated*
extension|`string`|*transmits extension of the file that will be updated*
format|`string`|*transmits format of the file that will be updated*

```js
import GudHub from '@gudhub/core';

const AUTHKEY =  "NWfWIMActkcc2Z3p4uJ8jvKh/aF...";
let data = "Hello World!";
let file_id = "232435";
let file_name = "Document";
let extension = "html";
let format = "html";

(async ()=>{
    const gudhub = await new GudHub(AUTHKEY);
    let updatedFile = await gudhub.updateFileFromString(data, file_id, file_name, extension, format);
    console.log(updatedFile);
})();
```

In response, the method will return a file list object with the updated file data.

```json
{
    "app_id": 29867,
    "extension": "html",
    "file_id": 962605,
    "file_name": "document_new",
    "item_id": 3206586,
    "last_update": 1663581687912,
    "url": "https://gudhub.com/userdata/29867/962605.html"
}
```

## duplicateFile()

The current method is called to create the copy of the file. It is only used on the server of the GudHub. Called when the user installs some application using [*Install* element](../Gh_Elements/install_element.md).

```js
async gudhub.duplicateFile(files)
```

The single argument of the current method transmits an array with all updated data of the certain files.

Argument Name|Type|Description
:---|:---|:---
files|`array`|*contains objects which consists of source of the file, namely the file ID, and IDs that determine the location where the file will be duplicated*

```js
import GudHub from '@gudhub/core';

const AUTHKEY =  "NWfWIMActkcc2Z3p4uJ8jvKh/aF...";
let files = [{
    source: 23455,
    destination: {
        app_id: 54683,
        item_id: 5323232,
        element_id: 123333
    }
},{
    source: 23456,
    destination: {
        app_id: 74788,
        item_id: 9434432,
        element_id: 436682
    }
}];

(async ()=>{
    const gudhub = new GudHub(AUTHKEY);
    let duplicatedFiles = await gudhub.duplicateFile(files);
    console.log(duplicatedFiles);
})();
```

In response, the method will return an array with updated files data objects.

```json
[{
    "app_id": 54683,
    "extension": "html",
    "file_id": 962605,
    "file_name": "document_new",
    "item_id": 5323232,
    "last_update": 1663581687912,
    "url": "https://gudhub.com/userdata/54683/962605.html"
},{
    "app_id": 74788,
    "extension": "html",
    "file_id": 962612,
    "file_name": "document_new",
    "item_id": 9434432,
    "last_update": 1663581687912,
    "url": "https://gudhub.com/userdata/74788/962612.html"
}]
```

## downloadFileFromString()

As the name suggests, this method allows you to download a file from the file list of a specific application.

```js
async gudhub.downloadFileFromString(app_id,file_id)
```

This method takes two IDs as arguments.

Argument Name|Type|Description
:---|:---|:---
app_id|`number`|*contains ID of the application where the file is located*
item_id|`number`|*contains ID of the item where the file is located*

```js
import GudHub from '@gudhub/core';

const AUTHKEY =  "NWfWIMActkcc2Z3p4uJ8jvKh/aF...";
let app_id = 29523;
let file_id = 954407;

(async ()=>{
    const gudhub = await new GudHub(AUTHKEY);
    let downloadedFile = await gudhub.downloadFileFromString(app_id,file_id);
    console.log(downloadedFile);
})();
```

In response, we will receive the data of the uploaded file.

```json
{
    "data": "PK\u0003\u0004\u0014\u0000\u0006\u0000\b\u0000\u0",
    "file": { 
        "app_id": 29867,
        "extension": "docx",
        "file_id": 962585,
        "file_name": "document",
        "item_id": 3207532,
        "last_update": 0,
        "url": "https://gudhub.com/userdata/29867/962585.docx"
    },
    "type": "file"
}
```

## deleteFile()

Due to this method files can be deleted from the application. It takes application and file IDs as arguments.

```js
async gudhub.deleteFile(app_id, file_id)
```

Two arguments are passed to the method:

Argument Name|Type|Description
:---|:---|:---
app_id|`number`|*application ID where we want to get the file*
file_id|`number`|*unique file ID*

```js
import GudHub from '@gudhub/core';

const AUTHKEY = "NKJHIUHknjcnkhios9w92ehds78/7T7GYfz67w2eu+dsidhfnc2365k";
let app_id = 23456;
let file_id = 92810029;

(async ()=>{
    const gudhub = await new GudHub(AUTHKEY);
    let deletedFile = await gudhub.deleteFile(app_id, file_id);
    console.log(deletedFile);
})();
```

The object of the deleted item will be returned in response:

```json
{
    "app_id": 26699,
    "extension": "png",
    "file_id": 885184,
    "file_name": "34",
    "item_id": 2917052,
    "last_update": 0,
    "url": "https://gudhub.com/userdata/26699/885173.jpg",
}
```
