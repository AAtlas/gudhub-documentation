# App Processor

App Processor is responsible for creating, updating and deleting applications, receiving them, their information, and application list.

This article will describe all app methods:

1. [getAppsList](#getappslist)

2. [getApp](#getapp)

3. [getAppInfo](#getappinfo)

4. [updateApp](#updateapp)

5. [updateAppInfo](#updateappinfo)

6. [createNewApp](#createnewapp)

7. [deleteApp](#deleteapp)

## getAppsList()

With this method you can get list for authentified users applications. The applications will be returned in array format.

```js
async gudhub.getAppsList()
```

Before calling this method you should be authentified.

```js
import GudHub from '@gudhub/core';
const AUTHKEY = "jlCKJDMAKSDOASOPPjk...";

(async ()=>{
    const gudhub = await new GudHub(AUTHKEY);
    let appList = await gudhub.getAppsList();
    console.log(appList);
})();
```

In response we will get an array of applications called apps_list:

```json
[{
    "app_id": 39,
    "app_name": "My Application",
    "chunks": [],
    "field_list": [],
    "file_list": [],
    "group_id": 42,
    "icon": {},
    "items_list": [],
    "keys_list": [],
    "last_update": 1626443488601,
    "permission": 2,
    "priority": 0,
    "privacy": 0,
    "show": true,
    "trash": false,
    "view_init": 25,
    "views_list": []
},
{
    "app_id": 49,
    "app_name": "My Application 2",
    "chunks": [],
    "field_list": [],
    "file_list": [],
    "group_id": 42,
    "icon": {},
    "items_list": [],
    "keys_list": [],
    "last_update": 1626443488601,
    "permission": 2,
    "priority": 0,
    "privacy": 0,
    "show": true,
    "trash": false,
    "view_init": 25,
    "views_list": []
}]
```

## getApp()

This method is called to get an application by its ID specified as an argument.

```js
async gudhub.getApp(app_id)
```

Argument Name|Type|Description
:---|:---|:---
app_id|`number`|*unique application ID*

```js
import GudHub from '@gudhub/core';

const AUTHKEY = "jlCKJDMAKSDOASOPPjk...";
let app_id = 23748;

(async ()=>{
    const gudhub = await new GudHub(AUTHKEY);
    let application = await gudhub.getApp(app_id);
    console.log(application);
})();
```

It returns the object of the certain application:

```json
{
    "app_id": 23748,
    "app_name": "Project Management",
    "chunks": [],
    "field_list": [...],
    "file_list": [...],
    "group_id": 26478,
    "icon": {...},
    "items_list": [...],
    "keys_list": [...],
    "last_update": 1638795735791,
    "permission": 4,
    "priority": 0,
    "privacy": 0,
    "show": true,
    "trash": false,
    "view_init": 1289987,
    "views_list": [...]
}
```

## getAppInfo()

For optimization of the processes was created such methods as getAppInfo. They exclude a need to get all information from server.

```js
async gudhub.getAppInfo(app_id)
```

This particular one is called to get an application overview.

Argument Name|Type|Description
:---|:---|:---
app_id|`number`|*unique application ID*

```js
import GudHub from '@gudhub/core';

const AUTHKEY = "jlCKJDMAKSDOASOPPjk...";
let app_id = 23748;

(async ()=>{
const gudhub = await new GudHub(AUTHKEY);
let appInfo = await gudhub.getAppInfo(app_id);
console.log(appInfo);
})();
```

As a result, we will get an application object with empty arrays, but with information about, for example, icon or permission.

```json
{
    "app_id": 23748,
    "app_name": "My Application",
    "chunks": [],
    "field_list": [],
    "file_list": [],
    "group_id": 42,
    "icon": {
        "id": 25250, 
        "icon_id": "paint", 
        "icon_color": "ffffff", 
        "gradient_up": "89f7fe", 
        "gradient_down": "66a6ff"},
    "items_list": [],
    "keys_list": [],
    "last_update": 1626443488601,
    "permission": 2,
    "priority": 0,
    "privacy": 0,
    "show": true,
    "trash": false,
    "view_init": 25,
    "views_list": [],
}
```

## updateApp()

This method is called to update existing application data. As an argument it takes existing application object.

```js
async gudhub.getAppInfo(app_id)
```

Argument Name|Type|Description
:---|:---|:---
app|`object`|*contains updated application data*

```js
import GudHub from '@gudhub/core';

const AUTHKEY = "jlCKJDMAKSDOASOPPjk...";
let app = {
    app_id: 26506,
    app_name: "Project Team",
    chunks: [],
    field_list: [{...},...],
    file_list: [],
    icon: {...},
    items_list: [],
    permission: 4,
    priority: 0,
    privacy: 1,
    show: true,
    view_init: 1289961,
    views_list: [{...},...],
};

(async ()=>{
    const gudhub = await new GudHub(AUTHKEY);
    let updatedApp = await gudhub.updateApp(app);
    console.log(updatedApp);
})();
```

It returns an application object with all updated information.

```json
{
    "app_id": 26506,
    "app_name": "Project Team",
    "chunks": [],
    "field_list": [{...},...],
    "file_list": [],
    "group_id": 26477,
    "icon": {...},
    "items_list": [],
    "keys_list": [{...}],
    "last_update": 1638802007942,
    "permission": 4,
    "priority": 0,
    "privacy": 1,
    "show": true,
    "trash": false,
    "view_init": 1289961,
    "views_list": [{...},...],
}
```

## updateAppInfo()

As well as getAppInfo, this method works with application overview. Namely, updateAppInfo updates general information.

```js
async gudhub.updateAppInfo(appInfo)
```

Argument Name|Type|Description
:---|:---|:---
appInfo|`object`|*contains only general data about application*

```js
import GudHub from '@gudhub/core';

const AUTHKEY = "jlCKJDMAKSDOASOPPjk...";
let appInfo = {
    app_name: "Sales",
};

(async ()=>{
    const gudhub = await new GudHub(AUTHKEY);
    let updatedAppInfo = await gudhub.updateAppInfo(appInfo);
    console.log(updatedAppInfo);
})();
```

This method returns an updated application.

```json
{
    "app_id": 26506,
    "app_name": "Christmas Sales",
    "chunks": [],
    "field_list": [{...},...],
    "file_list": [],
    "group_id": 26477,
    "icon": {...},
    "items_list": [],
    "keys_list": [{...}],
    "last_update": 1638802007942,
    "permission": 4,
    "priority": 0,
    "privacy": 1,
    "show": true,
    "trash": false,
    "view_init": 1289961,
    "views_list": [{...},...],
}

```

## createNewApp()

This method is called for creating a new application with default data.

```js
async gudhub.createNewApp(app)
```

Argument Name|Type|Description
:---|:---|:---
app|`object`|*contains new application`s data*

```js
import GudHub from '@gudhub/core';

const AUTHKEY = "jlCKJDMAKSDOASOPPjk...";
let app = {
    app_id: 26748,
    app_name: "Template New",
    chunks: [],
    field_list: [],
    file_list: [],
    group_id: 26719,
    icon: {...},
    items_list: [],
    permission: 4,
    priority: 0,
    privacy: 1,
    show: true,
    trash: false,
    view_init: 1314802,
    views_list: [{...},…],
};

(async ()=>{
    const gudhub = await new GudHub(AUTHKEY);
    let newApp = await gudhub.createNewApp(app);
    console.log(newApp);
})();
```

It returns a new application object:

```json
{
    "app_id": 26748,
    "app_name": "Template New",
    "chunks": [],
    "field_list": [],
    "file_list": [],
    "group_id": 26719,
    "icon": {...},
    "items_list": [],
    "keys_list": [],
    "last_update": 1638803480148,
    "permission": 4,
    "priority": 0,
    "privacy": 1,
    "show": true,
    "trash": false,
    "view_init": 1314802,
    "views_list": [{...},…],
}
```

## deleteApp()

This method is called to delete an application.

```js
async gudhub.deleteApp(app_id)
```

Argument Name|Type|Description
:---|:---|:---
app_id|`number`|*unique application ID*

```js
import GudHub from '@gudhub/core';

const AUTHKEY = "jlCKJDMAKSDOASOPPjk...";
let app_id = 23749;

(async ()=>{
    const gudhub = await new GudHub(AUTHKEY);
    let deletedList = await gudhub.deleteApp(app_id);
    console.log(deletedList);
})();
```

We will get a list of applications without the deleted application:

```json
[{
    "app_id": 23748,
    "app_name": "Marketing",
    "chunks": [],
    "field_list": [],
    "file_list": [],
    "group_id": 42,
    "icon": {},
    "items_list": [],
    "keys_list": [],
    "last_update": 1626443488601,
    "permission": 2,
    "priority": 0,
    "privacy": 0,
    "show": true,
    "trash": false,
    "view_init": 25,
    "views_list": [],
},{
    "app_id": 23750,
    "app_name": "Programming",
    "chunks": [],
    "field_list": [],
    "file_list": [],
    "group_id": 42,
    "icon": {},
    "items_list": [],
    "keys_list": [],
    "last_update": 1626443488601,
    "permission": 2,
    "priority": 0,
    "privacy": 0,
    "show": true,
    "trash": false,
    "view_init": 25,
    "views_list": [],
}]
```
