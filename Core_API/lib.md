# Core API

Core API is a universal tool for development applications. It contains all necessary method that can be called from it. In addition, it makes it easy to integrate the application with another programming language.

Core contains six separated sections with different packages of methods. Each of them carry out its own tasks.

1. [Initialization](#initialization)
   
2. [Auth](#auth)

3. [App Processor](#app-processor)

4. [Field Processor](#field-processor)

5. [Item Processor](#item-processor)

6. [Document Manager](#document-manager)

7. [File Manager](#field-processor)

## Initialization

The GudHub library provides two types of its initialization.

### Import

The first way is the importing library into you project. You can install Lib using **npm**

```js
npm install @gudhub/core
```

After the installation you can import the Lib in your project:

```js
import Gudhub from "@gudhub/core"

window.gudhub = new Gudhub(authKey);
```

And then the **GudHubLibrary** can be called in your code.

```js
let {GudHub} = new GudHub;
```

### Script

The second way is to use unpkg CDN:

```html
<script src="https://unpkg.com/@gudhub/core@1.1.30/umd/library.min.js"></script>
```

>The link in the example is usable. Note that the link must contain the *current version* of the library.

### Advanced Initialization

```js
import {GudHub} from '@gudhub/core';
const auth_key = 'JLKHKJSKJhkjsclkdjcijIOIoiuSTg';
const gudhub = new GudHub(auth_key,{
    server_url : "https://gudhub.com/GudHub", 
    wss_url : "wss://gudhub.com/GudHub/ws/app/", 
    initWebsocket : false
});
```

Parameter|Default Value|Description
:--|:--|:--
server_url|`https://gudhub.com/GudHub`|*URL of the server to which the request is sent. It allows you to send request to different servers.*
wss_url|`wss://gudhub.com/GudHub/ws/app/`|*URL of the websocket to which the request is sent. This way you can determine which websocket the request will be sent to.*
initWebsocket|`false`|*This property determines whether a websocket will be used or not. Send true to use them, or false to not use them.*

## Auth

One of the basic functions is authentication. In Core API responsibility for it lies with the Auth. It contains all methods that allow getting accesses to the user account and to edit all data connected with this account. [Read more about Auth.](auth_core_api.md)

## App Processor

The fundamental Core API`s task is creation applications and working with them. For the first purpose exists App Processor. It also allows to edit general application settings. [Read more about App Processor](app_processor.md).

## Field Processor

Every application contains variety functionality. It can contain different buttons, tables, input boxes, etc. All of them called fields. They determines how to work with all application data. For their creation and editing, Field processor is responsible. [Read more about Field Processsor.](field_processor.md)

## Item Processor

Applications contains and produce lots of data. And input ones occupies a special place in data models list. There are separated blocks for input fields called items. Item Processor is designed for their creation and editing. [Read more about Item Processor.](item_processor.md)

## Document Manager

As special arrays of objects, documents have a separated section for working with them. Document manager contains all needed methods for those purposes. [Read more about Document Manager.](document_manager.md)

## File Manager

File manager contains method for working with files. Those are the type of data that cannot be created in application, just uploaded by the user. [Read more about File Manager.](file_manager.md)
