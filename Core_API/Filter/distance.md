# Distance

This is the unique search type in the filter. Distance is used only in the GPS Coords item. It works with coordinates. Distance accepts two values:

- Place
- Radius of searching

In filters list we can see:

```json
{
  "data_type": "gps_coords",
  "field_id": 625329,
  "search_type": "distance",
  "selected_search_option_variable": "Value",
  "valuesArray": [""]
}
```

Numbers in the following table means search radius in kilometers.

F\S|San Francisco, 4000|New York, 500|London, 0|Tokyo, 24000|Denver, 2000
:---|:---:|:---:|:---:|:---:|:---:
Boston|-|+|-|+|-
New York|-|+|-|+|-
London|-|-|+|+|-
Tokyo|-|-|-|+|-
San Francisco|+|-|-|+|+

>S - search values
>
>San Francisco - type place
>
>4000 - radius in kilometers
>
>F - column of field values
>
>"+" - matched by filter
>
>"-" - not matched by filter

## Place

This argument accepts a name of the place in the text format. Counties, cities, streets and other objects one the map can be the value. Also, it allows to choose user current location.

## Radius of Searching

It accepts the radius of searching around the chosen place. It is calculated in kilometers.

## Examples

The table of locations is looks like this:

```json
[{
  "item_id": 2979065,
  "fields": [{
      "field_id": 629160,
      "field_value": "42.3600825:-71.0588801"
  }]
  },
  {
  "item_id": 2979066,
  "fields": [{
      "field_id": 629160,
      "field_value": "40.7127753:-74.0059728"
    }]
  },
  {
  "item_id": 2979067,
  "fields": [{
      "field_id": 629160,
      "field_value": "51.5072178:-0.1275862"
  }]
  },
  {
  "item_id": 2979068,
  "fields": [{
      "field_id": 629160,
      "field_value": "35.6803997:139.7690174"
    }]
  },
  {
  "item_id": 2979069,
  "fields": [{
      "field_id": 629160,
      "field_value": "37.7749295:-122.4194155"
    }]
}]
```

Results of filtering:

- Location: *San Francisco*
  
  Distance radius: *4000*

  ```json
  [{
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": "37.7749295:-122.4194155"
      }]
  }]
  ```

- Location: *New York*
  
  Distance radius: *500*

  ```json
  [{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": "42.3600825:-71.0588801"
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": "40.7127753:-74.0059728"
      }]
  }]
  ```

- Location: *London*
  
  Distance radius: *0*

  ```json
  [{
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "51.5072178:-0.1275862"
    }]
  }]
  ```

- Location: *Tokyo*
  
  Distance radius: *24000*

  ```json
  [{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": "42.3600825:-71.0588801"
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": "40.7127753:-74.0059728"
      }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "51.5072178:-0.1275862"
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "35.6803997:139.7690174"
      }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": "37.7749295:-122.4194155"
    }]
  }]
  ```

- Location: *Denver*
  
  Distance radius: *2000*

  ```json
  [{
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": "37.7749295:-122.4194155"
    }]
  }]
  ```
