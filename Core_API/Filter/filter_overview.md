# Filter Overview

**Filter** is the important entities of GudHub. It is responsible for the sorting and structuring data.

There are several items that have filter: table, . Some of them use it inside the code, others allows to use it in the application. The programmer decides which item will have the filter.

Every data that display items has its own filtering set.
The filtration takes field values for finding and selecting needed data. Also, you can configure filter for different fields and select the type of the filtration.

The filter accepts different types of search values (string, number etc.) and can be applied for different types of fields.

It accepts two arrays as arguments:

```js
import GudHub from '@gudhub/core';
const gudhub = await new GudHub();
let items = [{
  "item_id": 2987986,
  "fields": [{
        "field_id": 645888,
        "field_value": "John",
      }],
  },
  {
  "item_id": 2987996,
  "fields": [{
        "field_id": 645888,
        "field_value": "Johana",
      }]
  }
];

let filter_list = [{
    "data_type": "text",
    "field_id": 645888,
    "search_type": "search",
    "selected_search_option_variable": "Value",
    "valuesArray": ["john"]
    }
];

gudhub.filter(items, filter_list);
```

Name|Type|Description
:---|:---|:---
items|`array`|*array of items which will be filtered*
filter_list|`array`|*array of filters which will be applied to items*

> Filter is not applied without search values.

**Filter** allows you to add several filters for the same item at the same time. In turn, the fields can have only one filter. But, as fields can contain multiple value, filters can search for a few ones.

As for filters JSON, all filters data  is contained in filters_list which in turn located in the certain item. For example:

```json
filters_list: [{
    "data_type": "text",
    "field_id": 603733,
    "search_type": "contain_or",
    "selected_search_option_variable": "Value",
    "valuesArray": ["field value",...],
},...]
```

As you can see, filters_list consists of the filter object. If there is no filter in item, list is empty.

Property|Type|Description
:---|:---|:---
filter_list|`array`|*array of object with data about filtering*
data_type|`string`|*contains the values type*
field_list|`number`|*contains ID of the field that is being filtered*
search_type|`string`|*contains the type of searching*
selected_search_option_variable|`string`|*allows to add environment value*
valuesArray|`array`|*contains all values, by which filtering is carried out*

## getFilteredItems

There is also a separate method that returns items with filters applied to them. This method accepts the same arrays as the filter method plus a few new arguments. **getFilteredItems** can be call in two ways:

- [Directly through Utils](#through-utils)
- [Through Lib](#through-lib)

They are not that different, but have some differences.

### Through Utils

Accepts all arguments separately.

```js
import GudHub from '@gudhub/core';
const gudhub = await new GudHub();
let items = [{
  "item_id": 2987986,
  "fields": [{
        "field_id": 645888,
        "field_value": "John",
      }],
  },
  {
  "item_id": 2987996,
  "fields": [{
        "field_id": 645888,
        "field_value": "Johana",
      }]
  }
];

let filter_list = [{
  "data_type": "text",
  "field_id": 645888,
  "search_type": "search",
  "selected_search_option_variable": "Value",
  "valuesArray": ["john"]
  }
];

let element_app_id = 2343552;

let app_id = 13453;

let item_id = 122345;

let field_group = [];

let filteredItems = await gudhub.utils.getFilteredItems(items, filters_list, element_app_id, app_id, item_id, field_group);
console.log(filteredItems);
```

Property Name|Type|Description
:---|:---|:---
items|`array`|*array of items for filtration*
filter_list|`array`|*array of filters which are applied to items*
element_app_id|`number`|*accepts ID of the application in which the item is configured*
app_id|`number`|*contains ID of the application whose items will be filtered*
item_id|`number`|*contains ID of item where the filter is located*
field_group|`number`|*accepts ID of the field by which items are grouped according to*

### Through Lib

Accepts some arguments as properties of an object named **options**.

```js
import GudHub from '@gudhub/core';
const gudhub = await new GudHub();

let items = [{
  "item_id": 2987986,
  "fields": [{
        "field_id": 645888,
        "field_value": "John",
      }],
  },
  {
  "item_id": 2987996,
  "fields": [{
        "field_id": 645888,
        "field_value": "Johana",
      }]
  }
];

let filter_list = [{
  "data_type": "text",
  "field_id": 645888,
  "search_type": "search",
  "selected_search_option_variable": "Value",
  "valuesArray": ["john"]
  }
];

let options = {
  element_app_id: "665778",
  app_id: "342323",
  item_id: "3424222",
  field_group: "34323"
};


let filteredItems = await gudhub.getFilteredItems(items, filters_list, options);
console.log(filteredItems);
```

Property Name|Type|Description
:---|:---|:---
items|`array`|*array of items*
filter_list|`array`|*array of filters which are applied to items*
options|`object`|*contains additional settings*
element_app_id|`number`|*ID of the application where the element is rendered (could be view container)*
app_id|`number`|*ID of the application where items will be filtered*
item_id|`number`|*contains ID of item where the filter is located*
field_group|`number`|*accepts ID of the field by which items are grouped according to*

## Search Types

The filter can accept multiple values for searching at the same time. For comfortable filtering you can choose on what basis the filter will select the fields. For example, you want to get all results that contain you value or on the contrary all results without your value. So, you can configure filter for such needs due to search_type. It has a large range of values:

- [contain_or](contain_filters.md)
- [contain_and](contain_filters.md)
- [not_contain_or](contain_filters.md)
- [not_contain_and](contain_filters.md)
- [equal_or](equal_filters.md)
- [equal_and](equal_filters.md)
- [not_equal_or](equal_filters.md)
- [not_equal_and](equal_filters.md)
- [phone_equal_or](phone_equal_or.md)
- [distance](distance.md)
- [search](search.md)
- [bigger](bigger_lower_filters.md)
- [lower](bigger_lower_filters.md)
- [range](range.md)
- [date_in](date_filters.md)
- [date_out](date_filters.md)
- [recurring_date](recurring_date.md)
- [value](value.md)

For convenience, we can divide some of them into groups:

1. Contains
2. Equals
3. Number filters
4. Date filters

The rest of filters will be described separately.

All details about them you can read in the next chapters.
