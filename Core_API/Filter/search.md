# Search

This type is a smart searching. It can find and filter fields even by invalid search values. Namely, by:

- the word without any letter
- one matching letter
- two matching letters without space
- any part of field value
- a random jumble of letters in which at least two letters are in the correct order

> Note two main rules for right searching:
>
>- right order of letters
>- a random word with more than three letters must have at least two letters in the correct order

Despite this, it has a simple structure:

```json
{
    "data_type": "text",
    "field_id": 625327,
    "search_type": "search",
    "selected_search_option_variable": "Value",
    "valuesArray": []
}
```

The following [examples](#example) clearly demonstrate the filtering results shown above.

Search values and results of using them in table:

F\S|John|Edvard Dow|Dow John|John D|johana|Jhn|E d|n|
:---|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:
John Dow|+|+|+|+|+|+|+|+
Johana|+|-|-|+|+|-|-|+
Edvard Dow|-|+|-|-|-|-|+|+
[ ]|-|-|-|-|-|-|-|-

>S - search values
>
>F - column of field values
>
>"+" - matched by filter
>
>"-" - not matched by filter

## Example

This is the table that will be filtered:

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": "John Dow"
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": "Johana"
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "Edvard Dow"
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

Result of filtering:

- Search value: *John*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": "John Dow"
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": "Johana"
    }]
}]
```

- Search value: *Edvard Dow*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": "John Dow"
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "Edvard Dow"
    }]
}]
```

- Search value: *Dow John*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": "John Dow"
    }]
}]
```

- Search value: *John D*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": "John Dow"
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": "Johana"
    }]
}]
```

- Search value: *Jhn*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": "John Dow"
    }]
}]
```

- Search value: *E d*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": "John Dow"
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "Edvard Dow"
    }]
}]
```

- Search value: *n*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": "John Dow"
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": "Johana"
    }]
}]
```
