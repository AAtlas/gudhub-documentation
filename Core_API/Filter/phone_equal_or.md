# Phone

This is one more special search type in the filter. It was created for phone numbers and can accept its any formats.
That means, it does not matter you enter country code, a few numbers with or without brackets, full phone number or the single digit, phone_equal_or will display all suitable field values.

Its model is the same as the other filters:

```json
{
    "data_type": "phone",
    "field_id": 625320,
    "search_type": "phone_equal_or",
    "selected_search_option_variable": "Value",
    "valuesArray": []
}
```

We will take four different phone numbers as an [example](#examples).

F\S|+999(999)999-9999|2345670000|234567|
:---|:---:|:---:|:---:
+9999 (999) 999-9999|+|-|-|-|-|+|-|-|-|-
+1234(567) 890-123|-|-|+|-|+|-|-|+|+|-
+12(234) 567-0000|-|+|+|-|+|-|+|-|+|+
(025) 520-0255|-|-|-|+|-|-|+|-|-|-

F\S|(025)|+12|9|55|9-0|5)52|45
:---|:---:|:---:|:---:|:---:|:---:|:---:|:---:
+9999 (999) 999-9999|-|-|+|-|-|-|-
+1234(567) 890-123|-|+|-|-|+|-|+
+12(234) 567-0000|-|+|-|-|-|-|+
(025) 520-0255|+|-|-|+|-|+|-

>S - search values
>
>F - column of field values
>
>"+" - matched by filter
>
>"-" - not matched by filter

## Examples

For clearly demonstration, the following table will be used as example:

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": "9999.9999999999"
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": "1234.567890123"
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "12.2345670000"
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "0255200255"
    }]
}]
```

Result of filtering by:

- full number: *+9999 (999) 999-9999*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": "9999.9999999999"
    }]
}]
```

- number without country code and signs: *2345670000*

```json
[{
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "12.2345670000"
    }]
}]
```

- part of number: *234567*

```json
[{
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": "1234.567890123"
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "12.2345670000"
    }]
}]
```

- part of number in brackets: *(025)*

```json
[{
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "0255200255"
    }]
}]
```

- country code: *+12*

```json
[{
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": "1234.567890123"
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "12.2345670000"
    }]
}]
```

- a single digit: *9*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": "9999.9999999999"
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": "1234.567890123"
    }]
}]
```

- last two digits: *55*

```json
[{
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "0255200255"
    }]
}]
```

- digits with a dash: *9-0*

```json
[{
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": "1234.567890123"
    }]
    
}]
```

- part of number with a single bracket in it: *4) 56*

```json
[{
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": "1234.567890123"
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "12.2345670000"
    }]
}]
```

- two digit from any part of number: *45*

```json
[{
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": "1234.567890123"
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "12.2345670000"
    }]
}]
```
