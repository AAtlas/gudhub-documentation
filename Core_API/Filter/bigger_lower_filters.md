# Bigger/Lower

There two special filters that works mostly with numbers.

1. [Bigger](#bigger)

2. [Lower](#lower)

Bigger and lower works like standard mathematical signs '>' and '<'. They're pretty easy to understand. But, also, they have their own subtleties.

> The first is that these filters do not allow fields with multiple values.

Others are common to all number values:

- numbers with many zeros ahead are interpreted with ignoring them: 00001 is 1, 00000.2 is 0.2, -000003 is -3.
- negative numbers are written without gaps
- decimal numbers are written with a dot

In code this filers looks like this:

```json
{
    "data_type": "number",
    "field_id": 625329,
    "search_type": "bigger",
    "selected_search_option_variable": "Value",
    "valuesArray": []
}
```

## Bigger

The first one filter choses numbers that are bigger then entered search value.

F\S|[10]|[2]|[0.2]|[0]|[-0.2]|[-2]|[-10]
:---|:---:|:---:|:---:|:---:|:---:|:---:|:---:
[ ]|-|-|-|-|+|+|+
[10]|-|+|+|+|+|+|+
[2]|-|-|+|+|+|+|+
[0.2]|-|-|-|+|+|+|+
[0]|-|-|-|-|+|+|+
[-0.2]|-|-|-|-|-|+|+
[-2]|-|-|-|-|-|-|+
[-10]|-|-|-|-|-|-|-
[1,2]|-|-|-|-|-|-|-

>S - search values
>
>F - column of field values
>
>"+" - matched by filter
>
>"-" - not matched by filter

You can see all tables appropriate each searching value [in the end of the chapter](#examples-bigger).

## Lower

Opposite the previous one this filter helps to display numbers smaller then the entered search value.

F\S|[10]|[2]|[0.2]|[0]|[-0.2]|[-2]|[-10]
:---|:---:|:---:|:---:|:---:|:---:|:---:|:---:
[ ]|+|+|+|-|-|-|-
[10]|-|-|-|-|-|-|-
[2]|+|-|-|-|-|-|-
[0.2]|+|+|-|-|-|-|-
[0]|+|+|+|-|-|-|-
[-0.2]|+|+|+|+|-|-|-
[-2]|+|+|+|+|+|-|-
[-10]|+|+|+|+|+|+|+|-
[1,2]|-|-|-|-|-|-|-

>S - search values
>
>F - column of field values
>
>"+" - matched by filter
>
>"-" - not matched by filter

You can see all tables appropriate each searching value [in the end of the chapter](#examples-lower).

## Examples

The following tables shows what results the program returns for current table:

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 10
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 2
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 0.2
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 0
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": -0.2
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": -2
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": -10
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": [ 1 , 2 ]
    }]
    },
    {
    "item_id": 2979073,
    "fields": [{
        "field_id": 629160,
        "field_value": [ ]
    }]
}]
```

Here are the list of examples of filtering results from tables in the chapter.

### Examples Bigger

- Search value: *[10]*

```json
[]
```

- Search value: *[2]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": "10"
    }]
}]
```

- Search value: *[0.2]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 10
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 2
    }]
}]
```

- Search value: *[0]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 10
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 2
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 0.2
    }]
}]
```

- Search value: *[-0.2]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 10
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 2
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 0.2
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 0
    }]
    },
    {
    "item_id": 2979073,
    "fields": [{
        "field_id": 629160,
        "field_value": [ ]
    }]
}]
```

- Search value: *[-2]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 10
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 2
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 0.2
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 0
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": -0.2
    }]
    },
    {
    "item_id": 2979073,
    "fields": [{
        "field_id": 629160,
        "field_value": [ ]
    }]
}]
```

- Search value: *[-10]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 10
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 2
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 0.2
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 0
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": -0.2
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": -2
    }]
    },
    {
    "item_id": 2979073,
    "fields": [{
        "field_id": 629160,
        "field_value": [ ]
    }]
}]
```

### Examples Lower

- Search value: *[10]*

```json
[{
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 2
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 0.2
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 0
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": -0.2
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": -2
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": -10
    }]
    },
    {
    "item_id": 2979073,
    "fields": [{
        "field_id": 629160,
        "field_value": [ ]
    }]
}]
```

- Search value: *[2]*

```json
[{
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 0.2
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 0
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": -0.2
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": -2
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": -10
    }]
    },
    {
    "item_id": 2979073,
    "fields": [{
        "field_id": 629160,
        "field_value": [ ]
    }]
}]
```

- Search value: *[0.2]*

```json
[{
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 0
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": -0.2
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": -2
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": -10
    }]
    },
    {
    "item_id": 2979073,
    "fields": [{
        "field_id": 629160,
        "field_value": [ ]
    }]
}]
```

- Search value: *[0]*

```json
[{
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": -0.2
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": -2
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": -10
    }]
}]
```

- Search value: *[-0.2]*

```json
[{
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": -2
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": -10
    }]
}]
```

- Search value: *[-2]*

```json
[{
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": -10
    }]
}]
```

- Search value: *[-10]*

```json
[]
```
