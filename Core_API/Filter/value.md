# Value

Value is the most universal search type. It is anywhere where is possible to apply filter. Also, it is pretty easy to understand **value**s work. It determines whether the field has a value or not.
This type has two values:

- Is Defined
- Is Not Defined

For all data types this filter is as follows:

```json
{
    "data_type": "text",
    "field_id": 625327,
    "search_type": "value",
    "selected_search_option_variable": "Value",
    "valuesArray": []
}
```

It is no matter which type the field is. So, we will use two field types text and number for [examples](#example).

F\S|Is Defined|Is Not Defined
:---|:---:|:---:
[ ]|-|+
[not empty]|+|-

>S - search values
>
>F - column of field values
>
>"+" - matched by filter
>
>"-" - not matched by filter

## Example

For example we have a table with filled and empty fields.

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    },
    {
        "field_id": 629163,
        "field_value": []
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    },
    {
        "field_id": 629163,
        "field_value": [2]
    }]
}]
```

Filter out text by value **Is Defined**:

- Text

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    },
    {
        "field_id": 629163,
        "field_value": []
    }]
}]
```

- Number

```json
[{
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    },
    {
        "field_id": 629163,
        "field_value": [2]
    }]
}]
```

Filter out text by value **Is Not Defined**:

- Text

```json
[{
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    },
    {
        "field_id": 629163,
        "field_value": [2]
    }]
}]
```

- Number

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    },
    {
        "field_id": 629163,
        "field_value": []
    }]
}]
```
