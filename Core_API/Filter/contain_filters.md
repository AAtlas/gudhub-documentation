# Contains

This group of the search types is used for filtering field value by their components. That means that you can enter a single word or even a part of a word to find and sort needed data.

There are 4 such filters:

- [contain_or](#containor)
- [contain_and](#containand)
- [not_contain_or](#notcontainor)
- [not_contain_and](#notcontainand)

All these filters are consist of:

```json
{
    "data_type": "text",
    "field_id": 625327,
    "search_type": "contain_or",
    "selected_search_option_variable": "Value",
    "valuesArray": []
}
```

Following are the tables of selection results for certain field value and array of values. They show whether the field value be selected and thereby displayed in the table. Namely, the plus sign means the particular values_array will be shown. As you can see, for each searching type we made two tables. The first shows results by the full word as the search value, the second shows results by their first letter.

## contain_or

This type looks for field values that contain at least one of the search values.  

F\S|[John,Tom]|[John,Bill]|[Bill,Kate]|[John]|[Bill]|[John,Tom,Bill]
:---|:---:|:---:|:---:|:---:|:---:|:---:
[John,Tom]|+|+|-|+|-|+
[John]|+|+|-|+|-|+
[ ]|-|-|-|-|-|-
[Jack]|-|-|-|-|-|-
[John,Tom,Bill]|+|+|+|+|-|+

F\S|[j,t]|[j,b]|[b,k]|[j]|[k]|[\]|[j,t,b]
:---|:---:|:---:|:---:|:---:|:---:|:---:|:---:
[John,Tom]|+|+|-|+|-|+|+
[John]|+|+|-|+|-|+|+
[ ]|-|-|-|-|-|+|-
[Jack]|+|+|+|+|+|+|+
[John,Tom,Bill]|+|+|+|+|-|+|+

>S - search values
>
>F - column of field values
>
>"+" - matched by filter
>
>"-" - not matched by filter

You can see all tables appropriate each searching value [in the end of the chapter](#examples-contains(or)).

## contain_and

This search type helps to find all data that contains all of entered search values.

F\S|[John,Tom]|[John,Bill]|[Bill,Kate]|[John]|[Bill]|[John,Tom,Bill]
:---|:---:|:---:|:---:|:---:|:---:|:---:
[John,Tom]|+|-|-|+|-|-
[John]|-|-|-|+|-|-
[ ]|-|-|-|-|-|-
[Jack]|-|-|-|-|-|-
[John,Tom,Bill]|+|+|+|+|-|+

F\S|[j,t]|[j,b]|[b,k]|[j]|[k]|[\]|[j,t,b]
:---|:---:|:---:|:---:|:---:|:---:|:---:|:---:
[John,Tom]|+|-|-|+|-|+|-
[John]|-|-|-|+|-|+|-
[ ]|-|-|-|-|-|+|-
[Jack]|-|-|-|+|+|+|-
[John,Tom,Bill]|+|+|-|+|-|+|+

>S - search values
>
>F - column of field values
>
>"+" - matched by filter
>
>"-" - not matched by filter

You can see all tables appropriate each searching value [in the end of the chapter](#examples-contains(and)).

## not_contain_or

This type gathers elements that do not have at least one of search value.

F\S|[John,Tom]|[John,Bill]|[Bill,Kate]|[John]|[Bill]|[John,Tom,Bill]
:---|:---:|:---:|:---:|:---:|:---:|:---:
[John,Tom]|-|+|+|-|+|+
[John]|+|+|+|-|+|+
[ ]|+|+|+|+|+|+
[Jack]|+|+|+|+|+|+
[John,Tom,Bill]|-|-|-|-|+|-

F\S|[j,t]|[j,b]|[b,k]|[j]|[k]|[\]|[j,t,b]
:---|:---:|:---:|:---:|:---:|:---:|:---:|:---:
[John,Tom]|-|+|+|-|+|+|+
[John]|+|+|+|-|+|+|+
[ ]|+|+|+|+|+|+|+
[Jack]|+|-|+|-|-|+|+
[John,Tom,Bill]|-|-|+|-|+|+|-

>S - search values
>
>F - column of field values
>
>"+" - matched by filter
>
>"-" - not matched by filter

You can see all tables appropriate each searching value [in the end of the chapter](#examples-not-contains(or)).

## not_contain_and

This type selects elements that do not have a single search value within them.

F\S|[John,Tom]|[John,Bill]|[Bill,Kate]|[John]|[Bill]|[John,Tom,Bill]
:---|:---:|:---:|:---:|:---:|:---:|:---:
[John,Tom]|-|-|+|-|+|-
[John]|-|-|+|-|+|-
[ ]|+|+|+|+|+|+
[Jack]|+|+|+|+|+|+
[John,Tom,Bill]|-|-|-|-|+|-

F\S|[j,t]|[j,b]|[b,k]|[j]|[k]|[\]|[j,t,b]
:---|:---:|:---:|:---:|:---:|:---:|:---:|:---:
[John,Tom]|-|-|+|-|+|+|-
[John]|-|-|+|-|+|+|-
[ ]|+|+|+|+|+|+|+
[Jack]|-|-|-|-|-|+|-
[John,Tom,Bill]|-|-|-|-|+|+|-

>S - search values
>
>F - column of field values
>
>"+" - matched by filter
>
>"-" - not matched by filter

You can see all tables appropriate each searching value [in the end of the chapter](#examples-not-contains(and)).

## Examples

Here are examples of filtering results from tables. As an example, this table will be used:

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

### Examples Contains(or)

- Search value: *[John,Tom]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
}]
```

- Search value: *[John,Bill]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
}]
```

- Search value: *[Bill,Kate]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
}]
```

- Search value: *[John]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
}]
```

- Search value: *[Bill]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
}]
```

- Search value: *[John,Tom,Bill]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
}]
```

- Search value: *[j,t]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
}]
```

- Search value: *[j,b]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
}]
```

- Search value: *[b,k]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
}]
```

- Search value: *[j]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
}]
```

- Search value: *[k]*

```json
[{
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
}]
```

- Search value: *[\]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

- Search value: *[j,t,b]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
}]
```

### Examples Contains(and)

- Search value: *[John,Tom]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
}]
```

- Search value: *[John,Bill]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
}]
```

- Search value: *[Bill,Kate]*

```json
[]
```

- Search value: *[John]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
}]
```

- Search value: *[Bill]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
}]
```

- Search value: *[John,Tom,Bill]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
}]
```

- Search value: *[j,t]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
}]
```

- Search value: *[j,b]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
}]
```

- Search value: *[b,k]*

```json
[]
```

- Search value: *[j]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
}]
```

- Search value: *[k]*

```json
[{
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
}]
```

- Search value: *[\]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

- Search value: *[j,t,b]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
}]
```

### Examples Not Contains(or)

- Search value: *[John,Tom]*

```json
[{
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
}]
```

- Search value: *[John,Bill]*

```json
[{
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

- Search value: *[Bill,Kate]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

- Search value: *[John]*

```json
[{
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

- Search value: *[Bill]*

```json
[{
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

- Search value: *[John,Tom,Bill]*

```json
[{
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

- Search value: *[j,t]*

```json
[{
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

- Search value: *[j,b]*

```json
[{
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

- Search value: *[b,k]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

- Search value: *[j]*

```json
[{
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

- Search value: *[k]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

- Search value: *[\]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

- Search value: *[j,t,b]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

### Examples Not Contains(and)

- Search value: *[John,Tom]*

```json
[{
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

- Search value: *[John,Bill]*

```json
[{
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

- Search value: *[Bill,Kate]*

```json
[{
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

- Search value: *[John]*

```json
[{
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

- Search value: *[Bill]*

```json
[{
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

- Search value: *[John,Tom,Bill]*

```json
[{
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

- Search value: *[j,t]*

```json
[{
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

- Search value: *[j,b]*

```json
[{
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

- Search value: *[b,k]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

- Search value: *[j]*

```json
[{
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

- Search value: *[k]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

- Search value: *[\]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom", "Bill"]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": ["John", "Tom"]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": "John"
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": "Jack"
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```

- Search value: *[j,t,b]*

```json
[{
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": ""
    }]
}]
```
