# Range

This is the type that allows to filter out within two values. The values are chosen by user. You can see this type in items like number or date. It is pretty easy to understand but there are nuances.

> The start value is included in the range, the last one is not.
>
>For example, in the date item, if we enter 2021-12-23 UTC as a start and 2021-12-25 UTC as a finish, will be displayed two dates 2021-12-23 UTC and 2021-12-24 UTC.

Its JSON is:

```json
{
    "data_type": "date",
    "field_id": 629163,
    "search_type": "range",
    "selected_search_option_variable": "Value",
    "valuesArray": []
}
```

Next you can see filtering by four search values. More details find in [Example](#example).

F\S|12-23 : 12-24|12-20 : 12-25|12-23 : 12-23| 12-25 : 12-20
:---|:---:|:---:|:---:|:---:
12-20|-|+|-|-
12-21|-|+|-|-
12-22|-|+|-|-
12-23|+|+|-|-
12-24|-|+|-|-
12-25|-|-|-|-

>S - search values in UTC format MM-DD : MM-DD
>
>F - column of field values in UTC format MM-DD
>
>"+" - matched by filter
>
>"-" - not matched by filter

## Example

For clearly demonstration the work of range, we will use the following table:

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639951237618
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640037659294
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640124009668
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640210419989
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640296819258
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640383230448
    }]
}]
```

Filtering out by:

- two days straight: *12-23 : 12-24*

```json
[{
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640210419989
    }]
}]
```

- several days: *12-20 : 12-25*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639951237618
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640037659294
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640124009668
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640210419989
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640296819258
    }]
}]
```

- the same day: *12-23 : 12-23*

```json
[]
```

- start from later date: *12-25 : 12-20*

```json
[]
```
