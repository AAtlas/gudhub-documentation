# Equal

This is the group of filters that determines fields with value equals to searching value:

- [equal_or](#equalor)
- [equal_and](#equaland)
- [not_equal_or](#notequalor)
- [not_equal_and](#notequaland)

They search a match even among multiple fields. They can be applied almost for all types of fields.

```json
{
    "data_type": "number",
    "field_id": 625329,
    "search_type": "equal_or",
    "selected_search_option_variable": "Value",
    "valuesArray": []
}
```

Following tables are the examples of selection results for certain fields and arrays of values. They show whether the field value be selected and displayed in the table.

## equal_or

This type allow to show all field that has  at least one of the values from the searching field.

|F \ S|[1,2]|[1,3]|[3,4]|[1]|[3]|[ ]|[1,2,3]
:---|:---:|:---:|:---:|:---:|:---:|:---:|:---:
[1,2]|+|+|-|+|-|+|+
[1,3]|+|+|+|+|+|+|+
[3,4]|-|+|+|-|+|+|+
[1]|+|+|-|+|-|+|+
[3]|-|+|+|-|+|+|+
[ ]|-|-|-|-|-|+|-
[6]|-|-|-|-|-|+|-
[1,2,3]|+|+|+|+|+|+|+

>S - search values
>
>F - column of field values
>
>"+" - matched by filter
>
>"-" - not matched by filter

You can see all tables appropriate each searching value [in the end of the chapter](#examples-equal(or)).

## equal_and

This type of search allows you to display a field whose values are equal to all the values you are looking for.

|F \ S|[1,2]|[1,3]|[3,4]|[1]|[3]|[ ]|[1,2,3]
:---|:---:|:---:|:---:|:---:|:---:|:---:|:---:
[1,2]|+|-|-|+|-|+|-
[1,3]|-|+|-|+|+|+|-
[3,4]|-|-|+|-|+|+|-
[1]|-|-|-|+|-|+|-
[3]|-|-|-|-|+|+|-
[ ]|-|-|-|-|-|+|-
[6]|-|-|-|-|-|+|-
[1,2,3]|+|+|-|+|+|+|+

>S - search values
>
>F - column of field values
>
>"+" - matched by filter
>
>"-" - not matched by filter

You can see all tables appropriate each searching value [in the end of the chapter](#examples-equal(and)).

## not_equal_or

This type allows to gather fields which do not have at least one of search values.

|F \ S|[1,2]|[1,3]|[3,4]|[1]|[3]|[ ]|[1,2,3]
:---|:---:|:---:|:---:|:---:|:---:|:---:|:---:
[1,2]|-|+|-|+|+|+|-
[1,3]|+|-|+|+|+|+|-
[3,4]|+|+|-|+|+|+|+
[1]|-|-|+|-|+|+|-
[3]|+|-|-|+|-|+|-
[ ]|+|+|+|+|+|+|+
[6]|+|+|+|+|+|+|+
[1,2,3]|+|+|+|+|+|+|-

>S - search values
>
>F - column of field values
>
>"+" - matched by filter
>
>"-" - not matched by filter

You can see all tables appropriate each searching value [in the end of the chapter](#examples-not-equal(or)).

## not_equal_and

This search type allow you to see all field without entered values.

|F \ S|[1,2]|[1,3]|[3,4]|[1]|[3]|[ ]|[1,2,3]
:---|:---:|:---:|:---:|:---:|:---:|:---:|:---:
[1,2]|-|-|+|-|+|+|-
[1,3]|-|-|-|-|-|+|-
[3,4]|+|-|-|+|-|+|-
[1]|-|-|+|-|+|+|-
[3]|+|-|-|+|-|+|-
[ ]|+|+|+|+|+|+|+
[6]|+|+|+|+|+|+|+
[1,2,3]|-|-|-|-|-|+|-

>S - search values
>
>F - column of field values
>
>"+" - matched by filter
>
>"-" - not matched by filter

You can see all tables appropriate each searching value [in the end of the chapter](#examples-not-equal(and)).

## Examples

It does not matter, what type the values will have. For example we will take a table of numbers:

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,3]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": [3,4]
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": [1]
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": [3]
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": []
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": [6]
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2,3]
    }]
}]
```

### Examples Equal(or)

- Search value: *[1,2]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,3]
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": [1]
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2,3]
    }]
}]
```

- Search value: *[1,3]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,3]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": [3,4]
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": [1]
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": [3]
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2,3]
    }]
}]
```

- Search value: *[3,4]*

```json
[{
    
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,3]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": [3,4]
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": [3]
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2,3]
    }]
}]
```

- Search value: *[1]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,3]
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": [1]
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2,3]
    }]
}]
```

- Search value: *[3]*

```json
[{
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,3]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": [3,4]
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": [3]
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2,3]
    }]
}]
```

- Search value: *[ ]*

```json
[[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,3]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": [3,4]
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": [1]
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": [3]
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": []
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": [6]
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2,3]
    }]
}]
```

- Search value: *[1,2,3]*

```json
[[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,3]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": [3,4]
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": [1]
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": [3]
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2,3]
    }]
}]
```

### Examples Equal(and)

- Search value: *[1,2]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2]
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2,3]
    }]
}]
```

- Search value: *[1,3]*

```json
[{
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,3]
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2,3]
    }]
}]
```

- Search value: *[3,4]*

```json
[{
    
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": [3,4]
    }]
}]
```

- Search value: *[1]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,3]
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": [1]
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2,3]
    }]
}]
```

- Search value: *[3]*

```json
[{
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,3]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": [3,4]
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": [3]
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2,3]
    }]
}]
```

- Search value: *[ ]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,3]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": [3,4]
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": [1]
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": [3]
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": []
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": [6]
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2,3]
    }]
}]
```

- Search value: *[1,2,3]*

```json
[{
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2,3]
    }]
}]
```

### Examples Not Equal(or)

- Search value: *[1,2]*

```json
[{
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,3]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": [3,4]
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": [3]
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": []
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": [6]
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2,3]
    }]
}]
```

- Search value: *[1,3]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": [3,4]
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": []
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": [6]
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2,3]
    }]
}]
```

- Search value: *[3,4]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,3]
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": [1]
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": []
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": [6]
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2,3]
    }]
}]
```

- Search value: *[1]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,3]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": [3,4]
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": [3]
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": []
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": [6]
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2,3]
    }]
}]
```

- Search value: *[3]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,3]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": [3,4]
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": [1]
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": []
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": [6]
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2,3]
    }]
}]
```

- Search value: *[ ]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,3]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": [3,4]
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": [1]
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": [3]
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": []
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": [6]
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2,3]
    }]
}]
```

- Search value: *[1,2,3]*

```json
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": [3,4]
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": []
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": [6]
    }]
}]
```

### Examples Not Equal(and)

- Search value: *[1,2]*

```json
[{
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": [3,4]
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": [3]
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": []
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": [6]
    }]
}]
```

- Search value: *[1,3]*

```json
[{
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": []
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": [6]
    }]
}]
```

- Search value: *[3,4]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2]
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": [1]
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": []
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": [6]
    }]
}]
```

- Search value: *[1]*

```json
[{
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": [3,4]
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": [3]
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": []
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": [6]
    }]
}]
```

- Search value: *[3]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2]
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": [1]
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": []
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": [6]
    }]
}]
```

- Search value: *[ ]*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2]
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,3]
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": [3,4]
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": [1]
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": [3]
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": []
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": [6]
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": [1,2,3]
    }]
}]
```

- Search value: *[1,2,3]*

```json
[{
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": []
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": [6]
    }]
}]
```
