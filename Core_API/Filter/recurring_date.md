# Recurring Date

This is one of filters that works with date data in UTC format. It determines whether the date corresponds to the selected time period, regardless of the year. This is useful for identifying dates that recur from year to year.

There are three periods available:

- Today
- This week
- This month

Its data model look like this:

```json
{
    "data_type": "date",
    "field_id": 680175,
    "search_type": "recurring_date",
    "selected_search_option_variable": "Value",
    "valuesArray": ["day"]
}
```

The following table shows how the filter works. The full results of the example filtering are presented [below](#example).

F\S|Today|This week|This month
:---|:---|:---|:---
2022-06-02|+|+|+
2027-06-02|+|+|+
2016-06-02|+|+|+
2018-06-03|-|+|+
1991-06-14|-|-|+
2022-07-01|-|-|-

>S - search values
>
>F - column of field values
>
>"+" - matched by filter
>
>"-" - not matched by filter

## Example

This is an example of a table that will be filtered out:

```json
[{
    // 2022-06-02
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1654117210924
    }]
    },
    {
    // 2027-06-02
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1811883627581
    }]
    },
    {
    // 2016-06-02
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1464814832140
    }]
    },
    {
    // 2018-06-03
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1527973216455
    }]
    },
    {
    // 1991-06-14
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": 676846853525
    }]
    },
    {
    // 2022-07-01
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": 1656622855020
    }]
}]
```

The filtering results, which were shown in the table of filter performance:

- Search value: *Today*

```json
[{
    // 2022-06-02
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1654117210924
    }]
    },
    {
    // 2027-06-02
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1811883627581
    }]
    },
    {
    // 2016-06-02
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1464814832140
    }]
}]
```

- Search value: *This week*

```json
[{
    // 2022-06-02
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1654117210924
    }]
    },
    {
    // 2027-06-02
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1811883627581
    }]
    },
    {
    // 2016-06-02
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1464814832140
    }]
    },
    {
    // 2018-06-03
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1527973216455
    }]
}]
```

- Search value: *This month*

```json
[{
    // 2022-06-02
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1654117210924
    }]
    },
    {
    // 2027-06-02
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1811883627581
    }]
    },
    {
    // 2016-06-02
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1464814832140
    }]
    },
    {
    // 2018-06-03
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1527973216455
    }]
    },
    {
    // 1991-06-14
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": 676846853525
    }]
}]
```
