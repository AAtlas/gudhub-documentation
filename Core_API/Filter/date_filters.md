# Date

This group of filters operate data in UTC format. They are used in such items as Date and Calender.

- [date_in](#date-in)
- [date_out](#date-out)

Each of them sorts the data according to specific time periods.

- Today
- Tomorrow
- Yesterday
- Next and past seven days
- Days of the week
- Current weeks
- Next weeks
- Past weeks
- Current months
- Next months
- Past months
- Current years
- Next years
- Past years

>The user's current time is taken as today.

Both filter models are as follows:

```json
{
    "data_type": "date",
    "field_id": 629163,
    "search_type": "date_in",
    "selected_search_option_variable": "Value",
    "valuesArray": []
}
```

## date_in

This filter selects all dates that fall within certain period.

F\S|Today|Tomorrow|Yesterday|Next 7 days|Past 7 days
:---|:---:|:---:|:---:|:---:|:---:
2021-12-21|+|-|-|+|-
2021-12-22|-|+|-|+|-
2021-12-20|-|-|+|-|+
2022-01-01|-|-|-|-|-
2021-12-16|-|-|-|-|+
2021-11-18|-|-|-|-|-
2015-03-25|-|-|-|-|-
2025-07-21|-|-|-|-|-

F\S|Sunday|Monday|Tuesday|Wednesday|Thursday
:---|:---:|:---:|:---:|:---:|:---:
2021-12-21|-|-|+|-|-
2021-12-22|-|-|-|+|-
2021-12-20|-|+|-|-|-
2022-01-01|-|-|-|-|-
2021-12-16|-|-|-|-|+
2020-11-18|-|-|-|+|-
2015-03-25|-|-|-|+|-
2025-07-21|-|+|-|-|-

F\S|Friday|Saturday|Current week|Next week|Past week
:---|:---:|:---:|:---:|:---:|:---:
2021-12-21|-|-|+|-|-
2021-12-22|-|-|+|-|-
2021-12-20|-|-|+|-|-
2022-01-01|-|+|-|+|-
2021-12-16|-|-|-|-|+
2020-11-18|-|-|-|-|-
2015-03-25|-|-|-|-|-
2025-07-21|-|-|-|-|-

F\S|Current month|Next month|Past month|Current year|Next year|Past year
:---|:---:|:---:|:---:|:---:|:---:|:---:
2021-12-21|+|-|-|+|-|-
2021-12-22|+|-|-|+|-|-
2021-12-20|+|-|-|+|-|-
2022-01-01|-|+|-|-|+|-
2021-12-16|+|-|-|+|-|-
2020-11-18|-|-|-|-|-|+
2015-03-25|-|-|-|-|-|-
2025-07-21|-|-|-|-|-|-

>S - search values
>
>F - column of field values
>
>"+" - matched by filter
>
>"-" - not matched by filter

You can see all tables appropriate each searching value [in the end of the chapter](#examples-date-in).

## date_out

In turn, this filter filters out all dates except those that fall within the period entered.

F\S|Today|Tomorrow|Yesterday|Next 7 days|Past 7 days
:---|:---:|:---:|:---:|:---:|:---:
2021-12-21|-|+|+|-|+
2021-12-22|+|-|+|-|+
2021-12-20|+|+|-|+|-
2022-01-01|+|+|+|+|+
2021-12-16|+|+|+|+|-
2020-11-18|+|+|+|+|+
2015-03-25|+|+|+|+|+
2025-07-21|+|+|+|+|+

F\S|Sunday|Monday|Tuesday|Wednesday|Thursday
:---|:---:|:---:|:---:|:---:|:---:
2021-12-21|+|+|-|+|+
2021-12-22|+|+|+|-|+
2021-12-20|+|-|+|+|+
2022-01-01|+|+|+|+|+
2021-12-16|+|+|+|+|-
2020-11-18|+|+|+|-|+
2015-03-25|+|+|+|-|+
2025-07-21|+|-|+|+|+

F\S|Friday|Saturday|Current week|Next week|Past week
:---|:---:|:---:|:---:|:---:|:---:
2021-12-21|+|+|-|+|+
2021-12-22|+|+|-|+|+
2021-12-20|+|+|-|+|+
2022-01-01|+|-|+|-|+
2021-12-16|+|+|+|+|-
2020-11-18|+|+|+|+|+
2015-03-25|+|+|+|+|+
2025-07-21|+|+|+|+|+

F\S|Current month|Next month|Past month|Current year|Next year|Past year
:---|:---:|:---:|:---:|:---:|:---:|:---:
2021-12-21|-|+|+|-|+|+
2021-12-22|-|+|+|-|+|+
2021-12-20|-|+|+|-|+|+
2022-01-01|+|-|+|+|-|+
2021-12-16|-|+|+|-|+|+
2020-11-18|+|+|+|+|+|-
2015-03-25|+|+|+|+|+|+
2025-07-21|+|+|+|+|+|+

>S - search values
>
>F - column of field values
>
>"+" - matched by filter
>
>"-" - not matched by filter

You can see all tables appropriate each searching value [in the end of the chapter](#examples-date-out).

## Examples

Here are an example table and results of filtering. For examples, we will take 2021-12-21 UTC as today's date.

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640037659294
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640124009668
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639951237618
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640988019989
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639605619258
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": 1605650446630
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": 1427234459933
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": 1753045230448
    }]
}]
```

### Examples Date In

- Search value: *Today*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640037659294
    }]
}]
```

- Search value: *Tomorrow*

```json
[{
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640124009668
    }]
}]
```

- Search value: *Yesterday*

```json
[{
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639951237618
    }]
}]
```

- Search value: *Next 7 days*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640037659294
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640124009668
    }]
}]
```

- Search value: *Past 7 days*

```json
[{
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639951237618
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639605619258
    }]
}]
```

- Search value: *Sunday*

```json
[]
```

- Search value: *Monday*

```json
[{
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639951237618
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": 1753045230448
    }]
}]
```

- Search value: *Tuesday*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640037659294
    }]
}]
```

- Search value: *Wednesday*

```json
{
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640124009668
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": 1753045230448
    }]
}
```

- Search value: *Thursday*

```json
[{
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639605619258
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": 1605650446630
    }]
}]
```

- Search value: *Friday*

```json
[]
```

- Search value: *Saturday*

```json
[{
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640988019989
    }]
}]
```

- Search value: *Current week*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640037659294
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640124009668
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639951237618
    }]
}]
```

- Search value: *Next week*

```json
[{
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640988019989
    }]
}]
```

- Search value: *Past week*

```json
[{
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639605619258
    }]
}]
```

- Search value: *Current month*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640037659294
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640124009668
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639951237618
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639605619258
    }]
}]
```

- Search value: *Next month*

```json
[{
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640988019989
    }]
}]
```

- Search value: *Past month*

```json
[]
```

- Search value: *Current year*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640037659294
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640124009668
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639951237618
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639605619258
    }]
}]
```

- Search value: *Next year*

```json
[{
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640988019989
    }]
}]
```

- Search value: *Past year*

```json
[{
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": 1605650446630
    }]
}]
```

### Examples Date Out

- Search value: *Today*

```json
[{
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640124009668
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639951237618
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640988019989
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639605619258
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": 1605650446630
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": 1427234459933
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": 1753045230448
    }]
}]
```

- Search value: *Tomorrow*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640037659294
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639951237618
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640988019989
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639605619258
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": 1605650446630
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": 1427234459933
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": 1753045230448
    }]
}]
```

- Search value: *Yesterday*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640037659294
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640124009668
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640988019989
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639605619258
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": 1605650446630
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": 1427234459933
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": 1753045230448
    }]
}]
```

- Search value: *Next 7 days*

```json
[{
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639951237618
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640988019989
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639605619258
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": 1605650446630
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": 1427234459933
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": 1753045230448
    }]
}]
```

- Search value: *Past 7 days*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640037659294
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640124009668
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640988019989
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": 1605650446630
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": 1427234459933
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": 1753045230448
    }]
}]
```

- Search value: *Sunday*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640037659294
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640124009668
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639951237618
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640988019989
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639605619258
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": 1605650446630
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": 1427234459933
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": 1753045230448
    }]
}]
```

- Search value: *Monday*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640037659294
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640124009668
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640988019989
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639605619258
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": 1605650446630
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": 1753045230448
    }]
}]
```

- Search value: *Tuesday*

```json
[{
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640124009668
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639951237618
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640988019989
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639605619258
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": 1605650446630
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": 1427234459933
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": 1753045230448
    }]
}]
```

- Search value: *Wednesday*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640037659294
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639951237618
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640988019989
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639605619258
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": 1427234459933
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": 1753045230448
    }]
}]
```

- Search value: *Thursday*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640037659294
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640124009668
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639951237618
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640988019989
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": 1427234459933
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": 1753045230448
    }]
}]
```

- Search value: *Friday*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640037659294
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640124009668
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639951237618
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640988019989
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639605619258
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": 1605650446630
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": 1427234459933
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": 1753045230448
    }]
}]
```

- Search value: *Saturday*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640037659294
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640124009668
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639951237618
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639605619258
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": 1605650446630
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": 1427234459933
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": 1753045230448
    }]
}]
```

- Search value: *Current week*

```json
[{
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640988019989
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639605619258
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": 1605650446630
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": 1427234459933
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": 1753045230448
    }]
}]
```

- Search value: *Next week*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640037659294
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640124009668
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639951237618
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639605619258
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": 1605650446630
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": 1427234459933
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": 1753045230448
    }]
}]
```

- Search value: *Past week*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640037659294
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640124009668
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639951237618
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640988019989
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": 1605650446630
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": 1427234459933
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": 1753045230448
    }]
}]
```

- Search value: *Current month*

```json
[{
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640988019989
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": 1605650446630
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": 1427234459933
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": 1753045230448
    }]
}]
```

- Search value: *Next month*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640037659294
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640124009668
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639951237618
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639605619258
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": 1605650446630
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": 1427234459933
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": 1753045230448
    }]
}]
```

- Search value: *Past month*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640037659294
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640124009668
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639951237618
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640988019989
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": 1605650446630
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": 1427234459933
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": 1753045230448
    }]
}]
```

- Search value: *Current year*

```json
[{
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640988019989
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639605619258
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": 1605650446630
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": 1427234459933
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": 1753045230448
    }]
}]
```

- Search value: *Next year*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640037659294
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640124009668
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639951237618
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639605619258
    }]
    },
    {
    "item_id": 2979070,
    "fields": [{
        "field_id": 629160,
        "field_value": 1605650446630
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": 1427234459933
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": 1753045230448
    }]
}]
```

- Search value: *Past year*

```json
[{
    "item_id": 2979065,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640037659294
    }]
    },
    {
    "item_id": 2979066,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640124009668
    }]
    },
    {
    "item_id": 2979067,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639951237618
    }]
    },
    {
    "item_id": 2979068,
    "fields": [{
        "field_id": 629160,
        "field_value": 1640988019989
    }]
    },
    {
    "item_id": 2979069,
    "fields": [{
        "field_id": 629160,
        "field_value": 1639605619258
    }]
    },
    {
    "item_id": 2979071,
    "fields": [{
        "field_id": 629160,
        "field_value": 1427234459933
    }]
    },
    {
    "item_id": 2979072,
    "fields": [{
        "field_id": 629160,
        "field_value": 1753045230448
    }]
}]
```
