# Go To Link

**Go To Link** is a gh-element that allows you to create a drop-down list of customized links that can be used to find a specific item or page. It allows the user to find and open a specific item by a specific request.

![Using go to link element](gh-elements/go-to-link-work.gif "Go to link")

This is a useful tool for navigating applications. In combination with various filters, such as [Table Filter](./table_filter.md) and [Filter Advanced](./filter_advanced.md), you can create a complete user-centered application.

## Functional Characteristics

The current element is mostly used for searching the data by entering the certain titles. It can be either an auxiliary tool for organizing your data or a full-fledged search field for customers.

Due to the ability to add different groups of parameters from different applications, you can create a variety of search requests, provided that they are located on the same host.

Also, due to the flexible settings, the user can open different [views](../Understanding_Gudhub/Application_Data_Structure/views_list.md) according to their requests. That is, this element can be configured in such a way that, for example, the search query returns not only products, but also categories of these products. This system makes it easy to create different catalogs or online stores.

## Element Options

**Go To Link** element has lots of different settings. Each of setting group has its own unique features.

### Field Settings

The first group of settings is consists of standard [field settings](../Understanding_Gudhub/GH_Element/setting_overview.md) and additional element settings.

![Settings of go to link field](gh-elements/go-to-link-field-settings.jpg "Go to link field settings")

Name|Description
:---|:---
App ID as parameter|allows to use application ID as link parameter; [details...](#app-id-as-parameter)
Item ID as parameter|allows to use application ID as link parameter; [details...](#item-id-as-parameter)
Select first matched on Enter|allows to use the selection by pressing Enter; [details...](#select-first-matched-on-enter)
Show thumbnail|allows to show thumbnails in drop-down list; [details...](#show-thumbnail)
Use autofocus|allows user to automatically focus the cursor on this field after the page reloads; [details...](#use-autofocus)
<!-- tooltip-start app_id_as_parameter -->
#### **App ID As Parameter**

The current setting is designed to configure the links to the local GudHub pages. To be more precise, this allows you to configure the links to different applications. If this option is enabled, the [destination application ID](#application) will be used as the link parameter. It is also used to link to items.
<!-- tooltip-end app_id_as_parameter -->
<!-- tooltip-start item_id_as_parameter -->
#### **Item ID As Parameter**

The current setting determines whether the item ID is used as a link parameter or not. The item ID is needed to generate the link to the item of the [certain application](#app-id-as-parameter).
<!-- tooltip-end item_id_as_parameter -->
The items that will be available are determined by the [Dropdown Settings](#dropdown-settings).
<!-- tooltip-start select_first_matched_on_enter -->
#### **Select First Matched On Enter**

This setting is designed to make it easier to use the current gh-element. When it is enabled, the user can press the Enter key to select the option that matches their query. Namely, it allows them to select the first matching item.
<!-- tooltip-end select_first_matched_on_enter -->
![Using the select first matched on enter](gh-elements/go-to-link-select-first-matched.gif "Select first matched on enter")

This eliminates the need for the user to move the mouse over the desired option and click on it. All available options are stored in the item's drop-down list, which can be customized in the [Dropdown Settings](#dropdown-settings).
<!-- tooltip-start show_thumbnail -->
#### **Show Thumbnail**

**Show Thumbnail** is a customizing setting. It allows you to display the thumbnails for options in the drop-down list.
<!-- tooltip-end show_thumbnail -->
Namely, if this setting is enabled, the images from the field selected in [Thumbnail](#thumbnail) will be displayed in each variant of the drop-down list. Each option has a different image depending on the associated element.
<!-- tooltip-start autofocus -->
#### **Use Autofocus**

The last setting enables autofocus on the current gh-element. Autofocus means that user have no need to click on the field to enter the searching request. After the page reloads, the user can immediately enter a request.
<!-- tooltip-end autofocus -->
### URI Settings and Custom URI Parameters

There are two group of settings in the second block:

- **URI Settings** are mandatory setting of the element. They configure the main part of links of the drop-down list.

- **Custom URI Parameters** allows getting URI parameters for automatic link generation.

>*User URI parameters* are used if the host is not GudHub.

![URI settings of go to link element](gh-elements/go-to-link-uri-settings.jpg "Go to link URI settings")

Name|Description
:---|:---
Host|allows to enter the host for links; [details...](#host)
Use current host|allows to use the current host; [details...](#use-current-host)
Path|allows to enter the path for the link; [details...](#path)
URI params as|allows to select the type of the parameter of the URI; [details...](#uri-params-as)
Par. name|allows to enter name to the parameter; [details...](#parameter-name)
Application|allows to select the source application; [details...](#application)
Par. value|allows to select the source field for parameters; [details...](#parameter-value)
Edit|allows to edit or delete the parameter; [details...](#edit)

#### **Host**

The first setting of the block allows you to specify the host of the site to which the links lead. You can enter any host yourself or use the [current GudHub host](#use-current-host).
<!-- tooltip-start use_current_host -->
To configure different host types you need to use different settings. **For the GudHub host**, you need to enable [Use Current Host](#use-current-host) and additionally use [App ID as a parameter](#app-id-as-parameter) and [Item ID as a parameter](#item-id-as-parameter). For other hosts, use the [Custom URI parameters](#parameter-name).

#### **Use Current Host**

To use the current host, i.e. the GudHub host, you do not need to enter it manually. Just use the current setting and the [Host](#host) will be automatically filled with:

    https://gudhub.com

<!-- tooltip-end use_current_host -->
>When it is enabled the [Host](#host) is not available for editing.
<!-- tooltip-start path -->
#### **Path**

Each URI contains the path to the source directory. So, you need to enter the desired path in the current settings.

>The path saved here will be used to create all links in this element.
<!-- tooltip-end path -->
As you already understood, the same path is used for all URIs.
<!-- tooltip-start url_params_as -->
#### **URI Params As**

The current setting allows you to determine what type the URI parameters will be used in the options links. After selecting a type, all parameters in the link will be generated in the corresponding type.

- Query params
- Route params
<!-- tooltip-end url_params_as -->
>Only the **Route parameters** are used to generate links to **GudHub apps and items**.

For all other services, you can use both types, depending on your needs.

#### **Parameter Name**

This is the first setting of the **Custom URI parameters** group of settings. That allows you to set the name of the custom parameter.

>The entered name is also used in the URI with query parameters.

#### **Application**

Data for generating URIs should be stored in your applications. Then the data from certain fields will be used as parameters for the links. Here you have to select the application for the current parameter.

This means that parameters can have different source applications. The [following parameter setting](#parameter-value) of the current parameter will get the list of fields of this application.

#### **Parameter Value**

The **Parameter Value** retrieves data from the field selected in this setting. You must select different fields for each parameter. All parameters will be converted to the required [type](#uri-params-as) during URI generation.

In the case of **query param**, the value will be used in conjunction with the [parameter name](#parameter-name). Otherwise, only this value is used.

#### **Edit**

The last block setting allows you to edit or delete a parameter. Each parameter has separate buttons for these settings. This allows you to customize each option.

### Dropdown Settings

This group of settings is designed to configure the drop-down list of the element. Here you can configure groups of options displayed in the drop-down list.

>If you use [GudHUb host](#use-current-host), some of the current settings will also be used to generate links.

![Settings of go to link dropdown](gh-elements/go-to-link-dropdown-settings.jpg "Go to link dropdown settings")

Name|Description
:---|:---
Application|allows to select the source application; [details...](#option-application)
View|allows to select the view that will be opened; [details...](#view)
Thumbnail|allows to select the field for thumbnail; [details...](#thumbnail)
Title|allows you to select a name for an option from the drop-down list; [details...](#title)
Subtitle|allows you to select a subtitle for an option from the drop-down list; [details...](#subtitle)
Edit|allows to edit or delete the option; [details...](#edit-option)

#### **Option Application**

The first setting of the current block allows you to select the source application for the options in the dropdown. The following settings receive data from the application selected here. All matched items from this app will be displayed in the drop-own list.

>The drop-down list can display items from different applications.

So, you can add many options of different applications. All their items will be displayed in the current drop-down list.

#### **View**

The current setting allows you to select the [view](../Understanding_Gudhub/Application_Data_Structure/views_list.md) that will be opened after choosing a particular option. The list of views is retrieved from the [selected app](#option-application).

>This setting is mandatory, but the view will only be used when a **[GudHub host](#use-current-host) is used**.

#### **Thumbnail**

If the [Show Thumbnail](#show-thumbnail) option is enabled, small images are displayed in the drop-down list of the current gh-element. To display an image there, you need to select the field from which the images will be pulled. So, here you need to select the [Image](./image.md) field from the [previously selected application](#option-application).

>Although this setting is mandatory, images from it will be displayed only when the [Show Thumbnail](#show-thumbnail) option is enabled.

#### **Title**

The names for drop-down lists are derived from specific fields. These fields must be selected in the current setting. Fields can be completely different [gh-elements](../Understanding_Gudhub/GH_Element/gh_elements_overview.md). The values of all selected fields will be displayed separated by commas.

#### **Subtitle**

**Subtitle** is a text that displayed under the [Title](#title). As in the previous setting, you can select multiple fields here. Their values will be displayed separated by commas.

#### **Edit Option**

This column contains two buttons. One of them allows you to edit and the other allows you to delete an option.

## Element Style

Such as all other elements, this one has a [standard set of style settings](../Understanding_Gudhub/GH_Element/setting_overview.md) and its own [types of interpretation](#interpretation). But it has one difference. Namely, the additional option in [general settings](#general-settings).

![Style of go to link element](gh-elements/go-to-link-element-style.jpg "Go to link element style")

### General Settings

Every additional style setting is in the current group:

![General settings of go to link style](gh-elements/go-to-link-general-settings.jpg "Go to link general style settings")

Name|Description
:---|:---
Input Width|allows to enter the width of the field

#### **Input Width**

This is the only additional setting of the gh-element style. It allows you to set the width of the search field for the current gh-element.

## Filtration

This element cannot be filtered out.

## Interpretation

This element has only one type of interpretation:

![Types of go to link interpretations](gh-elements/go-to-link-interpretation-types.jpg "Go to link interpretation types")

### Default

This is an interpretation type that allow to enter the search query and follow the link.

## Value Format

The current element does not contain any value.

## Data Model

The data model consists of element settings:

```json
{
    "data_model": {
        "app_id_as_first_param": 1,
        "host": "https://development.gudhub.com:443",
        "interpretation": [],
        "item_id_as_second_param": 1,
        "path": "/act/open_item/",
        "select_first_matched_option": 1,
        "selected_apps": [{
            "app_id": "29008",
            "subtitle_field_ids": "682240",
            "thumbnail_field_id": "682241",
            "title_field_ids": "682241",
            "view_id": "1536589"
        }],
        "show_thumb": 1,
        "uri_params": [{
            "app_id": "29204",
            "appropriate_field": "684002",
            "parameter": "Param Name"
        }],
        "uri_type": "query",
        "use_current_host": 1
    }
}
```

Name|Type|Description
:---|:---|:---
app_id_as_first_param|`boolean`|*shows whether the ID of the selected application is used as a parameter*
host|`string`|*contains host that is used*
interpretation|`array`|*contains all element interpretation types*
item_id_as_second_param|`boolean`|*shows whether ID of the destination item is used as a parameter or not*
path|`string`|*contains path that is used*
select_first_matched_option|`boolean`|*shows whether the user can select the first matched option by pressing Enter*
selected_apps|`array`|*contains all options of the drop-down list*
app_id|`string`|*contains ID of the source application*
subtitle_field_ids|`string`|*contains ID of source field for subtitles*
thumbnail_field_id|`string`|*contains ID of source field for thumbnail images*
title_field_ids|`string`|*contains ID of source field for titles*
view_id|`string`|*contains ID of the view that will be opened after clicking on the option*
show_thumb|`boolean`|*shows whether the thumbnails will be shown*
uri_params|`array`|*contains all URI params and their settings*
app_id|`string`|*contains ID of the source application*
appropriate_field|`string`|*contains ID of the field from which params will be taken*
parameter|`string`|*contains the parameter name entered by the user*
uri_type|`string`|*contains the selected type of the URI*
use_current_host|`boolean`|*shows whether the GudHub host is used or not*
