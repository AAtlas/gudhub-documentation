# Element Customizer

**Element Customizer** is a gh-element that allows each user to customize the settings of a specific element. These settings are active **within a current item**. So, user can create different versions of one field. **Element Customizer** provides access to all element settings except [Field Name and Name Space](../Understanding_Gudhub/GH_Element/setting_overview.md).

![Options of element customizer](gh-elements/element-customizer-settings.gif "Customizer options")

>It creates alternative settings that are applied to the element. The original settings of the element are not changed.

## Functional Characteristics

Usually user can configure any field in application only he/she has Admin or Owner permissions that allows to configure the full application. In turn, the *Customizer* allows **users with any permissions** to customize a **specific element**, but only **within one item**.

![Work of element customizer](gh-elements/element-customizer-work.gif "Work with different elements")

This element can be used with absolutely any gh-element, even with itself. Once customized, it can be freely used in different items.

## Element Options

All the settings that this element has are the pair of [standard options](../Understanding_Gudhub/GH_Element/setting_overview.md) and setting that binds it to any field.

![Settings of element customizer field](gh-elements/element-customizer-field-settings.jpg "Customizer field settings")

Name|Description
:---|:---
Field Id|allows to select the field that user will be able to customize
<!-- tooltip-start field_id -->
### Field ID

The setting of the **Element Customizer** is responsible for the connection between the current element and any other element. This means that you can select any field from the current program, so that any user can then customize the available options for the selected item.

>The field can be selected only from the application in which the **Element Customizer** is located.
<!-- tooltip-end field_id -->
In other words, the current element gets access to the element settings by its **field ID**.

## Element Style

The current gh element has a fairly simple settings, including style options. Here, it contains only a [standard set of style settings](../Understanding_Gudhub/GH_Element/setting_overview.md) and a couple of [interpretation types](#interpretation).

![Style of element customizer](gh-elements/element-customizer-style.jpg "Element customizer style")

## Filtration

This element cannot be filtered out.

## Interpretation

Element Customizer has two different interpretation types.

![Types of element customizer interpretation types](gh-elements/element-customizer-interpretation-types.jpg "Customizer interpretation types")

### Element Customizer Type

The first type is the single interpretation that allows to use current element. This is a pencil-shaped button that, when clicked, opens the settings of the corresponding element.

### Default

The second type of interpretation displays only the name of the current gh-element.

## Value Format

This element does not contain a field value.

## Data Model

This gh-element has a small data model with two properties.

```json
{
    "data_model": {
        "field_id": "7043426",
        "interpretation": []
    }
}
```

Name|Type|Description
:---|:---|:---
field_id|`string`|*contains ID of the field that will be customized*
interpretation|`array`|*contains all element interpretation types*
