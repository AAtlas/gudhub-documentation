# String Join

**String Join** is a gh-element that allows you to join values of different fields and the entered value into one string.

## Functional Characteristics

This element is used to concatenate selected fields. For example, when the user enters his or her first and last name separately in different fields, this element will join them into one line. The String join element is very similar to the [text element](text.md), so it can be used in the same ways.

## Value Format

The string join does not contain any value until the [Persistent Value](#persistent-value) option is disabled.

```json
{
    "field_value": "Hello World"
}
```

## Element Options

String join has two groups of settings. One of them configures its value and the second one configures the element.

### Field Settings

This is the main settings group. It allows you to customize the field and contains standard and advanced options.

![Settings of string join field](gh-elements/string-join-field-settings.jpg "String join field settings")

Name|Description
:---|:---
Persistent value|allows to save the generated value; [details...](#persistent-value)
One time generation|allows you to generate a value only once; [details...](#one-time-generation)
<!-- tooltip-start persistent_value -->
#### **Persistent Value**

As long as this function is not enabled, the join string does not save the generated value. So, it allows to save value in the field.
<!-- tooltip-end persistent_value -->
<!-- tooltip-start one_time_generation -->
#### **One Time Generation**

This function allows you to fix the first generated value and display it in the application. This value can only be updated with the [Update Button](#update-button).
<!-- tooltip-end one_time_generation -->
### Expression Generator

The last group allows you to make an expression that join strings.

![Generator of the string join value](gh-elements/string-join-generator.jpg "String join generator")

Name|Description
:---|:---
Field|allows to select the field from which the value will be selected; [details...](#field)
Word|allows to enter the text in settings; [details...](#word)
ID|allows to select the certain ID; [details...](#id)
+|allows to concatenate all elements of expression; [details...](#concatenation)
Clear|allows to clear the full expression; [details...](#clear)
<|allows you to select only one sign of the expression; [details...](#clear-element)

#### **Field**

This button allows to take the field whose value will be used in the expression. It also allows you to configure which part and how many characters of a given value are used.

![Configuring the source field](gh-elements/string-join-field.jpg "String join field")

#### **Word**

This button opens a popup window in which you can enter your own value to be used in the expression.

![Entering window](gh-elements/string-join-word.jpg "String join word")

#### **ID**

This is the button that allows to select one of four values.

![ID configuring popup window](gh-elements/string-join-id.jpg "String join ID")

- Index number of item
- Current year
- Current month
- Current day of month

#### **Concatenation**

The concatenation button allows to enter the plus sign and connect all the parts of the expression.

#### **Clear**

This button allows to clear the full expression.

#### **Clear Element**

This button allows to clear one element of sign. It has '<' as a name.

## Element Style

The string join element has quite standard set of [style settings](../Understanding_Gudhub/GH_Element/setting_overview.md) and only one [interpretation type](#interpretation). The only additional setting is located in general group of settings.

![Style of string join element](gh-elements/string-join-element-style.jpg "String join element style")

### General Settings

Almost all style settings are set by default instead of the only one.

![General settings of string join style](gh-elements/string-join-general-settings.jpg "String join general style settings")

Name|Description
:---|:---
Update button|allows to add the updating button

#### **Update Button**

This is the button that allows you to update the value when [One Time Generation](#one-time-generation) is enabled.

## Data Model

The data model of this element contains all its settings, including configuration options.

```json
{
    "data_model": {
        "arguments": [{
            "word": "Hello World"
        },
        {
            "app_id": "28752",
            "field_id": "679043",
            "field_settings": {
                "count": 2,
                "display": "first_characters",
                "name": "first characters"
            }
        },
        {
            "type": "Current day of month"
        }],
        "expression": "WORD()+FIELD_STRING()+ITEM_INDEX()",
        "interpretation": [],
        "is_generate_once": 0,
        "is_persistent_value": 0,
        "tooltip": {
            "filter_valuesArray": ["string_join", "string_join"],
            "settings_file_id": "30233"
        }
    }
}
```

Name|Type|Description
:---|:---|:---
arguments|`array`|*contains all the connection generation settings*
word|`string`|*contains the entered text*
app_id|`string`|*contains ID of the source application*
field_id|`string`|*contains ID of the source field*
field_settings|`object`|*contains additional string settings*
count|`number`|*contains number of sign that will be displayed for joining*
display|`string`|*shows which part of the selected field value*
name|`string`|*contains the name of the selected part*
type|`string`|*contains type of ID*
expression|`string`|*contains the final expression by which the value will be joined*
interpretation|`array`|*contains all interpretation types and their settings*
is_generate_once|`boolean`|*show whether the One Time Generation is used or not*
is_persistent_value|`boolean`|*shows whether the generated value will be saved or not*
tooltip|`object`|*contains tooltip settings*
filter_valuesArray|`array`|*contains all value filters*
settings_file_id|`string`|*contains ID of the file with the tooltip text*

## Filtration

This element can be filtered using filters that compare strings:

- [Contains(or)](../Core_API/Filter/contain_filters.md)
- [Contains(and)](../Core_API/Filter/contain_filters.md)
- [Not contains(or)](../Core_API/Filter/contain_filters.md)
- [Not contains(and)](../Core_API/Filter/contain_filters.md)
- [Equals(or)](../Core_API/Filter/equal_filters.md)
- [Equals(and)](../Core_API/Filter/equal_filters.md)
- [Not equals(or)](../Core_API/Filter/equal_filters.md)
- [Not equals(and)](../Core_API/Filter/equal_filters.md)
- [Value](../Core_API/Filter/value.md)

## Interpretation

The current element has only two interpretation types.

![Types of string join interpretation](gh-elements/string-join-interpretation-types.jpg "String join interpretation types")

### Default

The default interpretation allows to display the result of the join.

### Value

The last interpretation displays the raw value of the field.
