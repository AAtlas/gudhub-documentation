# Markdown Viewer

**Markdown Viewer** is a gh-element that allows user to store the markdown links and see the their preview. That is, the markdown file to which you paste the link in the input field will be displayed in the element already formatted.

![Using the markdown viewer](gh-elements/markdown-viewer-work.gif "Markdown Viewer")

So, as mentioned, here you need to enter a link to the file. This should be a link to some kind of repository.

>Importantly, the link must be to a **raw file**. This can be seen in the link itself.

**Images in markdown files** are also worth mentioning. They will be displayed only if the file contains their **full path** and they are **located in the same repository as the file**.

## Functional Characteristics

The direct purpose of this element is to display files in the application, namely their contents. That is, this gh element performs two actions: it stores files and displays content. This allows you to use it for both development and applications. In the first case, data can be pulled into your project from any application. And in the second case, you can create full-fledged documentation inside the **GudHub** app.

## Element Options

This element has no special settings. There are only two [standard field settings](../Understanding_Gudhub/GH_Element/setting_overview.md).

![Settings of markdown viewer field](gh-elements/markdown-viewer-field-settings.jpg "Markdown viewer field settings")

## Element Style

As for the viewing style, you can adjust only the appearance  of the text and alignment in container using the [standard style options](../Understanding_Gudhub/GH_Element/setting_overview.md). You can also choose one of its [interpretation types](#interpretation).

![Style of markdown viewer element](gh-elements/markdown-viewer-element-style.jpg "Markdown viewer style")

## Filtration

The markdown viewer cannot be filtered out.

## Interpretation

This element has two types of interpretation.

![Types of markdown viewer interpretation](gh-elements/markdown-viewer-interpretation-types.jpg "Markdown viewer interpretation types")

### Default

The first type allows to enter the link to the markdown file. This file appears formatted in the area below.

### Value

The second type displays only the existed value of the element. It not allows to enter new value.

## Value Format

The current gh-element contains a link to the markdown file as a value.

```json
{
    field_value: "https://example.org/raw/master/Gh_Elements/markdown_viewer.md"
}
```

## Data Model

The data model of the current item does not contain any properties other than the interpretation array.

```json
{
    "data_model": {
        "interpretation": []
    }
}
```

Name|Type|Description
:---|:---|:---
interpretation|`array`|*contains all element interpretations*
