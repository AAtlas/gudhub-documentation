# Task Element

**Task** is a gh-element that allows you to configure the automatically executed processes. It is quite similar to the [trigger element](./trigger.md). That is because both of them are based on the automation. But the difference between them is that **Tasks** processes are executed on the server, regardless of whether the page is open in the browser. This allows you to create repeatable processes.

So, this element consists of a run button, an edit button, a run button, and a status field.

![Element appearance](gh-elements/task-element.jpg "Task element")

The first option is responsible for the repeatable processes. When the switch is on, automation will repeat according to the schedule configured in the **Task** options.

![The option for repeated automation](gh-elements/task-run-switcher.gif "Task switcher")

The button next to the switcher is an automation button. The pencil-shaped button allows the user to open the [automation settings](#automation-settings).

![Opening of the automation settings](gh-elements/task-pencil-button.gif "Operation of the pencil button")

After that button the **run button** is located. This is a button that allows the user to run a pre-configured automation process only once. The button has a specific button name - *RUN*, but after clicking on it, the name changes to *STOP* and remains so until the process is completed.

![Work of the task run button](gh-elements/task-run-button.gif "Run button")

And the last is the status field. As the name implies, it displays the current state of the process.

## Automation Settings

The task element has its own automation settings tab. It mostly consists of the [standard options](../Understanding_Gudhub/Automation/automation_settings.md).

But there is also an additional setting in the upper left corner. It is a [cron picker](./cron_picker.md) that allows you to schedule customized automation.

![Task automation](gh-elements/task-automation-settings.jpg "Automation Settings")

>This means that with this option, the user can set the periods of time when the automation will work.

The current element also has a [unique default node](../Automation_Nodes/task_node.md).It receives data from the current item and transmits it to the automation chain.

## Functional Characteristics

The current element is designed to create the automated processes that will be processed on GudHub server. This allows you to execute the configured process even if the browser is closed. Thus, the user can freely configure the automation chain and run it on the server without worrying that the process will be interrupted.

The current gh element can be used for **one-time** or **recurring processes**, depending on how the user uses the element. Basically, both of these methods can have the same automation chain. To start the automation once, the user only needs to click the **Run** button.

On the other hand, if the user needs to run the automation, for example, every day, then you first need to **set up a repeat schedule**. It can be configured using the [cron picker](./cron_picker.md) in the element [automation settings](#automation-settings). After setting up and saving the schedule, the user only needs to turn on the **Run switch**.

>The **switch should be turned on as long as you need the customized features to work**.

## Element Options

The settings of the current gh element consist of only one block of options.

### Field Settings

The main settings for this element are stored on another tab and are described above. So, in the **Field Settings**, there are only [two standard options](../Understanding_Gudhub/GH_Element/setting_overview.md) called **Field Name** and **Name Space**.

![Settings of task field](gh-elements/task-field-settings.jpg "Task field settings")

## Element Style

The style settings of the task element have not a big influence on the element appearance. But still the standard style options and its interpretation types are available.

![Style of task element](gh-elements/task-element-style.jpg "Task element style")

## Filtration

The current element cannot be filtered out.

## Interpretation

In addition to the standard style options, the task element has different types of interpretation. There are three of them in total:

![Types of task element interpretation](gh-elements/task-interpretation-types.jpg "Task element types")

### Default

This is the default interpretation type that displays the switcher, setting button, run button, and a status field.

### Icon

The second type of interpretation displays only an icon.

### Button

The last type of interpretation consists of a button only.

## Value Format

The current element does not contain any values.

## Data Model

The data model of this element is quite small:

```json
{
    "data_model": {
        "interpretation": []
    }
}
```

Name|Type|Description
:---|:---|:---
interpretation|`array`|*contains all element interpretation types*
