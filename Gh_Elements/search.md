# Search

**Search** is a gh-element that allows to

## Functional Characteristics

## Value Format

The search element contains no value

## Element Options

### Field Settings

![Settings of search field](gh-elements/search-field-settings.jpg "Search field settings")

Name|Description
:---|:---
Filter behavior|
Filtered App|
Item action|
Root|
Item action|

#### **Filter Behavior**

#### **Filtered App**

#### **Item Action In Dropdown**

#### **Root**

#### **Item Action For Following Location**

### Show/Hide Fields

![Show/hide fields of search element](gh-elements/search-show-hide-fields.jpg "Search show/hide fields")

Name|Description
:---|:---
Column|contains all fields names of selected application; [details...](#column)
Show|shows whether the field will be shown in dropdown; [details...](#show)

#### **Column**

This function displays the names of all gh-elements of the application. It ensures the work of the 

#### **Show**

This function allows you to configure the values of which fields will be searched.

## Element Style

The style of the current gh-element can be customized using the [standard set of settings](../Understanding_Gudhub/GH_Element/setting_overview.md). But it has one invariable appearance, which is set by its [type of interpretation](#interpretation).

![Style of search element](gh-elements/search-element-style.jpg "Search element style")

## Data Model

The search data model consists of different properties. Almost all of them are the values of element settings.

```json
{
    "data_model": {
        "dropdown_mode_app": "27904",
        "interpretation": [],
        "mode": "2",
        "table_id_bind_to": "678842",
        "table_settings": {
            "action": "follow_location",
            "actions_settings": {
                "follow_location": {
                    "root": "",
                    "selected_target": "_self"
                },
                "open_item": {}
            },
            "columns_to_view": [{
                "field_id": 648752,
                "show": 1
            }]
        }
    }
}
```

Name|Type|Description
:---|:---|:---
dropdown_mode_app|`string`|*contains ID of the filtered application*
interpretation|`array`|*contains all element interpretation types*
mode|`string`|
table_id_bind_to|`string`|
table_settings|`object`|*contains all table settings*
action|`string`|*contains the selected action of the dropdown*
actions_settings|`object`|*contains action settings*
follow_location|`object`|
selected_target|`string`|
open_item|`object`|
columns_to_view|`array`|
field_id|`number`|
show|`boolean`|

## Filtration

This is one of elements which cannot be filtered out.

## Interpretation

The current gh-element has only one type of interpretation. It displays element as a search field.

![Types of search interpretation](gh-elements/search-interpretation-types.jpg "Search interpretation types")
