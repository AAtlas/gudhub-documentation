# Text Editor

**Text Editor** is a gh-element that allows you to enter, edit, and display a large amount of text in application. This element creates a file and stores the entered text in it.

## Functional Characteristics

This element is usually used for any notes. Unlike the Text element, it accepts large amounts of text and has several editing functions.

## Value Format

Despite the way we enter text, the editor contains the ID of the file with that text.

```json
{
    "field_value": "930976"
}
```

In GudHub the large sets of data are contained as files and are added to the file_list.

### Text Area

Text editor has a special tool called *Text Area*.  It is designed for easy input and display of text data. Text Area has two types of view:

- for displaying data

    ![Displaying a text area with data](gh-elements/text-area-displaying.jpg "Text area displaying")

- for entering data

    ![Writing view of text area](gh-elements/text-editor-description.jpg "Text area")

The second one has a few extra functions. The first one is a drop-down list where user can select the size of font. The second one makes the font bold. And the last one resets the font settings.

## Element Options

This element has only a few options in field settings.

### Field Settings

Besides standard Name Space and Field Name, text editor has only one field option.

![Settings of text editor field](gh-elements/text-editor-field-settings.jpg "Text editor field settings")

Name|Description
:---|:---
Plain text|allows to turn off the HTML tags

#### **Plain Text**

The single unique option of text editor element turns off HTML tag in the text area. That is, when it is enabled, the user cannot change the displaying of the text using the buttons, namely, to change font size or make it bold.

## Element Style

As well as most of previous elements, text editor has standard style options and a few interpretation types. Read about them in [Setting Overview](../Understanding_Gudhub/GH_Element/setting_overview.md) and [Interpretation](#interpretation).

![Style of text editor element](gh-elements/text-editor-element-style.jpg "Text editor element style")

## Data Model

This is one of gh-elements with the most simple data model:

```json
{
    "data_model":{
        "interpretation": [],
        "plain_text": 0
    }
}
```

Name|Type|Description
:---|:---|:---
interpretation|`array`|*contains all editor interpretations*
plain_text|`boolean`|*shows whether plain text is used*

## Filtration

Since text editor can contains pretty big values, it can be filtered out only by [Value](../Core_API/Filter/value.md).

## Interpretation

As well as text element, this editor has two interpretations, editable and uneditable.

![Types of text editor interpretation](gh-elements/text-editor-interpretation-types.jpg "Text editor interpretation types")

### Input With Name

This interpretation type allows to update text value in a description and display it in the application.

### Default

This one does not allows to update value. It only display the icon.
