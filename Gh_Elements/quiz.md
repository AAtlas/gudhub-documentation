# Quiz

**Quiz** is a gh-element that allows to create different quizzes using automation nodes. Its automation configures not its settings. It has a pencil-like button that opens a *special automation tab*.

## Functional Characteristics

This element is designed to make a different types of quiz. For example, the call center operator usually have to ask the same things each time. Due to Quiz you can create dialogue draft. Walking along it, the operator can easily dialogue and record customer data without missing anything.

## Value Format

The current element contains ID of the JSON document with all its nodes settings.

```json
{
    "field_value": "62ce8b72900876f93163f4cd"
}
```

## Element Options

The Quiz element have three groups of settings. The last two of them are do not mandatory for gh-element work.

### Field Settings

The element field settings consists of only [standard settings](../Understanding_Gudhub/GH_Element/setting_overview.md).

![Settings of quiz field](gh-elements/quiz-field-settings.jpg "Quiz field settings")

### Default Value Settings

The second group of settings allows to configure the default items.

![Settings of quiz default value](gh-elements/quiz-default-value-settings.jpg "Quiz default value settings")

Name|Description
:---|:---
Application|allows to select the source application; [details...](#application)
View name|allows to select the view of items; [details...](#default-view-name)
<!-- tooltip-start application -->
#### **Application**

This is the application from which the view for the element will be taken.
<!-- tooltip-end application -->
<!-- tooltip-start view_name -->
#### **Default View Name**

This is the default view of the items of the application from which the data will be taken.
<!-- tooltip-end view_name -->
### Cases Settings

The last group of settings allows to configure view cases.

![Settings of quiz cases](gh-elements/quiz-cases-settings.jpg "Quiz cases settings")

Name|Description
:---|:---
View name|allows to select the view of the application items; [details...](#view-name)
Filter|allows to filter items of the selected application; [details...](#filter)
Edit|allows to edit or delete the case; [details...](#edit)

#### **View Name**

This is the alternative view of items of the [selected application](#application).

#### **Filter**

Allows you to add filters that determine which elements will be used in the element.

#### **Edit**

This is the column that contains the edit and delete buttons for each option.

## Quiz Automation

The **Quiz** element is mostly configures using automation. Because of this, it contains two special nodes that are only available in Quiz.

### Quiz Node

**Quiz Node** is the start node. It initializes the start of the *Quiz* work. Also, because of this, certain items can be transmitted to the next nodes. Read more about it in [Quiz Node](../Automation_Nodes/quiz_node.md).

![Special element node called Quiz Node](gh-elements/quiz-node.jpg "Quiz node")

### Quiz Form

**Quiz Form** is the main branching node.It displays the message and responses that lead to the different following nodes. Read more about it in [Quiz Form](../Automation_Nodes/quiz_form.md).

![Special element node called Quiz Form](gh-elements/quiz-form.jpg "Quiz form")

## Element Style

Despite its unusual appearance, the **Quiz** has no special style settings. Read about these settings in [Settings Overview](../Understanding_Gudhub/GH_Element/setting_overview.md) and about element interpretations [below](#interpretation).

![Style of quiz element](gh-elements/quiz-element-style.jpg "Quiz element style")

## Data Model

```json
{
    "data_model": {
        "app_id": "29008",
        "interpretation": [],
        "option": [{
            "filters_list": [],
            "view_id": "1536589"
        }],
        "view_id": "1536589"
    }
}
```

Name|Type|Description
:---|:---|:---
app_id|`string`|*contains ID of the source application*
interpretation|`array`|*contains all interpretation types*
option|`array`|*contains all cases settings*
filters_list|`array`|*contains all filters of the case*
view_id|`string`|*contains ID of the selected view of the case*
view_id|`string`|*contains ID of the view of app items*

## Filtration

The current element cannot be filtered out by its value.

## Interpretation

This element has three interpretation types:

![Types of quiz interpretation](gh-elements/quiz-interpretation-types.jpg "Quiz interpretation types")

### Default

The default interpretation displays the generated quiz. Namely, it display the message and buttons with answers under it.

### Icon

Due to this type, the element is displayed as an icon.

### Value

The last interpretation type displays the value of the field.
