# JSON Constructor Element

**JSON Constructor** is a gh-element that allows user to create a scheme by which the JSON object will be generated. It provides all the tools and settings for comfortable creation and customization of the scheme. But in general, **JSON Constructor** is a button that opens the constructor.

![Using the JSON constructor](gh-elements/json-constructor-work.gif "JSON constructor")

To use the constructor, user have to click on the element button. After that the constructor will be opened on the full screen. There, the user can create or edit a JSON schema depending on the [Particulary Editable](#particulary-editable) setting.

>The constructor and all its settings are described in detail in a [separate section of the documentation](../Utils/JSON_Conctructor/overview.md).

## Functional Characteristics

The main purpose of the current gh-element is to generate JSON based on data from your applications. It also provides tools to users to create the scheme for that JSON. The use of the current element is mostly related to development. That is, you can connect **JSON Constructor** to your web project, for example. Then it allows you to pull data from your applications into the project. It can be used in a similar way to set up [automation](../Understanding_Gudhub/Automation/automation_overview.md). Moreover, this element is also used as a setting in one of the automation nodes called [JSON Schema](../Automation_Nodes/json_scheme_node.md).

## Element Options

This element has only one group of settings called Field Settings. There two standard options and an extra one.

![Settings of JSON constructor field](gh-elements/json-constructor-field-settings.jpg "JSON constructor field settings")

Name|Description
:---|:---
Color|allows to select the button color; [details...](#color)
Saving|allows to configure the saving of the created scheme; [details...](#saving)
<!-- tooltip-start color -->
### **Color**

The first setting of the element allows you to customize it. Since **JSON Constructor** is a button, its color can be changed. The current setting does just that.
<!-- tooltip-end color -->
>The default color is **#0893d2**.

This setting works in the same way as the [Color](./color.md) element. That is, this setting makes it possible to choose absolutely any color for the **constructor**.
<!-- tooltip-start saving -->
### **Saving**

This is the setting that allows users to save their schemes. Specifically, saving the [JSON scheme](../Utils/JSON_Conctructor/scheme.md) depends on whether the current setting is enabled.
<!-- tooltip-end saving -->
So, when this setting is enabled, the created [JSON scheme](../Utils/JSON_Conctructor/scheme.md) will be saved on the server. Otherwise, the unsaved scheme will be deleted after the page reloads.

## Element Style

JSON constructor element has not a lot of extra options. There are only one in General Settings. All others are the standard settings which are described in [Setting Overview](../Understanding_Gudhub/GH_Element/setting_overview.md). Also, you can read about element interpretation types [below](#interpretation).

![Style of JSON constructor element](gh-elements/json-constructor-element-style.jpg "JSON constructor element style")

### General Settings

The constructor has one additional style option, which is in the general settings.

![General settings of JSON constructor style](gh-elements/json-constructor-general-settings.jpg "JSON constructor general settings")

Name|Description
:---|:---
Particulary editable|allows to make scheme in editor uneditable

#### **Particulary Editable**

This setting allows you to choose how the user can edit schemes. That is, depending on this setting, the user may have certain restrictions on editing the JSON scheme. So, the value of the **Particulary Editable** means:

- **False** - user can **create their own schemes and edit them**. In other words, the block with the schema entities is accessible.
​
    ![Scheme editor when Particulary editable is disable](gh-elements/scheme-editor-editable.jpg "Editable scheme editor")
​
- **True** - user can **only edit existing scheme**. That is, the user cannot add or remove any entries from the scheme.
​
    ![Scheme editor when Particulary editable is enable](gh-elements/scheme-editor-uneditable.jpg "Uneditable scheme editor")

## Filtration

Since this element does not have field value, it also cannot be filtered out.

## Interpretation

This element has only two interpretation types.

![Types of JSON constructor interpretation](gh-elements/json-constructor-interpretation-types.jpg "JSON constructor interpretation types")

### Editor

This is an interpretation type that allows to display the element as a button. Its action is to open the scheme editor.

### Default

Due to this type the element will be displayed as icon that has no action.

## Value Format

JSON constructor does not have any field value.

## Data Model

There are only a few properties in its data model.

```json
{
    "data_model": {
        "color": "#0893d2",
        "interpretation": [],
        "save": 1,
        "scheme": []
    }
}
```

Name|Type|Description
:---|:---|:---
color|`string`|*contains the hex code of button color*
interpretation|`array`|*contains all element interpretations*
save|`boolean`|*shows whether the created scheme is saved in GudHub or not*
scheme|`array`|*contains the scheme that is contained in the editor*
