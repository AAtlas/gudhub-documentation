# Duration

**Duration** is a gh-element that allows to set some time value. When the user clicks on the field, a [Time Picker](#time-picker) window opens. There the user can enter their value or select it.

![Use the duration element](gh-elements/duration-element.gif "Duration")

It is an element that is much more useful together with other elements such as [Table](./table.md), [Schedule](./sheduling.md), [Date](./date.md), [Smart Input](./smart_input.md), and others, or when used in [automation chains](../Understanding_Gudhub/Automation/automation_overview.md) than on its own.

## Time Picker

This is the pop-up window that allows users set the value of the field. It consists of two fields for entering hours and minutes and two buttons.

>When the [Show Seconds](#show-seconds) option is enabled, three input fields are displayed.

![Time picker of duration](gh-elements/duration-time-picker.jpg "Duration time picker")

So, the input fields are designed for entering different types of time values. That is, there are separate input fields for hours, minutes, and seconds. At the same time, each of them has separate buttons that allow you to adjust the value by one for hours and seconds and by 5 units for minutes.

![Using the time picker](gh-elements/duration-time-picker-work.gif "Time picker options")

There only two buttons in the **Time Picker**:

- **Clear** - allows user to clear the field
- **Close** - allows the user to close the picker while saving the value

All these options allow the user to work comfortably with the current element.

## Functional Characteristics

This gh-element has several different types of use. This element can be used to save any time or duration of any events. For example, it can be the length of a movie in the user's movie list or the duration of events in their schedule.

>The **Duration** element also allows you to configure some of others gh-element. This includes a [table](./table.md) and a [schedule](./sheduling.md).

In addition, like other gh-elements that store values, it can be used in [automation processes](../Understanding_Gudhub/Automation/automation_overview.md). For example, use it with the [Date](./date.md) and [Smart Input](./smart_input.md) elements to track the duration of tasks. You can configure them so that when user clicks on the **Smart Input button**, the start and end dates of the task are stored in the **Date element**. Then the [automation](../Understanding_Gudhub/Automation/automation_settings.md) will calculate the difference between the start and end time of the task, which will then be stored in the **Duration** element.

## Element Options

Duration has two type of settings. Despite that, it has not a big number of options.

### Field Settings

Duration field settings only contains standard options: Field Name and Name Space.

![Settings of duration element](gh-elements/duration-field-settings.jpg "Duration field settings")

Name|Description
:---|:---
Show seconds|allows to enter the seconds in the duration field
<!-- tooltip-start show_seconds -->
#### **Show seconds**

The **Duration** element usually accept and displays only hour and minute values. But this option adds an additional field to [time picker](#time-picker) if enabled. That allows user to enter seconds to its value. Accordingly, the seconds will be displayed in the element.
<!-- tooltip-end show_seconds -->
![Using the seconds in duration element](gh-elements/duration-show-seconds.gif "Show seconds")

This allows you to configure how precisely you need to record the time.

### Default Value Settings

As you may have gathered, you can set a default value for duration element. These settings will help you.

![Settings of duration default value](gh-elements/duration-default-value-settings.jpg "Duration default settings")

Name|Description
:---|:---
Use default value|allows to use default value; [details...](#use-default-value)
Default value|allows to set a default value; [details...](#default-value)
<!-- tooltip-start use_default_value -->
#### **Use Default Value**

The first setting of this block allows you to determine whether the default value will be available or not. This is a switch, so if it is enabled, the default value will be used when creating an item. In turn, the value can be entered in the [next setting](#default-value), which appears if you enable the **Use default values** option.
<!-- tooltip-end use_default_value -->
<!-- tooltip-start default_value -->
#### **Default value**

This is a setting that allows you to enter the default value for the current field. This means that the duration field will be filled with this value if it is empty when the item is created.

>Regardless of whether the default value is entered or not, it will not be used if [Use Default Value](#use-default-value) is disabled.
<!-- tooltip-end default_value -->

![Use the default value](gh-elements/duration-default-value.gif "Default value")

In addition, this option is not displayed unless [Use Default Value](#default-value) is enabled.

## Element Style

The style of duration element can be configured with the help of standard options about that you can read in [Setting Overview](../Understanding_Gudhub/GH_Element/setting_overview.md). Also, read about interpretation [below](#interpretation).

![Style of duration element](gh-elements/duration-element-style.jpg "Duration element  style")

## Filtration

The duration element can be filtered out by filters what compare full values:

- [Equals](../Core_API/Filter/equal_filters.md)
- [Not equals](../Core_API/Filter/equal_filters.md)
- [Is bigger than](../Core_API/Filter/bigger_lower_filters.md)
- [Is lower than](../Core_API/Filter/bigger_lower_filters.md)
- [Value](../Core_API/Filter/value.md)

## Interpretation

There are only two interpretations for this element.

![Types of duration interpretation](gh-elements/duration-interpretation-types.jpg "Duration interpretation types")

### Default

This one allows to update duration value with the help of time picker which appears after clicking on the field.

### Plain Text

The interpretation allows to display the set value and does not update it.

### Value

This type allows you to display an uninterpreted value of the element.

## Value Format

The duration value is the set time in milliseconds. In application it is saved as a string type data.

```json
{
    "field_value": "900000"
}
```

User can enter the value with the help of Time Picker.

## Data Model

The data model of this element contains only values of default options and interpretations, so it is quite simple.

```json
{
    "data_model":{
        "default_value": "any default value",
        "interpretation": [],
        "use_default_value": false
    }
}
```

Name|Type|Description
:---|:---|:---
default_value|`string`|*contains default value*
interpretation|`array`|*contains all duration interpretations*
use_default_value|`boolean`|*shows whether the default value is used*
