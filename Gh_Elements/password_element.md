# Password Element

**Password** is a gh-element that allows the user to enter any password in a hidden manner. This field can also be filled in automatically using built-in browser functions.

![Using the password](gh-elements/password-work.gif "Password element")

When user enters any text here it is automatically hidden. There is a button that allows the user to see the text entered. In both cases, the entered password can be edited.

## Functional Characteristics

As the name suggests, the current element is used to save the password. This also allows you to use the entered password in the future. This means that you can set up [automation](../Understanding_Gudhub/Automation/automation_overview.md) in combination with this element and, for example, [Email](./email.md) to create an authorization for your app.

## Element Options

The only options of the password element that you can configure are the standard Field Name and Name Space.

![Settings of password field](gh-elements/password-field-settings.jpg "Password field settings")

## Element Style

The only unique settings of the password style lies in [interpretation types](#interpretation). All others are a set of standard settings and are described in a [separate article](../Understanding_Gudhub/GH_Element/setting_overview.md).

![Style of password element](gh-elements/password-element-style.jpg "Password element style")

## Filtration

This element cannot be filtered out.

## Interpretation

The password can be displayed in two interpretations.

![Types of password interpretation](gh-elements/password-interpretation-types.jpg "Password interpretation types")

### Password interpretation

This interpretation type allows to enter and display the password. Also, it has a button to hide the value.

### Default

This is the interpretation type that displays the element as an icon.

## Value Format

This element value is an entered password.

```json
{
    "field_value": "userpassword"
}
```

## Data Model

The password element does not have special properties in its data model.

```json
{
    "data_model": {
        "interpretation": []
    }
}
```

Name|Type|Description
:---|:---|:---
interpretation|`array`|*contains all element interpretation types*
