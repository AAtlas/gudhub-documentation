# App Constructor

**App Constructor** is a gh-element that allows users to create a new applications. After clicking this button, the creation window opens. In general, the creation of an application is divided into 6 steps:

- **Type App Name** - at the first stage user can enter the name of the future application.

    ![Entering the app name](gh-elements/app-constructor-type-app-name.gif "Type App Name")

- **Select Background Color** - allows user to select the background color style of the app icon.

    ![Selecting color of the background](gh-elements/app-constructor-select-background-color.gif "Select Background Color")

- **Select Icon** allows the user to choose an image for the app icon. It can be one of the GudHub icons or any emoji.

    ![Selecting app icon](gh-elements/app-constructor-select-icon.gif "Select Icon")

- **Select Icon Color** allows the user to choose the color of the previously selected icon. the color can be changed only for GudHub icons.

    ![Selecting a color of app icon](gh-elements/app-constructor-select-icon-color.gif "Select Icon Color")

- **What is Made for** - for the created app user can select one of the seven templates. Each of them is the ready-made application with template items.

    ![Selecting the app template](gh-elements/app-constructor-what-is-made-for.gif "What is Made for")

- **App Creation** - the last step is waiting for the application to be created. This is the final stage.

    ![Waiting for creation of the app](gh-elements/app-constructor-app-creation.gif "App Creation")

## Functional Characteristics

The only usage of the current element is to create applications. When you add it to an app, it eliminates the need to go to the main menu to create a new app. Although, **App Constructor** is used by default in the user's application list.

## Element Options

The only element options are the [standard field settings](../GudHub_Structure/../Understanding_Gudhub/GH_Element/setting_overview.md).

![Settings of app constructor field](gh-elements/app-constructor-field-settings.jpg "App constructor field settings")

## Element Style

Despite the interpretation types, the element has only [standard style options](../Understanding_Gudhub/GH_Element/setting_overview.md). Even an additional one, called Show button name, you might have already seen in other articles. And as usual, the types of interpretation are described at the [end of this article](#interpretation).

![Style of app constructor element](gh-elements/app-constructor-element-style.jpg "App constructor element style")

### General Settings

As always, additional element style settings can be found in the **General Settings** group.

![General settings of constructor type](gh-elements/app-constructor-general-settings.jpg "App constructor general style settings")

Name|Description
:---|:---
Show button name|allows to hide name  of the button

#### **Show Button Name**

Depends of its value, this function allows to show or hide the button name.

## Filtration

This element is unfiltered.

## Interpretation

With these types of interpretation you can choose the appearance of the current button.

![Types of app constructor interpretation](gh-elements/app-constructor-interpretation-types.jpg "App constructor interpretation type")

### Default

The first type displays the element as an icon with the name of the button below it.

### Only Name

Displays a huge colored icon with the name of the button below it.

### Icon

The current interpretation allows you to display the element as an icon with the name of the button next to it.

### Button

The last type of element interpretation displays the element as a blue button with an icon and a button name.

## Value Format

App Constructor has no field value.

## Data Model

The current element has no properties other than `interpretation` in its data model.

```json
{
    "data_model": {
        "interpretation": []
    }
}
```

Name|Type|Description
:---|:---|:---
interpretation|`array`|*contains all element interpretation types*
