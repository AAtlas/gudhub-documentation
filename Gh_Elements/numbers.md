# Number

**Number** is a gh-element that allows user to store and customize different number values. It accepts only digits, not letters. The values of this field are available for use in calculations.

![Using the number element](gh-elements/number-work.gif "Number element")

This element accepts various types of values, including negative and decimal numbers. Together with the user settings, you can customize this element to suit your needs.

>It is also worth noting that Number accepts a period instead of a comma for decimal numbers.

## Functional Characteristics

The current gh-element has a large number of usages. As was said before, the current gh-element is used only for number values. This element can be stylized for any needs. For example that can be a field for price, currency exchange rate, or square footage, quantity, etc.

Also, regardless of the purpose of use, the values of this element can be used for calculations. Only values from this field can be used in the [Calculator](./calculator.md) formulas.

## Element Options

This gh-element has many options that allow you to customize the numbers to your liking. As most of others elements, *number* has two types of options.

### Field Settings

Field settings contains the general number options. Most of them are unique. Only Field Name and Name space are standard.

![Field settings of the number element](gh-elements/number-field-settings.jpg "Number field settings")

Name|Description
:---|:---
Precision|allows to choose how many signs will be after the point; [details...](#precision)
Multiple value|allows to add multiple values; [details...](#multiple-value)
Edit max min|allows to change range of entry number; [details...](#edit-max-min)
Min|minimum entry number; appears if Edit max min is on; [details...](#min)
Max|maximum entry number; appears if Edit max min is on; [details...](#max)
Min digit count|allows to set minimum number of signs in the entry field; [details...](#min-digit-count)
Prefix|signs before number; [details...](#prefix)
Suffix|signs after number; [details...](#suffix)
<!-- tooltip-start precision -->
#### **Precision**

The first element setting allows you to set the precision of the stored value. That is that you configures how many decimal places will be always displayed in the field.
<!-- tooltip-end precision -->
![Using the max precision](gh-elements/number-precision.gif "Precision")

There are four available options:

- `0` - do not have any decimal places. This is a **default value**.
- `0.0` - have only one decimal place.
- `0.00` - have two decimal places.
- `0.000` - have three decimal places.

That is, if the number of digits entered by the user is less than the number specified here, the blank space will be filled with a zero. For example, if you set three decimal places and user enters:

    5.34

Then in the element will display:

    5.340

This feature helps you adjust the element to the unit of measurement: pieces, dollars, meters, etc.
<!-- tooltip-start multiple_value -->
#### **Multiple value**

This is a setting that allows you to set how many values an element can store. That is, if it is enabled, the current field will take a multiple value.
<!-- tooltip-end multiple_value -->
![Using the multiple setting](gh-elements/number-multiple.gif "Multiple value")

This setting changes the appearance of the element. Each of the values is displayed in separate blocks. A button also appears that allows users to add new values.

>When this function is used, user needs to **press the Enter key** after entering the number **to save the value**.
<!-- tooltip-start edit_max_min -->
#### **Edit Max Min**

The current setting have not much influence on the work of the element. It is responsible for the limit of [filtering](#filtration) the current element. Namely it allows to configure only one filter called [Between](../Core_API/Filter/range.md).
<!-- tooltip-end edit_max_min -->

![Using the configured filter](gh-elements/number-edit-max-min.gif "Edit max and min")

So, in general, if it is enabled, the [minimum](#min) and [maximum](#max) filtering will be available for editing. Otherwise, the settings for editing them are not even displayed.
<!-- tooltip-start min -->
#### **Min**

This is the setting that allows you to set the minimum number of the filtering the current element. Here you just need to enter a number that is less than its [maximum value](#max).
<!-- tooltip-end min -->
>The default value is `0` (zero).

It's worth mentioning that you can enter absolutely any number, regardless of what you selected in the [selected precision](#precision). Together with the next setting, this is only available if [Edit Max Min](#edit-max-min) is enabled.
<!-- tooltip-start max -->
#### **Max**

Similar to the previous option, this one allows you to set a limit for [filtering numbers](../Core_API/Filter/range.md). Namely, it configures the **upper limit**. Here you can enter any number greater than the [minimum](#min) for the **maximum** value.
<!-- tooltip-end max -->
>The default value is `100`.

You can enter any value here, even decimal numbers, regardless of the value of [Precision](#precision). This setting appears only if the [Edit Max Min](#edit-max-min) is enabled.
<!-- tooltip-start min_digits_count -->
#### **Min Digit Count**

In addition to setting the number of decimal places, there is another setting that adjusts the number of digits. The current setting allows you to configure the minimum number of digits in a value.
<!-- tooltip-end min_digits_count -->
![Configured the minimum digit count](gh-elements/number-min-digit-count.gif "Min digit count")

>By default, this setting is `-1`.

This setting can be used for generating IDs. It also cannot be used with [Precision](#precision) other than the default value.
<!-- tooltip-start prefix -->
#### **Prefix**

The current setting is responsible for the characters before the value. This means that you can specify any character or word to be displayed in the field. This is used to label currency or anything else.
<!-- tooltip-end prefix -->
![Setting the prefix](gh-elements/number-prefix.gif "Prefix")

The **prefix** is not part of the value. It only appears in the element and is not stored in the [field value](#value-format).
<!-- tooltip-start suffix -->
#### **Suffix**

The current setting is quite similar to the previous one. It allows you to set any character or word at the end of the field. Here you can specify the units of measurement of the value or anything else.
<!-- tooltip-end suffix -->
![Using the suffix](gh-elements/number-sufix.gif "Suffix")

As with the previous setting, the **suffix** is not stored in the [field value](#value-format). Because it is not a part of it. It is displayed only in the field.

### Default Value Settings

This group of options contains settings of the field default value.

![Default value settings of the number element](gh-elements/number-default-value-settings.jpg "Number default value settings")

Name|Description
:---|:---
Use default value|allows to use default value; [details...](#use-default-value)
Use counter|allows to use counter as a default value; appears if Use default value is on; [details...](#use-counter)
Default value|allows to set default value; appears if Use default value is on; [details...](#default-value)
Default counter|allows you to set the number from which the counter will starts; [details...](#default-counter)
<!-- tooltip-start use_default_value -->
#### **Use Default Value**

This is the main setting of this block. If it is enabled, all other settings will appear. But in general, this setting allows you to use the default values.
<!-- tooltip-end use_default_value -->
To be more precise, **Use Default Value** allows you to apply all the changes from the following settings. So if this setting is off, then even if the following options are configured, they will not be used.
<!-- tooltip-start use_counter -->
#### **Use Counter**

The current setting allows you to enable the counter. That is, the current element will be filled with a certain sequence number. This way, each newly created item will have a unique identifier by default.
<!-- tooltip-end use_counter -->
This option simply allows you to use a counter. In turn, the [Default Counter](#default-counter) is used to set the initial value of the counter. And this setting is only available if the **Use Counter** is enabled.
<!-- tooltip-start default_value -->
#### **Default Value**

This setting is directly related to the [Use Default Value](#use-default-value). It allows you to set a specific value that will be used by default. That is, when the user creates an item, the current field will be automatically filled with the current value.
<!-- tooltip-end default_value -->
It is also important that when you fill in this setting, you make sure that you do not turn off [Use Default Values](#use-default-value). Without this setting, the current will not work and will not even be displayed.
<!-- tooltip-start default_counter -->
#### **Default Counter**

This is a setting that allows you to set the initial count value. That is, the first item user creates will automatically be filled with the current value, and the next one will have a value that is one more.
<!-- tooltip-end default_counter -->
This setting is not available if the [Use Counter](#use-counter) and [Use Defaults Value](#use-default-value) are disabled.

## Element Style

The number elements do not have unique options for styles, so you can read about general ones in [Setting Overview](../Understanding_Gudhub/GH_Element/setting_overview.md) and below about [Interpretation](#interpretation).

![Style of the number element](gh-elements/number-element-style.jpg "Number element style")

## Filtration

For filtering number elements you can use such filters:

- [Equals(or)](../Core_API/Filter/equal_filters.md)
- [Equals(and)](../Core_API/Filter/equal_filters.md)
- [Not equals(or)](../Core_API/Filter/equal_filters.md)
- [Not equals(and)](../Core_API/Filter/equal_filters.md)
- [Is bigger than](../Core_API/Filter/bigger_lower_filters.md)
- [Is lower than](../Core_API/Filter/bigger_lower_filters.md)
- [Between](../Core_API/Filter/range.md)
- [Value](../Core_API/Filter/value.md)

## Interpretation

As elements of text, numbers have the same interpretation in different places.

![Interpretation types of number](gh-elements/number-interpretation-types.jpg "Number interpretations")

And also, it has a bit differences between multiple and single value interpretations.

![Interpretation types of multiple values of number](gh-elements/number-interpretation-types-multiple.jpg "Multiple number interpretation")

### Default

The default type is editable either for single value or multiple ones.

### Plain Text

Plain text is an interpretation type that allows only displaying field values and makes field uneditable.

### Value

Displays the value of the element field.

## Value Format

Despite number is a string, it accepts any letters and signs except minus and dot.

```json
{
    "field_value": "-3.00"
}
```

That is because of their conversion to a numeric data type.

## Data Model

Number is quite understandable element. But it also has considerable quantity of settings. Its data model looks like this:

```json
{
    "data_model": {        
        "default_counter": 1,
        "default_field_value": 1,
        "interpretation": [],
        "min_digits_count": -1,
        "multiple_value": 0,
        "precision": "0",
        "prefix": "",
        "range": {
            "min": 0, 
            "max": 100
            },
        "regular_expression": "\\d+(.d+)?$",
        "show_min_max": 1,
        "suffix": "",
        "use_counter": false,
        "use_default_value": 1
    }
}
```

Name|Type|Description
:---|:---|:---
default_counter|`number`|*contains a default value for counter*
default_field_value|`number`|*contains a default value for field*
interpretation|`array`|*contains all number interpretations*
min_digits_count|`number`|*minimum number of signs in the field*
multiple_value|`boolean`|*allows you to add multiple values to the field*
precision|`string`|*allows to set the number of characters after the point*
prefix|`string`|*contains signs that will be before number in the field*
range|`object`|*contains limits of number that can be in the field*
min|`number`|*contains the minimum value that can be in the field*
max|`number`|*contains maximum value that can be in the field*
regular_expression|`string`|*contains a regular expression that checks if the format of the value is correct*
show_min_max|`boolean`|*allows to edit min and max values*
suffix|`string`|*contains signs that will be after number*
use_counter|`boolean`|*allows to use counter values*
use_default_value|`boolean`|*allows to use default value*
