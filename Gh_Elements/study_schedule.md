# Study Schedule

**Study Schedule**is a gh-element that allows the user to create a schedule and generate lessons based on it. It is essentially a tool that allows the user to develop a schedule for different structures based on data from different applications.

![Using the study schedule](gh-elements/study-schedule-work.gif "Study schedule")

This element can be divided into three areas. One of them **contains blocks of all subjects** for all classes. They are divided between **tabs by class**. The tabs are placed on the side and allow the user to switch between subjects of different classes. All these blocks can be dragged to the second area. There is also a **tab with classrooms**, which can also be dragged into the schedule.

![Subject blocks list](gh-elements/study-schedule-subject-blocks-list.gif "list of subject blocks")

A subject block contains the name of the subject, the course for which it is intended, the name of the teacher, and the class to which the block belongs. The number of hours allocated to this subject per week is displayed next to each block in the list. The **upper number is the total number of hours**, and the **lower number is the number of hours remaining**. This way, when the user arranges subjects in the schedule, the number of hours decreases. That is, **user can place an subject** in the schedule **as long as there are hours**. As soon as the **bottom value reaches zero, the block turns green and the user will not be able to use it** anymore until he or she deletes a lesson from the schedule.

In turn, the **blocks of classrooms** contain **only their names** and have no placement restrictions.

![Blocks of classrooms](gh-elements/study-schedule-classroom-blocks.gif "Classroom blocks")

The **second area is the table where user have to place block from the list**. It has five row groups, which are weekdays. Each of them is divided into same number of rows. Each of them is divided into the same number of rows and these rows are lessons. This number corresponds to the number of options in the [Lesson Schedule](#rooms-settings-and-lesson-schedule). That is, **schedule rows are lessons** in a week.

In turn, the **number of columns is a number of [classes in the schedule](#classes-filter)**. That is, there is a column for each class. In addition, each column is divided into two parts: one for the subject block and the other for the classrooms.

>**Subject blocks of a particular class can be placed only in its column.**

![The main schedule area](gh-elements/study-schedule-work-area.gif "Schedule area")

Please note that **blocks of subjects with the same teachers cannot be located in the same row but in different columns**. The same with classroom blocks, the user cannot **place the same classroom several times in a row**. This is to prevent overlapping schedules.

![Delete blocks from schedule](gh-elements/study-schedule-delete-blocks.gif "Delete blocks")

To delete a block from the schedule, move the mouse cursor over the desired block. After that, a button with a cross appears on the right side. By clicking this button, the lesson block will be deleted from the schedule, and the time of the deleted lesson will be added to the hour counter in the list. You can also delete a block by returning it back to the list.

![Buttons for the generating lessons](gh-elements/study-schedule-generate-lessons.gif "Generating buttons")

The last part of the **Study Schedule** element is two buttons above the list of blocks. The first of them called **Generate** allows user to generate lessons based on the schedule. And the **Delete Lessons** button allows you to delete all lessons created based on the current schedule. They allow the user to generate and re-generate lessons for a certain [number of weeks](#academic-weeks-in-semester-field), starting from a [certain date](#semester-start-date-field).

## Functional Characteristics

Despite the name of this element and its components, it can be used not only as a study schedule. Here you can create a schedule of tasks, events, or even meals. In this case, you can set up a schedule of events and then generate items to be displayed in the [Calendar](./calendar.md).

But let's get back to the main purpose of the schedule. As you already understood, it was created for educational institutions. This can be a **school, college, university, or even additional courses and clubs**. For each of these establishments, you can make many different schedules depending on the needs or time intervals. Despite the complexity of setting up **Study Schedule**, it is very easy to use.

It is also worth mentioning the use of this element together with [Study Journal](./study_journal.md). Just like with the [Calendar](./calendar.md), where events are generated, the schedule generates lessons that are later used in the **Journal**. This allows you to automatically determine the dates on which grades should be assigned.

## Element Options

The current element has a large set of settings, which is divided into 5 blocks. Each of them is responsible for a separate functionality of the element.

### Field and General Settings

The first block is divided in two groups. The first of them is a set of [standard settings](../Understanding_Gudhub/GH_Element/setting_overview.md) for gh-elements. And the other group consists of settings for [Lesson Generation](#lesson-generation).

![Settings of the schedule field](gh-elements/study-schedule-field-general-settings.jpg "General field settings")

Name|Description
:---|:---
Academic weeks in semester field|allows you to select the field in which the number of weeks for the schedule is stored; [details...](#academic-weeks-in-semester-field)
Semester start date field|allows you to select the field in which the start date of the study is stored; [details...](#semester-start-date-field)
<!-- tooltip-start academic_weeks_in_semester_field -->
#### **Academic Weeks in Semester Field**

The first setting of the current gh element is associated with the [Lesson Generation](#lesson-generation). Here you have to select the application field where the class schedule is located. This field must contain a number, which will be the number of weeks. This is how the gh-element determines how many lessons should be created according to the schedule set by the user.
<!-- tooltip-end academic_weeks_in_semester_field -->
This is useful for planning lessons and working with the [Study Schedule](./study_journal.md) element. That's because one of the journal modes displays columns of lessons that already exist.
<!-- tooltip-start semester_start_date_field -->
#### **Semester Start Date Field**

This option is also mainly for [Lesson Generation](#lesson-generation). Since each lesson is tied to a specific day of the week and time, and in turn, lessons are generated for a certain number of weeks, the element needs to know from which date it should start generating elements. So, here you have to select a field that stores the date of the first day of the study period.
<!-- tooltip-end semester_start_date_field -->
### Classes Settings and Filter

The second block is completely dedicated to setting up classes for the schedule. With these settings, you create tabs with classes and the main columns in which subjects will be placed.

![Settings of the classes](gh-elements/study-schedule-classes-settings.jpg "")

Name|Description
:---|:---
Classes App|allows you to select an application that stores all information about classes; [details...](#classes-app)
Class Title|allows you to select a field that stores the name of the class; [details...](#class-title)
Class Course|allows you to select allows you to select the field with the class course; [details...](#class-course)
Classes Filter|allows you to determine which classes will be displayed in the current schedule; [details...](#classes-filter)
Sorting Type|allows you to set the order of class tabs; [details...](#sorting-type)
<!-- tooltip-start classes_app -->
#### **Classes App**

First of all, you need to select the application that stores all classes. The data from it will be used for the following settings and, accordingly, for the element itself.
<!-- tooltip-end classes_app -->
<!-- tooltip-start class_title -->
#### **Class Title**

When you set up a schedule, you first need to specify the classes for which it is created. To do this, select the field that stores the names of the classes. These names will be displayed in the subject blocks. For each individual class, there will be a separate week column, as well as a tab with subjects for that class.
<!-- tooltip-end class_title -->
<!-- tooltip-start class_course -->
#### **Class Course**

The current setting is intended to define the subjects that are required for a particular class. Here you have to select the field that stores the course of the class. Its value is compared to the [Subject Course](#subject-course). If the values match, this subject will be available for the class in the schedule.
<!-- tooltip-end class_course -->
#### **Classes Filter**

This parameter allows you to define which classes will be available in the gh element. This setting works in a similar way to the [Filter element](./table_filter.md). So, due to the configured filters, different classes will be available for scheduling, for example, in different items.
<!-- tooltip-start sorting_type -->
#### **Sorting Type**

This is the setting that allows you to set the sorting type of the classes. That is this setting sorts the class tabs and the columns of the main schedule area. Tabs are sorted from top to bottom and columns from left to right.
<!-- tooltip-end sorting_type -->
>If this setting is empty, the order of the classes will depend on the order in which their items were added to the [Classes app](#classes-app).

### Subjects Settings and Filter

This is a block of settings that consists of all the settings that are responsible for the configuration of the study subjects in the element. The schedule exists mainly to schedule subjects for all classes and teachers.

![Settings of the schedule subjects](gh-elements/study-schedule-subjects-settings.jpg "")

Name|Description
:---|:---
Subjects App|allows you to select an application that contains a list of subjects; [details...](#subjects-app)
Subject Title|allows you to select the field in which the subject name is stored; [details...](#subject-title)
Subject teacher|allows you to select the field with the name of the teacher who teaches this subject; [details...](#subject-teacher)
Subject course|allows you to select the field in which the course for which this subject is intended is stored; [details...](#subject-course)
Subject academic hours|allows you to select a field that contains the number of hours allocated to this subject; [details...](#subject-academic-hours)
Subjects Filter|allows you to determine which subjects will be available in the schedule; [details...](#subjects-filter)
<!-- tooltip-start subjects_app -->
#### **Subjects App**

The first setting of the current block is the one that allows you to set the source application for subjects. All the following settings will take data from this application. And accordingly, all the data about the subjects in the schedule are taken from here.
<!-- tooltip-end subjects_app -->
<!-- tooltip-start subject_title -->
#### **Subject Title**

The next setting allows you to select the field that stores the title of the subject. That field is taken from the Subject App. The name of each subject will be displayed in the subject blocks of the schedule.
<!-- tooltip-end subject_title -->
<!-- tooltip-start subject_teacher -->
#### **Subject Teacher**

Each subject is assigned a specific teacher. This is done so that teachers do not have overlaps in subjects. That is, the same teacher cannot teach different classes at the same time. Accordingly, his or her subject cannot be taught in several classes at the same time. Therefore, in the current setting, you need to select the field that stores [subject](#subject-title) teachers.
<!-- tooltip-end subject_teacher -->
<!-- tooltip-start subject_course -->
#### **Subject Course**

This is a setting that determines the course for which the subject is intended. That is here you have to select the field that stores the course of the subject. This field must have the same values that the [Class course](#class-course) can store. This is because the values of these fields are compared to determine which subjects are available to whom.
<!-- tooltip-end subject_course -->
<!-- tooltip-start subject_academic_hours -->
#### **Subject Academic Hours**

Each subject in the schedule has a limited number of lessons per class per week. It is limited by the number of hours, which is set for each item individually. In order for the number of hours to be taken into account in the schedule, you need to select the corresponding field in the [Subjects application](#subject-academic-hours) in the current setting.
<!-- tooltip-end subject_academic_hours -->
The number of hours in the schedule will vary depending on the lessons arranged in the schedule. That is, if some lessons for this subject are already placed in the schedule, the number of hours is reduced. This number is determined individually for each [class](#classes-settings-and-filter).

#### **Subjects Filter**

This setting works similarly to the [Filter element](./table_filter.md). It allows you to set which subjects will be available in the schedule. You can do this by setting up filters.

### Rooms Settings and Lesson Schedule

The current setting block contains two groups. The **Rooms Settings** is responsible for configuring the source of classes in the schedule. And **Lesson Schedule** allows you to customize the number of lessons per day of the schedule.

As you have already understood, the **Study Schedule** also provides the schedule of classrooms. That is, everything is set up so that two different lessons cannot be held in the same classroom at the same time.

![Settings of rooms and a lesson schedule](gh-elements/study-schedule-rooms-settings-lessons.jpg "Rooms ans lessons settings")

Name|Description
:---|:---
Rooms App|allows you to select an application with a list of study rooms; [details...](#rooms-app)
Room Number|allows you to select the field containing the room number; [details...](#room-number)
Time|allows you to set the start time of the lesson; [details...](#time)
Edit|allows you to edit or delete a lesson; [details...](#edit)
<!-- tooltip-start rooms_app -->
#### **Rooms App**

The current setting is for selecting a data source for classes. This is the application that stores all the data about each auditory. This data will be used for the element and for the following settings.
<!-- tooltip-end rooms_app -->
<!-- tooltip-start room_number -->
#### **Room Number**

This is the setting that defines the name of the classrooms. So, here you need to select the field where the names of the rooms are stored. These can be room numbers or classroom names. All this names will be displayed in the schedule.
<!-- tooltip-end room_number -->
#### **Time**

This is the first setting of the **Lesson Schedule**. It allows you to set the number of lessons per day and set their start time. A new lesson created here provides another lesson slot for each day of the week. In turn, the time you set is used to [Lesson Generation](#lesson-generation). Each lesson created will have the time set here.

>This setting works in the same way as [Duration element](./duration.md).

#### **Edit**

This is the configuring setting that is responsible for the work with time options. It consists of two buttons. The first one is for editing and the second one allows to delete the option.

### Lesson Generation

The last block contains settings that customize the generation of lessons.

![Settings of the lessons generation](gh-elements/study-schedule-lesson-generation.jpg "Lessons generation")

Name|Description
:---|:---
Lessons App|allows you to select a destination application for the lessons creation; [details...](#lessons-app)
Lesson subject|allows you to select the field where the lesson subject will be saved; [details...](#lesson-subject)
Lesson teacher|allows you to select a field for the teacher's name; [details...](#lesson-teacher)
Lesson class|allows you to select a field for storing a class reference; [details...](#lesson-class)
Lesson date|allows you to select a field for the date of the lesson; [details...](#lesson-date)
Lesson schedule id|allows you to select the field in which the schedule ID will be saved; [details...](#lesson-schedule-id)
Lesson room|allows you to select a field for a reference to a room item; [details...](#lesson-room)
<!-- tooltip-start lessons_app -->
#### **Lessons App**

First of all, you need to set up the application in which you want to create lessons. So, the current settings is where you should select this application.
<!-- tooltip-end lessons_app -->
<!-- tooltip-start lesson_subject -->
#### **Lesson Subject**

This is the setting where you need to select a field in the [Lessons App](#lessons-app). This will be the field in which the subject to which the created lesson belongs will be saved. In other words, this setting automatically fills the field selected here with a reference to the item in the [Subject App](#subjects-app).
<!-- tooltip-end lesson_subject -->
<!-- tooltip-start lesson_teacher -->
#### **Lesson Teacher**

This setting allows you to select the destination field for the teacher who leads the lesson. This field will be automatically filled in by the teacher who teaches the [above subject](#lesson-subject). That is, the value for the current field will be taken from the one that was selected in the [Subject Teacher](#subject-teacher).
<!-- tooltip-end lesson_teacher -->
<!-- tooltip-start lesson_class -->
#### **Lesson Class**

Like the previous two settings, the current one is responsible for selecting the destination field, but for a class. The class for the current field is taken from the [Class App](#classes-app), namely, references to its items.
<!-- tooltip-end lesson_class -->
Which class will be saved to the lesson depends on the [course](#subject-course) for which the subject is intended and in which [column](#class-title) the subject block is placed
<!-- tooltip-start lesson_date -->
#### **Lesson Date**

Here you should select the field where the date of the lesson will be saved. The date is determined when generating all lessons. It depends on the [start date of the semester](#semester-start-date-field) and the week for which a particular lesson is created. That is, the date of the lesson is automatically determined depending on what day of the week and time it was scheduled. The number of studying weeks also plays a role. Namely, that the date is also influenced by the week from the start date.
<!-- tooltip-end lesson_date -->
<!-- tooltip-start lesson_schedule_id -->
#### **Lesson Schedule ID**

The next setting allows you to select the field where ID of the current **Study Schedule** will be saved. It is not important for users. This is used so that the generated lessons can later be deleted using a button in the schedule. The **Study Schedule** determines which lessons belong to this particular schedule using this identifier.

>Basically, this setting helps user to regenerate lessons based on a specific schedule.
<!-- tooltip-end lesson_schedule_id -->
<!-- tooltip-start lesson_room_id -->
#### **Lesson Room**

This is a setting that allows you to select a field for classrooms. For each lesson in the schedule, a classroom is set in which this lesson will be held. That is, a reference to an item from the [Classes App](#classes-app) will be stored here. The classroom for each lesson is selected by the user in the schedule.
<!-- tooltip-end lesson_room_id -->
## Element Style

For the current gh element, the style settings don't really matter. This is because its appearance is determined by its [interpretation type](#interpretation). It only has the usual [set of those settings](../Understanding_Gudhub/GH_Element/setting_overview.md).

![Settings of the element style](gh-elements/study-schedule-element-style.jpg "Element style")

## Filtration

An item cannot be filtered by the value of the current element.

## Interpretation

The current element has only one interpretation type. This is the default type, which is always used. This turns the element into an interactive schedule with many tabs, drag-and-drop blocks and two buttons.

![Type of element interpretation](gh-elements/study-schedule-interpretation-types.jpg "Interpretation types")

## Value Format

This gh-element has no field value.

## Data Model

The model of **Study Schedule** consists of many different properties:

```json
{
    "data_model": {
        "academic_weeks_in_semester_field_id": "807789",
        "cabinets_app_id": "33956",
        "cabinets_app_number_field_id": "807581",
        "classes_app_course_field_id": "807567",
        "classes_app_id": "33870",
        "classes_app_title_field_id": "807568",
        "classes_filters_list": [],
        "classes_sorting_type": "",
        "interpretation": [],
        "lessonsTime": [{
            "number": "1",
            "time": 30600000
        }],
        "lessons_app_class_field_id": "806231",
        "lessons_app_date_field_id": "806235",
        "lessons_app_id": "33868",
        "lessons_app_room_field_id": "808481",
        "lessons_app_schedule_id_field_id": "808300",
        "lessons_app_subject_field_id": "806230",
        "lessons_app_teacher_field_id": "806237",
        "rooms_app_id": "33956",
        "rooms_app_number_field_id": "807581",
        "semester_start_date_field_id": "808309",
        "subjects_app_academic_hours_field_id": "807565",
        "subjects_app_course_field_id": "807566",
        "subjects_app_id": "33866",
        "subjects_app_teacher_field_id": "806193",
        "subjects_app_title_field_id": "806191",
        "subjects_filters_list": [],
        "trigger": {}
    }
}
```

Name|Type|Description
:---|:---|:---
academic_weeks_in_semester_field_id|`string`|*contains ID of the field that stores the number of academic weeks*
cabinets_app_id|`string`|*contains the identifier of the application that stores the list of classrooms*
cabinets_app_number_field_id|`string`|*contains the ID of the field that stores the number of the classroom*
classes_app_course_field_id|`string`|*contains the ID of the field that contains the course of the class*
classes_app_id|`string`|*contains the ID of the application that stores all classes and their data*
classes_app_title_field_id|`string`|*contains the ID of the field that stores the name of the class*
classes_filters_list|`array`|*contains all filters applied to classes items*
classes_sorting_type|`string`|*contains the selected sorting type of the class tabs*
interpretation|`array`|*contains all element interpretation types*
lessonsTime|`array`|*contains all the configured options for lesson time*
number|`string`|*contains the ordinal number of the lesson*
time|`number`|*contains the start time of the lesson in milliseconds, counted from midnight*
lessons_app_class_field_id|`string`|*contains the identifier of the field that stores the class in which the lesson is held*
lessons_app_date_field_id|`string`|*contains the ID of the field in which the date of the lesson is stored*
lessons_app_id|`string`|*contains the ID of the application where the new lessons will be stored*
lessons_app_room_field_id|`string`|*contains the ID of the field where the classroom for lesson is stored*
lessons_app_schedule_id_field_id|`string`|*contains the ID of the field where the schedule identifier will be saved*
lessons_app_subject_field_id|`string`|*contains the ID of the field that contains the subject of the future lesson*
lessons_app_teacher_field_id|`string`|*contains the identifier of the field that will be filled with the name of the teacher*
rooms_app_id|`string`|*contains an application ID that stores all classes and data about them*
rooms_app_number_field_id|`string`|*contains the field ID with the classroom number, which is also its title*
semester_start_date_field_id|`string`|*contains the ID of the field that contains the date from which the creation of lessons will begin*
subjects_app_academic_hours_field_id|`string`|*contains the field identifier that contains the number of hours allocated to this subject*
subjects_app_course_field_id|`string`|*contains the identifier of the field in which the course for which the item is intended is stored*
subjects_app_id|`string`|*contains the ID of the application that stores all the subjects*
subjects_app_teacher_field_id|`string`|*contains the ID of the field that stores the name of the teacher who teaches the current subject*
subjects_app_title_field_id|`string`|*contains the ID of the field that saves the title of the subject*
subjects_filters_list|`array`|*contains all the filters applied to the subjects*
trigger|`object`|*contains all element trigger options*
