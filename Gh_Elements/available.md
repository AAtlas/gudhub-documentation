# Available

**Available** is a gh-element that allows users to set the time period of time when they are available. Here, the user **combines text phrases that describe a certain period of time**. It is a text field with a drop-down list, where pieces of different phrases are collected.

![Adding the first part](gh-elements/available-drop-down-list.gif "Set the phrase part")

First, the user selects the initial words, after which the next parts become available. After selecting different phrases, the user can assemble the final phrase that will be saved.

![Entering of the full phrase](gh-elements/available-enter-full-phrase.gif "Value saving")

>Note that only the full phrase will be saved as a [field value](#value-format).

This element also accepts the multiple value. Thus, the user can set the entire time when he/she is available in one field.

![Adding the multiple value](gh-elements/available-add-multiple-value.gif "Multiple value")

You can delete values or parts of a phrase by clicking the cross at the end of the phrase block.

![Deleting of the phrases](gh-elements/available-delete-values.gif "Value deleting")

Thus, the **Available** has a set of available extensions for each part of each phrase up to the end of the phrase. An element can also contain multiple phrases as values.

## Functional Characteristics

The current item is used to set the period when you are available for work or calls. This allows users to select the appropriate time instead of entering it, which saves them time.

## Element Options

The whole settings which can configure the element are the [standard field settings](../Understanding_Gudhub/GH_Element/setting_overview.md).

![Settings of available field](gh-elements/available-field-settings.jpg "Available field settings")

## Element Style

The style of an available element can be customized using its own [interpretation types](#interpretation) and a set of [standard settings](../Understanding_Gudhub/GH_Element/setting_overview.md).

![Style of available element](gh-elements/available-element-style.jpg "Available element style")

## Filtration

This element cannot be filtered out.

## Interpretation

The available element has only two interpretations:

![Types of available interpretation](gh-elements/available-interpretation-types.jpg "Available interpretation types")

### Input

This type allows to add new value or update the existed one.

### Default

This is a type of interpretation that only allows you to display the value of the field.

## Value Format

The field value is contains of the combination of selected words. As a result, you will receive a complete offer.

```json
{
    "field_value": "every weekday"
}
```

If a field contains several phrases, all of them are stored in one line separated by commas.

```json
{
    "field_value": "on the first minutes , except every weekday"
}
```

## Data Model

The data model of the element is very small. It contains only interpretation array.

```json
{
    "data_model": {
        "interpretation": []
    }
}
```

Name|Type|Description
:---|:---|:---
interpretation|`array`|*contains all element interpretation types*
