# Text Editor MSE

**Text Editor MSE** is an advanced text editor. This gh-element has various tools that enhance text editing capabilities.

>It has a second name - *Tiny MSE*.

![Text editor MSE area](gh-elements/text-editor-mse-area.jpg "Text editor MSE")

This element is mostly used in combination with [print document](print_document.md) which respectively allows you to print the created document.

## Functional Characteristics

Tiny MSE can be used to create different types of documents. It contains a lot of different functions including the feature for adding the variables. Those variables are the field whose values vary depending on the item. When the user clicks the [print button](print_document.md), the variable fields will be filled with data from the specific item.

## Value Format

The value of this element is the ID of the file that was created in it.

```json
{
    "field_value": "944016"
}
```

## Element Options

The current item has two groups of settings, which are located in one block.

### Field And Main Settings

The tiny MSE field settings do not contain any options other than the [standard](../Understanding_Gudhub/GH_Element/setting_overview.md).

![Main settings of text editor MSE field](gh-elements/text-editor-mse-field-main-settings.jpg "Text editor MSE field main settings")

Name|Description
:---|:---
App for Field List|allows to select the source application for the field variables; [details...](#app-for-field-list)
Name of Template|allows to select the source field whose values will be taken for the template names; [details...](#name-of-template)
<!-- tooltip-start field_name_1 -->
#### **App For Field List**

The selection of the source application in this element different from others. Here you should not select the application itself, but the field in which the application will be selected. That field is the [app element](app_element.md).
<!-- tooltip-end field_name_1 -->
This method allows user to choose different source applications for different document templates.

>More precisely, it works for documents from different elements. The user selects the source application for all documents in the item.
<!-- tooltip-start name_of_template -->
#### **Name OF Template**

This is the function that allows to select the field whose values will be taken for names of the templates. This means that the value of this field in a particular item will be used as the template name for a document from the same item.
<!-- tooltip-end name_of_template -->
## Element Style

The current element have no extra style settings. It can be customized using only [the standard style options](../Understanding_Gudhub/GH_Element/setting_overview.md) and its own [interpretation types](#interpretation).

![Style of text editor MSE element](gh-elements/text-editor-mse-element-style.jpg "Text editor MSE style")

## Data Model

The data model of the current element is quite small:

```json
{
    "data_model": {
        "field_id": "678845",
        "field_template_id": "678844",
        "interpretation": []
    }
}
```

Name|Type|Description
:---|:---|:---
field_id|`string`|*contains ID of field from which the source application is taken*
field_template_id|`string`|*contains ID of the field from which name for the template is taken*
interpretation|`array`|*contains all element interpretation types*

## Filtration

This element cannot be filtered out.

## Interpretation

The MSE text editor has four completely different types of interpretation.

![Types of text editor MSE interpretation](gh-elements/text-editor-mse-interpretation-types.jpg "Text editor MSE interpretation types")

### Input With Name

The first interpretation type is an interactive area for creating text files and editing them. There are lots of useful buttons for that.

### Default

The second type displays only the icon.

### Value

This type of interpretation displays the URL of the file that was created in MSE editor field.

### Web

The last type of interpretation allows the element to display the content of the document in web format in the application.
