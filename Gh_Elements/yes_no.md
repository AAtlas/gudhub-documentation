# Yes/No

**Yes/No** gh-element is used then you need either strictly negative or strictly positive answer.

## Functional Characteristics

This element is mostly used in different questionnaire and tests. For example, you can use it when you need the user to consent to the processing of personal data.

## Value Format

The possible values of this element are in its data model as the value of the property is literally called *value*. Each of them has the certain name:

- *Yes*

```json
{
    "field_value": "1"
}
```

- *No*

```json
{
    "field_value": "0"
}
```

## Element Options

Yes/No element does have standard Field Settings and has no additional settings.

![Settings of yes/no field](gh-elements/yes-no-field-settings.jpg "Yes/No field settings")

## Element Style

As many gh-elements, this one has only standard options for its style. About each of them you can read in [Settings Overview](../Understanding_Gudhub/GH_Element/setting_overview.md) and about its interpretation read [below](#interpretation).

![Style of yes/no element](gh-elements/yes-no-element-style.jpg "Yes/No element style")

## Data Model

The main distinctive feature of this gh-element is an array of the only two values.

```json
{
    "data_model": {
        "default_field_value": 0,
        "interpretation": [],
        "options": [{
            "name": "Yes",
            "value": 1
            },
            {
            "name": "No",
            "value": 0
        }],
        "use_default_value": 0
    }
}
```

Name|Type|Description
:---|:---|:---
default_field_value|`number`|*contains default value of the element*
interpretation|`array`|*contains all element interpretations*
options|`array`|*contains both element options*
name|`string`|*name of the option*
value|`number`|*value of the certain option*
use_default_value|`boolean`|*shows whether the default value is used or not*

## Filtration

Since yes/no element has only two values, it can be filtered by not a big variety of filters.

- [Equals(or)](../Core_API/Filter/equal_filters.md)
- [Not equals(or)](../Core_API/Filter/equal_filters.md)
- [Value](../Core_API/Filter/value.md)

## Interpretation

Despite of its simplicity, this element is very useful tool. So it needs lots of interpretations.

![Types of yes/no interpretation](gh-elements/yes-no-interpretation-types.jpg "Yes/No interpretation types")

### Default

The default interpretation of yes/no element allows to display it as a field with a drop-down list of two values.

### Checkbox

It displays a small checkbox. When it is checked, the value is 'Yes'. When it is not, the value is 'No'.

### Switch

This interpretation allows to display switch. When it is on, the value is 'Yes'. When it is off, the value is 'No'.

### Plain Text

This is the interpretation type that allows to set a value and does it uneditable.

### Plain Text Symbol

Allows to display uneditable symbol for true value.

### Value

The last interpretation type displays the field value.
