# Nested Filter

**Nested Filter** is a gh-element that allows you to create the nested list by using different filters. The way it works is that groups with certain filters are created. Then certain dependencies are established between these groups.

![Using the nested filter](gh-elements/nested-filter-work.gif "Nested filter")

After setting up, a nested list is created. Each of its tabs is a set of filters.

>Each individual group can have different sets of filters.

Filters are applied to items when you click on the corresponding tab. And the set of filtered items is displayed in a [specific gh-element](#bind-to-table).

![View subfolders](gh-elements/nested-filter-collapse-list.gif "Collapse and expand list")

This element also allows you to collapse and expand folders to see and hide groups within them, respectively.

## Functional Characteristics

The main purpose of the current gh-element is to allow you to create absolutely any customized nested list. It works very similarly to a [Nested List](./nested_list_element.md) element, but they are configured in completely different ways.

So, **Nested Filter** can be used to create a table of contents for your documentation. For example, to divide items into categories and subcategories.

They can also be tabs with predefined filters as in [Filter Tabs](./filter_tabs.md). But here you have the opportunity to make nesting.

## Element Options

The settings for this element consist of [Field Settings](#field-settings) and [Data Settings](#data-settings).

### Field Settings

This block contains the main settings of the element. The settings for the next block depend on them. Also, there are located the standard settings of the current gh-element.

![Settings of nested filter field](gh-elements/nested-filter-field-settings.jpg "Nested filter field settings")

Name|Description
:--|:--
App Id|allows to select the application whose items will be filtered; [details...](#app-id)
Bind to table|allows to select the gh-element to which the filter will be attached; [details...](#bind-to-table)
Show Icon|allows to determine whether icons will be displayed in the filter; [details...](#show-icon)
Folder Icon|allows to set icon for folders; [details...](#folder-icon)
Item Icon|allows to select icon for items; [details...](#item-icon)
<!-- tooltip-start app_id -->
#### **App Id**

This setting allows you to select the application whose items will be filtered. Depending on it, a list of fields to which the filter can be attached will be generated. This means that the element from [Bind to Table](#bind-to-table) is taken in the current application. [Filters](#filter) are also configured based on the selected application.
<!-- tooltip-end app_id -->
<!-- tooltip-start bind_to_table -->
#### **Bind to Table**

This is the setting where you select the element where the filter results will be displayed. These are gh-elements such as [Table](./table.md) and [Cards](./cards.md).
<!-- tooltip-end bind_to_table -->
<!-- tooltip-start show_icon -->
#### **Show Icon**

The current settings allow you to customize whether icons are displayed or not. This is basically a switch. When it is enabled, the corresponding icons are displayed before the groups names.
<!-- tooltip-end show_icon -->
![Using show icon](gh-elements/nested-filter-show-icon.gif "Show icon")

[Folders and items](#data-settings) icons can be customized separately. There are two settings for this: [Folder Icon](#folder-icon) and [Item Icon](#item-icon).
<!-- tooltip-start folder_icon -->
#### **Folder Icon**

This setting allows you to select an icon for all **folders** in the current element. A group of items becomes a **folder** when it has groups that depend on it.
<!-- tooltip-end folder_icon -->
>The current setting works as an [Icon element](./icon_element.md).

Therefore, all groups that have children will have the icon selected here before the [group name](#name).
<!-- tooltip-start item_icon -->
#### **Item Icon**

The last setting of its block allows you to select an icon for **items** in the **Nested Filter**. These **items** are groups of filtered application items that have no child groups. They are the last chain of dependencies in the current element.
<!-- tooltip-end item_icon -->
The selected icon appears before the names of the corresponding groups. Like the previous setting, this one works as an [Icon element](./icon_element.md).

### Data Settings

The second block of element settings allows you to create groups of items for the **Nested Filter**.This is where you set up group dependencies. All of groups are divided into:

- **Folder** - group that has groups dependent on it
- **Item** - group that has only a parent group and no dependent groups

![Settings of nested filter data](gh-elements/nested-filter-data-settings.jpg "Nested filter data settings")

Name|Description
:--|:--
Name|allows to enter the of the current folder; [details...](#name)
Parent|allows to set the parent of the current folder; [details...](#parent)
Filter|allows to filter the items that will belong to the current folder; [details...](#filter)
Edit|allows to edit or delete the folder; [details...](#edit)

#### **Name**

This setting lets you enter a name for the item group you're creating. Names from this setting are displayed in the item's nested list. These names will also be displayed in the [following option](#parent).

#### **Parent**

The current option allows you to select a parent for the current group. That is, you can select the folder in which the current group will be located.

>The list of potential parents consists of existing groups, except for the current one.

If a group is someone's parent, it automatically becomes a folder.

#### **Filter**

This setting allows you to configure filters that determine which items are included in the group. It works similarly to the [Table Filter](./table_filter.md) element. With this setting, you literally put items into groups.

#### **Edit**

The current setting consists of two buttons. The first button allows you to edit an item group by clicking on it, and the second button allows you to delete it.

## Element Style

The current gh-element has a [standard set of style settings](../Understanding_Gudhub/GH_Element/setting_overview.md) that allow you to customize it. It also has a [type of interpretation](#interpretation).

![Style settings of nested filter element](gh-elements/nested-filter-element-style.jpg "Nested filter style")

## Filtration

The items cannot be filtered by the current field.

## Interpretation

The current element has only one interpretation type.

![Types of nested filter interpretation](gh-elements/nested-filter-interpretation-types.jpg "Nested filter interpretation types")

### Default

A single type of interpretation of this element is used throughout. It displays a nested list with items sorted into different folders.

## Value Format

This element does not store any value.

## Data Model

Due to the large number of folders and filters, the data model of this element can be really large:

```json
{
    "data_model": {
        "app_id": "30192",
        "interpretation": [],
        "options": [{
            "filters_list": [],
            "name": "Main",
            "parent": "0"
        }], 
        "table_field_id": "701333",
    }
}
```

Name|Type|Description
:---|:---|:---
app_id|`string`|*contains the application ID with the items to be filtered*
interpretation|`array`|*contains all interpretation types of the element*
options|`array`|*contains all created folders*
filters_list|  `array`|*contains all filters of the current folder*
name|`string`|*contains a name of the folder*
parent|`string`|*contains ID of the folder parent*
table_field_id|`string`|*contains the ID of the element to which the filter relates*
