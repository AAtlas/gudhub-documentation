# Barcode

**Barcode** is a gh-element that allows to generate the barcode based on the items data. A barcode is generated based on certain data that is encrypted into it. This can be the [value of a certain field](#field-reference) or some [static data](#code-text) entered in the settings.

![The example of the barcode](gh-elements/barcode-element.jpg "Barcode")

There is no generation animation, the user just sees the finished barcode. You can change the appearance of the barcode in the [settings](#code-type).

![Generation of the barcode](gh-elements/barcode-generating.gif "Barcode generation")

>All generated barcodes are available for scanning.

## Functional Characteristics

The main purpose of this element is to generate barcodes based on some data from the product. This allows you to create a special application for the store, where all products with all important data, including their barcodes, will be stored. Then, by scanning the barcode, the user will get access to the product data.

## Element Options

The element options consists of three groups of settings.

### Field Settings

The first one is a standard group that described in [Setting Overview](../Understanding_Gudhub/GH_Element/setting_overview.md).

![Settings of barcode field](gh-elements/barcode-field-settings.jpg "Barcode field settings")

### Code Generation Settings

The main settings are assembled in this block. They are responsible of the code generation.

![Settings of barcode generation](gh-elements/barcode-code-generation-settings.jpg "Barcode generation settings")

Name|Description
:---|:---
Static value|allows you to choose whether the value is static or not; [details...](#static-value)
Code text|allows to enter the static value; [details...](#code-text)
Field reference|allows to select the field reference for barcode generation; [details...](#field-reference)
Code-type|allows to select the type of the barcode; [details...](#code-type)
<!-- tooltip-start static_value -->
#### **Static Value**

This is the function that allows you to select the type of value. If **Static Value** is enabled, the [Code Text](#code-text) option appears. Here you can enter text to create an unchangeable barcode in the settings. If **Static Value** is disabled, you can use the [Field Reference](#field-reference) option. It allows you to write data from a specific field to a barcode.
<!-- tooltip-end static_value -->
<!-- tooltip-start code_text -->
#### **Code Text**

This is the field for entering the value on the basis of which the barcode is generated. This value is entered only in the settings and cannot be changed by the user.
<!-- tooltip-end code_text -->
>This allows you **to create a single barcode for the entire application**.

This option appears when [Static Value](#static-value) is enabled.
<!-- tooltip-start field_reference -->
#### **Field Reference**

When [Static Value](#static-value) is disabled, you can select the field from which the data for the barcode will be taken. This allows the user to repeatedly change the barcode data and regenerate it.
<!-- tooltip-end field_reference -->
![Work of the field reference](gh-elements/barcode-field-ref.gif "Field reference")

So, you bind the field to the current item and then the data entered there will be encrypted into a barcode.

>Unlike the previous setting, the current one allows you **to create a barcode for each item separately**.

#### **Code-Type**

The current setting allows you to choose the barcode type according to your needs. The Barcode element can generate 11 types of barcodes:

- code39
- code128A
- code128B
- code128C
- pharmacode
- itf
- itf14
- ean8
- ean13
- upcA
- upcE

After selecting the type, all generated barcodes will be of this type, regardless of the previous settings.

>Each of those types has its own appearance.

## Element Style

The barcode style settings are mostly consists of standard option. All of them are described in detail in [Setting Overview](../Understanding_Gudhub/GH_Element/setting_overview.md). And about element interpretation you can read in [the end of this article](#interpretation).

![Style of barcode element](gh-elements/barcode-element-style.jpg "Barcode element style")

### General Settings

The barcode style settings differ from the standard ones in several options.

![General settings of barcode style](gh-elements/barcode-general-settings.jpg "Barcode general settings")

Name|Description
:---|:---
Font Size|allows to set the size of the barcode font; [details...](#font-size)
Line Width|allows to set the width of the barcode lines; [details...](#line-width)
Height|allows to set the height of the barcode; [details...](#height)

#### **Font Size**

Some of the code types allows the barcode to be displayed with the text. You can change its font size due to this setting.

#### **Line Width**

This is the option that allows to customize the width of barcode lines.

#### **Height**

This is the option that allows to increase the barcode height.

## Filtration

The only way to filter the barcode element is using [Value](../Core_API/Filter/value.md) filter.

## Interpretation

The only interpretation type allows to display the generated barcode.

![Types of barcode interpretation](gh-elements/barcode-interpretation-types.jpg "Barcode interpretation types")

## Value Format

The barcode element does not have a field value.

## Data Model

The barcode data model contains all the data about the code generation settings and its interpretation.

```json
{
    "data_model": {
        "code_type": "pharmacode",
        "interpretation": [],
        "ref": {
            "field_id": "678459"
        },
        "static_mode": 1,
        "static_value": "barcode 1"
    }
}
```

Name|Type|Description
:---|:---|:---
code_type|`string`|*contains the type of code*
interpretation|`array`|*contains all interpretation types*
ref|`object`|*contains data about field reference*
field_id|`string`|*contains ID of source field for reference*
static_mode|`boolean`|*shows whether the static mode is on or not*
static_value|`string`|*contains the static value*
