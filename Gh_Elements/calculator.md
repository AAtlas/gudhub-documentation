# Calculator

**Calculator** is a gh-element that allows you to make expressions that will automatically calculate a value according to given conditions.

## Functional Characteristics

This element allows you to automate various calculations. For example, it can be configured for payroll calculations. It can calculate not only number data. It can also count the amount of items sold by their status, which is set with [radio button](radio_button.md) in items.

## Element Options

The calculator has two types of options, which allow you to compose an expression to calculate the value and which allow you to customize the value in the field.

### Field Settings

The first setting group contains [standard field settings](../Understanding_Gudhub/GH_Element/setting_overview.md) and additional settings that configures the displaying of the calculated value.

![Settings of calculator field](gh-elements/calculator-field-settings.jpg "Calculator field settings")

Name|Description
:---|:---
Precision|allows to select the precision of the calculation result; [details...](#precision)
Prefix|allows to enter the special *prefix* for the calculation result; [details...](#prefix)
Suffix|allows to enter the special *suffix* for the calculation result; [details...](#suffix)
Persistent value|allow to configure whether the value will be saved or not; [details...](#persistent-value)
One time generation|allows to make the stored value unchanged; [details...](#one-time-generation)
<!-- tooltip-start precision -->
#### **Precision**

The current setting allows you to specify the accuracy of the calculation result. This is a drop-down list where you can select the number of decimal places.

>The maximum available number is 3 decimal places.
<!-- tooltip-end precision -->
It is also worth noting that the value is rounded in accordance with well-known rules. For example, we got this figure from our calculations:

![The number for rounding](gh-elements/calculator-start-number.jpg "Initial number")

Since the thousandth number is less than 5, the hundredth number remains 6.

![Rounding of the number](gh-elements/calculator-round-to-hundredth.jpg "Rounding to hundredth")

If we round to tens, the tenth number will be 2, because the hundredth number is greater than 5.

![Rounding to the biggest number](gh-elements/calculator-round-to-tenth.jpg "Rounding to tenth")

With this setting, you can also set the number of decimal places for various values that require special precision, such as monetary values.
<!-- tooltip-start prefix -->
#### **Prefix**

This setting allows you to enter a special prefix for numbers. This is useful when you need to customize a numeric field, for example, for a price field.

>The prefix can be **any character, word, number, or any string data**.
<!-- tooltip-end prefix -->
![Using the prefix](gh-elements/calculator-prefix.gif "Prefix")

After entering the prefix, it will be displayed before each value of this field.

>The **Number** prefix is **not stored in the field value**.
<!-- tooltip-start suffix -->
#### **Suffix**

This is an additional setting where you can enter the custom suffix. That is, you enter the data that will be displayed after the number value.

>This can be **any string data**, such as words or numbers.
<!-- tooltip-end suffix -->
Using a suffix does not affect the field value. It only displays the suffix after the value in the items.

![Using the suffix](gh-elements/calculator-suffix.gif "Suffix")

>That is the suffix is not saved in the field value.
<!-- tooltip-start persistent_value -->
#### **Persistent Value**

By default, the **Calculator** element only calculates the created expression, but cannot save the resulting value. So, the current setting allows you to enable value saving.

>Only when this option is enabled can the resulting value be saved in the field. After the update, the value will be updated.
<!-- tooltip-end persistent_value -->

<!-- tooltip-start one_time_generation -->
#### **One Time Generation**

This option allows you to save the first generated value. That is, no matter how the value is updated through the variables, it will not affect the calculator value. This is used to prevent values from being updated.

>One-time generation only only works with  [Persistent value](#persistent-value).
<!-- tooltip-end one_time_generation -->

But in fact, there is only one way to update the value - using the [update button](#update-button) . This allows the user to update the Calculator value. If you update the [calculation](#calculation) result, the value will be replaced.

### Calculation

This is the calculation settings where you can enter and configure the required expression. It can consist of constant numbers and various variables based on the values of fields or items.

![Value calculator](gh-elements/calculator-calculation.gif "Value selection")

Name|Description
:---|:---
Constants|allows you to enter the constant numbers; [details...](#constants)
Standard Calculator Buttons|allows to enter the basic calculation element and numbers; [details...](#standard-calculator-buttons)
Field|allows to select the value from which the value will be taken for calculation; [details...](#field)
IF|allows to use the value with the conditions; [details...](#if)
Items count|allows to use the number of items in calculation; [details...](#items-count)
SUM|allows to get the sum of all value of the selected field; [details...](#sum)
\<|allows to erase one element of the expression; [details...](#clear-element)
Clear|allows you to erase expressions completely; [details...](#clear)

#### **Constants**

This is the set of buttons which allows you to enter the static numeric values. You can also enter decimals using the decimal point button.

![Entering numbers](gh-elements/calculator-constants.gif "Constants")

#### **Standard Calculator Buttons**

The calculator element offers basic math operations. That's why there are buttons with the appropriate symbols.

![Mathematical operations](gh-elements/calculator-math-symbols.gif "Math symbols")

#### **Field**

This function allows you to add a variable that gets a value from the field of the current item.

![Field settings](gh-elements/calculator-field.jpg "Field")

Name|Description
:--|:--
Field|allows you to select the field from which value will be taken.
Interpretation Type|allows you to select the type of the value. There are only two available types: **Interpreted Value** and **Raw Value**.

#### **IF**

The current function allows you to add a variable with a condition. With it, you can get the value of a field or field value **if the item meets a certain condition**.

![If settings](gh-elements/calculator-if.jpg "IF")

Name|Description
:--|:--
Default Value|allows you to enter the value that will be used in cases when the source field is empty. So this is the default value for the **Field mode**.
Mode|allows you to select the mode of the IF calculation. That is, here you can choose which type of value will be used if the condition is met: **Field If Value** or **Value**
Value|here you have to enter the value or select the field with the required value depending on the mode
Conditions|allows you to set the conditions in which the **Value** will be used
Edit|contains two buttons which allows you to edit or delete the value and its condition

#### **Items Count**

This function allows to use the number of items of the selected application in calculation.

![Item count settings](gh-elements/calculator-item-count.jpg "Item count")

Name|Description
:--|:--
App|allows you to select the application the item of which will be counted.
Add Filter|allows you to add filters to determine which items will be taken in the calculation.. Different filters are used here to set the type of the items which you want to count.

#### **SUM**

This function allows you to get the sum of the values of a certain field from all items. It also allows you to set conditions for fields and sum not all values.

![Sum settings](gh-elements/calculator-sum.jpg "SUM")

Name|Description
:--|:--
App|allows you to select the application items of which will be used in the calculation. All values for calculating the sum are stored here.
Field|allows you to select the fields whose values will be summed. This field must contain numeric values, so select the [Number element](numbers.md).
Add Filter|allows you to add filters that will determine the items that will be used in the calculation. Thanks to this, you can get the sum of a property by a certain condition.

#### **Clear Element**

This button removes the last one element of the expression after clicking it.

#### **Clear**

This the button that allows to erase all the expression.

## Element Style

In set of style settings, besides [standard options](../Understanding_Gudhub/GH_Element/setting_overview.md), the calculator has only one unique setting, which slightly changes its [interpretation](#interpretation).

![Style of calculator element](gh-elements/calculator-element-style.jpg "Calculator element style")

### General Settings

The general style settings of calculator contains one additional option that allows to expand the functionality of the element.

![General settings of calculator style](gh-elements/calculator-general-settings.jpg "Calculator general style settings")

Name|Description
:---|:---
Update button|allows to add the update button

#### **Update Button**

This is the button that allows to update the field value. It is only useful when the [One Time Generation](#one-time-generation) function is enabled.

![Using the update button](gh-elements/calculator-update-button.gif "Update button")

This option allows the user to control the change of the calculator value. In addition, it allows the user to determine after which variable update the value will be saved.

## Filtration

The current element can be filtered by value using these filters:

- [Equals(or)](../Core_API/Filter/equal_filters.md)
- [Equals(and)](../Core_API/Filter/equal_filters.md)
- [Not equals(or)](../Core_API/Filter/equal_filters.md)
- [Not equals(and)](../Core_API/Filter/equal_filters.md)
- [Bigger than](../Core_API/Filter/bigger_lower_filters.md)
- [Lower than](../Core_API/Filter/bigger_lower_filters.md)
- [Between](../Core_API/Filter/range.md)
- [Value](../Core_API/Filter/value.md)

## Interpretation

The calculator look is quite similar to the simple [number](numbers.md). Namely, its interpretation types display this element as a number.

![Types of calculator interpretation](gh-elements/calculator-interpretation-types.jpg "Calculator interpretation types")

### Default

The default type of interpretation displays allows the result of calculations with all additional signs, such as prefix, suffix, and precision.

### Value

The second interpretation type displays the raw field value. Namely, it is without additional signs.

## Value Format

The value depends of the [Persistent Value](#persistent-value) setting. When that is off, calculator does not save the any value. In other case, the calculation result with all additional elements entered in settings is saved as a field value.

```json
{
    "field_value": "4.00"
}
```

## Data Model

```json
{
    "data_model": {
        "arguments": [{
            "app_id": "27904",
            "sum_filters": []
        }],
        "expression": "5*6+ITEMSCOUNT()",
        "interpretation": [],
        "is_generate_once": 1,
        "is_persistent_value": 1,
        "precision": "2",
        "prefix": "",
        "suffix": ""
    }
}
```

Name|Type|Description
:---|:---|:---
arguments|`array`|*contains the settings of each of the arguments of the expression*
app_id|`string`|*contains ID of the source application*
sum_filters|`array`|*contains all the filters that determine which values will be used in the calculations*
expression|`string`|*contains full expression of the calculation*
interpretation|`array`|*contains all element interpretation types*
is_generate_once|`boolean`|*shows whether the value will be updated automatically*
is_persistent_value|`boolean`|*shows whether the calculated value will be saved as field value or not*
precision|`string`|*contains number of values after the point*
prefix|`string`|*contains value of the prefix*
suffix|`string`|*contains value of the suffix*
