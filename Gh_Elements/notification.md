# Notification

**Notification** is a gh-element that allows to send notifications about adding items or updating items of the selected applications.

## Functional Characteristic

The main function of this element is to send notifications about [some application updates](#event-type). This is especially useful for elements that update items or add new items, but do not display changes or are configured not to display them:

- [Update Items](update_items.md)
- [Add Items](add_items.md)
- [Smart Input](smart_input.md)

## Value Format

This element does not contain any value.

## Element Options

The notification element has two blocks of settings.

### Field Settings

Field settings of the notification element consist of two [standard settings](../Understanding_Gudhub/GH_Element/setting_overview.md) Field name and Name space, as well as one additional option.

![Settings of notifications field](gh-elements/notifications-field-settings.jpg "Notifications field settings")

Name|Description
:---|:---
Hide automatically|allows to turn on the automatically hiding of the pop-up window
<!-- tooltip-start auto_hide -->
#### **Hide Automatically**

When this function is on, the pop-up window with updates will hide automatically. This means that user do not have to click the cancel button to hide the pop-up window.
<!-- tooltip-end auto_hide -->
### Cases Settings

The current element can track updates of several applications at the same time and notifications can look different.

![Settings of notifications cases](gh-elements/notification-cases-settings.jpg "Notifications cases settings")

Name|Description
:---|:---
Application|allows you to select the application in which updates will be tracked; [details...](#application)
View name|allows to select thee view that will be displayed in the pop-up window; [details...](#view-name)
Conditions|allows to filter updates of which items will be tracked and which will not; [details...](#conditions)
Event Type|allows you to select the type of event that will be tracked; [details...](#event-type)
Edit|allows to edit or delete the case; [details...](#edit)

#### **Application**

This is the application whose [updates](#event-type) will be monitored. It is also the source of the [view](#view-name) for the pop-up window.

#### **View Name**

This is the selected view that will be displayed in the pop-up window.

#### **Conditions**

The current feature allows you to add filters that will determine which [events](#event-type) of which items will be tracked.

#### **Event Type**

There are two types of application updates that can be tracked. That updates are events:

- Item add
- Item update

#### **Edit**

The last column contains two buttons for editing and deleting the case.

## Element Style

Since the current element is not interactive, there are no additional style settings. This style block consists only of [standard options](../Understanding_Gudhub/GH_Element/setting_overview.md) and several [types of interpretation](#interpretation).

![Style of notification element](gh-elements/notifications-element-style.jpg "Notifications element style")

## Data Model

The element data model consists of two arrays:

```json
{
    "data_model": {
        "interpretation": [],
        "options": [{
            "app_id": "29655",
            "event_type": "gh_item_update,gh_items_add",
            "filters_list": [],
            "view_id": "1568475"
        }]
    }
}
```

Name|Type|Description
:---|:---|:---
interpretation|`array`|*contains all element interpretation types*
options|`array`|*contains all cases of applications and their updates*
app_id|`string`|*contains ID of the selected application*
event_type|`string`|*contains all selected types of events that are monitored*
filters_list|`array`|*contains all filters applied to the application items*
view_id|`string`|*contains ID of the app view*

## Filtration

Since the current element does not contain a value, it cannot be filtered.

## Interpretation

The current element has only one type of interpretation. This is the default type, which displays the element as an icon.

![Types of notifications interpretation](gh-elements/notifications-interpretation-types.jpg "Notifications interpretation types")
