# Field Element

**Field** is a gh-element that is used to get a list of fields from an application. It allows the user to get the ID and name of a particular field.

![Using field element](gh-elements/field-element.gif "Field")

The resulting fields can be further used in the operation of other elements or in [automation](../Understanding_Gudhub/Automation/automation_overview.md).

## Functional Characteristics

The use of this element is limited to configuring other elements or processes. In other words, in most cases, users do not need to use it as an independent gh element. At the same time, it can be used to advantage in setting up, for example, [automation](../Understanding_Gudhub/Automation/automation_overview.md).

So, using it, you will be able to receive a field from the user. Then process this data using [automation](../Understanding_Gudhub/Automation/automation_overview.md) and return a certain result, such as changing the value in the field selected by the user.

## Element Options

The field element options contains of, already familiar to us,field settings and main settings.

### Field Settings

Here we have the standard pair of settings: Field Name and Name Space. Read about them in [Setting Overview](../Understanding_Gudhub/GH_Element/setting_overview.md).

![Settings of field element](gh-elements/field-settings.jpg "Field settings")

### Main Settings

Due to these settings you can set how many field will be taken and from which application.

![Main settings of field element](gh-elements/field-main-settings.jpg "Field main settings")

Name|Description
:---|:---
Application|application from which the field will be taken; [details...](#application)
Multiple|allows to add multiple fields; [details...](#multiple)
<!-- tooltip-start application -->
#### **Application**

All fields are stored in one of the applications. So, to get a list of fields, you have to select the application where these fields are located. This option is intended for this purpose. Here you can select **one of the applications from your app list**.
<!-- tooltip-end application -->
<!-- tooltip-start multiple -->
#### **Multiple**

The current setting is a switch that determines how many values the current element can store. If enabled, **Field** can take several values at once.
<!-- tooltip-end multiple -->
![Using the multiple setting](gh-elements/field-element-multiple.gif "Multiple value")

All fields are retrieved from the application selected in the [previous setting](#application).

## Element Style

As well as most of the gh-element, this one has only standard style options and a few [interpretation types](#interpretation). About standard style settings read in [Setting Overview](../Understanding_Gudhub/GH_Element/setting_overview.md).

![Style of field element](gh-elements/field-element-style.jpg "Field element style")

## Filtration

The field can be filtered out by only those ones which compare full values:

- [Equals(or)](../Core_API/Filter/equal_filters.md)
- [Equals(and)](../Core_API/Filter/equal_filters.md)

## Interpretation

In terms of interpretation, the field is very similar to text element. Namely, it has two types, editable and uneditable, too.

![Types of field interpretation](gh-elements/field-interpretation-types.jpg "Field interpretation types")

### Input

This is the interpretation type that allows user to set needed field from the certain application. In simple terms, it makes the value of the field editable.

### Default

This interpretation type allows to display the field value and won't let user edit it.

## Value Format

The field element accepts the IDs of fields from selected application. If the value is multiple, IDs are separated by commas.

```json
{
    "field_value": "645630,594814"
}
```

## Data Model

The field element contains only two extra properties, apart from interpretation, in t=its data model:

```json
{
    "data_model": {
        "app_id": "26506",
        "interpretation": [],
        "multiple_value": 1
    }
}
```

Name|Type|Description
:---|:---|:---
app_id|`string`|*ID of the application from which fields are taken*
interpretation|`array`|*contains all element interpretations*
multiple_value|`boolean`|*shows whether the field accepts multiple values*
