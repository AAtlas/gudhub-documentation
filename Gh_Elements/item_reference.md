# Item Reference

**Item Reference** is a gh-element that allows the user to select an item from a specific application and store a reference to it. This is a useful tool for connecting items from different applications to each other.

![Using the item reference](gh-elements/item-reference-work.gif "Item reference")

This gh-element provides a list of items for the [selected application](#application) in the settings. Items in the list have titles that are pulled from a specific field. So as soon as the user selects an item, a [reference is generated](#value-format) based on it and stored in the same field.

>If the [application](#application) is **not public** and the **user does not have access to it**. Then **nothing will be displayed** in the field, neither the list of items nor the previously saved values.

To delete a value, the user just needs to click on the cross that appears when the cursor is placed over the field.

## Functional Characteristic

This is quite unique element. Besides its direct purpose, it is used to customize many other gh-elements. For example, [Add Items](./add_items.md) has one additional customization based on links. There are even gh elements whose operation is based on the **Item Reference**. These are elements such as [Data Reference](./data_reference.md), [Item Remote Add](./item_remote_add.md).

As mentioned above, this element is used to connect applications to each other. For example, it allows you to link your tasks to specific projects. But if you use the **Item Reference** with other elements, you can expand this functionality. Together with [Table](./table.md), it can be configured so that the table displays all tasks attached to the project that the user is currently viewing.

Thus, using various combinations of gh-elements together with the **Item Reference**, you can develop complete systems of two or more applications.

## Element Options

Element options of item reference consists of only two blocks, Field and Reference settings. As you can see, this gh-element does not have default settings and default value correspondingly. Though it has extra settings for options from Reference Settings.

### Field Settings

In field settings, item reference has only two options, besides the standard Field Name and Name Space.

![Field settings of the item reference](gh-elements/item-reference-field-settings.jpg "Item reference field settings")

Name|Description
:---|:---
Multiple value|allows to add multiple value; [details...](#multiple-value)
Express add|allows to add entered value to the source item, if it does not exist there; [details...](#express-add)
<!-- tooltip-start multiple_value -->
#### **Multiple value**

The first element setting is responsible for the value type. That is you can select whether the reference field will store single or multiple value.
<!-- tooltip-end multiple_value -->
Since this is a switch, if it is enabled, the user can select more than one value to store in the field. Otherwise, only one value can be saved.
<!-- tooltip-start express_add -->
#### **Express add**

**Express Add** is a setting that allows users to create new items using **Item Reference**. If the item the user is looking for does not exist, they can create it. This is possible if the current setting is enabled.
<!-- tooltip-end express_add -->
![Using the express add](gh-elements/item-reference-express-add.gif "Express add")

It works like this: when a user enters the name of a non-existent item, an add button appears in the drop-down list. After the user clicks on it, a new item will be created in the [source application](#application).

This setting provides similar functionality to the [Add Item element](./add_items.md) and even has similar [additional settings](#settings).

### Reference Settings

The next block of settings is responsible for reference configuration. This is where you define which applications the current gh-element can refer to. This is also where you configure the drop-down list of options for **Item Reference**.

![Settings of the item reference](gh-elements/item-reference-settings.jpg "Item reference settings")

Name|Description
:---|:---
Application|allows to select the source application; [details...](#application)
Field|allows to select the source field; [details...](#field)
View|allows to select the view that will be opened; [details...](#view)
Filter|allows to add filters for items; [details...](#filter)
Settings|contains additional settings of the [express add](#express-add); [details...](#settings)
Edit|contains two buttons that allows edit and delete option; [details...](#edit)

#### **Application**

The first option allows you to select the source application. This is an application whose items will be referenced by the current gh-element. All the following settings will also pull data from the current application.

>The ID of this application will be part of the reference [value](#value-format).

#### **Field**

Every item in the **Item Reference** has their own titles. These titles are taken from a specific field in the [source application](#application). That field have to be selected here. This should be a field that stores data that can characterize the item, such as [Text](./text.md). This way, the user can easily find the required item of the [selected application](#application).

#### **View**

When the user selects any item, they can open it by clicking on the Item Reference value. But first, you have to choose the view that will be opened in the **current setting**. All *views* from the [selected application](#application) are available for selection.

#### **Filter**

This setting allows you to specify which items are available in the **Item Reference** drop-down list. You set the conditions by which the required items will be defined and only they will be displayed in the list.

This setting works like [Filter element](./table_filter.md). In other words, you set up filters that select the required items.

#### **Edit**

The last setting of this block is **Edit**. This is the default setting for such settings blocks. It consists of two buttons that provide two different functions: edit an option and delete an option.

## Settings

As was mentioned above, this group of settings are advanced settings of the [express add](#express-add). Specifically, it allows you to set the default values of any field in an element, just like in [Add Items element](add_items.md).

### Additional Reference Settings

With these settings, you can select the item reference field from [other application](#application) and leave notes about settings of the current.

![Additional settings](gh-elements/item-reference-additional-settings.jpg "Item reference settings")

Name|Description
:---|:---
Field for reference|allows to select the item reference setting; [details...](#field-for-reference)
Notes|allows to note something in settings; [details...](#notes)
<!-- tooltip-start field_for_reference -->
#### **Field for reference**

**Field for reference** is a setting that is used to connect the current item with the item created by [Express Add](#express-add). To do this, you need to select a field in the [destination application](#application) where the reference to the current item will be stored.
<!-- tooltip-end field_for_reference -->
>Therefore, this field will be automatically filled with a value when user creates an item using [Express Add](#express-add).

#### **Notes**

This is the only setting that does not affect the operation of the element. This is a useful field for no-code developers. Here you can leave any notes about the current gh element or other notes. This will help you and your colleagues remember or recognize what this element is responsible for.

### Additional Fields Settings

The current group configures default values for fields. Depending on [Show Input](#show-input) the table has two views:

1. Source Field

    ![Show input is off](gh-elements/item-reference-fields-settings.jpg "Fields settings with Source field")

1. User Value

    ![Show input is on](gh-elements/item-reference-fields-settings-show-input.jpg "Fields settings with User value")

Name|Description
:---|:---
Field|allows to select the destination field; [details...](#destination-field)
Show Input|allows to select the type of field value; [details...](#show-input)
Source Field|allows to select the field whose value will be taken for filling other field; [details...](#source-field)
User value|allows to enter the value for the field; [details...](#user-value)

#### **Destination Field**

This is the first setting of the current block. It determines which field will be filled in when user creates an item using [Express Add](#express-add). Therefore, the following settings will determine the default value for the current field.

#### **Show Input**

The current setting allows you to change the default value source for the [selected field](#destination-field). The value for a field can be either static or dynamic. A separate setting is responsible for each of them: [User Value](#user-value) and [Source Field](#source-field), respectively. You can switch between them using the **Show Input** setting.

>It is disabled by default.

If it is enabled, the [User Value](#user-value) is available. Otherwise, the [Source Field](#source-field) is used.

#### **Source Field**

This is one of the settings that is responsible for filling in the [field](#destination-field). It allows you to select the field of the current application. Its value will be used by default for the item created with [Express Add](#express-add). This option takes the field value from the current item and fills it in the newly created item in the destination application.

#### **User Value**

Unlike the previous setting, this allows you to set one specific value for a field. That is, this value will be the same for all new items created by the [Express Add](#express-add).

>To make this setting appear, turn on the [Show Input](#show-input).

## Element Style

Despite the different types of source fields, Item Reference has a [standard set of style settings](../Understanding_Gudhub/GH_Element/setting_overview.md) and one additional setting. Its [interpretation types](#interpretation) are also independent of the source field.

![Style of the item reference](gh-elements/item-reference-element-style.jpg "Item reference element style")

### General Settings

The only additional style setting is located in general settings.

![General settings of element style](gh-elements/item-reference-general-settings.jpg "Item reference general style settings")

Name|Description
:---|:---
No Link|allows to disable the link

#### No Link

Usually, user can open the item they have been selected in the current field. But the current setting allows you to disable references in values. So, when the **No Link** is enabled, user can only select the item, not open it.

## Filtration

Despite the complexity of item references, there are five filters that can filter them out:

- [Equals(or)](../Core_API/Filter/equal_filters.md)
- [Equals(and)](../Core_API/Filter/equal_filters.md)
- [Not equals(or)](../Core_API/Filter/equal_filters.md)
- [Not equals(and)](../Core_API/Filter/equal_filters.md)
- [Value](../Core_API/Filter/value.md)

## Interpretation

Item reference has a few not very different interpretations.

![Interpretation types of the item reference](gh-elements/item-reference-interpretation-types.jpg "Item reference interpretation types")

### Input With Name

This type of interpretation allows to display and change selected value. Due to it you also can delete value from the field.

### Default

Default interpretation for item reference is the same to the one above.

### Input List

The last interpretation type for item reference also allows to display selected value, but not to change or delete it.

### Value

The last interpretation type displays the value of the selected item.

## Value Format

Item reference value looks like this:

```json
{
    "field_value": "23823.2992890"
}
```

It consists of two parts:

- application ID: *23823*

- item ID: *2992890*

The both IDs belong to the source application.

Item reference is used mostly in different methods. Due to this we can connect a few applications, update linked data without any problems.

## Data Model

The data model of the current element consists of element settings:

```json
{
    "data_model": {
        "express_add": 1,
        "interpretation": [],
        "multiple_value": false,
        "refs": [{
            "app_id": "38996",
            "field_id": "645003",
            "filters_list": [],
            "settings": {
                "notes": "Your notes",
                "reference_field_id": "623002",
                "refs": [{
                    "field_id": "643402",
                    "source_field_id": "683450"
                }]
            }
        }],
        "view_id": "1635936"
    }
}
```

Name|Type|Description
:---|:---|:---
express_add|`boolean`|*shows whether the express add is used or not*
interpretation|`array`|*contains all interpretation types*
multiple_value|`boolean`|*shows whether the element accepts multiple value or not*
refs|`array`|*contains all the settings of the reference instances*
app_id|`string`|*contains ID of the source application*
field_id|`string`|*contains ID of the source field*
filters_list|`array`|*contains all filters of the source items*
settings|`object`|*contains additional settings of the express add*
notes|`string`|*contains notes of the express add*
reference_field_id|`string`|*contains ID of the item reference field*
refs|`array`|*contains additional settings of reference of the express add*
field_id|`string`|*contains ID of the destination field*
source_field_id|`string`|*contains ID of the source field*
view_id|`string`|*contains ID of the source view*
