# Range Element

**Range** is a gh-element that is represented as a slider with established number boundaries. It allows user to save a specified numerical boundaries.

## Functional Characteristics

Use this element to select the number of items in the online store, assess the quality of service, or set price limits.

## Value Format

Range element value format depends on the range type. If it is Single, value format will be a maximum number in string format:

```json
{
    "field_value": "7"
}
```

If range type is Double, value will be range limits separated by a colon in string type.

```json
{
    "field_value": "4:10"
}
```

## Element Options

Element options of the range element are contains:

- **Field Settings**
- **Range Settings**

Range Field Settings consists only of the standard options: Field Name and Name Space.

![Range and Field settings of the range element](gh-elements/range-field-settings.jpg "Range and Field settings")

### Range Settings

It is a set of options that allows to configure the range.

Name|Description
:---|:---
Min|allows to set minimum range value; [details...](#min)
Max|allows to set maximum range value; [details...](#max)
Precision|allows to set range precision; [details...](#precision)
Range type|allows to set which type of range will be displayed; [details...](#range-type)
<!-- tooltip-start min -->
#### **Min**

These two options allow you to limit the minimum value of the range.
<!-- tooltip-end min -->
<!-- tooltip-start max -->
#### **Max**

Allows you to set the maximum value of the range scale.
<!-- tooltip-end max -->
<!-- tooltip-start precision -->
#### **Precision**

Due to this option you can adjust the range precision. Like a number element, it can have no more than three digits after the dot.
<!-- tooltip-end precision -->
<!-- tooltip-start range -->
#### **Range type**

The range types allow to set whether the user could correct the both limits of the range or only the maximum one.
<!-- tooltip-end range -->
## Element Style

Range element has no special options in element style. About all standard settings you can read in [Setting Overview](../Understanding_Gudhub/GH_Element/setting_overview.md).

![Style of the range element](gh-elements/range-element-style.jpg "Range element style")

## Data Model

Range element data model is very similar to numbers, but much easier:

```json
{
    "data_model": {
        "interpretation": [],
        "precision": "0",
        "range": {
            "min": 0, 
            "max": 10
        },
        "type": "double"
    }
}
```

Name|Type|Description
:---|:---|:---
interpretation|`array`|*contains all element interpretations*
precision|`string`|*contains value of the precision*
range|`object`|*contains the limits of range*
min|`number`|*contains the minimum value of the range*
max|`number`|*contains the maximum value of the range*
type|`string`|*contains selected type of range*

## Filtration

The rang element can be filtered with the same filters as [numbers](numbers.md):

- [Equals(or)](../Core_API/Filter/equal_filters.md)
- [Equals(and)](../Core_API/Filter/equal_filters.md)
- [Not equals(or)](../Core_API/Filter/equal_filters.md)
- [Not equals(and)](../Core_API/Filter/equal_filters.md)
- [Is bigger than](../Core_API/Filter/bigger_lower_filters.md)
- [Is lower than](../Core_API/Filter/bigger_lower_filters.md)
- [Between](../Core_API/Filter/range.md)
- [Value](../Core_API/Filter/value.md)

## Interpretation

Range element has two interpretation types, which are different for each type of range.

- Double range type:

    ![Default type of interpretation of the double range type](gh-elements/range-interpretation-types-double.jpg "Default interpretation type for Double range type")

- Single range type:

    ![Default type of interpretation of the single range type](gh-elements/range-interpretation-types-single.jpg "Default interpretation type for Single range type")

### Default

This is a type of interpretation that allows the gh range elements to be displayed as a slider and one/two entering fields.

### Value

This type displays the set value of the element.
