# Slider

**Slider** is a gh-element that allows to make a slideshow of images from items. Due to it, the images can be swiped like a slider.

## Functional Characteristics

This element can be used to display advertising banners or to display products.

## Value Format

Slider does not contain any value.

## Element Options

Slider settings are consists of three blocks.

### Field Settings

The first block of settings configures the main aspects of the element. It also contains [standard field settings](../Understanding_Gudhub/GH_Element/setting_overview.md).

![Settings of slider element](gh-elements/slider-field-settings.jpg "Slider field settings")

Name|Description
:---|:---
Application|allows to select the source application; [details...](#application)
Image|allows to select the source field; [details...](#image)
Height|allows to set the element height; [details...](#height)
<!-- tooltip-start application -->
#### **Application**

This is the application from which all needed data will be taken.
<!-- tooltip-end application -->
<!-- tooltip-start image -->
#### **Image**

This function allows to select the field from which images will be taken for slideshow.
<!-- tooltip-end image -->
<!-- tooltip-start height -->
#### **Height**

You can set the slider height due to this function. There are seven possible values:

- 64
- 128
- 256
- 384
- 512
- 768
- 1024
<!-- tooltip-end height -->
### Advanced Settings

This group of settings allows you to add a link and an image name to the item image, in addition to the main settings.

![Advanced settings of slider element](gh-elements/slider-advanced-settings.jpg "Slider advanced settings")

Name|Description
:---|:---
Show URL|allows to add link to the image; [details...](#show-url)
URL|allows to select the field from which links wil be taken; [details...](#url)
Show Text|allows you to show the text associated with the image; [details...](#show-text)
Text|allows to select field from which text will be taken; [details...](#text)
<!-- tooltip-start show_url -->
#### **Show URL**

This setting allows you to add URLs to images. When a user clicks on an image, he goes to a specific link.
<!-- tooltip-end show_url -->
<!-- tooltip-start url -->
#### **URL**

This is the setting where you have to enter the link which will be used in the slider.
<!-- tooltip-end url -->
<!-- tooltip-start show_text -->
#### **Show Text**

This function allows to configure whether any text will appears when user hover his/her cursor over the image in the slider.
<!-- tooltip-end show_text -->
<!-- tooltip-start text -->
#### **Text**

This is the text that will be displayed when user hover his/her cursor over the image in the slider.
<!-- tooltip-end text -->
### Filter

This block contains the standard filter element. Due to this, you can customize which items will be displayed by using different filters.

![Filter of the slider element](gh-elements/slider-filter.jpg "Slider filter")

## Element Style

The slider is the element whose [interpretation types](#interpretation) are more useful for its style than all [other style settings](../Understanding_Gudhub/GH_Element/setting_overview.md), which are standard.

![Style of slider element](gh-elements/slider-element-style.jpg "Slider element style")

## Data Model

The slider data model contains all element settings:

```json
{
    "data_model": {
        "app_id": "28560",
        "filters_list": [],
        "height": "512",
        "image": {
            "field_id": "671841"
        },
        "interpretation": [],
        "text": {
            "active": 1,
            "field_id": "671831"
        },
        "url": {
            "active": 1,
            "field_id": "679766"
        }
    }
}
```

Name|Type|Description
:---|:---|:---
app_id|`string`|*contains ID of the source application*
filters_list|`array`|*contains all filters that are applied to the element*
height|`string`|*contains value of height of the slider*
image|`object`|*contains settings of the image for slider*
field_id|`string`|*contains ID of the field from which images will be taken for slider*
interpretation|`array`|*contains all element interpretation types*
text|`object`|*contains text settings*
active|`boolean`|*shows whether the text will be shown in slider or not*
field_id|`string`|*contains ID of the from which text will be taken for slider*
url|`object`|*contains URL settings*
active|`boolean`|*shows whether links will be used in slider*
field_id|`string`|*contains ID of the field which URL will be taken for slider*

## Filtration

This element cannot be filtered out.

## Interpretation

Both types of slider interpretation display images of items and allows to [follow specific links](#show-url), but they still have few differences:

![Types of slider interpretation](gh-elements/slider-interpretation-types.jpg "Slider interpretation types")

### Logos

The first type of interpretation displays images of items with gaps between them. It also do not allow image names be displayed.

![Logos interpretation type](gh-elements/slider-logos.jpg "Slider Logos")

### Projects

This type displays images without gaps. In addition, when you hover your cursor over any image, it is shaded and [its name](#show-text) is displayed on it.

![Projects interpretation type](gh-elements/slider-projects.jpg "Slider Projects")
