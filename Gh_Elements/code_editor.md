# Code Editor

**Code Editor** is a gh-element that allows to enter, save, and edit code. It is a useful tool for work with code in your application.

![Using the code editor](gh-elements/code-editor-element-use.gif "Entering the code")

The entered code will be highlighted depending on its syntax. This will help you work with the code.

>Please note that this element only supports HTML and JS syntax. And for each of them, you need to use a [different mode](#code-mode).

![Increase the number of lines after adding a code](gh-elements/code-editor-lines.gif "Adding lines")

By default, the text editor has 17 lines. But if the code is longer, additional lines will be added.

## Functional Characteristics

The main ways to use this element are quite obvious. The **Code Editor** is used to save and edit code. The saved code can be extracted and used in other [automation elements](../Understanding_Gudhub/Automation/automation_overview.md) or processes. In fact, it copies the classic code editor. So if you know how it works, you won't have any problems using it.

## Element Options

The code editor element has not a big number of settings that can configure it and all of them are contained in one group.

### Field Settings

The only configuration settings of the code editor consist two [standard](../Understanding_Gudhub/GH_Element/setting_overview.md) and one additional options.

![Settings of code editor field](gh-elements/code-editor-field-settings.jpg "Code editor field settings")

Name|Description
:---|:---
Code Mode|allows to select the operating mode of the element
<!-- tooltip-start code_mode -->
#### **Code Mode**

The **Code Editor** can work with two types of code. This element has different work modes for both of those types:

- HTML
- JavaScript

After selecting one of the modes, the entered code will be highlighted according to the code syntax.
<!-- tooltip-end code_mode -->

![Change the mode](gh-elements/code-editor-modes.gif "Modes")

This allows you to work comfortably with the code.

## Element Style

Element style of the code editor does not have any additional options. It can only be configured using [standard settings](../Understanding_Gudhub/GH_Element/setting_overview.md). The single interpretation type is shown in [the end of the article](#interpretation).

![Style of code editor element](gh-elements/code-editor-element-style.jpg "Code editor element style")

## Filtration

This element cannot be filtered out.

## Interpretation

The only interpretation type allows the element to be displayed the space where the code is entered and where it could be updated.

![Types of code editor interpretation](gh-elements/code-editor-interpretation-types.jpg "Code editor interpretation types")

## Value Format

When the user enters the code, GudHub creates a new file where this code is saved. And exactly the ID of that file is a field value of the code editor element.

```json
{
    "field_value": "940407"
}
```

All such files are contained in the file list of the application where the element is located.

## Data Model

The half of properties in the data model cannot be configured. They have a constant default value.

```json
{
    "data_model": {
        "code_mode": "htmlmixed",
        "interpretation": [],
        "save_file": true,
        "theme": "default"
    }
}
```

Name|Type|Description
:---|:---|:---
code_mode|`string`|*contains selected code mode*
interpretation|`array`|*contains all element interpretation types*
save_file|`boolean`|*shows whether the file will be saved or not*
theme|`string`|*contains the element theme*
