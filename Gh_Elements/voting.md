# Voting

**Voting** is a comfortable tool for automatic calculation of the users votes and save the information about that. The vote is counted only after clicking on the icon.

## Functional Characteristics

Use this element to vote for any topic. It's also handy for collecting likes under posts.

## Value Format

The value of the voting element are the IDs of the user who had voted, separated by commas.

```json
{
    "field_value": "1547"
}
```

## Element Options

The voting options consists of one group - Field Settings. It contains its own unique set of options.

### Field Settings

The main settings of configuration the voting element are contained here. Two of them are the standard Field Name and Name Space.

![Settings of voting field](gh-elements/voting-field-settings.jpg "Voting field settings")

Name|Description
:---|:---
Icon|allows to select icon which will be displayed; [details...](#icon)
Show Legend|allows users to see others who voted; [details...](#legend)
Icon Color|allows to select icon color; [details...](#icon-color)
<!-- tooltip-start icon -->
#### **Icon**

This button allows to open the catalogue of different icons and to select which of them will be displayed in application.
<!-- tooltip-end icon -->
<!-- tooltip-start show_legend -->
#### **Legend**

The legend is a list of users who voted. It is displayed in application as a black popup list with users names and icons.
<!-- tooltip-end show_legend -->
<!-- tooltip-start icon_color -->
#### **Icon Color**

This option allows you to set the color of the icon with the help of color picker.
<!-- tooltip-end icon_color -->
## Element Style

As you can see, this element does not have special options of its style, only have standard settings about that you can read in [Setting Overview](../Understanding_Gudhub/GH_Element/setting_overview.md). As for its interpretations, they are described [below](#interpretation).

![Style of voting element](gh-elements/voting-element-style.jpg "Voting element style")

## Data Model

This element has quite big data model. Part of its properties related to the icon.

```json
{
    "data_model":{ 
        "icon": "love_heart",
        "icon_color": "#18a9e8",
        "interpretation": [],
        "min_digits_count": -1,
        "show_legend": 1
    }
}
```

Name|Type|Description
:---|:---|:---
icon|`string`|*name of selected icon*
icon_color|`string`|*selected color of icon*
interpretation|`array`|*contains all interpretation types of voting*
min_digits_count|`number`|*minimum number of digits*
show_legend|`boolean`|*shows voting legend is displayed or not*

## Filtration

Since the voting element always has a value, it can be filtered out by only that filters that search by comparing full values.

- [Equals(or)](../Core_API/Filter/equal_filters.md)
- [Equals(and)](../Core_API/Filter/equal_filters.md)

## Interpretation

The voting element has only three types of interpretation.

![Types of voting interpretation](gh-elements/voting-interpretation-types.jpg "Voting interpretation types")

### Default

The default type allows users to vote and accordingly update the value.

### Plain text

This interpretation allows only display the number of votes. Namely, it makes the element uneditable.

### Value

The last interpretation type displays the value of the field.
