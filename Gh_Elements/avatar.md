# Avatar

**Avatar** is a gh-element that allows you to create a header image with a person's name for your items. It is most often used in conjunction with the [Conversation element](./conversation_element.md). In this case, the avatar is a classic chat avatar.

![Using the avatar](./gh-elements/avatar-work.jpg "Avatar usage")

It has no special purpose other than to summarize information about the user. If you use it with [Conversation](./conversation_element.md), it can also be filled in automatically if you make the source fields for the avatar [image](#images-field) and [name](#user-name-field) the [destination fields for Conversation](https://gudhub.com/docs/gh-elements/conversation_element/#conection-settings).

## Functional Characteristics

As mentioned above, this element is mostly used to create full-fledged chats with [Conversation](./conversation_element.md) and [Cards](./cards.md).

![Using avatar with conversation](./gh-elements/avatar-conversation.jpg "Avatar in chats")

But it can also be used to represent the content of items. Namely, since an avatar is the most useful for people's information, it can display a photo and a person's name at the top of the item.

![Using the avatar in items](./gh-elements/avatar-item.jpg "Avatar in items")

## Element Options

The settings for the current element consist of only one set of settings.

### Field Settings

This is a set of settings that provides all the necessary settings for configuring the element.

![Settings of avatar field](gh-elements/avatar-field-setting.jpg "Field settings")

Name|Description
:---|:---
Application|allows you to select the source application for the element; [details..](#application)
Images Field|allows you to select field that stores required images; [details..](#images-field)
User Name Field|allows you to select the source field for the user name; [details..](#user-name-field)
Show User Name|allows you to set whether the user name will displayed or not; [details..](#show-user-name)
<!-- tooltip-start application -->
#### **Application**

This is the setting where you have to select the application for the following element settings. The source fields for an [image](#images-field) and a [user name](#user-name-field) will be taken from the current app.
<!-- tooltip-end application -->
<!-- tooltip-start images_field_id -->
#### **Images Field**

The next setting allows you to select the multiple fields from which the images for the avatar will be taken. This is the image that will be displayed inside in the circle. Here you can select several fields, and the image will be taken from one of them.

> Note that **all these fields must be [Image](./image.md) elements**.
<!-- tooltip-end images_field_id -->
Of all the fields, priority is given to the first selected one. That is, if three fields are connected here and all of them have images, the image will be taken from the field that was selected first in this setting.

![Image priorities](./gh-elements/avataer-image-field.gif "Multiple images")

>If all fields are empty, an abbreviation of the [User Name](#user-name-field) will be displayed in the circle.
<!-- tooltip-start user_name_field_id -->
#### **User Name Field**

Here you have to select the field that contains the name of the user. The value of the selected field will be displayed below the avatar circle if the [Show User Name](#show-user-name) setting is enabled. But the main role it plays is in abbreviation. When there is no photo in the avatar circle, two capital letters taken from the value of the current field will be displayed there:

- the first letter of the first word;
- the first letter of the last word.
<!-- tooltip-end user_name_field_id -->
<!-- tooltip-start show_name -->
#### **Show User Name**

The last setting of this element allows you to show or hide the [User Name](#user-name-field). If the current setting is enabled, the [user name](#user-name-field) is displayed below the avatar circle. This text can be customized with some [style settings](#element-style). If the setting is disabled, the [user name](#user-name-field) will not be displayed at all.
<!-- tooltip-end show_name -->
## Element Style

This element has a [full set of style settings](../Understanding_Gudhub/GH_Element/setting_overview.md). That is, there are no additional settings that would greatly affect the appearance of the element. The only customization to which the current element is subject is to change the [interpretation types](#interpretation) and [Show User Name](#show-user-name) setting.

![Settings of avatar style](gh-elements/avatar-element-style.jpg "Style settings")

## Filtration

The **Avatar** is not available for filtering.

## Interpretation

Interpretations of this element are only responsible for the size of the avatar. There are three sizes in total and, accordingly, three types of interpretation.

![Types of avatar interpretation](gh-elements/avatar-interpretation-types.jpg "Interpretation types")

### Default

The first type of interpretation is the default type. It makes the avatar the largest of all interpretations. That is 64 pixels in diameter.

### Medium

The next type allows you to create a medium-sized avatar. The diameter is 32 pixels.

### Small

The last interpretation makes the element the smallest in size, namely 24 pixels in diameter.

## Value Format

This element does not store any value.

## Data Model

The current gh-element has a rather small data model:

```json
{
    "data_model": {
        "images_app_id": "34166",
        "images_field_id": "810583, 810456",
        "interpretation": [],
        "show_name": 1,
        "user_name_field_id": "810519"
    }
}
```

Name|Type|Description
:---|:---|:---
images_app_id|`string`|*contains the ID of the application from which the photo and name for the element are taken*
images_field_id|`string`|*contains the ID of the field in which the image is stored*
interpretation|`array`|*contains all element interpretations*
show_name|`boolean`|*shows whether the required name will be displayed or not*
user_name_field_id|`string`|*contains the ID of the field in which the name for element is stored*
