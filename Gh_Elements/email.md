# Email

**Email** is a gh-element that allows user to enter and store an email. The value entered in this field is checked for the correct format. The check is performed using a special regular expression.

![Using the email element](gh-elements/email-element.gif "Email element")

In addition, one of [interpretations](#interpretation) makes emails interactive. This means that the values saved here become links that automatically open the email application with the created email to the saved address when user clicks on them.

## Functional Characteristics

This element is mostly used to store email addresses. When paired with the [Phone](./phone.md) element, it is used to store contact information for a person or company. On the other hand, if it is combined with a [Password](./password_element.md) element, they can be used as a login menu. For this pair of gh-elements, browser password autofill is available.

## Element Options

Email element does not have unique options and default values. There are only field settings with standard Field Name and Name Space, and one familiar extra option.

![Field settings of the email element](gh-elements/email-field-settings.jpg "Email field settings")

Name|Description
:---|:---
Multiple|allows to add multiple values
<!-- tooltip-start multiple -->
### Multiple

The current setting allows you to determine how many values an item will accept for saving. If enabled, the user can enter multiple values in the field.
<!-- tooltip-end multiple -->
![Using multiple value](gh-elements/email-multiple-value.gif "Multiple value")

This setting provides the extra functions to the field. The adding button and editing button are always displayed next to the value. The user can see them by hovering the cursor over the value.

Other buttons appear only after the value is allowed to be edited. The user must click on the pencil-shaped button to edit the value. After that they can user three buttons:

- **Cancel** - a button in the form of a cross that allows user to close editing while saving the value.
- **Apply** - a checkmark button that saves the edited value.
- **Clear** - a trash can button that deletes the value and clears the field.

## Element Style

Email element style consists of only standard options about that you can read in [Setting Overview](../Understanding_Gudhub/GH_Element/setting_overview.md). And about interpretation read [below](#interpretation).

![Style of the email element](gh-elements/email-element-style.jpg "Email element style")

## Filtration

As a string type data, email can be filtered by:

- [Contains(or)](../Core_API/Filter/contain_filters.md)
- [Contains(and)](../Core_API/Filter/contain_filters.md)
- [Not contains(or)](../Core_API/Filter/contain_filters.md)
- [Not contains(and)](../Core_API/Filter/contain_filters.md)
- [Equals(or)](../Core_API/Filter/equal_filters.md)
- [Equals(and)](../Core_API/Filter/equal_filters.md)
- [Not equals(or)](../Core_API/Filter/equal_filters.md)
- [Not equals(and)](../Core_API/Filter/equal_filters.md)
- [Value](../Core_API/Filter/value.md)

## Interpretation

The current gh-element has only three interpretation types.

![Types of email interpretation](gh-elements/email-interpretation-types.jpg "Email interpretation types")

### Default

The default interpretation type for email element allows to display data in the item, update and delete it.

### Plain Text

This type allows to make email value uneditable.

### Value

This interpretation types displays the email field value.

## Value Format

Email does not accepts no other value than the ones that fit to a regular expression. Values could be either single or multiple. All of them are stored as a string. The multiple ones separated by a comma:

```json
{
    "field_value": "john@dow.com,johana@dow.com"
}
```

## Data Model

Email has a pretty simple data model. Besides interpretation and multiple, there is a special regular expression.

```json
{
    "data_model": {
        "interpretation": [],
        "multiple": 0,
        "regular_expression": "^[a-z0-9](.?[a-z0-9_-]){0,}@[a-z0-9-]+.([a-z]{1,6}.)?[a-z]{2,6}$"
    }
}
```

Name|Type|Description
:---|:---|:---
interpretation|`array`|*contains all email interpretations*
multiple|`boolean`|*shows whether the element accepts the multiple value*
regular_expression|`string`|*contains a regular expression that checks if the format of the value is correct*
