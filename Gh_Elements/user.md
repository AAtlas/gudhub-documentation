# User

**User** is a gh-element which allows to select the user and display his/her name and profile picture.

## Functional Characteristics

This item is used for GudHub users. It is used to bind an account to an item and therefore control access to items. This is useful for teamwork, for example, when assigning tasks.

## Value Format

This element accepts the application ID as a field value.

```json
{
    "field_value": "1271"
}
```

## Element Options

The element options are consists of two settings groups. For the most part, there are standard or familiar options here.

### Field Settings

The first group contains all the same default Field Name and Name Space.

![Settings of user element field](gh-elements/user-field-settings.jpg "User field settings")

### Default Value Settings

This is a group of standard settings that allows to configure the default value.

![Settings of user default value](gh-elements/user-default-value-settings.jpg "User default value settings")

Name|Description
:---|:---
Use default value|allows to use default value; [details...](#use-default-value)
Default value|allows to select the default value from users list; [details...](#default-value)
<!-- tooltip-start use_default_value -->
#### **Use Default Value**

This is the setting that allows you to use the default value for this element.
<!-- tooltip-end use_default_value -->
<!-- tooltip-start default_value -->
#### **Default Value**

The current element is created to enter the default value.
<!-- tooltip-end default_value -->
## Element Style

Besides the option described [below](#image-size), the user element has a set of [standard settings](../Understanding_Gudhub/GH_Element/setting_overview.md) to configure its style and its own unique [interpretation types](#interpretation).

![Style of user element](gh-elements/user-element-style.jpg "User element style")

### Image Size

Below the general style settings there are one extra option. It allows to select the size of the profile picture.

![General settings and image size of user element](gh-elements/user-general-settings.jpg "User general settings and Image size")

There are four available sizes:

- 64px
- 128px
- 256px
- 100%

## Data Model

The element data model contains these properties:

```json
{
    "data_model": {
        "default_field_value": 1547,
        "display_type": "photo_fullname",
        "interpretation": [],
        "photo_size": 64,
        "use_default_value": false,
    }
}
```

Name|Type|Description
:---|:---|:---
default_field_value|`number`|*contains the default field value*
display_type|`string`|*contains the type of the element display*
interpretation|`array`|*contains all interpretation types*
photo_size|`number`|*contains the size of the user photo*
use_default_value|`boolean`|*shows whether the default value is used or not*

## Filtration

Since this element has value, user can filter it out.

- [Equals(or)](../Core_API/Filter/equal_filters.md)
- [Equals(and)](../Core_API/Filter/equal_filters.md)
- [Value](../Core_API/Filter/value.md)

## Interpretation

The user element has many different interpretation types:

![Types of user element interpretation](gh-elements/user-interpretation-types.jpg "User interpretation types")

### Full Name

The first type allows you to display only the name of the selected user.

### Avatar

The second interpretation type is opposite to the previous one. It only displays the account picture of the selected user.

### Avatar With Name

This type allows the element to be displayed as an account picture and the user name below it.

### Icon

This type does not display user data. The element is displayed as an icon.

### Inline

This type displays the account picture with user name next to it.

### Value

The last interpretation type displays the selected value of the element.
