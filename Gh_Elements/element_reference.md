# Element Reference

**Element Reference** is a gh-element that allows you to use already configured elements from different applications. This allows you to save time on setting up an individual gh-element.

![Using the element reference](gh-elements/element-referrence-element.gif "Element reference")

>Note that you can duplicate items even from applications where you do not have [Admin or Owner permissions](../Rest_API/Sharing/permission_policy.md).

That is, you can duplicate items from applications to which you have any access permission except [Blocked](../Rest_API/Sharing/permission_policy.md).

The **Element Reference** works quite specifically. Even though the element and its settings are taken from another application, the [value](#value-format) is saved in the current application.

## Functional Characteristics

The current element is mostly used for creating applications. It can be used to duplicate element with lots of settings to save time. It also allows you to retrieve a configured field from another application. This is especially useful if you need to use the same field but don't have the required permissions to view its settings.

## Element Options

The element reference can be configured using only one group of settings.

### Field Settings

The main settings of the element are in the field settings with [standard options](../Understanding_Gudhub/GH_Element/setting_overview.md).

![Settings of element reference field](gh-elements/element-reference-field-settings.jpg "Element reference field settings")

Name|Description
:---|:---
Application|allows to select the source application; [details...](#application)
Field|allows to select the field for reference; [details...](#field)
<!-- tooltip-start application -->
#### **Application**

The first setting of the current element allows you to select application from your app list. The application selected here is used as the source for the **Element Reference**. The list of fields in this application will be used for the [next option](#field).

>So, we will take the gh-element for reference from this application.
<!-- tooltip-end application -->
<!-- tooltip-start field -->
#### **Field**

This option allows you to select the field from the [source application](#application). It will be duplicated in the current application. Here you can select absolutely any gh-element.
<!-- tooltip-end field -->
When you select a field and apply the settings, the customized element is displayed here and becomes available for work.

## Element Style

Like most gh-elements, this one has only [standard settings of the style](../Understanding_Gudhub/GH_Element/setting_overview.md) and a set of [interpretation types](#interpretation).

![Style of element reference](gh-elements/element-reference-element-style.jpg "Element reference style")

## Filtration

The element reference can be filtered with the same filters as the element to which the current element refers.

## Interpretation

This element has only two types of interpretation:

![types of element reference interpretation](gh-elements/element-reference-interpretation-types.jpg "Element reference interpretation types")

### Default

This type allows to edit the element value.

### Plain Text

The second type of interpretation makes the value of the current item unavailable for editing.

## Value Format

The value of the field depends on the gh-element to which the current element is referencing. For example, if the source element is [Text](text.md), the current value will be the string of the entered text.

## Data Model

The data model of the current element contains all needed IDs and an array of interpretations:

```json
{
    "data_model": {
        "app_id": "28752",
        "field_id": "634545",
        "interpretation": []
    }
}
```

Name|Type|Description
:---|:---|:---
app_id|`string`|*contains ID of the source application*
field_id|`string`|*contains ID of the field to which the element is referenced*
interpretation|`array`|*contains all interpretation types*
