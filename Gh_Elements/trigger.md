# Trigger

**Trigger** is a gh element that allows you to configure automatic functions that will be processed continuously, regardless of whether the application is open or not. It is like a separate application in another application. Due to this element, you create a listener that listens to all messages from Web Socket of the selected. The responses to these messages will then be transmitted to the nodes.

The work of the node is based on a [chain of nodes](../Understanding_Gudhub/Automation/automation_overview.md) that performs a specific set of functions. You can create it in a special area that opens after clicking on the button in the form of a pencil. Here you can immediately select the application to which the element will be listened. The chain is always starts from the special node called [Trigger](../Automation_Nodes/trigger_node.md).

>This element has a protection against recursion. If the nodes in the trigger create a repeating cycle, the node diagram will not be saved.

## Functional Characteristic

Using this element you can configure a huge number of automation processes. They can be completely different, but the common thing is that due to the trigger they can work autonomously.

## Value Format

This element does not contain any value.

## Element Options

The trigger element has only standard field setting to configure it. But, its work configures in the special window using the [automation notes](../Understanding_Gudhub/Automation/automation_overview.md).

![Settings of trigger field](gh-elements/trigger-field-settings.jpg "Trigger field settings")

## Element Style

The trigger is a rather complicated element, but it have no unique style settings. Its appearance can be customized using only [standard style options](../Understanding_Gudhub/GH_Element/setting_overview.md). Also, it has a set of [interpretation types](#interpretation).

![Style of trigger element](gh-elements/trigger-element-style.jpg "Trigger element style")

## Data Model

The trigger element has a unique data model. It do not contains any settings.

```json
{
    "data_model": {
        "automation": {},
        "interpretation": []
    }
}
```

Name|Type|Description
:---|:---|:---
automation|`object`|*contains all element settings of its  automation*
interpretation|`array`|*contains all element interpretation types*

## Filtration

The current element cannot be filtered out.

## Interpretation

The current element has two types of interpretation:

![Types of trigger interpretation](gh-elements/trigger-interpretation-types.jpg "Trigger interpretation types")

### Default

The default interpretation type displays the element as an icon.

### Switch

The second interpretation type is an interactive one. Due to it you can open the editor window.
