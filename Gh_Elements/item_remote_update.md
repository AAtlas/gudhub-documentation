# Item Remote Update

**Item Remote Update** is a gh-element that allows the user to remotely update a reference in a specific field in the selected item. More precisely, the field value will be replaced with a reference to the item in which the gh-element is located.

>This element can be only used in items.

![Using the item remote update](gh-elements/item-remote-update-work.gif "Item remote update")

In order for **Item Remote Update** to work, you must first configure a list of existing items. After that, the user can select any item from this list and update the [Item Reference](./item_reference.md) field selected in the [settings](#destination).

## Functional Characteristic

The current gh element is a specific element that allows the user to perform one specific action: replace the field value with the current item's reference. It is useful in different cases. For example, we have two applications:

- **Tasks** - contains a link to a specific project.
- **Projects** - many tasks can be assigned to a project. The **Item Remote Update** is located here.

![Example of item remote update using](gh-elements/item-remote-update-example.gif "Example app")

Then, within the project item, the user can reassign the task to another project. It can also be used in conjunction with [Item Remote Add](./item_remote_add.md), which creates items remotely.

## Value Format

This element does not contains any value.

## Element Options

*Item Remote Update* has two blocks of settings.

### Field And Destination Reference Settings

As usual, the first block contains [standard Field Settings](../Understanding_Gudhub/GH_Element/setting_overview.md) and **Destination Reference Settings**, which consists of various additional settings.

![Settings of item remote update field and destination reference](gh-elements/item-remote-update-field-destination-reference-settings.jpg "Item remote update field and destination reference settings")

Name|Description
:---|:---
Select first matched on Enter|allows you to enable the selection of the corresponding item by pressing the Enter key; [details...](#select-first-matched-on-enter)
Show thumbnail|allows to show selected thumbnails in the drop-down list of items; [details...](#show-thumbnail)
Autoselect by|allows you to select unique items without clicking on them; [details...](#autoselect-by)
<!-- tooltip-start select_first_matched_on_enter -->
#### **Select First Matched On Enter**

Since this block consists of additional settings, the first setting only allows you to add a feature to an element. That is, if you have enabled the current setting, the user can select the desired item by clicking on the Enter button.
<!-- tooltip-end select_first_matched_on_enter -->
![Using selection on Enter](gh-elements/item-remote-update-select-on-enter.gif "Select first matched on Enter")

To do this, the user first needs to enter the name of the desired item. When it is first in the list, the user can select it. Obviously, if this option is disabled, nothing will happen after pressing Enter.
<!-- tooltip-start show_thumbnail -->
#### **Show Thumbnail**

The current setting allows you to determine whether thumbnails are displayed in the drop-down list. It works in pair with the [Thambnails](#thumbnail).
<!-- tooltip-end show_thumbnail -->
![Showing the thumbnails in the list](gh-elements/item-remote-update-show-thumbnails.gif "Show thumbnails")

The point is that you choose which images will be in the list using the [Thumbnail](#thumbnail) setting. And the current setting determines whether thumbnails will be displayed at all, even if you have already selected the image source.
<!-- tooltip-start autoselect_by -->
#### **Autoselect By**

Such as the Item Remote Add, **Item Remote Update** has an extra feature. It allows users to select the required item by entering its full title or subtitle. And the current setting is  responsible for that.

>Auto-selection works only if the required item has a unique title or subtitle.
<!-- tooltip-end autoselect_by -->
That is that the current setting provides two options:

- **Title field**
- **Subtitle field**

So, you choose which field will be used for auto-selection. When you select **Title field**, the user will have to **enter the title** of the item. If you select **Subtitle field**, the user will have to **enter a subtitle**.

### Dropdown Settings

The second set of settings configures the drop-down list of items and the destination field to be updated.

![Settings of item remote update dropdown](gh-elements/item-remote-update-dropdown-settings.jpg "Item remote update dropdown settings")

Name|Description
:---|:---
Application|allows to select the application where the item reference will be changed; [details...](#application)
Thumbnail|allows to select the field from which images for thumbnails will be taken; [details...](#thumbnail)
Destination|allows to select the field in which the value will be changed; [details...](#destination)
Title|allows to select the fields from which the titles of the list items will be taken; [details...](#title)
Subtitle|allows to select the fields whose values will be used as subtitles to the list items; [details...](#subtitle)
Filter|allows to filter for which elements these settings will be used; [details...](#filter)
Edit|allows to edit or delete the option; [details...](#edit)

#### **Application**

In this setting, you have to select the application that will be updated with the current gh-element. In other words, the items of this particular application will be updated when it is selected. Also, all data for the following settings will be taken from this application..

#### **Thumbnail**

This setting is responsible for the image on the thumbnails. Specifically, here you have to select the source field for the element thumbnails.

This is the field that is taken from the [selected application](#application). That should be the [Image element](image.md). If this field is not filled in for any element, a gray cross will be displayed instead of an image in the list.

#### **Destination**

Here you must select the field whose value will be updated. When the user selects any item, the value will be overwritten in this field. This setting provides you with a list of fields of a particular application for selection.

>The field must be [Item Reference](item_reference.md), since the value in it will be rewritten to a reference to the item where **Item Remote Add** is located.

#### **Title**

Each item has a name in the drop-down list. This title comes from a specific field. So, here you have to select the source field for the title of the item in the list.

>A list of fields from the previously [selected application](#application) is provided for selection.

#### **Subtitle**

Similar to the previous setting, this one allows you to select a source field from the [application](#application) for a specific entity in the drop-down list. In this case, the value of the selected field is used as a subtitle for the item.

#### **Filter**

This setting is used to set the conditions for displaying items. This means that it allows you to add and customize filters to weed out items that may appear in the drop-down list. The works of the setting is based on the [Filter element](./table_filter.md).

#### **Edit**

The last setting of the block allows you to configure the option itself. It consists of two buttons. One of them, which looks like a pencil, allows you to edit the current option. The other, with a trash can icon, allows you to delete an option.

## Element Style

This is not the element that has a large number of style settings. There is only a [standard set of settings](../Understanding_Gudhub/GH_Element/setting_overview.md) with one additional. And, of course, it has its own [types of interpretations](#interpretation).

![Style of item remote update element](gh-elements/item-remote-update-element-style.jpg "Item remote update element style")

### General Settings

The general style settings contain that single additional option.

![General settings of item remote update style](gh-elements/item-remote-update-general-settings.jpg "Item remote update general style settings")

Name|Description
:---|:---
Input Width|allows you to enter the width of the input field

#### **Input Width**

This is a customizing setting that allows you to set the width of the current gh-element. To do that you just have to enter the required width into this field.

>The width is **measured in pixels**. The **default value is the minimum width** at the same time. It is equal to **190 pixels**.

## Data Model

The current element has the following data model:

```json
{
    "data_model": {
        "autoselect_by": "title",
        "interpretation": [],
        "select_first_matched_option": 1,
        "selected_apps": [{
            "app_id": "298650",
            "destination_field_ids": "699427",
            "filters_list": [],
            "subtitle_field_ids": "697431,697430",
            "thumbnail_field_id": "697435",
            "title_field_ids": "697436,697435"
        }],
        "show_tubnail": 1
    }
}
```

Name|Type|Description
:---|:---|:---
autoselect_by|`string`|*contains the type of value by which the autoselection will be performed*
interpretation|`array`|*contains all element interpretation types*
select_first_matched_option|`boolean`|*shows whether user can select the first found item by pressing the Enter key*
selected_apps|`array`|*contains all drop-down settings*
app_id|`string`|*contains ID of the selected application*
destination_field_ids|`string`|*contains ID of the field in which the value will be updated*
filters_list|`array`|*contains all filters applied to the drop-down list item*
subtitle_field_ids|`string`|*contains all IDs of fields from which subtitles are taken*
thumbnail_field_id|`string`|*contains ID of the field from which images for thumbnails are taken*
title_field_ids|`string`|*contains all IDs of header source fields*
show_tubnail|`boolean`|*shows whether thumbnails are displayed or not*

## Filtration

This element cannot be filtered out.

## Interpretation

This gh-element has only one type of interpretation, which displays an input field and a customized drop-down list.

![Types of item remote update interpretation](gh-elements/item-remote-update-interpretation-types.jpg "Item remote update interpretation types")
