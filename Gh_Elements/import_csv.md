# Import CSV

**Import CSV** is a gh-element that allows user to import data to application from CSV files. It turns each row of a document into a separate item. They can also be used to update existing items.

![Using CSV import](gh-elements/import-csv-work.gif "Import CSV")

Despite the fact that this element does not require customization, it provides extensive functionality for transferring data from a csv file to the current application.

>It is also important to remember that data can **only be imported into the application where this *Import CSV* is located**.

To start importing, users just need to click on the button. After that, the [import settings](#importing-data) will be opened in full screen.

## Importing Data

The process of data importing consists of five steps. Each of them is responsible for a specific stage and contains a set of different settings.

Also, each of them has navigation buttons which allows user to move between steps. They are located at the bottom. They are located at the bottom. At the same time, in the upper part of the window, the user sees what step they are at and what its title.

### Step 1. Choosing CSV File

The first step is to upload a CSV file and prepare its data for import. To do this, the user first needs to click on a specific area and select a CSV file or drag it to this area. In short, it works like the [File element](./file_element.md).

After selecting a file, the data preparation settings appear. Here you set the namespace, delimiters, and wrappers for your data.

![Choosing CSV file](gh-elements/import-csv-choosing-file.gif "Step 1")

Name|Description
:--|:--
First line as namespace|allows you to make the first line in the file a namespace; [details...](#first-line-as-namespace)
Values delimiter|allows you to select a separator for columns; [details...](#values-delimiter)
Value wrapper|allows you to select the wrapper type; [details...](#value-wrapper)

### First Line As Namespace

This option allows you to specify whether the first line of the file is used as namespaces for the columns. These names will be used when [corresponding fields to columns](#step-3-setting-columns-correspondence).

### Values Delimiter

This setting allows you to select a separator for values in a CSV file. Due to it the current element can separate the values to fill in the application fields in the [next steps](#step-3-setting-columns-correspondence). There are three delimiters are available:

- **Semicolon**
- **Comma**
- **Space**

So, you need to select one of them that is used in the file. You can check if you have chosen the right delimiter using the table at the bottom of the screen. If you have selected a suitable separator, the values will be separated by columns.

### Value Wrapper

The wrapper here is a quotation mark, which denotes a value consisting of words separated by a comma, for example. This allows you to import a CSV file without separating the values inside the quotes into separate fields. But this is only if the [comma is a delimiter](#values-delimiter) in the file.

>In this setting, **single and double wrappers** are available.

### Step 2. Setting Importing Type

The next step is to select the importing type. That is, you must determine what exactly you need to do with the data from the CSV file.

![Selecting the type of importing](gh-elements/import-csv-setting-import-type.gif "Setting import type")

In most of cases the data from file is compared with item in the app. Namely, rows are elements in a CSV file. Therefore, they are compared by columns and fields, respectively. In other words, GudHub takes a value from a certain field and compares it with a value from a certain cell in a column. If the values are equal, they are considered to be the same item.

>The field and column for comparison are selected in the [next step](#step-3-setting-columns-correspondence).

There are four options, and three of them use item comparisons:

- **Add new items** - create new items based on the imported data. The number of items created depends on the number of rows.
- **Update existing items** - update items in application with the data from the file. User doesn't have to update all items.
- **Update existing items and add non-existing items** - update the items that match certain rows of data and create new items based on the remaining rows that do not have a match among the items.
- **Add non-existing items** - create new items, but only from rows that do not match existing items.

### Step 3. Setting Column's Correspondence

The third step is to connect columns to fields. This allows the element to pass data from CSV file columns to application fields.

![Setting up the column correspondence](gh-elements/import-csv-setting-column-correspondence.gif "Column correspondence")

The main settings of this step consist of a list of all fields of the application. It looks like a small table in the middle of the window. Each of them has a list of options for the columns of the uploaded file. Here the user has to select the source column for the current field.

>Unlike comparative options, the current setting is required for [all import types](#step-2-setting-importing-type).

#### **Compare Fields**

The other settings available on this step are the **Compare Fields** setting. They are important for identifying items in a file that have already been added to the application as new items. They are used in these cases:

- **Update existing items**
- **Update existing items and add non-existing items**
- **Add non-existing items**

### Step 4. Items Preview

This step allows the user to see the preliminary result of the settings.

![Preview of imported items](gh-elements/import-csv-items-preview.gif "Items preview")

### Step 5. Importing

The final step of the process. Here, the element processes all the settings and, based on them, creates and/or updates the items of the current application.

![Process of importing](gh-elements/import-csv-importing.gif "Importing")

## Functional Characteristics

As you may have guessed, the main purpose of this element is to import data from CSV files. This saves time and effort when entering data. You can also download data from other systems. It is a convenient tool for working with large amounts of data. So it also helps to update existing data. Use it, for example, when your company transfers data to a new CRM system on the [GudHub platform](https://gudhub.com/).

## Element Options

The current element has quite small set of settings. It consists of standard field settings described in [Setting Overview](../Understanding_Gudhub/GH_Element/setting_overview.md).

![Settings of import CSV field](gh-elements/import-csv-field-settings.jpg "Import CSV field settings")

## Element Style

This element has the same style settings as all other buttons. They contains [standard style options](../Understanding_Gudhub/GH_Element/setting_overview.md) and [option unique to buttons](#general-settings). Even his [interpretation](#interpretation) is very similar to others.

![Style of import CSV element](gh-elements/import-csv-element-style.jpg "Import CSV element style")

### General Settings

The only extra option is located in this group of style settings.

![General settings of import CSV element](gh-elements/import-csv-general-settings.jpg "Import CSV general settings")

Name|Description
:---|:---
Show button name|allows to hide the button name

#### **Show Button Name**

This is the unique button setting that allows to configure whether the button name will be displayed or not.

## Filtration

This element cannot be filtered out.

## Interpretation

This element has three types of interpretation:

![Types of import CSV interpretation](gh-elements/import-csv-interpretation-types.jpg "Import CSV interpretation types")

### Default

This interpretation allows the element to be displayed as an icon with the button name next to it.

### Large Button

This type is similar to the previous one. It displays the element as the icon and the button name under it.

### Large Button Inline

The last interpretation type displays element as a big blue button.

## Value Format

This element do not have field value.

## Data Model

The data model of import CSV element contains only one property.

```json
{
    "data_model": {
        "interpretation": []
    }
}
```

Name|Type|Description
:---|:---|:---
interpretation|`array`|*contains all element interpretations*
