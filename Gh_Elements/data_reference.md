# Data Reference

**Data Reference** is a gh-element that allows to automatically pull data from other items. This is a unique element that cannot be filled in by entering data directly into it.

![Operation of the data reference](gh-elements/data-reference-element.gif "Data reference")

>It only works in conjunction with [Item Reference](./item_reference.md).

That means the value of the **Data Reference** is determine by the value of the [Item Reference](./item_reference.md). Usually, these are the values of different fields of one element. Depending on the value of the [item reference](./item_reference.md), it identifies the item and retrieves the corresponding data from the fields that were selected in the settings.

## Functional Characteristics

The current element is one of those that minimizes the role of humans in data entry. In other words, it allows existing data to be reused in different applications. To do this, the user simply needs to select one of the options in the [Item Reference](./item_reference.md). Then the associated **Data Reference** elements will pull the corresponding data.

![Using data reference](gh-elements/data-reference-usage.gif "Data reference")

So, you can use the current element to create any kind of document blanks or templates. It also useful for applications that are used for a big amount of similar information. It can be an accounting application or any other corporate application.

## Element Options

This gh-element has three groups of settings.

### Field Settings

The first group is a pair of [standard field settings](../Understanding_Gudhub/GH_Element/setting_overview.md). They provide indexing of the gh-element.

![Settings of data reference field](gh-elements/data-reference-field-settings.jpg "Data reference field settings")

### Options And Cases Settings

Next two groups contains main element settings. They are the most important for data reference work.

![Settings of data reference options and cases](gh-elements/data-reference-options-cases-settings.jpg "Options and cases settings")

Name|Description
:---|:---
Field To Watch|allows to select the item reference field for with the element will be watch; [details...](#field-to-watch)
Use Self Value|allows to use the first select value all the time; [details...](#use-self-value)
Use Save Value|allows to save selected value; [details...](#use-save-value)
Application|allows to select the source application; [details...](#application)
Field|allows to select the source field; [details...](#field)
Filter|allows to add conditions which filter items; [details...](#filter)
Edit|allows to edit or delete the case; [details...](#edit)
<!-- tooltip-start field_to_watch -->
#### **Field To Watch**

This is an option where you have to select the [Item Reference](./item_reference.md) element. Data will be extracted based on this element. In other words, the Data Reference element defines the desired item by this item reference.

>That is why the **field selected here must refers to the same application as the current Data Reference**.
<!-- tooltip-end field_to_watch -->
Otherwise, the current gh-element will not pull up any values. Therefore, this parameter is one of the most important for **Data Reference** configuration.
<!-- tooltip-start use_self_value -->
#### **Use Self Value**

The current option allows you to configure whether the self value will be used permanently or not. The self value is the first saved value of the **Data Reference**. This means that if this option is enabled, the current gh-element stores the first value and displays it for all values from the corresponding [item reference](#field-to-watch).
<!-- tooltip-end use_self_value -->
![Using of the self value option](gh-elements/data-reference-use-self-value.gif "Use self value")

It can be used to fix the first choice of the user or in cases where, regardless of the user's choice, the value of the current field should remain the same.
<!-- tooltip-start use_save_value -->
#### **Use Save Value**

**Use saved value** is an option that allows you to save the extracted data as a [field value](#value-format). That is, each new value will be stored in this field and can be used in the future.

>This option is **not available** if you use the [Use Self Value](#use-self-value) option.
<!-- tooltip-end use_save_value -->
By default, the data reference is irrelevant, and all information associated with the [item reference](#field-to-watch) is simply displayed in it. Accordingly, the data that the user sees cannot be used, for example, in calculations or exported. So, as mentioned above, the current setting solves this problem.

#### **Application**

The current option is responsible for selecting the source application. That is an application from which the data will be taken.

>This must be the same application that the field in [Field To Watch](#field-to-watch) refers to.

#### **Field**

This option allows you to select the source field from the selected application. This means that the data from this field will be displayed in the current field.

>This field have to be from the same application as a related [Item Reference](./item_reference.md).

It's also worth noting that the **Data Reference** stores the **same type of data as the field you select here**. Because of this, the field value of the current element can be in different formats.

#### **Filter**

The current button allows you to add filters that determine which value will be available for display in the **Data Reference** field. In other words, if the value associated with the item reference does not meet the filter constraints, the value will not be displayed.

This setting works like the [Filter](./table_filter.md) element. Namely, after clicking on this button, a filter panel will open in the right part of the window.

#### **Edit**

This is the setting that allows you to work with one of the element cases. You can use it to edit a case or delete it.

>There is a separate setting for each case.

### Default Value Settings

As you may have already gathered, these settings are responsible for the default value of the data reference.

![Settings of data reference default value](gh-elements/data-reference-default-value-settings.jpg "Data reference default value settings")

Name|Description
:---|:---
Use default value|allows to store the default value
<!-- tooltip-start use_default_value -->
#### **Use Default Value**

This setting is quite common for various gh-elements. But in this case, it works a little differently. When this option is enabled, the **value you entered before creating the item** is saved as the [field value](#value-format).
<!-- tooltip-end use_default_value -->
![Default value of the data reference](gh-elements/data-reference-use-default-value.gif "Use default value")

If this option is enabled, the value you entered when you added the item and before you applied it is saved.

>So, note that the value will be saved in this way only when you need to [apply a new item](./add_items.md).

## Element Style

The data reference style is configured using [standard style options](../Understanding_Gudhub/GH_Element/setting_overview.md) and a few additional ones from general style settings. You can also change its [type of interpretation](#interpretation), then its appearance will also change.

![Style of data reference element](gh-elements/data-reference-element-style.jpg "Data reference element style")

### General Settings

General style settings of the data reference contains  a couple of interesting options. They increase the capabilities of the element.

![General settings of data reference style](gh-elements/data-reference-general-settings.jpg "Data reference general style settings")

Name|Description
:---|:---
Editable descendants|allows to change the source data; [details...](#editable-descendants)
Show field name for descendants|allows to show the field name of the source data; [details...](#show-field-name-for-descendants)

#### **Editable Descendants**

This element allows you to change the data in the source (ancestor) fields through the descendant.

#### **Show Field Name For Descendants**

This function allows to show the field name of the data ancestor in descendants.

## Filtration

Since a data reference can contain different types of data, it can be filtered using a variety of filters:

- [Contains(or)](../Core_API/Filter/contain_filters.md)
- [Contains(and)](../Core_API/Filter/contain_filters.md)
- [Not contains(or)](../Core_API/Filter/contain_filters.md)
- [Not contains(and)](../Core_API/Filter/contain_filters.md)
- [Equals(or)](../Core_API/Filter/equal_filters.md)
- [Equals(and)](../Core_API/Filter/equal_filters.md)
- [Not equals(or)](../Core_API/Filter/equal_filters.md)
- [Not equals(and)](../Core_API/Filter/equal_filters.md)
- [Value](../Core_API/Filter/value.md)
- [Is bigger than](../Core_API/Filter/bigger_lower_filters.md)
- [Is lower than](../Core_API/Filter/bigger_lower_filters.md)
- [Between](../Core_API/Filter/range.md)

## Interpretation

The current element has only two interpretation types. Their differences mainly depend on general style settings.

![Types of data reference interpretation](gh-elements/data-reference-interpretation-types.jpg "Data reference interpretation types")

### Input With Name

This type displays the value of the selected field. It allows you to edit the source data if the corresponding settings are enabled.

### Default

This is the default type that displays the data corresponding to the selected [item reference](item_reference.md) value.

### Value

The last type of interpretation displays the [value](#value-format) of the field even if it is not saved.

## Value Format

This element retrieves the value via [item reference](item_reference.md), but does not always contain it. The data reference is only relevant when [Use Save Value](#use-save-value) is enabled.

```json
{
    "field_value": "john@dow.com"
}
```

## Data Model

The data model contains the values of all the settings that have been applied to the item.

```json
{
    "data_model": {
        "cases": [{
            "app_id": "27904",
            "field_id": "648759",
            "filters_list": []
        }],
        "default_array_value": [],
        "interpretation": [],
        "use_default_value": false,
        "use_save_value": false,
        "watch": {
            "field_id": "680567"
        }
    }
}
```

Name|Type|Description
:---|:---|:---
cases|`array`|*contains all configured cases of the element*
app_id|`string`|*contains ID of the source application*
field_id|`string`|*contains ID of the source field*
filters_list|`array`|*contains all filters which filter items for the case*
default_array_value|`array`|*contains array of the element default data*
interpretation|`array`|*contains all element interpretation types*
use_default_value|`boolean`|*shows whether the default value is used or not*
use_save_value|`boolean`|*shows whether the selected value will be saved or not*
watch|`object`|*contains data about the element that the current element is observing*
field_id|`string`|*contains ID of the field to watch*
