# Graph

**Graph** is a gh-element that allows to generate graphs using different item data. You can use graphs to track product sales, profit statistics or other things you wish by date.

## Functional Characteristics

The purpose of this element is to display various statistics by dates. The graph can display different statistics at the same time for comparison.

## Value Format

This element does not contains any value.

## Element Options

All parts of the current element are configured with three groups of settings.

### Field Settings

The first group is the standard set of [field settings](../Understanding_Gudhub/GH_Element/setting_overview.md).

![Settings of graph field](gh-elements/graph-field-settings.jpg "Graph field settings")

### Main Settings

The main settings configures the main element functions and connects the certain application and fields.

![Main settings of graph element](gh-elements/graph-main-settings.jpg "Graph main settings")

Name|Description
:---|:---
Source App|allows to select the source application; [details...](#source-app)
Date field|allows to select the source date field; [details...](#date-field)
Summ by|allows you to select the field by which the elements will be summarized; [details...](#summ-by)
Group items by|allows to select by which day period items will be grouped; [details...](#group-items-by)
Initial range start date|allows to set the start date of the time range of the graph; [details...](#initial-range-start-date)
Initial range end date|allows to set the end date of the time range of the graph; [details...](#initial-range-end-date)
Graph height, px|allows to set the graph height; [details...](#graph-height)
<!-- tooltip-start source_app -->
#### **Source App**

This is the application from which the desired fields and items will be taken.
<!-- tooltip-end source_app -->
<!-- tooltip-start date_field -->
#### **Date Field**

This is the date until which the items will be displayed on the chart. This field is one of the fields responsible for the placement of element points on the chart.
<!-- tooltip-end date_field -->
<!-- tooltip-start summ_by -->
#### **Summ By**

This function allows to select the field by which the items will be summed. This means that this field is used for the vertical scale and for placing points of elements on the chart.
<!-- tooltip-end summ_by -->
<!-- tooltip-start group_items_by -->
#### **Group Items By**

Select the day period by which the items dots will be grouped. This means that elements of the same period will be displayed on the same vertical line. There are five set value:

- Day
- Week
- Month
- Quarter
- Year
<!-- tooltip-end group_items_by -->
<!-- tooltip-start initial_range_start_date -->
#### **Initial Range Start Date**

This is one of functions that allows you to set the start limit of the lower scale.
<!-- tooltip-end initial_range_start_date -->
<!-- tooltip-start initial_range_end_date -->
#### **Initial Range End Date**

This setting allows you to set the end date of the graph time period.
<!-- tooltip-end initial_range_end_date -->
<!-- tooltip-start Graph_height,_px -->
#### **Graph Height**

This is the height of the graph element. You can select the desired value in pixels.
<!-- tooltip-end Graph_height,_px -->
### Groups Settings

The last group of settings allows to create a groups of items. They will be displayed in the graph as a similar connected points.

![Settings of graph groups](gh-elements/graph-groups-settings.jpg "Graph groups settings")

Name|Description
:---|:---
Group Name|allows to enter the group name; [details...](#group-name)
Graph color|allows to select the color of the group mark that will be displayed in the graph; [details...](#graph-color)
Conditions|allows to configure which items are contained in this group; [details...](#conditions)
Edit|allows to edit or delete the group; [details...](#edit)

#### **Group Name**

This is the name of the group, which you can enter yourself.

#### **Graph Color**

This is the color of the group mark. All elements of this group will be displayed on the graph as a point of the selected color.

#### **Conditions**

This feature allows you to add filters to create conditions that determine which items are contained in a particular group.

#### **Edit**

This column consists of two buttons which allows you to delete or edit the group.

## Element Style

Despite its complex appearance, the gh-element has only [standard style settings](../Understanding_Gudhub/GH_Element/setting_overview.md). The graph interpretation types are described [below](#interpretation).

![Style of graph element](gh-elements/graph-element-style.jpg "Graph element style")

## Data Model

The graph data model contains only main element settings:

```json
{
    "data_model": {
        "dateField": "678889",
        "groupItemsByRange": "month",
        "groups": [{
            "color": "#5e3987",
            "filters_list": [],
            "name": "Group Name"
        }],
        "height": "800",
        "initialXAxisRange": {
            "end": 1650488437784,
            "start": 1649883633326
        },
        "interpretation": [],
        "source_app_id": "28752",
        "sumByField": "679043"
    }
}
```

Name|Type|Description
:---|:---|:---
dateField|`string`|*contains ID of the date field*
groupItemsByRange|`string`|*contains value by which items in graph will be grouped*
groups|`array`|*contains all element groups and their settings*
color|`string`|*contains hex code of the color of the group*
filters_list|`array`|*contains filters that determine which items belong to this group*
name|`string`|*contains the name of the group*
height|`string`|*contains value of the graph height*
initialXAxisRange|`object`|*contains data about the time limits of the lower scale which will be displayed in the element*
end|`number`|*contains value of the limit start; date in milliseconds*
start|`number`|*contains value of the limit end; date in milliseconds*
interpretation|`array`|*contains all element interpretation types*
source_app_id|`string`|*contains ID of the source application*
sumByField|`string`|*contains the identifier of the field by which the items will be summed up*

## Filtration

This element cannot be filtered out.

## Interpretation

The graph has only one type of interpretation by default. It displays the customized graph.

![Types of graph interpretation](gh-elements/graph-interpretation-types.jpg "Graph interpretation types")
