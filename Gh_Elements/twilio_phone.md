# Twilio Phone

**Twilio Phone** is a gh-element that allows you to connect your application to the [Twilio](https://www.twilio.com/) account. Thanks to this, you can make calls, send messages directly from the application.

## Functional Characteristic

The feature of this element in integration with the [Twilio service](https://www.twilio.com/). This allows user to pull up all the necessary data and immediately call the appropriate contacts. Thus, you can collect all the necessary information about the client in one place and see their data during the call.

This element also allows to forward the call to a mobile device if no one picks up the phone in the application. If no one answers the phone, a [voice message](#voice-mail-url) will be automatically sent to the calling customer.

One more function of this gh-element is chatting. It gives you caller a space for exchange of messages with the client.

### Twilio Phone Panel

The main part of using a Twilio phone is its **panel**. This is the workspace of the current element, where many useful functions are located.

First of all, there are the selected phone number is displayed in the upper part of the element. Above it you can see the buttons with the names of the fields from which the numbers are taken. They allows to switch between phone numbers.

Below are three tabs:

- **Numpad** - allows you to enter, correct and erase the phone number.
- **Call history** - allows you to view all calls made to a specific number.
- **Messages** - allows to exchange messages with the selected contact.

To switch between these tabs, use the buttons at the bottom of the panel.

![Panel of Twilio phone](gh-elements/twilio-phone-panel.jpg "Phone panel")

## Value Format

This element have no value.

## Element Options

The Twilio Phone element can look like a very complex element because of its options. It has three blocks of settings, each of which contains different settings.

### Field, Authentication, and Additional Settings

The first block of settings is divided into three parts. The **Field Settings** contains three settings, two of which are [standard](../Understanding_Gudhub/GH_Element/setting_overview.md). **Authentication** is used to connect the element to your Twilio account. **Additional settings** are optional but very useful.

![Three groups of twilio phone settings](gh-elements/twilio-phone-authentification-field-additional-settings.jpg "Twilio phone first three groups of settings")

Name|Description
:---|:---
Multiple value|allows to enter the multiple phone numbers; [details...](#multiple-value)
Account Sid|allows to enter string identifier of your Twilio account; [details...](#account-sid)
Auth Token|allows to enter auth token of your Twilio account; [details...](#auth-token)
App Sid|allows to enter the SID of your application; [details...](#app-sid)
SIP domain|allows to enter the domain of your SIP account; [details...](#sip-domain)
Voice Mail URL|allows you to enter a link to a voice message; [details...](#voice-mail-url)
<!-- tooltip-start multiple_value -->
#### **Multiple Value**

This is a switch that lets you control how many numbers your Twilio phone can display. If it is enabled, several numbers will be displayed, otherwise only one phone number will be displayed.
<!-- tooltip-end multiple_value -->
<!-- tooltip-start account_sid -->
#### **Account Sid**

As you already understood, you need to have a Twilio account to use this gh element. And the first thing that you have to enter is your 34 digit [account SID](https://www.twilio.com/docs/glossary/what-is-a-sid). It is used with [Auth Token](#auth-token) to authenticate API requests.
<!-- tooltip-end account_sid -->
<!-- tooltip-start auth_token -->
#### **Auth Token**

The second thing for Twilio credentials is **Auth Token**. Used with [Account Sid](#account-sid) to successfully send a request. Read more about it [here](https://www.twilio.com/docs/iam/api/authtoken).
<!-- tooltip-end auth_token -->
<!-- tooltip-start app_sid -->
#### **App Sid**

This is the [TwiML App SID](https://www.twilio.com/docs/voice/sdks/deprecated-pages/client/tutorials/outgoing-calls#twiml-app-configuration) that allows Twilio to make outgoing to your application. More precisely, this is the SID of your application to which you want calls to be received. To get this SID you have to create TwiML App in your Twilio account.
<!-- tooltip-end app_sid -->
<!-- tooltip-start sip_domain -->
#### **SIP Domain**

The purpose of this function is to save a [SIP domain](https://www.twilio.com/docs/global-infrastructure/regional-sip-domains) for forwarding calls to your phone number. As you may have gathered, Twilio phone also supports calls via the SIP network. So, in addition to the main functions, you can configure call forwarding to the phone using [SIP account](https://www.onsip.com/voip-resources/voip-solutions/5-ways-to-use-a-free-sip-account).
<!-- tooltip-end sip_domain -->
<!-- tooltip-start voice_mail_url -->
#### **Voice Mail URL**

Twilio phone gives you the ability to use voicemail. The current URL is a link to the audio file that will be used if the person does not reach either the computer or the phone through the [SIP domain](#sip-domain).
<!-- tooltip-end voice_mail_url -->
### Phone Numbers

The second group of settings configures the source of the phone numbers.

![Numbers of twilio phone](gh-elements/twilio-phone-numbers.jpg "Twilio phone numbers")

Name|Description
:---|:---
Application|allows to select the application where all numbers are stored; [details...](#application)
Fields|allows to select fields that contains needed phone numbers; [details...](#fields)
View Name|allows you to select the view that will open when an incoming call comes in; [details...](#view-name)
Edit|allows to edit or delete an option; [details...](#edit)

#### **Application**

This is the application that contains all needed phone numbers.

#### **Fields**

This is the [Phone](phone.md) element that contains the required phone number. You can select several fields at once. Then they will be displayed in the [default interpretation type](#interpretation) pop-up window and you will be able to make calls to all these numbers in *Twilio Phone*.

#### **View Name**

This is the view of the item where the Twilio phone is located. When you receive an incoming call, after accepting it, this view will open.

>An incoming call can only be received when the application with the Twilio Phone is open.

#### **Edit**

This is a column with two settings to edit and delete the option.

### Callers

The last group of settings allows you to enter the data of callers.

![Callers](gh-elements/twilio-phone-callers.jpg "Twilio phone callers")

Name|Description
:---|:---
User Id|allows to select the user; [details...](#user-id)
Caller Id|allows to enter the phone of selected user; [details...](#caller-id)
Client Mail|allows to enter the name of selected user; [details...](#client-mail)
Edit|allows to edit or delete the option; [details...](#edit)

#### **User ID**

This is the function that allows you to select the user which will be a caller.

#### **Caller ID**

In this setting, you can enter a Twilio phone number that will be associated with the [selected user](#user-id). The caller will receive all messages, including voice messages, to this mail.

#### **Client Mail**

Here you can enter an email that will be associated with the [current caller](#user-id). On this mail caller will get all messages and voice mails.

## Element Style

The **Twilio Phone** style can be customized using the common [style settings](../Understanding_Gudhub/GH_Element/setting_overview.md). And its appearance can be changed using the [types of its interpretations](#interpretation).

![Style of twilio phone element](gh-elements/twilio-phone-element-style.jpg "Twilio phone element style")

## Data Model

The current gh-element has quite big data model:

```json
{
    "data_model": {
        "account_sid": "83rhefhjfwkkk",
        "app_sid": "aksjoiuo82b",
        "auth_token": "iusdoinshiodsa22",
        "callers": [{
            "caller_id": "456574745",
            "client_mail": "dgdfg@gmail.com",
            "user_id": "1547"
        }],
        "fields_with_phones": [{
            "app_id": "29862",
            "field_id": "697437,699277",
            "view_id": "1575865"
        }],
        "interpretation": [],
        "multiple_value": 0,
        "regular_expression": "\\.*$",
        "sip_domain": "sdhkui&89a7ihween",
        "voice_mail_url": "dsfssdg@gmail.com"
    }
}
```

Name|Type|Description
:---|:---|:---
account_sid|`string`|*contains Twilio account string ID*
app_sid|`string`|*contains TwiML App SID*
auth_token|`string`|*contains token of the Twilio account*
callers|`array`|*contains all callers data*
caller_id|`string`|*contains Twilio phone number associated with the caller*
client_mail|`string`|*contains email address associated with the caller*
user_id|`string`|*contains user ID of the caller*
fields_with_phones|`array`|*contains data of the source of phone numbers*
app_id|`string`|*contains ID of the source application*
field_id|`string`|*contains ID of the source field*
view_id|`string`|*contains ID of the view that will be opened when the call is received*
interpretation|`array`|*contain all element interpretation types*
multiple_value|`boolean`|*shows whether this element accepts one or more phone numbers*
regular_expression|`string`|*contains an expression that determines in what form the number will be displayed in the element*
sip_domain|`string`|*contains the entered sip domain*
voice_mail_url|`string`|*contains link to the audio file*

## Filtration

This element cannot be filtered out.

## Interpretation

The current element has only a few types of interpretation.

![Types of twilio phone interpretation](gh-elements/twilio-phone-interpretation-types.jpg "Twilio phone interpretation types")

### Default

The default interpretation of the *Twilio Phone* is a button that opens the pop-up list with all available phone numbers.

### Twilio phone Panel

This is the interpretation that the Twilio phone panel displays.
