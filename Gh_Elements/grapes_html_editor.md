# Grapes HTML Editor

**Grapes HTML Editor** is a gh-element that provides instruments for making the file that can be easily converted in the HTML page.

![Grapes HTML Editor](gh-elements/grapes-html-editor-working-area.jpg "Working area")

## Functional Characteristics

Due to this gh-element user can automatically update page content in blogs or other sites.

## Value Format

This is one of element that does not have any value.

## Element Options

This gh-element has only three element options. All of them are contained in one group of settings.

### Field Settings

The element field can be configured with two [standard settings](../Understanding_Gudhub/GH_Element/setting_overview.md) and an extra one.

![Settings of grapes HTML editor field](gh-elements/grapes-html-editor-field-settings.jpg "Grapes HTML editor field settings")

Name|Description
:---|:---
Field for images|allows to select the source field for images
<!-- tooltip-start field_for_images -->
#### **Field For Images**

This is the field from which images will be loaded into the HTML Editor. Due to it, those images will be added to the GudHub server with new generated name.
<!-- tooltip-end field_for_images -->
## Element Style

Despite the big functionality of the element, its style has no additional style settings. Read [Setting Overview](../Understanding_Gudhub/GH_Element/setting_overview.md) to configure the editor style and about interpretation read [below](#interpretation).

![Style of grapes HTML editor element](gh-elements/grapes-html-editor-element-style.jpg "Grapes HTML editor element style")

## Data Model

The data model of the current element contains only interpretation array and an ID of the certain field.

```json
{
    "data_model": {
        "images_field_id": "23445",
        "interpretation": []
    }
}
```

Name|Type|Description
:---|:---|:---
images_field_id|`string`|*contains ID of the field with images*
interpretation|`array`|*contains all element interpretations*

## Filtration

There are no filters to filter this element.

## Interpretation

The grapes HTML editor can be displayed in three interpretation types.

![Types of grapes HTML editor interpretation](gh-elements/grapes-html-editor-interpretation-types.jpg "Grapes HTML editor interpretation types")

### Default

The first interpretation type is a working area with lots of tool for making html page.

### Web

The web interpretation displays the contents of the element file in web format.

### Value

The last type displays only the value of the Grapes HTML Editor.
