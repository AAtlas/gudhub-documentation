# View List

**View List** is a gh-element that allows to add a list of views of any application and display one of them in the current application.

## Functional Characteristics

This element can be used to obtain the name of some view for further use.

## Value Format

Since the view list have to take views somewhere,  it takes the ID of the application with the desired views as the value.

```json
{
    "field_value": "1289974"
}
```

## Element Options

View list has quite small number of options but they are divided into two groups.

### Field Settings

As the most of the elements, the view list has only standard field options.

![Settings of view list field](gh-elements/view-list-field-settings.jpg "View list field settings")

### Main Settings

Main settings of the view list has only one option.

![Main settings of view list](gh-elements/view-list-main-settings.jpg "View list main settings")

Name|Description
:---|:---
Application|allows to select the source application
<!-- tooltip-start application -->
#### **Application**

This is the source application from which the views will be taken.
<!-- tooltip-end application -->
## Element Style

The element style contains only standard settings which are described in [Setting Overview](../Understanding_Gudhub/GH_Element/setting_overview.md), and [interpretation types](#interpretation).

![Style of view list element](gh-elements/view-list-element-style.jpg "View list element style")

## Data Model

```json
{
    "data_model": {
        "app_id": "31345",
        "interpretation": [],
        "multiple_value": false
    }
}
```

Name|Type|Description
:---|:---|:---
app_id|`string`|*ID of the source application*
interpretation|`array`|*contains all element interpretations*
multiple_value|`boolean`|*shows whether the element accepts multiple value or not*

## Filtration

There no filters for this element.

## Interpretation

The view list has two unique types of interpretation. At first glance are the same, but they do not.

![Types of view list interpretation](gh-elements/view-list-interpretation-types.jpg "View list interpretations")

### Default

The default interpretation provides views only from current application. This means that the user can only select a view from the application in which the list is located.

### Input

This types allows to select any application for the list. Namely, you can select the application from your app list.
