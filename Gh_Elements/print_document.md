# Print Document

**Print Document** is a gh-element that allows the user to print files from the [Text Editor MSE](./text_editor_mse.md). To be more precise, the purpose of this element is to fill in a certain document template with data from the corresponding item, create its PDF version, and then print it or download it.

![Using the print document](gh-elements/print-document-work.gif "Printing documents")

In terms of functioning, it is a button. So, to print any document, the user must first select the items that will fill the document. After that, the user clicks on the current button. A window with a preview of the document will open.

>It's worth noting that if you select several items at a time, the template will be filled in with the appropriate data for each item. As a result, the document will consist of a repeating template filled with different data.

Here, the user can see and check the final appearance of the document. The **Create PDF** button is located in the upper left corner. The created PDF file will opens in the new tab. There will be additional settings. Two of them are buttons for downloading and printing a document.

## Functional Characteristics

The name of this element makes it immediately clear what it will be used for. But despite this, it is used not only for printing. Basically, it generates a document from a template. Then it allows the user to create a PDF file from it. And it is this generated file that can be printed or downloaded to a device.

So, the main purpose of the current gh-element is to generate a PDF file based on data from user applications. It also can be used to export any data from apps.

## Element Options

Current element has two blocks of settings.

### Field Settings

The first block contains the [standard field settings](../Understanding_Gudhub/GH_Element/setting_overview.md) and the main settings of the element that sets up the data sources.

![Settings of print document field](gh-elements/print-document-field-settings.jpg "Print document fields settings")

Name|Description
:---|:---
Application for Printing|allows to select the application whose items will be printed; [details...](#application-for-printing)
Template Application|allows to select the app from which data will be taken for templates; [details...](#template-application)
Template field name|allows to select the field whose value will be used as a template name; [details...](#template-field-name)
Template field|allows to select the template field; [details...](#template-field)
Print Item Current|allows to print the item that is open at the current moment of time; [details...](#print-item-current)
<!-- tooltip-start app_for_printing -->
#### **Application For Printing**

Since the current element combines a template and data, there must be settings that define the source of this data. So, the current setting is the one that allows you to select the source application for the data. That is, the template will be filled with data from the current application.
<!-- tooltip-end app_for_printing -->
<!-- tooltip-start template_application -->
#### **Template Application**

As mentioned above, a template must have a source. The current setting allows you to select the source application for them. That is, the current application must store the [Text Editor MSE](./text_editor_mse.md) with the required templates.
<!-- tooltip-end template_application -->
>The [Text Editor MSE](./text_editor_mse.md) must be connected to the same application that was selected in the [previous setting](#application-for-printing).
<!-- tooltip-start template_field_name -->
#### **Template Field Name**

This setting allows you to select a field from the [application for templates](#template-application). The field selected here will be the source for template names in the [Template List](#template-list). This allows users to distinguish between templates.
<!-- tooltip-end template_field_name -->
<!-- tooltip-start template_field -->
#### **Template Field**

This is the setting where you have to select the field where the templates themselves are stored. It should be [Text Editor MSE](./text_editor_mse.md). The current setting provides a list of fields from the [template application](#template-application).
<!-- tooltip-end template_field -->
The **Print Document** element will pull a file from this setting and fill it with data from the item of the [corresponding application].
<!-- tooltip-start print_item_current -->
#### **Print Item Current**

By default, the current gh-element requires user to select the item you want to print. But this setting allows you to select the current item as the default. That is, if this setting is enabled, the item in which the element is located will automatically be the source for the document.
<!-- tooltip-end print_item_current -->
### Template List

When user clicks on the Print Document button, the drop-down list of names of document templates will be opened. Options from the second setting block allows to add a new template to the list, edit the order of template name, and delete templates from the drop-down.

![Template list of print document](gh-elements/print-document-template-list.jpg "Print document template list")

Name|Description
:---|:---
Template|allows to select the certain template for the list by its name; [details...](#template)
Edit|allows to edit and delete the templates in list; [details...](#edit)

#### **Template**

Each template in the [Template Application](#template-application) has its own name. It is taken from the field selected in [Template Field Name](#template-field-name). In the current setting, you have to choose which of these names will be displayed in the [template list](#template-list). In other words, this is where you define **which templates will be available for printing** in the current **Print Document** element.

>For each option, you can choose only one template. And, accordingly, the number of available templates will correspond to the number of such options.

#### **Edit**

The last setting is the one that is responsible for the options configurance. It consists of two buttons:

- **Edit Button** - a button in the form of a pencil. It allows you to edit the current option.
- **Delete Button** - button with a trash can icon. Use to delete the current option.

## Element Style

The current button has no special style options, only a set of [standard options](../Understanding_Gudhub/GH_Element/setting_overview.md) and its own [types of interpretations](#interpretation).

![Style of print document element](gh-elements/print-document-element-style.jpg "Print document element style")

### General Settings

Except for one option, the general style settings are common.

![General settings of print document style](gh-elements/print-document-general-settings.jpg "Print document general style settings")

Name|Description
:---|:---
Show button name|allows to show and hide the button name

#### **Show Button Name**

When this switch is off, the button name is not displayed in any of the [interpretation types](#interpretation).

## Filtration

This element cannot be filtered out.

## Interpretation

Since this element is a button, it has the same types of interpretation as the other buttons.

![Types of print document interpretation](gh-elements/print-document-interpretation-types.jpg "Print document interpretation types")

### Default

This type displays the item as an icon with the name of the button next to it.

### Icon

The second type displays an icon with the name of the button below it.

### Button

The last type of interpretation allows you to display the element as a blue button with its icon and the button name.

## Value Format

This element does not have any value.

## Data Model

All element settings are in its data model: demise

```json
{
    "data_model": {
        "doc_app_id": "27904",
        "interpretation": [],
        "print_item_current": 0,
        "ref": {
            "app_id": "28856",
            "field_id": "680375"
        },
        "template_field": {
            "field_id": "680375"
        },
        "templates": [{
            "item": "28856.3109109"
        }]
    }
}
```

Name|Type|Description
:---|:---|:---
doc_app_id|`string`|*contains ID of the application, the data of which are used to fill the document template*
interpretation|`array`|*contains all element interpretation types*
print_item_current|`boolean`|*shows whether the current item will be printed or not*
ref|`object`|*contains the data of the reference*
app_id|`string`|*contains ID of the source application*
field_id|`string`|*contains ID of the field from which names for the templates will be taken*
template_field|`object`|*contains settings of the source of the document template*
field_id|`string`|*contains ID of the field contains the identifier that contains the document template*
templates|`array`|*contains the list of templates*
item|`string`|*contains references to items with templates*
