# Twilio Auto Dialer

**Twilio Auto Dialer** is a gh-element that allows user to select the caller for the current item. This allows subscribers to receive calls from specific phone numbers.

## Functional Characteristic

The current element works both similar and opposite to the [Twilio phone](twilio_phone.md). Let's look at an example. If you have multiple people calling your customers, you can use **Twilio Auto Dialer** to configure which caller will receive the call from the current customer. So, this element is responsible for incoming calls and who will receive them.

## Value Format

This element has no value

## Element Options

The *Twilio Auto Dialer* has three important blocks of settings.

### Field and Authentification Settings

The first block consists of 2 groups. The first of them is **Field Settings** that contains only [standard element settings](../Understanding_Gudhub/GH_Element/setting_overview.md). And the other group **Authentication** is responsible for the communication between the app and your [Twilio](https://www.twilio.com/) account.

![Settings of twilio auto dialer field and authentication](gh-elements/twilio-auto-dialer-field-settings-aythentification.jpg "Twilio auto dialer field settings and authentication")

Name|Description
:---|:---
Account Sid|allows to enter ID of your Twilio account; [details...](#account-sid)
Auth Token|allows to enter the auth token of your Twilio account; [details...](#auth-token)
App Sid|allows to enter the SID of your application; [details...](#app-sid)
<!-- tooltip-start account_sid -->
#### **Account Sid**

This is the setting where you have to enter SID of your Twilio account. This SID is 34 digit string ID that you can use to authenticate API requests.
<!-- tooltip-end account_sid -->
<!-- tooltip-start auth_token -->
#### **Auth Token**

This is the second important thing for authentication API requests. **Auth Token** is used with [account SID](#account-sid). It can be taken taken in your [Twilio](https://www.twilio.com/) account.
<!-- tooltip-end auth_token -->
<!-- tooltip-start app_sid -->
#### **App Sid**

The current setting where you have to enter the [TwiML SID](https://www.twilio.com/docs/whatsapp/tutorial/send-and-receive-media-messages-whatsapp-ruby#what-is-twiml) of your application. This is an identifier that allows calls to be received on your app. It can be created in your Twilio account.
<!-- tooltip-end app_sid -->
### Phone Numbers

The second block is designed to enter the data of customers whose call you have to receive.

![Phone numbers](gh-elements/twilio-auto-dialer-phone-numbers.jpg "Twilio auto dialer phone numbers")

Name|Description
:---|:---
Field Name|allows to select the field that contains name of the call recipient; [details...](#field-name)
Field Lastname|allows to select the field that contains surname of the call recipient; [details...](#field-lastname)
Field Phone|allows to select the field the phone number of the call recipient is stored; [details...](#field-phone)
Field Email|allows to select the field with the email of the call recipient; [details...](#field-email)
<!-- tooltip-start field_name -->
#### **Field Name**

This is the field where the phone numbers of the call recipients are stored. Due to this setting, you can select the certain field from the field list of the current application.
<!-- tooltip-end field_name -->
<!-- tooltip-start field_lastname -->
#### **Field Lastname**

Like the previous setting, this one is designed to select the field of the current application. The names of the call recipients will be taken from it.
<!-- tooltip-end field_lastname -->
<!-- tooltip-start field_phone -->
#### **Field Phone**

This is the field that contains phone numbers of all call recipients.
<!-- tooltip-end field_phone -->
<!-- tooltip-start field_email -->
#### **Field Email**

This setting allows you to select one field from the current application that contains the email of the call recipient.
<!-- tooltip-end field_email -->
### Callers

The last block of settings allows you to add subscribers and add their GudHub accounts, Twilio numbers and email addresses. All created subscribers will be added to the drop-down list of the item and will be available for selection.

![Callers of twilio auto dialer](gh-elements/twilio-auto-dialer-callers.jpg "Twilio auto dialer callers")

Name|Description
:---|:---
User Id|allows to select the user who is a caller; [details...](#user-id)
Caller Id|allows to enter the Twilio number of the caller; [details...](#caller-id)
Caller Email|allows to enter the email of the caller; [details...](#caller-email)
Edit|allow to edit and delete callers; [details...](#edit)

#### **User ID**

The first block setting allows you to select the user to which the following contacts will be bound. That allows the certain  user to get calls in the GudHub.

#### **Caller ID**

This is the setting where you have to enter the Twilio number of the caller. Calls will be made to this number.

#### **Caller Email**

The purpose of this setting is to store the email of the caller. Voice messages will be sent to this email.

#### **Edit**

This is the setting that consists of two buttons. One of them allows you to edit the option, and the other - to delete it.

## Element Style

This element has no extra style settings. The [default settings](../Understanding_Gudhub/GH_Element/setting_overview.md) are sufficient for customizing it. Also, Twilio Auto Dialer has a few [interpretation types](#interpretation).

![Style of twilio auto dialer element](gh-elements/twilio-auto-dialer-element-style.jpg "Twilio auto dialer element style")

## Data Model

This element contains all its settings in its data model:

```json
{
    "data_model": {
        "account_sid": "dgdgdfgdr",
        "app_sid": "dgsdgdgdg",
        "auth_token": "gsdgdfg",
        "callers": [{
            "caller_email": "john@dow.com",
            "caller_id": "3432545323",
            "user_id": "1578"
        }],
        "fields_to_field": {
            "email": "697438",
            "lastname": "697445",
            "name": "697446",
            "phone": "697437"
        },
        "interpretation": []
    }
}
```

Name|Type|Description
:---|:---|:---
account_sid|`string`|*contains SID of entered Twilio account*
app_sid|`string`|*contains SID of entered application*
auth_token|`string`|*contains authentication token of entered Twilio account*
callers|`array`|*contains all callers and their data*
caller_email|`string`|*contains entered email of the caller*
caller_id|`string`|*contains entered phone number of the caller*
user_id|`string`|*contains user ID of the caller*
fields_to_field|`object`|*contains all data the call recipient*
email|`string`|*contains ID of the field from which call recipient email is taken*
lastname|`string`|*contains ID of the field that contains the last name of the called recipient*
name|`string`|*contains ID of the field that contains the call recipient name*
phone|`string`|*contains ID of the field from which the phone number of the call recipient will be taken*
interpretation|`array`|*contains all element interpretation types*

## Filtration

The current element cannot be filtered out.

## Interpretation

The current gh element has only one type of interpretation, which allows the user to open the drop-down list of callers by clicking on the icon.

![Types of twilio auto dialer interpretation](gh-elements/twilio-auto-dialer-interpretation-types.jpg "Twilio auto dialer interpretation types")
