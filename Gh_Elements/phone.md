# Phone

**Phone** is a gh-element that allows user to enter any phone number. The entered value will be customized to the phone number format. That is, the user can enter only digits, and they will be displayed in a telephone format.

![Using the phone element](gh-elements/phone-work.gif "Phone element")

In fact, this element has other useful tools that are available only for multiple mode. These include the auto-call function. That is, when the user clicks on a phone number, the corresponding program will automatically dial it.

## Functional Characteristics

As you can see, the current gh-element is used to store phone numbers and format them. So, it is mostly used to control that users enter phone numbers in the correct format. It is also used to customize other elements. For example, [Twilio Phone](./twilio_phone.md) uses **Phone** to get the phone number to call. In addition, the value of the current field can be used for [automation](../Understanding_Gudhub/Automation/automation_overview.md).

## Element Options

This gh-element has just a few options. In addition, it does not have default values.

### Field Settings

In addition to the standard options, this group contains those that complement the phone number.

![Field settings of the phone element](gh-elements/phone-field-settings.jpg "Phone field settings")

Name|Description
:---|:---
Country code|allows to add country code to the value; [details...](#country-code)
Extension|allows to add extension to the value; [details...](#extension)
Multiple|allows to enter multiple values; [details...](#multiple)
<!-- tooltip-start country_code -->
#### **Country code**

The first setting of the current element allows you to enable an additional property for phone numbers. That is, if the current setting is enabled, the country code is available. The **country code** field appears before the main phone field. And, accordingly, the **country code is entered separately from the main number**.
<!-- tooltip-end country_code -->
![Using the country code](gh-elements/phone-country-code.gif "Country Code")

This setting also stylizes the code, meaning that it automatically adds a plus sign in front of it. The entered code [value](#value-format) will be saved with the main phone number, separated by a dot.
<!-- tooltip-start extension -->
#### **Extension**

The next setting also allows you to enable one of the phone number properties. If it is enabled, the extension will appear after the main phone number. The extension will also be customized.
<!-- tooltip-end extension -->
![Using the phone extension](gh-elements/phone-extension.gif "Extension")

As with the previous property, the value entered here will be saved along with the main number. It will also be separated by a dot, but at the end of the [value](#value-format).
<!-- tooltip-start multiple -->
#### **Multiple**

The current setting is used to configure the number of values that can be stored in the current field. When this option is enabled, the field accepts multiple values.
<!-- tooltip-end multiple -->
![Using the multiple mode](gh-elements/phone-multiple.gif "Multiple setting")

In addition, for the multiple mode, additional tools appear that allow users to work with values. The main two buttons are:

- **Editing button** - looks like a pencil. Only after the user clicks on it will the value become available for editing.
- **Adding button** - looks like a plus. Allows the user to add a new value.

Three buttons for editing values allow the user to apply changes to the value, delete it, or simply close the editing mode.

>When this feature is enabled, all values become interactive. This means that when a user clicks on one of the phone numbers, they will be able to call it.

## Element Style

Apart from interpretation type, about that you can read [below](#interpretation), this element has all general [settings of style](../Understanding_Gudhub/GH_Element/setting_overview.md).

![Element Style of the phone](gh-elements/phone-element-style.jpg "Phone element style")

## Filtration

Phone elements can be filtered with the using a variety of filters:

- [Contains(or)](../Core_API/Filter/contain_filters.md)
- [Contains(and)](../Core_API/Filter/contain_filters.md)
- [Not contains(or)](../Core_API/Filter/contain_filters.md)
- [Not contains(and)](../Core_API/Filter/contain_filters.md)
- [Equals(or)](../Core_API/Filter/equal_filters.md)
- [Equals(and)](../Core_API/Filter/equal_filters.md)
- [Not equals(or)](../Core_API/Filter/equal_filters.md)
- [Not equals(and)](../Core_API/Filter/equal_filters.md)
- [Phone equals(or)](../Core_API/Filter/phone_equal_or.md)
- [Value](../Core_API/Filter/value.md)

## Interpretation

Whether it’s multiple or not, the phone element has tree interpretation types.

![Types of the phone interpretation](gh-elements/phone-interpretation-types.jpg "Phone interpretation types")

### Default

The default interpretation type of phone elements allows to edit field value.

### Icon

This type displays the icon instead of field value and does not allows to edit field.

### Plain Text

Unlike the previous one, this type of interpretation displays the value of the field, but does not allow you to edit it too.

### Value

This type of interpretation displays the raw value of the field.

## Value Format

Phone value has a string type, but specifically phone number is a number without brackets and dashes. In case with multiple values, numbers are separated with commas.

```json
{
    "field_value": "98890977587,780.67979790.7878"
}
```

Full phone number has points between country code and extension. Country code is placed before number, and extension after.

## Data Model

The phone has a fairly simple data model and not large number of options:

```json
{
    "data_model": {
        "interpretation": [],
        "multiple": 1,
        "show_country_code": 1,
        "show_extension": 1
    }
}
```

Name|Type|Description
:---|:---|:---
interpretation|`array`|*contains all interpretation types of the phone element*
multiple|`boolean`|*allows to add multiple values*
show_country_code|`boolean`|*allows to add country code to the value*
show_extension|`boolean`|*allows to add extension to the value*
