# Cron Picker

**Cron Picker** is a gh-element that allows you to get a cron expression using convenient options and save it. It can be used to set the schedule for your server.

![Work of cron picker element](gh-elements/cron-picker-work.gif "Cron picker")

After the user clicks on the current field, a cron [pop-up window](#picker-options) will open. This pop-up window contains the main list of options.

## Picker Options

As mentioned earlier, a cron pop-up window appears after clicking on the field. This pop-up window contains all the options that allow the user to customize the cron value.

In the upper left part of the window, the user can see a field that displays the final cron value. On the right side, there are three buttons that allow to copy, reset, and save the configured value.

![Cron value options](gh-elements/cron-picker-pop-up.jpg "Cron pop-up")

The next interface elements are the options tabs. Each of them allows a user to customize a specific unit of time. There are four default tabs:

1. [Minutes](#minutes)
2. [Hours](#hours)
3. [Day](#day)
4. [Month](#month)

>But, there is also an **additional extra tab** called [Seconds](#seconds). It is responsible for the seconds. This **tab appears** if the [Show Seconds](#show-seconds) **option is enabled**.

In general, the unit settings are very similar. All of them have options that allow users to choose: **all units at once**, units **for a certain period**, each **unit starting with a certain one**, or select **several specific unit values**. But also, some tabs has a unique options or unique variations of the above options.

Above the tabs, the user can see the value of each unit separately:

- Seconds
- Minutes
- Hours
- Day of month
- Month
- Day of week

Depending on which tab and how the user edits, the value will change.

### Minutes

The first default tab of the picker allows user to select the certain minutes or minute periods.

![Options of the minutes tabs](gh-elements/cron-picker-minutes-tab.gif "Minutes tab options")

### Hours

This is the default tab, which has a standard set of settings customized for its units.

![Options of hours tab](gh-elements/cron-picker-hours-tab.gif "Hours tab options")

### Day

The current tab has the biggest set of options. In addition to the standard options, this tab allows users to select a specific day of the week and several additional time periods by default.

![options of day tab](gh-elements/cron-picker-day-tab.gif "Day tab options")

### Month

The last default tab allows user to configure the month unit.

![Options of month tab](gh-elements/cron-picker-month-tab.gif "Month tab options")

### Seconds

A single additional options tab allows the user to set the required seconds.

![Options of seconds tab](gh-elements/cron-picker-seconds-tab.gif "Seconds tab option")

## Functional Characteristics

The current gh-element is mostly used to **generate cron expressions**. If you're working with a server, you can use the current element to get the expression you need.

## Element Options

The current gh-element has only one block of settings.

### Field Settings

Field settings of the current element has quite small set of settings. The [first two settings are standard](../Understanding_Gudhub/), and the third is unique to this element.

![Settings of cron picker field](gh-elements/cron-picker-field-settings.jpg "Cron picker field settings")

Name|Description
:---|:---
Show seconds|allows to use the seconds in the cron picker
<!-- tooltip-start show_seconds -->
#### **Show Seconds**

The current setting allows you to add seconds to your cron. When this option is on, the [seconds tab](#seconds) appears in the cron picker.
<!-- tooltip-end show_seconds -->
![Cron picker with a seconds tab](gh-elements/cron-picker-seconds.png "Seconds tab")

## Element Style

**Cron Picker** is one of the gh elements that are almost unaffected by style settings. Therefore, it has only a [standard set of style options](../Understanding_Gudhub/GH_Element/setting_overview.md) and a [couple of interpretation types](#interpretation).

![Style of cron picker element](gh-elements/cron-picker-element-style.jpg "Cron picker element style")

## Filtration

Despite that this element has a field value, it cannot be filtered out.

## Interpretation

The current gh-element has only two types of interpretation:

![Types of cron picker interpretation](gh-elements/cron-picker-interpretation-types.jpg "Cron picker interpretation types")

### Default

The first interpretation type allows user to open the cron picker and enter any value.

### Value

The second type of interpretation displays only the saved value without the ability to update it.

## Value Format

The value of the current element stores the created cron expression in the string format:

```json
{
    "field_value": "0 0,2 0,2 1,7 JAN,APR ?"
}
```

## Data Model

The current element has only two properties in its data model:

```json
{
    "data_model": {
        "interpretation": [],
        "show_seconds": 1
    }
}
```

Name|Type|Description
:---|:---|:---
interpretation|`array`|*contains all element interpretation types*
show_seconds|`boolean`|*shows whether seconds are used in the cron picker or not*
