# Export CSV

**Export CSV** is a gh-element that allows users to export data from the program in a CSV file. This is the button that, when clicked, will open a pop-up window with [export options](#data-selecting).

![Using the export element](gh-elements/export-csv-element.gif "Export CSV")

There, the user can customize which items and fields they want to export. After that, a CSV file with all the matched items will be downloaded to your local storage.

>In the generated file, the items are in rows and the fields are in columns.

## Data Selecting

This pop-up contains the main settings of exporting. Here the user can define what data he wants to export. The following set of settings is available for this purpose:

- [Select value type](#select-value-type)
- [Select items](#select-items)
- [Create self link column](#create-self-link-column)
- [Select fields](#select-fields)

![Pop-up window of export CSV element](gh-elements/export-csv-value-selection.jpg "Export CSV pop-up window")

Also, there are two buttons that allows to work with the current element:

- **Cancel** - close the pop-up window.
- **Download** - download the generated CSV file after selecting items.

### Select Value Type

The first function allows user to set the type of field values, namely whether the field value will be interpreted or not. Consequently, there are two varieties:

- *Interpreted*
- *Raw*

![Selecting the type of values to export](gh-elements/export-csv-select-value-type.gif "Select value type")

### Select Items

The second function configures which items will be exported. User can export all application items or only selected ones:

- *Selected*
- *All*

![Selecting items for export](gh-elements/export-csv-items-selecting.gif "Select items")

> Select items before clicking on the Export CSV button to use the *Selected* mode. The file will not download without pre-selecting the elements.

### Create Self Link column

This function allows you to add an extra column to the resulting file. That column contains self links of every exported item. It consists of the application ID and the item ID, separated by a dot.

![Add column with item references](gh-elements/export-csv-create-self-link-column.gif "Create self link column")

In other words, if this option is enabled, a new column with [references](./item_reference.md) to each item will be created in the generated CSV file.

### Select Fields

The last setting allows the user to choose which fields from the selected items will be added to the file. This prevents users from adding unnecessary columns to their files and prevents columns from being created from elements that don't store field values, such as [Table](./table.md), [Add Items](./add_items.md), etc.

![Selecting fields for export](gh-elements/export-csv-select-fields.gif "Select fields")

## Functional Characteristics

Use this element when you need to get data from some application. All information will be saved in the SCV, which will be downloaded to your device.

## Element Options

The export CSV does not have many field settings, and they are divided into two groups.

### Field Settings

As all other elements, this one has [standard field settings](../Understanding_Gudhub/GH_Element/setting_overview.md).

![Settings of export CSV element](gh-elements/export-csv-field-settings.jpg "Export CSV field settings")

### Option

The second group contains only one option, which is very important for configuring the export.

![Option of export CSV](gh-elements/export-csv-option.jpg "Option")

Name|Description
:---|:---
Application|allows to select the source application
<!-- tooltip-start application -->
#### **Application**

To export data, you first need to specify the location where the data will be received. The current option is used to select the source application.

>The application selected here **must be the one whose items will be exported**.
<!-- tooltip-end application -->

This is because you need to select items before exporting them. Accordingly, if the items of a particular application are selected, the same application should be attached to the **Export CSV**.

## Element Style

This button has quite standard style settings. The only unique option is [Show button name](#show-button-name). There are also types of interpretation of the current gh-element. These are described [below](#interpretation).

![Style of export CSV element](gh-elements/export-csv-element-style.jpg "Export CSV element style")

### General Settings

The general style settings contain one additional option in addition to the standard settings.

![General settings of export CSV element](gh-elements/export-csv-general-settings.jpg "Export CSV general settings")

Name|Description
:---|:---
Show button name|allows to show the button name

#### **Show Button Name**

This function allows to configure whether the button name will be displayed or not.

## Filtration

There are no filters to filter out this element.

## Interpretation

The export CSV element has a few different types of interpretation.

![Types of export CSV interpretation](gh-elements/export-csv-interpretation-types.jpg "Export CSV interpretation types")

### Default

Thanks to this interpretation, the item is displayed as an icon with the name of the button next to it.

### Large Button

This interpretation displays an icon and a button name under it.

### Large Button Inline

This interpretation allows to display current element as a blue button.

## Value Format

There are no field values.

## Data Model

The current element has a small data model with only two properties.

```json
{
    "data_model": {
        "app_id": "23423",
        "interpretation": []
    }
}
```

Name|Type|Description
:---|:---|:---
app_id|`string`|*contains ID of the source application*
interpretation|`array`|*contains all interpretation types*
