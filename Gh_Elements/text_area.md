# Text Area

**Text Area** is a gh-element that allows users to enter and store large amounts of text. Its feature is that it is convenient to use in [automation](../Understanding_Gudhub/Automation/automation_overview.md) or other processes. This is because the text is stored in unformatted form.

![Using the text area element](gh-elements/text-area-work.gif "Text area work")

To use it, the user only needs to click on the text input area, enter the desired text, and click outside the area to save the text.

## Functional Characteristics

This element can be used for a variety of purposes. You can use it instead of the [Text element](./text.md) when you need to display a large amount of text. Or you can use it instead of a [Text Editor](./text_editor.md) if you don't need to customize the text.

Also, as mentioned, the current element is well suited for use in [automation](../Understanding_Gudhub/Automation/automation_overview.md).

>It is also recommended for use with the [Send Message node](../Automation_Nodes/send_message.md).

This is because when you transfer text from other gh-elements, you may encounter problems with text formatting.

## Element Options

The current gh-element has a rather limited set of settings.

### Field Settings

The **Text Area** settings consist of only three options. Two of them are [common to all gh-elements](../Understanding_Gudhub/GH_Element/setting_overview.md).

![Settings of the text area field](gh-elements/text-area-field-settings.jpg "Field settings")

Name|Description
:---|:---
Max Symbols|allows you to limit the number of characters that can be saved here
<!-- tooltip-start max_symbols -->
#### **Max Symbols**

The only setting for the current element allows you to limit the number of characters that can be entered in the **Text Area**. You can do it by **entering here the maximum number of symbols**. Because of this, the user cannot enter more characters than this number.

>The **default value of this setting is 256**.
<!-- tooltip-end max_symbols -->
## Element Style

The style settings are not as diverse as the [Field Settings](#field-settings). They include only a set of [standard style settings](../Understanding_Gudhub/GH_Element/setting_overview.md).

![Style settings of the text area element](gh-elements/text-area-element-style.jpg "Element style settings")

## Filtration

This element cannot be used for filtering.

## Interpretation

The current gh element has only one interpretation type, which is also the default type. This makes the element look like a white area where users can enter any text.

![Types of text area's interpretation](gh-elements/text-area-interpretation-types.jpg "Interpretation types")

## Value Format

The text area stores all the text you enter as a field value. There is no limit to the length of the value except for the [Max Symbols](#max-symbols).

```json
{
    "field_value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. A lacus vestibulum sed arcu."
}
```

## Data Model

The data model of the current gh-element contains only two properties.

```json
{
    "data_model": {
        "interpretation": [],
        "maxSymbols": 256
    }
}
```

Name|Type|Description
:---|:---|:---
interpretation|`array`|*contains all element interpretation types*
maxSymbols|`number`|*contains the maximum number of characters that can be entered in the element*
