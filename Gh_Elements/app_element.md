# App Element

**App** is a gh-element that allows you to select any application from your list. The name of the selected application will be displayed in the current field. This allows you to use the element as part of an [automation](../Understanding_Gudhub/Automation/automation_overview.md) or simply to show title of the current application. Different types of interpretation are used for different needs. And accordingly, they look different.

For the first purpose, the element is mostly used in this form.

![App element with the drop-down list](gh-elements/app-drop-down-list.gif "Drop-down list")

To delete the value you just need to click the cross button in the end of the field.

![Value deleting](gh-elements/app-delete-value.gif "Delete field value")

>The **App** element is [uneditable by default](../Understanding_Gudhub/GH_Element/setting_overview.md). So make sure you activate it for the element to work properly. However, in the case of the [Current Application](#current-application), this is not mandatory.

Otherwise, it can look like an icon with or without the application name.

![Appearance with icon](gh-elements/app-select-with-icon.gif "App with icon")

>Note that editing mode isn't always the right way to use the current item.

## Functional Characteristics

It is usually used in element settings when the element needs to retrieve data from some application. Also, due to the app element and [one of its settings](#current-application), you can display the name and icon of the current application inside itself. This element is also used to configure an element called [Install](install_element.md).

## Element Options

This element can only be configured with field settings.

### Field Settings

This group of settings contains [standard](../Understanding_Gudhub/GH_Element/setting_overview.md) and advanced options.

>For each of the additional settings, there is a recommended type of interpretation and place of use for the **App element**.

![Settings of the app field](gh-elements/app-field-settings.jpg "App field settings")

Name|Description
:---|:---
Multiple value|allows to display multiple applications; [details...](#multiple-value)
Current Application|allows to display current applications; [details...](#current-application)
Hide Menu|allows to hide the application menu; [details...](#hide-menu)
Size|allows to select the size of the app icon; [details...](#size)
<!-- tooltip-start multiple_value -->
#### **Multiple Value**

This setting allows you to store a multiple value in the current element. It can be useful for any automation processes that require the app IDs.

>User can select applications only from his/her application list.
<!-- tooltip-end multiple_value -->

Any application can be selected by clicking on its name in the drop-down list.

![Multiple value mode using](gh-elements/app-multiple-value.gif "Multiple value")

Recommended for use in items and with the interpretation type [Text](#text). This mode also does not work with all of the following settings.
<!-- tooltip-start current_application -->
#### **Current Application**

This is the mode that allows you to display the name and icon of the application where the element is located. In this case, it is used as the title of the application and can be placed in any view.
<!-- tooltip-end current_application -->

![Using current application](gh-elements/app-current-application.gif "Current application")

>When this mode is on, you cannot select a application other than the current one. However, the user can [open a menu](#hide-menu) with their own list of applications. And go to any application by clicking on the selected one.

This mode can only be used with three types of interpretation: [Default](#default), [Icon](#icon), and [Default Inline](#default-inline).
<!-- tooltip-start hide_menu -->
#### **Hide Menu**

>This option is only used with the [Current application](#current-application).

As you already know, when the [Current Application](#current-application) is enabled, the user can open the application menu. So, the current setting called **Hide menu** allows you to disable it.
<!-- tooltip-end hide_menu -->
![Using hide menu](gh-elements/app-hide-menu.gif "Hide menu")

>The menu consists only of applications from the user's application list and a button that leads to the home page.

This is an additional setting to the one type of interpretation, [Default Inline](#default-inline). This is because only this type allow you to open the menu.
<!-- tooltip-start size -->
#### **Size**

The current setting is used to set the size of the application icon. There are five available sizes of the app icon:

- 32x32
- 50x50
- 64x64
- 128x128
- 256x256

<!-- tooltip-end size -->
Use this option to customize the icon of your application. That allows you to customize the app for different needs.

>This option can be used with the [Default](#default), [Icon](#icon), or [Default Inline](#default-inline) interpretation types.

## Element Style

The item has a set of options for setting its style. It is similar to [standard settings](../Understanding_Gudhub/GH_Element/setting_overview.md). The unique app interpretation types located in the [end of current article](#interpretation).

![Style of app element](gh-elements/app-element-style.jpg "App element style")

## Filtration

There are five filters that allow you to filter items by the app element:

- [Equals(or)](../Core_API/Filter/equal_filters.md)
- [Equals(and)](../Core_API/Filter/equal_filters.md)
- [Not equals(or)](../Core_API/Filter/equal_filters.md)
- [Not equals(and)](../Core_API/Filter/equal_filters.md)
- [Value](../Core_API/Filter/value.md)

## Interpretation

The current gh-element has quite a variety of types of interpretation.

![Types of app interpretation](gh-elements/app-interpretation-types.jpg "App interpretation types")

### Default

Displays the element as an icon with the application name below it.

### Icon

This interpretation type displays only the application icon.

### Text

Due to this type, only the application name will be displayed.

### Default Inline

This type allows the current element to be displayed as an application icon and name next to it.

### Value

This interpretation type displays the value of the element field.

## Value Format

The value of the element contains the ID of the selected application.

```json
{
    "field_value": "28560"
}
```

However, when [Multiple Value](#multiple-value) is enabled, a comma-separated string of application IDs is stored:

```json
{
    "field_value": "27822,27904,28560"
}
```

If the Current application is enabled, the field has no value at all:

```json
{
    "field_value": ""
}
```

## Data Model

The data model contains values of element settings.

```json
{
    "data_model": {
        "current_app": true,
        "interpretation": [],
        "multiple_value": false,
        "size": 50
    }
}
```

Name|Type|Description
:---|:---|:---
current_app|`boolean`|*contains ID of current application*
interpretation|`array`|*contains all element interpretation types*
multiple_value|`boolean`|*shows whether the element accepts the multiple value*
size|`number`|*contains size of the app icon*
