# Timeline

**Timeline** is a gh-element that allows to track application updated and updates of selected fields. This element also allows users to chat in created apps.

## Functional Characteristics

This element has two simple goals: tracking product changes and communication between users. It allows you to create a mini-chat, which greatly facilitates communication.

## Value Format

This is one of elements that do not contains field value.

## Element Options

The element settings are divided into two groups which are closely related to each other.

### Field Settings

The first group is Field Settings. It contains the default settings and an additional one, on which the next group depends.

![Settings of timeline field](gh-elements/timeline-field-settings.jpg "Timeline field settings")

Name|Description
:---|:---
Application|allows to select the source application
<!-- tooltip-start application -->
#### **Application**

This is the application whose updates will be displayed in the timeline.
<!-- tooltip-end application -->
### Display Fields

This table is empty until you select the source [application](#application). All fields of the application are displayed here.

![Display settings of timeline fields](gh-elements/timeline-display-fields.jpg "Timeline display fields")

Name|Description
:---|:---
Field|contains names of all application fields
Show|allows to show or hide the field

#### **Field**

This is the column of field names from the source application.

#### **Show**

This function allows to configure which fields updates will be displayed in the timeline.

## Element Style

The timeline is like most other gh-elements have only [standard options](../Understanding_Gudhub/GH_Element/setting_overview.md) to configure its style. Another thing is [interpretation](#interpretation). The element has a few unique types of it.

![Style of timeline element](gh-elements/timeline-element-style.jpg "Timeline element style")

## Data Model

The data model of web camera contains all element settings:

```json
{
    "data_model": {
        "app_id": "28752",
        "interpretation": [],
        "table_settings": {
            "columns_to_view": [{
                "field_id": 678838,
                "show": 1
            }]
        }
    }
}
```

Name|Type|Description
:---|:---|:---
app_id|`string`|*contains ID of the application*
interpretation|`array`|*contains all element interpretation types*
table_settings|`object`|*contains all table settings*
columns_to_view|`array`|*contains all fields properties*
field_id|`number`|*contains ID of the application field*
show|`boolean`|*shows whether the updates of the current field will be displayed in the timeline*

## Filtration

The element can be filtered out using these filters:

- [Contains](../Core_API/Filter/contain_filters.md)
- [Not contains](../Core_API/Filter/contain_filters.md)
- [Equals](../Core_API/Filter/equal_filters.md)
- [Not equals](../Core_API/Filter/equal_filters.md)
- [Value](../Core_API/Filter/value.md)

## Interpretation

The current element has only two types of interpretation:

![Types of timeline interpretations](gh-elements/timeline-interpretation-types.jpg "Timeline interpretation types")

### Input With Name

This is a convenient type of interpretation that allows you to display all app updates and to chat.

### Default

This type allows the element to be displayed as an icon.
