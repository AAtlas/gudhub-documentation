# JSON Editor

**JSON Editor** is a gh-element that allows enter, edit, and save any JSON code.

## Functional Characteristics

The current element provides users a full-fledged editor for work this JSON. It allows you to create or paste the document and save where all entered code, edit, filter, and copy it.

## Value Format

This element have no field value.

## Element Options

JSON editor has only one group of settings.

### Field Settings

This is a group that consists of [standard options](../Understanding_Gudhub/GH_Element/setting_overview.md) and one additional option.

![Settings of JSON editor field](gh-elements/json-editor-field-settings.jpg "JSON editor field settings")

Name|Description
:---|:---
Mode|allows to select the mode of the editor
<!-- tooltip-start mode -->
#### **Mode**

The JSON editor has four modes. They have different properties.
<!-- tooltip-end mode -->
- Tree

    ![Tree mode of JSON editor](gh-elements/json-editor-tree.jpg "Tree mode")

- Form

    ![Form mode of JSON editor](gh-elements/json-editor-form.jpg "Form mode")

- Code

    ![Code mode of JSON editor](gh-elements/json-editor-code.jpg "Code mode")

- Text

    ![Text mode of JSON editor](gh-elements/json-editor-text.jpg "Text mode")

## Element Style

The style of the JSON editor is customizable using the [standard options](../Understanding_Gudhub/GH_Element/setting_overview.md). Also, it has a few [interpretation types](#interpretation) in this set of options.

![Style of JSON editor element](gh-elements/json-editor-element-style.jpg "JSON editor element style")

## Data Model

Despite its look, the JSON  editor has a small data model.

```json
{
    "data_model": {
        "interpretation": [],
        "mode": "text"
    }
}
```

Name|Type|Description
:---|:---|:---
interpretation|`array`|*contains all interpretation types*
mode|`string`|*contains current mode of the editor*

## Filtration

This element cannot be used for item filtration.

## Interpretation

There are only two types of interpretation to display the editor:

![Types of JSON editor interpretation](gh-elements/json-editor-interpretation-types.jpg "JSON editor interpretation types")

### JSON

This type allows to display the element as a space for entering and editing JSON code. This is a full-fledged editor with all the buttons available to work with the code.

### Default

This is the interpretation type that displays the icon instead of full editor.
