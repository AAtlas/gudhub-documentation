# GPS Coords

**GPS Coords** is a gh-element that allows the user to find and save any location on the map. It is most often used in combination with [Google Map](./google_map.md). This is because the location of an item on *Google Maps* is determined by the [value](#value-format) of the current element.

![Using GPS coordinates](gh-elements/gps-coords-work.gif "GPS coords")

So, this element determines the coordinates of the location entered by user. The user can enter both the name or address of the location and the coordinates themselves. In the first case, the GPS Coords determine the coordinates by the entered name or address.

The selected location will be displayed as a mark on the mini-map. Thus, it can also be used as a full-fledged map with a single mark. But this is not allowed for all [types of interpretation](#interpretation).

## Functional Characteristics

As was mentioned above, the current gh-element is mostly used in pair with the [Google Map](./google_map.md). But actually, it allows you to configure the **Google Map**, namely, determines the location of items on the map. That allows you to track user's location or location of some places. In practice, it has been used for realtor applications, tourist maps, or GPS applications.

You can also update the **GPS coordinates** by connecting them to the [Text](./text.md) or [Number](./numbers.md) elements, for example. You can do this in [Address Transfer Settings](#address-transfer-settings). This allows you to make the location available for input from the appropriate fields.

## Element Options

The **GPS Coords** has many different options that allow you to customize its operation.

### Field Settings

The GPS coords have a set of general options that allows to customize it.

![Field settings of GPS coords element](gh-elements/gps-coords-field-settings.jpg "GPS coords field settings")

Field settings contains standard properties Field Name and Name Space, and its unique ones.

Name|Description
:---|:---
Units system for filter|allows to choose units that will be used during filtering; [details...](#units-system-for-filter)
Open map on dblclick|allows you to customize the opening of the map after a double click; [details...](#open-map-on-dblclick)
Confirm modal popup|allows you to use a pop-up window when you change coordinates; [details...](#confirm-modal-popup)
Use current location|allows you to take the coordinates of the current location of the user; [details...](#use-current-location)
Reload location time, ms|allows you to configure at wha interval the location will be updated; appears if Use current location is on; [details...](#reload-location-time-ms)
Zoom|allows to customize map zoom; [details...](#zoom)
Map style|allows to customize map style; [details...](#map-style)
<!-- tooltip-start units_system -->
#### **Units System For Filter**

Due to this setting you can set the unit system which is used for filtering. This means that when you use [filters associated with the current element](#filtration), you can only enter filter values in the system of the selected units. There are two systems of blocks available:

- **Metric**
- **Imperial**
<!-- tooltip-end units_system -->
The area and distance will be calculated in different units depending on the mode selected.
<!-- tooltip-start open_map -->
#### **Open Map On Dblclick**

With the current setting, you can determine how the map will respond to a double click. That is, if the current setting is enabled, the location saved in the element will be opened in Google Maps in a new tab.
<!-- tooltip-end open_map -->
Due to this setting, the user can examine the location in more detail. If this option is disabled, the map will zoom in relative to the clicked location after each double-click.
<!-- tooltip-start modal -->
#### **Confirm Modal Popup**

In **GPS coordinates** there is a setting to confirm the data update. Namely, it allows you to enable a confirmation pop-up window. This means that every time the user updates their location, a confirmation window will appear. And to save the new location, the user must click the corresponding button.
<!-- tooltip-end modal -->
![Using the confirm pop-up](gh-elements/gps-coords-comfirm-modal-popup.gif "Comfirm modal popup")

Accordingly, if this function is disabled, the user can even accidentally change the value. And the current setting will ensure that the data is saved.
<!-- tooltip-start use_current_location -->
#### **Use Current Location**

The current settings allow **GPS Coords** to track the user's location. If this feature is enabled, the item will automatically save the user's current location and the user will not be able to enter any other location. It is used in combination with the following setting to track the user's location in real time.
<!-- tooltip-end use_current_location -->
>This feature will only work if the **user allows their location to be tracked** in the browser.
<!-- tooltip-start reload_location_time -->
#### **Reload Location Time, ms**

This setting allows you to set the time when the user's location data is reloaded. This allows the user to track their location in real time. The value must be entered in milliseconds.
<!-- tooltip-end reload_location_time -->
>This option is available only if the [Use Current Location](#use-current-location) option is enabled.

That is, due to this function, at every specified interval, the value of the current field will be overwritten. Use this setting to implement the navigation function in your app.
<!-- tooltip-start zoom -->
#### **Zoom**

This setting allows you to adjust the map zoom around the saved coordinates. You can do this with the range bar or the input box next to it. Depending on the selected value the map of the **GPS Coords** will display the different map sizes.
<!-- tooltip-end zoom -->
<!-- tooltip-start map_style -->
#### **Map Style**

Using this setting you can customize the map appearance. It offers a list of three styles. Each of them has its unique appearance.

- **Standard** - classic map appearance.
- **Retro** - displays map in light pastel colors.
- **Night** - map displayed in dark blue colors.
<!-- tooltip-end map_style -->
![using the map styles](gh-elements/gps-coords-map-style.gif "Map style")

Since the **GPS Coords** mini map is very similar to a [Google Map](./google_map.md), the map styles work the same way.

>The styles are applied only for the **Map mode** of displaying.

In addition, there are a limited number of [types of interpretation](#interpretation) that the current setting works with.

### Address Transfer Settings

These are parameters that allow you to configure where various location data will be stored and where the location coordinates will be taken from.

![Address transfer settings of GPS coords](gh-elements/gps-coords-address-transfer-settings.jpg "Address transfer settings")

Name|Description
:---|:---
Address|allows to configure which data will be stored in the selected field; [details...](#address)
Short name|allows to save short names instead of full ones; [details...](#short-name)
Required|allows to configure whether the field will be mandatory for changing coordinates or not; [details...](#required)
Field|allows to choose the field from which the data for coordinates will be taken; [details...](#field)
Edit|allows to update or delete option; [details...](#edit)

#### **Address**

The current setting is the first in the block responsible for saving the address from **GPS Coords**. Since the current element stores GPS coordinates as a value, you must choose which part of the address will be passed to another field.

So, the current setting consists of a list of address components available for transfer. This allows you to store and display various information about the selected location in the current item and also search locations by entering the selected **address component** in the [related field](#field). That is, it determines which part of the address the user can enter into a certain field to find the desired location, and which part will be saved in the field. There are all existing components:

Address|address_component|Value
:---|:---|:---
City|`"locality"`|name of the city
Street|`"route"`|name of the street
Neighborhood|`"neighborhood"`|name of the neighborhood
Apartment Number|`"subpremise"`|number of the apartment
Street Number|`"street_number"`|number of the street
County|`"administrative_area_level_2"`|name of the county
State|`"administrative_area_level_1"`|name of the state
Country|`"country"`|name of the country
Postal Code|`"postal_code"`|ZIP code
Full Address|`"formatted_address"`|full address
Address|`"address"`|address
Place ID|`"place_id"`|ID of the place

When the user enters any location in **GPS Coords**, the **selected component** will be taken from its address and saved in the [related field](#field).

>Note that any of these components will be saved only if it **exists for the current location and is sufficiently detailed**.

That is, if the **user simply enters "New York"**, the fields with an **address or neighborhood will not be filled in**. It is even possible that the location of the wrong city will be selected.

#### **Short Name**

Many geographic entities, such as countries and cities, have short versions of their names. To store short names of [certain components](#address) in the [corresponding field](#field), you can use this switch. So, when this feature is enabled, short versions of the names will be saved to the attached fields.

>But only if the short name of this location and, accordingly, this [address component](#address) exists at all.

![Using short name setting](gh-elements/gps-coords-short-name.gif "Short name")

The same works in the opposite direction. Namely, this setting allows the user to enter a short name for the location search. And also the entered full name will be rewritten to a short one.

#### **Required**

In brief, this setting makes the corresponding field mandatory for location search. This means that the user must enter a value in this field, otherwise the location will not be found. So, this setting is intended to find locations from [related field](#field), not directly in the element.

#### **Field**

The next important setting is the field associated with the current gh-element. This setting allows you to select the field that will be connected to the current option. Then the [component](#address) of the location entered in the **GPS Coords** will be saved here. And the value entered in this field will be used to find the location.

![Using field to set the location](gh-elements/gps-coords-search-by-field.gif "Searching location by the field value")

For example, imagine that you have configured the address transfer option as a `City` component with an associated field called **Selected City**. Then the user enters **London** into this field. As a result, the **GPS Coords** element will **find the coordinates of the city of London and save them**. Accordingly, the location with a marker will be shown on the mini-map.

![Using field to display the address component](gh-elements/gps-coords-store-location-in-field.gif "Saving location in the field")

In case you transfer an address from GPS coordinates to a field with the same settings, the situation will be almost the same. When the user enters a location directly into **GPS coordinates**, this element will determine which city the coordinates refer to and save the city of London in the **Selected city** field.

#### **Edit**

The last setting in this block allows you to configure only the current option. That is, this setting is unique for each option, as well as all previous settings in the **Address Transfer Settings**. So, this **Edit** block contains two buttons that are responsible for editing and deleting an option. That is, when you click on the button with a pencil, the option becomes available for editing. And when you click on the button with a trash can, the option will be deleted.

## Element Style

Element style of GPS coords has all general settings about which you can read in [Setting Overview](../Understanding_Gudhub/GH_Element/setting_overview.md). The other case is a variety of interpretations about that you can read [below](#interpretation).

![Element style of GPS coords](gh-elements/gps-coords-element-style.jpg "GPS coords element style")

## Filtration

There are only two filters which can filter out gps coords:

- [Place](../Core_API/Filter/distance.md)
- [Current location](../Core_API/Filter/distance.md)

## Interpretation

GPS Coords has many interpretation types. Most of them are related to map displaying.

![Interpretation types of GPS coords](gh-elements/gps-coords-interpretation-types.jpg "GPS coords interpretation types")

### Default

Default interpretation for GPS coords is an icon. It is uneditable.

### Coords

This interpretation type allows to enter address in the format of geographic coordinates and edit them.

### Coords Input

This interpretation allows to enter, edit, and display address in the text format.

### Map

This is an interactive interpretation type. It allows to enter address and shows that place on the map immediately.

### Size 256x256

This is one of map interpretation. It has fixed size 256x256.

### Size 512x512

This is an interpretation of a fixed-size map. The size is 512x512.

### Size Auto

This is one interpretation of the map that flexibly adjusts to the size of the browser window.

## Value Format

As mentioned above, GPS coors value is geographical coordinates that are contained as a string.

```json
{
    "field_value": "40.7127753:-74.0059728"
}
```

First number is latitude, the second one is longitude. They are separated by a colon.

In most of interpretation types, you can enter an address in text format. Then it will be converted in coordinates.

## Data Model

GPS coords has quite complex data model.

```json
{
    "data_model": {
        "interpretation": [],
        "map_style": "night",
        "modal": 1,
        "open_map": 0,
        "options": [{
            "address_component": "locality",
            "field_id": "638831",
            "required": 1,
            "short_name": 1
        }],
        "regExpDistance": "^\\d+(.d+)?$",
        "regular_expression_lat": "^-?([1-8]?\\d(\\.\\d+)?|90(\\.0+)?)$",
        "regular_expression_lon": "^-?(180(\\.0+)?|((1[0-7]\\d)|([1-9]?\\d))(\\.\\d+)?)$",
        "useCurrentLocation": 0,
        "reloadLocationTime": "124556567567",
        "units": "metric",
        "zoom": "13"
    }
}
```

It contains all data about the element and its options.

Name|Type|Description
:---|:---|:---
interpretation|`array`|*contains all gps coords interpretations*
map_style|`string`|*contains a style of map*
modal|`boolean`|*shows whether the pop-up window will appear when user changes coordinates*
open_map|`boolean`|*shows whether the map will open after double click*
options|`array`|*object array of gps coords options*
address_component|`string`|*contains the type of field address; [details...](#address)*
field_id|`string`|*ID of field from that the address is taken*
required|`boolean`|*shows whether field is mandatory to be filled*
short_name|`boolean`|*shows whether will be displayed full value name or short*
regExpDistance|`string`|*regular expression that checks format of the distance value*
regular_expression_lat|`string`|*contains regular expression that checks whether latitude has a right format*
regular_expression_lon|`string`|*contains regular expression that checks whether longitude has a right format*
useCurrentLocation|`boolean`|*shows whether the coordinates of the user's current location will be used*
reloadLocationTime|`string`|*contains time between location updating*
units|`string`|*contains a type of units systems for filters*
zoom|`string`|*contains a value of a zoom*
