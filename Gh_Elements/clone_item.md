# Clone Item

**Clone Item** is a gh-element that allows you to clone items in application. More precisely, this is a button that will duplicate the selected items. Clones have the same data in the fields selected in the settings as the original item.

![Example of using the element](gh-elements/clone-item-work.gif "Cloning items")

>In order for this element to work, you **must first select the desired items** and then click the button.

After clicking the **Clone Item** the pop-up window will appears. Here you can enter the number of clones you want to create.

![Pop-up window for the number of clones](gh-elements/clone-item-number-clones.gif "Number of clones")

This element allows you to create duplicates of several items at once. The number of clones selected earlier will be created for each of the items.

## Functional Characteristics

Using the current element makes it easier for the user to work with application data. Creating duplicates eliminates the need to enter data manually, especially when it is often repeated.

If you use it with the [Update Items](update_items.md) element, you can create many items in a short time without even opening them.

![Clone and update items](gh-elements/clone-item-update.gif "Update cloned items")

## Element Options

All element settings are divided into two groups.

### Field Settings

The first group consists of [standard field settings](../Understanding_Gudhub/GH_Element/setting_overview.md) and a single additional option.

![Settings of clone item field](gh-elements/clone-item-field-settings.jpg "Clone item field settings")

Name|Description
:---|:---
Destination App|allows you to select the destination application to which the cloned items will be saved
<!-- tooltip-start destination_app -->
#### **Destination App**

**Clone Items** clones the selected items to a specific application. Thus, its only setting is responsible for defining the destination application.

>Items can be cloned only within the same application. So, the **destination application should be the same as the one where the items to be cloned are located**.
<!-- tooltip-end destination_app -->

Clones of items will get new IDs as if they were newly created.

### Fields Clone

The second group contains settings that configure each field separately.

>If the destination application is not selected, this table will be empty.

![Fields clone of clone item element](gh-elements/clone-item-fields-clone.jpg "Clone item fields clone")

Name|Description
:---|:---
Field|contains name of the application field; [details...](#field)
Clone|allows to configure whether the field will be cloned or not; [details...](#clone)

#### **Field**

Every field of the application selected earlier has two options for configuring in **Clone Item**. And the first of them displays the name of the field. This will help you determine which field you are customizing.

#### **Clone**

The second option of this block allows you to determine whether a particular field will be cloned or not. To clone, turn on the switch next to the name of the desired field.

>More precisely, the values of the selected fields will be cloned in the new element.

## Element Style

The styles of all button elements can be configured using [standard settings](../Understanding_Gudhub/GH_Element/setting_overview.md) and one special option. The clone item is no exception. It also has several [types of interpretation](#interpretation).

![Style of clone item element](gh-elements/clone-item-element-style.jpg "Clone item element style")

### General Settings

The main group of style settings contains the only additional setting.

![General settings of clone item style](gh-elements/clone-item-general-settings.jpg "Clone item general settings")

Name|Description
:---|:---
Show button name|allows to show the button name

#### **Show Button Name**

This is a standard button setting that adjusts the display of the button name. That is, if it is enabled, the name of the button will be displayed otherwise - no.

>The button name is the [Field Name](#field-settings) of this element.

## Filtration

There are no filters to filter out the current element.

## Interpretation

The current button has three interpretations:

![Types of clone item interpretation](gh-elements/clone-item-interpretation-types.jpg "Clone item interpretation types")

### Default

The first type allows element to be displayed as an icon and a button name next to it.

### Icon

This type of interpretation displays the item as an icon and the name of the button below it.

### Button

The last interpretation type displays element as a blue button with an icon and a button name on it.

## Value Format

Like a lot of buttons, this element does not contains value.

## Data Model

This gh-element has a large data model because of the number of fields:

```json
{
    "data_model": {
        "dest": {
            "app_id": "27904"
        },
        "interpretation": [],
        "table_settings": {
            "columns_to_view": [{
                "field_id": 648752,
                "show": 0
            }]
        }
    }
}
```

Name|Type|Description
:---|:---|:---
dest|`object`|*contains destination settings*
app_id|`string`|*contains ID of the destination application*
interpretation|`array`|*contains all element interpretations*
table_settings|`object`|*contains all table settings*
columns_to_view|`array`|*contains all fields and their settings*
field_id|`number`|*contains ID of the current field*
show|`boolean`|*shows whether certain field will be cloned or not*
