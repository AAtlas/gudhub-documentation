# Add Items From Template

**Add Items From Template** is a gh-element

## Functional Characteristics

## Value Format

This is the gh-element that have no value.

## Element Options

This gh-element has three blocks of settings.

### Field Settings

The first block contains **Field Settings** and **Reference Settings**. There are two [standard settings](../Understanding_Gudhub/GH_Element/setting_overview.md), an option that allows you to select a destination application and one reference setting.

![Settings of field of add items from template element](gh-elements/add-items-from-template-field-settings.jpg "Add items from template field settings")

Name|Description
:---|:---
Destination App|allows to select application where the item will be added
Field for reference|

#### **Destination App**

#### **Field For Reference**

### Field To Field

![Field to field settings of add items from template element](gh-elements/add-items-from-template-field-to-field.jpg "Add items from template field to field")

Name|Description
:---|:---
Source Field|
Destination Field|
Edit|allows to update or delete the template

#### **Source Field**

#### **Destination Field**

#### **Edit**

This is a column that contains two buttons. These buttons are responsible for editing and deleting the current template.

### Dropdown Settings

The last block of settings is responsible for creating templates that consist of various items. All of them will be displayed in the drop-down list.

![Settings of dropdown list of add items from template](gh-elements/add-items-from-template-dropdown-settings.jpg "Add items from template dropdown settings")

Name|Description
:---|:---
Title template|allows ro enter the name of the name of the template; [details...](#title-template)
Filter|allows to filter which items will be are included in the current template; [details...](#filter)
Edit|allows to edit and delete the template; [details...](#edit)

#### **Title template**

The current setting allows you to enter the name of the created template. This name will be displayed in the drop-down list of the current element.

#### **Filter**

This function allows you to add different filters to your template. They determine which items are included into the current template.

## Element Style

The style of the current element can be customized using all [standard settings](../Understanding_Gudhub/GH_Element/setting_overview.md) and one [additional function](#general-settings). It also has its own [types of interpretation](#interpretation).

![Style settings of add items from template](gh-elements/add-items-from-template-element-style.jpg "Add items from template style")

### General Settings

The main block of style settings consists of all important style parameters. Also the only additional option is located here.

![General style settings of add items from template](gh-elements/add-items-from-template-general-settings.jpg "Add items from template general settings")

Name|Description
:---|:---
Show button name|allow to show and hide the name of the button

#### **Show Button Name**

This is the switcher that allows you to show or hide the name of the current button. If it is disabled, the name will be hidden.

## Data Model

The data model of the current element consists of many different properties.

```json
{
    "data_model": {
        "dest": {
            "app_id": "29860"
        },
        "dialog_view_id": 0,
        "interpretation": [],
        "itself_filter": {
            "active": false
        },
        "multiple_items": 0,
        "options": [{
            "dest_field_id": "697430",
            "source_field_id": "697446"
        }],
        "reference": {
            "field_id": "699427"
        },
        "show_dialog": 0,
        "source": {
            "app_id": ""
        },
        "templates": [{
            "filters_list": [],
            "title_template": "dfd"
        }],
        "tooltip": {
            "default": "Dest app",
            "en": "EN",
            "ua": "UA"
        },
        "view_id": 0
    }
}
```

Name|Type|Description
:---|:---|:---
dest|`object`|*contains data about the destination*
app_id|`string`|*contains ID of the destination application*
dialog_view_id|`boolean`|
interpretation|`array`|*contains all element interpretation*
itself_filter|`object`|-
active|`boolean`|-
multiple_items|`boolean`|-
options|`array`|
dest_field_id|`string`|
source_field_id|`string`|
reference|`object`|
field_id|`string`|
show_dialog|`boolean`|-
source|`object`|?
app_id|`string`|?
templates|`array`|*contains all element templates*
filters_list|`array`|
title_template|`string`|
tooltip|`object`|?
default|`string`|
en|`string`|
ua|`string`|
view_id|`number`|?

## Filtration

This element cannot be filtered out.

## Interpretation

This element has two interpretation types.

![Types of add items from template element](gh-elements/add-items-from-template-interpretation-types.jpg "Add items from template interpretation types")

### Input

The first type is an interactive one. It allows to open the drop-down list of templates and select the item which will be added.

### Default

The default interpretation type displays the element as an icon, which is not interactive, unlike the previous type.
