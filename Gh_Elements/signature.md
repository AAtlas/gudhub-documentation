# Signature

**Signature** is a gh-element that allows to sign the item or application. It provides the space where user can write his signature.

## Functional Characteristics

This element is used to sign documents immediately in application. Use it to get digital signature.

## Value Format

Every signature is saved in the separated file. Exactly the ID of that file is a value of the field.

```json
{
    "field_value": "941185"
}
```

## Element Options

Signature does not have any additional options. It can be configure using only [standard field settings](../Understanding_Gudhub/GH_Element/setting_overview.md).

![Settings of signature field](gh-elements/signature-field-settings.jpg "Signature field settings")

## Element Style

The signature as lots of other elements has a standard set of [style settings](../Understanding_Gudhub/GH_Element/setting_overview.md). As for interpretation, its types are described in [the last chapter](#interpretation).

![Style of signature element](gh-elements/signature-element-style.jpg "Signature element style")

## Data Model

The element data model is quite simple:

```json
{
    "data_model": {
        "interpretation": []
    }
}
```

Name|Type|Description
:---|:---|:---
interpretation|`array`|*contains all interpretation types and their properties*

## Filtration

With [Value](../Core_API/Filter/value.md) you can filter items by whether they are signed or not.

## Interpretation

Almost all item interpretations allow you to enter and save the user's signature. They differ only in size.

![Types of signature interpretation](gh-elements/signature-interpretation-types.jpg "Signature interpretation types")

### Default 128x128

Displays the signature as a 128x128 image.

### Default 64x64

This type allows the element to be displayed as a 64x64 image.

### Default 256x256

Due to this type the element is displayed as a 256x256 image.

### Default 512x512

The current interpretation type allows to display the item as a 512x512 image.

### Plain Text

This type displays the signature as an icon.

### Value

The last interpretation type displays the value of the current gh-element.
