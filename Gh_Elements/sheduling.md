# Sheduling

**Sheduling** is a gh-element that displays the items in the special timeline. This allows you to visualize the duration of element events and compare them.

## Functional Characteristics

Use this element to create a useful scheduling for projects. Track the duration and status of project stages, edit information about them. More precisely, in Scheduling, you can create different tasks, mark their status by color, and divide them into groups by stages. And, of course, you can set the task execution period on the element timeline.

## Value Format

This element does not have field value.

## Element Options

The sheduling element has a huge number of options. They are divided into five groups. Each of them is responsible for the configuration of different parts of the element.

### Field Settings

The first group is a set of [basic field settings](../Understanding_Gudhub/GH_Element/setting_overview.md).

![Settings of sheduling field](gh-elements/sheduling-field-settings.jpg "Sheduling field settings")

### Main Settings

Main settings of sheduling allows to configure connection between element and any selected application.

![Main settings of sheduling element](gh-elements/sheduling-main-settings.jpg "Sheduling main settings")

Name|Description
:---|:---
Source App|allows to select the source application for the element; [details...](#source-app)
View name|allows to select the view that will be opened; [details...](#view-name)
Name to display|allows to select the field whose value will be displayed as a item name; [details...](#name-to-display)
Start field|allows to select the field with start date; [details...](#start-field)
Use Duration|allows to use the duration field instead of end date; [details...](#use-duration)
Duration Field|allows to select the field with duration; [details...](#duration-field)
End Date|allows to select the field with end date; [details...](#end-date)
Initial time interval|allows to select the default time period of sheduling; [details...](#initial-time-interval)
Prevent group change|allows to make groups unchanged; [details...](#prevent-group-change)
Sheduling height|allows to select the sheduling height; [details...](#sheduling-height)
<!-- tooltip-start source_app -->
#### **Source App**

This is the application from which all views and items will be taken.
<!-- tooltip-end source_app -->
<!-- tooltip-start view_name -->
#### **View Name**

This is the selected view which will be opened after clicking on the sheduling item.
<!-- tooltip-end view_name -->
<!-- tooltip-start Name_to_display -->
#### **Name To Display**

This name is displayed in sheduling. It is the value of the field selected from the source application.
<!-- tooltip-end Name_to_display -->
<!-- tooltip-start start_field -->
#### **Start Field**

This is the date from which the duration count of the item will be started.
<!-- tooltip-end start_field -->
<!-- tooltip-start use_duration -->
#### **Use Duration**

This function allows to use duration instead of the end date. This helps to display the item within certain dates.
<!-- tooltip-end use_duration -->
<!-- tooltip-start duration_field -->
#### **Duration Field**

This is the duration of the item, by which the sheduling element displays the item at a certain interval on the timeline.
<!-- tooltip-end duration_field -->
<!-- tooltip-start end_date_field -->
#### **End Date**

This is the end point of the item on the sheduling timeline.
<!-- tooltip-end end_date_field -->
<!-- tooltip-start initial_time_interval -->
#### **Initial Time Interval**

This is the period of time that will be displayed in the sheduling by default:

- Current day
- Current week
- Current month
<!-- tooltip-end initial_time_interval -->
<!-- tooltip-start prevent_group_change -->
#### **Prevent Group Change**

This function is used for items groups. This prevents any changes to it. That means the user cannot move items in group.
<!-- tooltip-end prevent_group_change -->
<!-- tooltip-start sheduling_height -->
#### **Sheduling Height**

Select the maximum height of the sheduling. There are three values:

- 30% screen
- 60% screen
- 100% screen
<!-- tooltip-end sheduling_height -->
### Group Settings and Table Filter

Here we have two groups of settings. The first allows you to create groups of items, and the second allows you to filter them. Both groups configure the display of application items on the sheduling timeline. Specifically, you can add conditions under which some elements will not be displayed.

![Settings of sheduling groups and filters](gh-elements/sheduling-group-settings-table-filter.jpg "Group settings and Table filter")

### Items Styles Settings

The sheduling displays items on its special timeline. The last group of settings allows you to add different styles, customize them and determine which elements will be displayed in which style.

![Styles settings of sheduling items](gh-elements/sheduling-items-styles-settings.jpg "Sheduling items styles settings")

Name|Description
:---|:---
Text color|allows to select the color of the item name; [details...](#text-color)
Background color|allows to select the color of the item background; [details...](#background-color)
Conditions|allows to configure the items which will use this style; [details...](#conditions)
Edit|allows to edit or delete the style; [details...](#edit)

#### **Text Color**

The color of the item name in the sheduling timeline.

#### **Background Color**

This is the background color of the item that is displayed in the timeline.

#### **Conditions**

The conditions configure to which elements this style will be applied. It is based on the [table filter](table_filter.md).

#### **Edit**

Tis column contains two buttons. One of them allows to change item colors or to edit conditions, the other allows you to remove the style altogether.

## Subgroup Settings

*Subgroup* is a marker in the sheduling that cannot be changed. It is located below the usual items.

### Subgroup Main Settings

The first groups of settings configures the main subgroup functions.

![Main settings of sheduling subgroup](gh-elements/sheduling-subgroup-main-settings.jpg "Subgroup main settings")

Name|Description
:---|:---
Source App|allows to select the source application for subgroup; [details...](#subgroup-source-app)
Name to display|allows to select the field whose value will be used as a subgroup name; [details...](#name-to-display-1)
Start field|allows to select the field with a start date for subgroup; [details...](#subgroup-start-field)
Use Duration|allows to use duration instead of the end date for subgroup; [details...](#subgroup-use-duration)
End Date|allows to select the field with the end date for subgroup; [details...](#subgroup-end-date)
Duration Field|allows to select the field with duration for subgroup; [details...](#subgroup-duration-field)

#### **Subgroup Source App**

This is the application from which all the data will be taken to display in the item.

#### **Name to Display**

The name of the sheduling item is taken from the selected in settings field.

#### **Subgroup Start Field**

This is the start point of the subgroup item on the timeline.

#### **Subgroup Use Duration**

This function allows to select duration instead of the end date for the item length.

#### **Subgroup End Date**

This is the end point of the event item.

#### **Subgroup Duration Field**

This is the duration of the item event and in combination the length of the sheduling item.

### Sub Group Filter

As other items in sheduling, subgroups can be filtered. Specifically, you can filter which items will be displayed as a subgroup.

![Filter of sheduling subgroup](gh-elements/sheduling-sub-group-filter.jpg "Subgroup filter")

### Sub Group Styles Settings

The styles settings of the subgroup are the same to the [items styles settings](#items-styles-settings). The only difference between them is the different source applications.

![Styles settings of sheduling subgroup](gh-elements/sheduling-sub-group-styles-settings.jpg "Subgroup styles settings")

Name|Description
:---|:---
Text color|allows to select the text color; [details...](#text-color)
Background color|allows to select the background color for subgroup items; [details...](#background-color)
Conditions|allows to configure the items which will use this style for subgroup; [details...](#conditions)
Edit|allows to edit or delete the subgroup style; [details...](#edit)

## Element Style

Despite that the sheduling has a big amount of items styles settings, it has no additional options to customize exactly the element. The sheduling style consists of only [standard settings](../Understanding_Gudhub/GH_Element/setting_overview.md) and a few [interpretation types](#interpretation).

![Style of sheduling element](gh-elements/sheduling-element-style.jpg "Sheduling element style")

## Data Model

The element data model contains a big number of properties:

```json
{
    "data_model": {
        "defaultItemDuration": 3600000,
        "defaultItemEndDate": 7200000,
        "filters_list": [],
        "groups": [],
        "initialTimeInterval": "current_week",
        "interpretation": [],
        "itemsConfig": {
            "displayFieldId": "678845",
            "durationFieldId": "678890",
            "endDateFieldId": "",
            "groupFieldId": "",
            "startFieldId": "678889"
        },
        "itemsStyles": [{
            "background-color": "#67f98a",
            "color": "#5e3987",
            "filters_list": []
        }],
        "preventEditingGroup": false,
        "sheduling_height": "100%",
        "source_app_id": "28752",
        "subGroup": {
            "displayFieldId": "678870",
            "durationFieldId": "678915",
            "endDateFieldId": "",
            "filters_list": [],
            "source_app_id": "28753",
            "startFieldId": "678914",
            "use_duration": 1
        },
        "subGroupStyles": [{
            "background-color": "#08aeea",
            "color": "#2d4a84"
        }],
        "use_duration": true,
        "view_id": "1527725"
    }
}
```

Name|Type|Description
:---|:---|:---
defaultItemDuration|`number`|*contains value of the duration field in milliseconds*
defaultItemEndDate|`number`|*contains value of the end date field in milliseconds*
filters_list|`array`|*contains filters that configures which item are displayed in the element*
groups|`array`|*contains all element groups and their properties*
initialTimeInterval|`string`|*contains value of the initial time interval*
interpretation|`array`|*contains all element interpretations*
itemsConfig|`object`|*contains all properties which are needed for configuring sheduling items*
displayFieldId|`string`|*contains ID of the field with the item name*
durationFieldId|`string`|*contains ID of the duration field*
endDateFieldId|`string`|*contains ID of the end date field*
groupFieldId|`string`|*contains ID of the field by which items are grouped*
startFieldId|`string`|*contains ID of the field with start date*
itemsStyles|`array`|*contains all items styles and their properties*
background-color|`string`|*contains the hex code of the item background color*
color|`string`|*contains the hex code of item text color*
filters_list|`array`|*contains all style conditions*
preventEditingGroup|`boolean`|*shows whether the group editing is prevented or not*
sheduling_height|`string`|*contains the value of sheduling height*
source_app_id|`string`|*contains ID of the source application*
subGroup|`object`|*contains all subgroup properties*
displayFieldId|`string`|*contains ID of the field that will be displayed in the sheduling*
durationFieldId|`string`|*contains ID of the field with duration for subgroup*
endDateFieldId|`string`|*contains ID of the field with the end date for subgroup*
filters_list|`array`|*contains all subgroup filters*
source_app_id|`string`|*contains ID of source application for subgroup*
startFieldId|`string`|*contains ID of the field with a start date of the subgroup*
use_duration|`string`|*shows whether the the duration field is used for subgroup or not*
subGroupStyles|`array`|*contains all subgroup styles and their properties*
background-color|`string`|*contains a hex code of background color of the subgroup style*
color|`string`|*contains a hex code of the item name color of the subgroup*
use_duration|`boolean`|*shows whether the duration is used or not*
view_id|`string`|*contains ID of the view that will be opened after clicking on the sheduling item*

## Filtration

This element does not have any filters to be filtered out.

## Interpretation

The only type of interpretation displays sheduling as the interactive element with different items on it.

![Types of sheduling interpretation](gh-elements/sheduling-interpretation-types.jpg "Sheduling interpretation types")
