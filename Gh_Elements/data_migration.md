# Data Migrations

**Data migration** is a gh-element that allows you to copy item data from one application to another. In fact, this is a button that opens the migration settings window.

![Data configuring of data migration](gh-elements/data-migration-options.gif "Data migration configure data")

Here you can find all the important data transfer settings. They are divided into source and destination settings.

Name|Description
:--|:--
Source App|is an application from which data will be transferred to another application
Destination App|is the application where the data will be transferred
Source Fields|a field from the source application from which data will be transferred to a field from another application
Destination Fields|a field in which data from another application is entered
Interpreted Value|determines whether the value will be passed as interpreted or not

Therefore, the main functionality of the current element is contained in this window.

## Functional Characteristics

The main purpose of this gh-element is to copy data from application to application. This allows the user to comfortably duplicate any information and avoid the monotonous repetition of data entry. It is quite similar to the [Clone Item](./clone_item.md) element and can even partially replace it.

## Element Options

Only [standard field settings](../Understanding_Gudhub/GH_Element/setting_overview.md) can be applied to the current element.

![Settings of data migrations field](gh-elements/data-migrations-field-settings.jpg "Data migrations field settings")

## Element Style

The element has quite simple style, that can be customized using [default settings](../Understanding_Gudhub/GH_Element/setting_overview.md). It also has a few interpretation types. They are described [below](#interpretation).

![Style of data migrations element](gh-elements/data-migrations-element-style.jpg "Data migrations element style")

## Filtration

This element cannot be filtered out.

## Interpretation

There are only two quite similar types of interpretations.

![Types of data migrations interpretation](gh-elements/data-migrations-interpretation-types.jpg "Data migrations interpretation types")

### Default

The first type allows the element to displayed as an icon with a element name under it. Clicking this icon will open the data migration options.

### Icon

This type displays the element as an icon. Nothing happens after user clicks on it.

## Value Format

This is one of that buttons that does not have any field value.

## Data Model

The data model of the current element has no properties other than the usual interpretive array.

```json
{
    "data_model": {
        "interpretation": []
    }
}
```

Name|Type|Description
:---|:---|:---
interpretation|`array`|*contains all element interpretation types*
