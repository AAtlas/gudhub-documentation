# Editor JS

**Editor JS** is a gh element that allows the user to create a document and store various data in it. Namely, it provides an area and instruments to work with the document. This element contains a lots of different custom blocks.

![Using the editor JS](gh-elements/editor-js-element.gif "Editor JS")

All of them have their own settings and can be customized according to the user's needs.

Name|Description
:---|:---
**Text**|allows to enter te simple text. The text font can be customized.
**List**|allows to create one of two different types of list.
**Heading**|allows user to add headings of different sizes.
**Faq**|allows user to add questions to the document.
**Blockquote**|allows users to add a quote to their text.
**Pros Cons**|allows user to add and edit the pros and cons block.
**Live Code Editor**|allows the user to add a block of code intended for code of custom blocks. Here the user can enter their own code that will be rendered on the site as a unique block if this document is used to create it. For example, using this block, you can display posts from Twitter or YouTube videos on your site.
**How To**|allows the user to add a tutorial template.
**Image**|allows you to add images to a document.
**Table**|allows to add and edit tables to the document.
**Checklist**|allows user to use the checklist in their document.
**Code Mirror**|allows you to add a block that displays code with the appropriate syntax highlighting. There are three types of syntax available: Javascript, HTML, CSS.
**Columns**|allows user to add column that divides the area in two or three parts. Other blocks can be added inside these parts.

Due to these various blocks, this element is a useful tool for any user's needs, both for notes and for layout of full-fledged articles.

## Functional Characteristics

The current gh-element is created to works with a text and custom blocks. This allows users to use it in many different cases. The most obvious use is to create notes for both personal and work purposes, such as task descriptions, event notes, or anything else. The user can also use this element to manage documents.

Since **Editor JS** has lots of custom blocks, it can be used for creating various articles for sites. Afterward, you can post them on your website.

## Element Options

The current element has only one block of settings that contains

### Field Settings

![Settings of editor JS field](gh-elements/editor-js-field-settings.jpg "Editor JS  settings")

Name|Description
:---|:---
Show Image Properties|allows to add properties of the images
<!-- tooltip-start show_image_properties -->
#### **Show Image Properties**

The current setting allows you to determine whether the user will have access to the image properties in the **Editor JS**. Image properties include:

- Alt
- URL
- Title
<!-- tooltip-end show_image_properties -->
![Using the image properties](gh-elements/editor-js-show-image-properties.gif "Show image properties")

These properties are most useful when data from the current element is posted on a website.

## Element Style

Nested List is an interesting interactive element. But it have only [standard style settings](../Understanding_Gudhub/GH_Element/setting_overview.md). Also, this list have a few [interpretation types](#interpretation).

![Style of editor JS element](gh-elements/editor-js-element-style.jpg "Editor JS element style")

## Filtration

The current element cannot be filtered out.

## Interpretation

This element has four different interpretations:

![Types of editor JS interpretation](gh-elements/editor-js-interpretation-types.jpg "Editor JS interpretation types")

### Default

The first interpretation is the default one. It displays the functional version of the item.

### HTML

The second type displays the entered text with customized formatting, but it cannot be edited.

### Value

The current type displays the JSON code of the created document.

### Icon

The last interpretation type displays element as an icon.

## Value Format

This element stores the ID of the JSON document created in it.

```json
{
    "field_value": "62fe48cb90089904e853c9f8"
}
```

>All data is contained in JSON.

## Data Model

The element data model contains only a few properties:

```json
{
    "data_model": {
        "interpretation": []
    }
}
```

Name|Type|Description
:---|:---|:---
interpretation|`array`|*contains all element interpretation types*
