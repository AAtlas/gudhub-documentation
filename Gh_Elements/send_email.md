# Send Email

**Send Email** is a gh-element that allows to send emails by a few clicks. It can send the needed email from a big list to a big number of people from the individual sender email. Validation of email addresses before sending is also provided, which will ensure that emails will be delivered to the existing mail.

## Functional Characteristic

The main task of this element is to automatically fill in email templates and send them to different people. *Send Email* works in cooperation with different element. The [Text Editor MSE](text_editor_mse.md) provides email templates and their automatic filling. The [Text](text.md), [Data Reference](data_reference.md) and [Email](email.md) elements function as source fields for email addresses, names and subjects.

## Value Format

This element has no field value.

## Element Options

The **Send Email** has a big set of different settings. They are divided into three block.Each of them is responsible for different parts of the work of the element.

### Field Settings And Document

The first block of settings consists of two different groups, *Field Settings* and *Document* settings. The first one is responsible for standard naming of the element by using [standard options](../Understanding_Gudhub/GH_Element/setting_overview.md), for selecting the source application, and for validation. The second group is created to customize the email template.

![Settings of send email field and document](gh-elements/send-email-field-settings-document.jpg "Send email field settings and document")

Name|Description
:---|:---
Application for Emailing|allows to select the source application where recipients emails are stored; [details...](#application-for-emailing)
Validate|allows to turn on the preliminary validation of email; [details...](#validate)
Template Application|allows to select the source application with the templates of the emails; [details...](#template-application)
Template Name|allows you to select the field from which values will be used as template names; [details...](#template-name)
Template Field|allows to select the field with the template text; [details...](#template-field)
Current Item|allows to take template from the current item; [details...](#current-item)
Template Theme|allows you to select the field from which the text for the email theme will be taken; [details...](#template-theme)
<!-- tooltip-start application_for_emailing -->
#### **Application For Emailing**

This is the main application that contains data about senders and recipients, namely their emails and names. You can choose any application that contains such data.
<!-- tooltip-end application_for_emailing -->
<!-- tooltip-start validate -->
#### **Validate**

If this feature is enabled, recipients' emails will be validated before being sent. If the email address contains errors or does not exist, the letter will not be sent.
<!-- tooltip-end validate -->
<!-- tooltip-start template_application -->
#### **Template Application**

It is a program that stores all email templates and all the data about them, such as template names and subject lines.
<!-- tooltip-end template_application -->
<!-- tooltip-start template_name -->
#### **Template Name**

The names of templates are taken from the selected in this setting field. These names are also the names of the links in the [Templates List](#templates-list) and are displayed in the drop-down list of the *Send Email* element.
<!-- tooltip-end template_name -->
<!-- tooltip-start template_field -->
#### **Template Field**

This is the field that stores the templates of the emails. To be more precise, it is [MSE Text Editor](text_editor_mse.md), where the user enters the text for the email and configures the automatically filled fields.
<!-- tooltip-end template_field -->
<!-- tooltip-start current_item -->
#### **Current Item**

When this feature is enabled, the data for sending the email will be taken from the current item. That is, the item that the user has open at the moment.
<!-- tooltip-end current_item -->
<!-- tooltip-start template_theme -->
#### **Template Theme**

The current element can also add themes to users emails. This function allows you to select the field from which the themes will be taken. If the source field in any of the items is not filled in, the corresponding email will be sent without them.
<!-- tooltip-end template_theme -->
### Recipient And Sender

The second block allows you to configure the sender and recipient data source. The *Recipient* settings takes data from fields of the [selected application](#application-for-emailing). In turn, the *Sender* data can be taken both from this application and from special settings. You can select the needed type using [Static Email Sender](#static-email-sender). Then the settings of the *Sender* group will be different, depending on your choice:

- Settings for a group of senders

    ![Recipient and sender](gh-elements/send-email-recipient-sender.jpg "Send email recipient and sender")

- Settings for one sender

    ![Another sender settings](gh-elements/send-email-sender-static-email-sender.jpg "Send email sender")

Name|Description
:---|:---
Recipient Name|allows to select the field from which recipient names will be taken; [details...](#recipient-name)
Recipient Email|allows to select field from which names of recipients names; [details...](#recipient-email)
Static Email Sender|allows you to configure whether all emails will be sent from one email or not; [details...](#static-email-sender)
Sender Email Field|allows to select the field from which emails of senders will be taken; [details...](#sender-email-field)
Use User Data|allows not to enter the sender's name, but instead use their account details; [details...](#use-user-data)
Sender Name Field|allows to select the field from which the names of senders will be selected; [details...](#sender-name-field)
Default Email Sender|allows to enter an email of the sender for all letters; [details...](#default-email-sender)
Default Email Name|allows to enter the name of the sender; [details...](#default-email-name)
Reply To|allows you to enter the e-mail to which the reply will be sent; [details...](#reply-to)
<!-- tooltip-start recipient_name -->
#### **Recipient Name**

In this function, you can select the fields whose values will be used as recipient names. Then the corresponding name will be attached to a certain [email address](#recipient-email).
<!-- tooltip-end recipient_name -->
<!-- tooltip-start recipient_email -->
#### **Recipient Email**

The current function contains the selected [Email](email.md) field that stores all recipient emails. Thanks to this, when the user sends an email, it will be sent to all addresses that are stored here.
<!-- tooltip-end recipient_email -->
<!-- tooltip-start static_email_sender -->
#### **Static Email Sender**

This function allows you to define how many senders there will be. If it is enabled, all emails will be sent from [one email address](#default-email-sender). Otherwise, each email will be sent from the [email address associated with it](#sender-email-field).
<!-- tooltip-end static_email_sender -->
<!-- tooltip-start sender_email_field -->
#### **Sender Email Field**

This is the field where all email addresses of senders are stored. Every address is usually located at the same item as the [recipient's corresponding email](#recipient-email) or transferred there using [Data Reference](data_reference.md).

>*Sender Email Field* is only available if the [Static Email Sender](#static-email-sender) is **disabled**.
<!-- tooltip-end sender_email_field -->
<!-- tooltip-start use_user_data -->
#### **Use User Data**

If this feature is enabled, the sender's name will be taken from their mail account. Otherwise, the name will be taken from [Sender name field](#sender-name-field).

>*Use User Data* field is only available if the [Static Email Sender](#static-email-sender) is **disabled**.
<!-- tooltip-end use_user_data -->
<!-- tooltip-start sender_name_field -->
#### **Sender Name Field**

The current field stores all of the sender names. Each name is associated with a corresponding [email](#sender-email-field).

>*Sender Name Field* field is only available if the [Static Email Sender](#static-email-sender) is **disabled**.
<!-- tooltip-end sender_name_field -->
<!-- tooltip-start default_email_sender -->
#### **Default Email Sender**

This field is created for the static sender email. That is an email address from which emails will be sent to all of [recipient emails](#recipient-email).

>*Default Email Sender* field is only available if the [Static Email Sender](#static-email-sender) is **enabled**.
<!-- tooltip-end default_email_sender -->
<!-- tooltip-start default_email_name -->
#### **Default Email Name**

This is the name of the [default sender](#default-email-sender). All emails sent from that email will be signed with this name.

>*Default Email Name* field is only available if the [Static Email Sender](#static-email-sender) is **enabled**.
<!-- tooltip-end default_email_name -->
<!-- tooltip-start default_reply_to -->
#### **Reply To**

Here you can enter the email address to which replies to letters will be sent. Just like for the [Default Email Sender](#default-email-sender), this address will be the same for all emails.

>*Reply To* field is only available if the [Static Email Sender](#static-email-sender) is **enabled**.
<!-- tooltip-end default_reply_to -->
### Templates List

The last group of settings configures the dropdown list of the element. Namely, they allow you to add or remove templates from the list.

![Templates list](gh-elements/send-email-templates-list.jpg "Send email templates list")

Name|Description
:---|:---
Template|allows to select the template that will be displayed in the element dropdown list; [details...](#template)
Edit|allows to edit or delete the template; [details...](#edit)

#### **Template**

The first column allows you to select references to email templates that will be available in the *Send Email*. The [names of that templates](#template-name) will be displayed in  drop-down list.

#### **Edit**

This column contains two buttons. One of them allows you to edit the template option, and the second one allows you to delete it.

## Element Style

The current element is very similar to some buttons, but it does not have additional style settings. Of course, these options have all the [standard settings](../Understanding_Gudhub/GH_Element/setting_overview.md) and one [type of interpretation](#interpretation).

![Style of send email element](gh-elements/send-email-element-style.jpg "send email element style")

## Data Model

The current element has quite big data model:

```json
{
    "data_model": {
        "app_for_emailing": "29860",
        "current_item": 1,
        "default_email_name": "Test mail",
        "default_email_sender": "sales@atlasiko.com",
        "default_reply_to": "example@gudhub.com",
        "doc_app_id": "29862",
        "emailSendTheme": "697442",
        "field_sender_email": "697425",
        "field_sender_name": "697431",
        "fields_to_field": {
            "email": "697425",
            "name": "697431"
        },
        "interpretation": [],
        "static_email_sender": 1,
        "template_fieldId": "697442",
        "template_field_name": "697446",
        "templates": [{
            "item": "29862.3205043"
        }],
        "use_user_data": 0,
        "validate": 1
    }
}
```

Name|Type|Description
:---|:---|:---
app_for_emailing|`string`|*contains ID of the application where the destination emails are stored*
current_item|`boolean`|*shows whether the letter template will be taken from the current element*
default_email_name|`string`|*contains name of the default sender*
default_email_sender|`string`|*contains email of the default sender*
default_reply_to|`string`|*contains email address to which replies will be sent*
doc_app_id|`string`|*contains ID of the application from which the email templates will be taken*
emailSendTheme|`string`|*contains ID of the field from which letters themes will be taken*
field_sender_email|`string`|*contains ID of the field from which emails of different senders will be taken*
field_sender_name|`string`|*contains ID of the field from which names of the senders will be taken*
fields_to_field|`object`|*contains all data of the recipient settings*
email|`string`|*contains ID of the field from which emails of the recipients will be taken*
name|`string`|*contains ID of the field from which names of the recipients will be taken*
interpretation|`array`|*contains all element interpretation types*
static_email_sender|`string`|*shows whether the mail of one static sender will be used to send emails or not*
template_fieldId|`string`|*contains ID of the field from which the letters template will be taken*
template_field_name|`string`|*contains Id of the field which contains the name of the template*
templates|`array`|*contains all templates that are available in the current element*
item|`string`|*contains reference to the certain template*
use_user_data|`boolean`|*shows whether the sender's data will be used or not*
validate|`boolean`|*shows whether the recipients' email addresses will be validated before sending the email or not*

## Filtration

This element cannot be filtered out.

## Interpretation

The current element has only one type of interpretation. It looks like an icon with the field name below it.

![Types of send email interpretation](gh-elements/send-email-interpretation-types.jpg "Send email interpretation types")
