# QR Code

**QR code** is a gh-element that allows you to generate the QR code of the item where this element is located. It takes the current item and the [selected view](#view-name), and then the item generates a unique QR code based on their identifiers.

![Using the qr code element](gh-elements/qr-code-work.gif "QR code work")

By scanning the QR code, anyone can open the item on their device. However, it is recommended that you make the desired application [Public](../Rest_API/Sharing/permission_policy.md).

## Functional Characteristics

The only purpose of the current gh element is to provide quick access to a specific product. Thus, each product has its own unique qr-code.

It will allow you to make stickers for inventorying some things. That is, you can store information about the technique in the application, and print out the qr code and stick it on the corresponding devices. Thus, you will have free access to information about the equipment by scanning the code. This element can also be used in post.

## Element Options

All the options you need to configure the current element are assembled in one group called Field Settings.

### Field Settings

This group consists of two [standard settings](../Understanding_Gudhub/GH_Element/setting_overview.md) and one additional.

![Settings of QR code fields](gh-elements/qr-code-field-settings.jpg "QR code field settings")

Name|Description
:---|:---
View name|allows to select the view for QR code generation
<!-- tooltip-start view_name -->
#### **View Name**

This is the only configuration option for the current gh-element. It allows you to select the view that will be opened. That is, the generated QR code is linked not only to a specific item, but also to its view. Due to this, the user can open the item by scanning the code.
<!-- tooltip-end view_name -->
So, with this setting, you can determine what data the user can see after scanning. For example, you can create a view where fields are not editable. In this case, the user will only see the data of the item, but will not be able to edit it.

## Element Style

Most of style options of QR code element are the standard settings. They are described in [Setting Overview](../Understanding_Gudhub/GH_Element/setting_overview.md). That about interpretation, you can read about it [below](#interpretation).

![Style of QR code element](gh-elements/qr-code-element-style.jpg "QR code element style")

### General Settings

The only additional style setting of the QR code is located here.

![General settings of QR code style](gh-elements/qr-code-general-settings.jpg "QR code general settings")

Name|Description
:---|:---
Size|allows to configure size of the QR code

#### **Size**

The current setting is the one that allows you to set the size of the generated QR code. Here you have to enter the required value.

>By default, the measurement **units are pixels**. The default size value is **64 px**.

It determines the length of the QR code side. That is, each side of the code will be extended to the specified length.

## Filtration

This element cannot be filtered out.

## Interpretation

The QR code interpretation types allows the element to be displayed as an icon or as a full QR code.

![Types of QR code interpretation](gh-elements/qr-code-interpretation-types.jpg "QR code interpretation type")

### Icon

Due to this interpretation, the element will be displayed as an icon.

### Default

This interpretation type allows to display the QR code itself.

## Value Format

There is no field value in QR code element.

## Data Model

The data model of the QR code element does not contain a great variety of properties.

```json
{
    "data_model": {
        "interpretation": [],
        "view_id": "1526918"
    }
}
```

Name|Type|Description
:---|:---|:---
interpretation|`array`|*contains all element interpretations*
view_id|`string`|*contains ID of the source view*
