# Web Camera

**Web Camera** is a gh-element that allows user to use their web cameras in the application. This element also has the function of taking photos from users cameras. The resolution and quality of photos can be adjusted in [element settings](#element-options).

## Functional Characteristics

**Web Camera** can be used as an instrument for authentication or registration in your application. That is, to be able to take pictures on the spot to know your customers by sight. The camera is also used to register the arrival of employees to work by taking photos in a certain application.

## Value Format

This element does not have any value.

## Element Options

This element has only one group of configuring settings.

### Field Settings

Field settings contains two standard options and a set of unique settings.

![Settings of web camera field](gh-elements/web-camera-field-settings.jpg "Web camera field settings")

Name|Description
:---|:---
Multiple value|allows to save multiple images made in the current element; [details...](#multiple-value)
Quality Level|allows to select the quality of the image; [details...](#quality-level)
Image recording field|allows to select the field where the taken images will be saved; [details...](#image-recording-field)
Output image width|allows to select the width of the taken image; [details...](#output-image-width)
<!-- tooltip-start multiple_value -->
#### **Multiple Value**

The current setting allows to take the multiple photos. If this function is off, the new photo will replace an old one. Otherwise, all taken photos will be saved in the [multiple image](image.md) field.
<!-- tooltip-end multiple_value -->
<!-- tooltip-start quality_level -->
#### **Quality Level**

The quality of the resulting picture depends on this setting. There is a slider, thanks to which you can choose absolutely any value of quality.
<!-- tooltip-end quality_level -->
<!-- tooltip-start image_field_record -->
#### **Image Recording Field**

This is the setting where you have to select the field where the taken images will be saved.
<!-- tooltip-end image_field_record -->
<!-- tooltip-start output_img_width -->
#### **Output Image Width**

Use this option to select the width of the captured photo. Its height will be adjusted automatically. That is, this setting allows you to scale the resulting photo.
<!-- tooltip-end output_img_width -->
## Element Style

The style of this element can be customized with [standard settings](../Understanding_Gudhub/GH_Element/setting_overview.md). In addition, it has two standard [types of interpretation](#interpretation).

![Style of web camera element](gh-elements/web-camera-element-style.jpg "Web camera element style")

### General Settings

There are two extra settings in this group which allows to adjust the resolution of the camera in the certain [interpretation type](#camera).

>The taken photos will have the same resolution.

![General settings of web camera style](gh-elements/web-camera-general-settings.jpg "Web camera general style settings")

Name|Description
:---|:---
Width|allows to set the width of web camera element; [details...](#width)
Height|allows to set the height of web camera element; [details...](#height)

#### **Width**

The current setting allows you to enter the width of the displaying field.

#### **Height**

This is the function that determines the height of the camera.

## Data Model

The data model of the current element contains the values of main element settings:

```json
{
    "data_model": {
        "interpretation": [],
        "output_img_height": 720,
        "output_img_width": 1280,
        "quality": 100
    }
}
```

Name|Type|Description
:---|:---|:---
interpretation|`array`|*contains all of interpretation types*
output_img_height|`number`|*contains the height of image of web camera*
output_img_width|`number`|*contains the width of image of web camera*
quality|`number`|*contains quality of image*

## Filtration

The web camera can not be filtered out.

## Interpretation

The web camera has only two types of interpretations.

![Types of web camera interpretation](gh-elements/web-camera-interpretation-types.jpg "Web camera interpretation types")

### Camera

This type of interpretation allows the user to display an image from the user's webcam.

### Default

This type of interpretation displays the element as an icon.
