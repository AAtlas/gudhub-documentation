# Sharing Element

**Sharing** is a gh-element that allows to share the application with other users. Also, it allows to manage the access of added users. There are five types of access:

- Blocked - allows you to block access to a specific user to an application
- Read - allows user to only view the application and do not edit it or add items.
- Read&Write - this access allows to edit application data, add new items, but not to configure the application.
- Admin - access to configure the application. But it does not allows to share the current application
- Owner - access of the application creator. For owner all settings are available.

## Functional Characteristics

This item is only used to share any application with GudHub users and manage their permissions for that application.

## Value Format

This element does not save any value in its field.

## Element Options

The sharing element does not have any extra settings besides standard [field settings](../Understanding_Gudhub/GH_Element/setting_overview.md).

![Settings of sharing field](gh-elements/sharing-field-settings.jpg "Sharing field settings")

## Element Style

You may have seen all of style option before in other element articles or in [Setting Overview](../Understanding_Gudhub/GH_Element/setting_overview.md). The element interpretation consists of three types which described in [the end of the article](#interpretation).

![Style of sharing element](gh-elements/sharing-element-style.jpg "Sharing element style")

### General Settings

There is the only additional style option.

![General settings of sharing style](gh-elements/sharing-general-settings.jpg "Sharing general settings")

Name|Description
:---|:---
Show button name|allows to display the button name

#### **Show Button Name**

This is a style of the gh-element that allows you to display the name of the button or hide it.

## Data Model

The sharing data model is very small and contains only an array of interpretation settings.

```json
{
    "data_model": {
        "interpretation": []
    }
}
```

Name|Type|Description
:---|:---|:---
interpretation|`array`|*contains all element interpretations*

## Filtration

Since this item provides access to the entire application, it cannot be filtered out by any filter.

## Interpretation

Sharing is a button, and accordingly it has the same interpretations as the others.

![Types of sharing element](gh-elements/sharing-interpretation-types.jpg "Sharing interpretation types")

### Default

This type allows the element to be displayed as an icon with element name next to it.

### Icon

Due to this type, the sharing appears as an icon with the name of the button below.

### Button

The last type of interpretation allows you to display the current element as a blue button with its name.
