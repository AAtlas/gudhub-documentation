# Table Filter

**Table filter** is a gh-element that allows you to filter items by the value of a specific field. The filtered items are displayed in the [linked elements](#bind-to-fields).

In general, a filter is a button that opens a side menu. There, the user can select the field by which the filtering will be performed and the type of filter. The number of added filters will be indicated on the filter button.

![Adding field for filtering](gh-elements/filter-add.gif "Adding filter")

>Note that you can apply multiple filters to items at once. But only one type of filter is applied to one field.

After applying the filters, only the items that match the filters will remain in the linked element.

![Filtering of items](gh-elements/filter-items.gif "Items filtration")

## Functional Characteristics

The main purpose of this element is to filter items in several elements that display them. Those gh-elements are [Table](table.md), [Calendar](calendar.md), and [Cards](cards.md). Use it to find the items you need.

## Element Options

Filter element has two types of settings. The first one is standard for all elements. The second one is a quite interesting table.

### Field Settings

Besides standard options like Field Name and Name Space, filter has a few other settings.

![Settings of filter field](gh-elements/filter-field-settings.jpg "Filter field settings")

Name|Description
:---|:---
App for filter list|allows to select from which application fields for filtering will be taken; [details...](#app-for-filter-list)
Bind to fields|allows to select for which elements the filter is applied; [details...](#bind-to-fields)
<!-- tooltip-start app_to_filter_list -->
#### **App For Filter List**

The current setting allows you to select an application whose items will be filtered. Since items can be filtered only the values of the fields, the [filter list](#filter-list) consists of the fields of this application. That means here you have to select the source application for the current filter element.
<!-- tooltip-end app_to_filter_list -->
<!-- tooltip-start bind_to_table -->
#### **Bind To Fields**

This function binds the filter to different elements which are located in the same application. These are the elements that can display items. Namely, these are [Table](table.md), [Cards](cards.md) and [Calendar](calendar.md).

>By default they are taken from the application where the filter element is located
<!-- tooltip-end bind_to_table -->
### Filter List

This is the list of all fields of the application. Its options allows to configure which fields will be available for filtering.

![List of filters](gh-elements/filter-list.jpg "Filter list")

Name|Description
:---|:---
Field|column with names of all elements in application; [details...](#field)
Show|allows set whether the field will be displayed in filter list; [details...](#show)

#### **Field**

This column contains names of all fields of the selected [App For Filter List](#app-for-filter-list). They help you find a specific item and configure whether it is enabled for filtering.

#### **Show**

The current function determines whether the field will be enable in filter or not. That is, if this switch is turned off, the corresponding field will not be displayed to users and, accordingly, it will not be possible to filter by it.

![Filter list configuration](gh-elements/filter-fields-in-list.gif "Fields in list")

>So, this switchers are used to limit the filtering of items by certain fields.

## Element Style

Despite of a variety of interpretations which are described [below](#interpretation), all other options are standard. So about them you can read in [Setting Overview](../Understanding_Gudhub/GH_Element/setting_overview.md).

![Style of the filter element](gh-elements/filter-element-style.jpg "Filter element style")

## Filtration

This gh-element filters out others but cannot be filtered.

## Interpretation

Filter has three interpretation types, which differ only in appearance.

![Types of filter interpretations](gh-elements/filter-interpretation-types.jpg "Filter interpretation types")

### Default

The default interpretation allows filter to be displayed as an icon with the name next to it.

### Icon with name under it

This interpretation is like a default one, but the name is under the icon.

### Button

Allows to display filter element as a blue button with white icon and text on it.

## Value Format

This element does not have any value.

## Data Model

The data model of filter contains all values of its settings and interpretations.

```json
{
    "data_model":{
        "app_id_bind_to": 27290,
        "field_id_of_table": "625324",
        "filter_settings": {
            "fields_to_view": [{
                "field_id": "625324",
                "show": 0,
            }]
        },
        "interpretation": []
    }
}
```

Name|Type|Description
:---|:---|:---
app_id_bind_to|`number`|*contains the ID of the application with which the filter is associated*
field_id_of_table|`string`|*contains ID of the table which is filtered*
filter_settings|`object`|*contains settings of the filter*
fields_to_view|`array`|*contains all fields of the application and whether they will be shown in the filter list or not*
field_id|`string`|*ID of the field from the previously selected application*
show|`boolean`|*shows whether the field be shown in the filter list*
interpretation|`array`|*contains all filter interpretations*
