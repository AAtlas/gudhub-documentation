# Vs Code

**VS Code** is a gh-element that allows to edit code in tiny VS Code editor. This element designed for working with JavaScript. It contains all the same options and hotkeys as the customary VS Code Editor.

## Functional Characteristics

The main function of this element is the work with code. It has the necessary tools to work with the code and save it in the item.

## Value Format

The value of the element is an ID of the file that is being edited in it.

```json
{
    "field_value": "944020"
}
```

## Element Options

The gh element settings include only the [standard field setting](../Understanding_Gudhub/GH_Element/setting_overview.md) group.

![Settings of VS code field](gh-elements/vs-code-field-settings.jpg "VS code field settings")

## Element Style

This element has the [similar appearance](#interpretation) to VS Code editor. In addition, it has [standard option](../Understanding_Gudhub/GH_Element/setting_overview.md) for customizing its style.

![Style of VS code element](gh-elements/vs-code-element-style.jpg "VS code element style")

## Data Model

The data model of the current element is quite small:

```json
{
    "data_model": {
        "interpretation": []
    }
}
```

Name|Type|Description
:---|:---|:---
interpretation|`array`|*contains all interpretation types*

## Filtration

This element cannot be filtered out.

## Interpretation

The VS code has three interpretation types.

![Types of VS code interpretation](gh-elements/vs-code-interpretation-types.jpg "VS code interpretation types")

### Default

The first type of interpretation is the default one. It is the working space for entering and saving code.

### Web

The current type displays the entered code as uneditable text.

### Value

The last type displays only the ID of the file created in this element.
