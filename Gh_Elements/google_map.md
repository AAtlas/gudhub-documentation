# Google Map

**Google Map** is a gh-element that allows you to display items on a map depending on their location coordinates. Those coordinates are determined by the related [GPS Coords](./gps_coords.md) element.

>Items for which this field is not filled in are not displayed on the map.

![Work of the google map element](gh-elements/google-map-work.gif "Google map")

Each mark on the map corresponds to a specific item. Therefore, when you click on any of the marks, the item associated with it will open.

![Map navigation functions](gh-elements/google-map-navigation.gif "Map navigation")

The current element also has functionality for easy map navigation. They allow you to change the map resolution, change the display style, use Street mode and Full screen mode.

## Functional Characteristics

The **Google Map** is a unique gh-element that is used when you use location in items. It can be used to create a real estate application. There, the user can map apartments for rent and sale. Such an application would also be suitable for travel agencies. Here you can mark all available tours and store information about them. In the case of personal use, it can be a travel diary. That is, the user can save the places they visit with photos and notes. Also suitable for trip planning.

And this is only a small number of places where the card can be used.

## Element Options

The google map has six groups of settings. Each of them configures different parts of element.

### Field and Map Settings

The first group is the [standard field settings](../Understanding_Gudhub/GH_Element/setting_overview.md). It does not have any additional options.

The second group allows to configure the map and source data.

![Settings of google map field](gh-elements/google-map-field-settings.jpg "Google map field settings")

Name|Description
:---|:---
Application|allows to select the source application; [details...](#application)
Field GPS Coords|allows to select the field from which the GPS coordinates will be taken; [details...](#field-gps-coords)
Field For Marker Label|allows to select the field whose value will be used as the marker name; [details...](#field-for-marker-label)
Add Item on dblclick|allows the user to add a new element after double-clicking on the map; [details...](#add-item-on-dblclick)
Use current location|allows to mark the current user location on the map; [details...](#use-current-location)
View Name For Adding Item|allows to select which view will be opened after double-click; [details...](#view-name-for-adding-item)
Map Style|allows to select the style of the map; [details...](#map-style)
Zoom on select|allows to set the zoom value; [details...](#zoom-on-select)
<!-- tooltip-start application -->
#### **Application**

The first element setting is the most important one. It allows you to select the source application from your app list for the current gh-element. Most of the following settings get their data from this application.

>Items of the current application will be displayed on the map as marks.
<!-- tooltip-end application -->
<!-- tooltip-start field_gps_coords -->
#### **Field GPS Coords**

This is the setting that determines the coordinates of the item mark on the map. Here you have to select the [GPS Coords](./gps_coords.md) element from the [app](#application).

>To be displayed on the map, each object must have this field filled in.
<!-- tooltip-end field_gps_coords -->
Also, this field will be automatically filled in when the item is created by double-clicking. This feature is made available by [Add Item on dblclick](#add-item-on-dblclick).
<!-- tooltip-start marker_label -->
#### **Field For Marker Label**

The map marker consists of marker itself and its label. To display any text on this label, you need to select the field from which the text for it will be taken in this setting.
<!-- tooltip-end marker_label -->

You can use many different gh-elements in it. In general, any element that can display data in text format, such as [Text](./text.md), [Number](./numbers.md), [Item Reference](./item_reference.md), [Date](./date.md), etc. will do.
<!-- tooltip-start add_item -->
#### **Add Item On Dblclick**

This is a setting that allows you to enable the items adding by double clicking on the required location. This means that if it is enabled and the user double-clicks anywhere on the map, an item with the saved marker coordinates will be created.
<!-- tooltip-end add_item -->
Specifically, a [view for adding items](#view-name-for-adding-item) will open, allowing the user to fill in the item and then apply it.

>When it is enabled, an [additional setting](#view-name-for-adding-item) appears below that is important for configuring the feature.
<!-- tooltip-start use_current_location -->
#### **Use Current Location**

The current setting allows you to enable user location detection. This means that the user's current location will be displayed on the map as an unlabeled mark.
<!-- tooltip-end use_current_location -->
>In order for this feature to work, the user must authorize the use of their location in browser security settings.
<!-- tooltip-start view_name -->
#### **View Name For Adding Item**

The current setting is important for **setting up** double-click addition of items. Here you have to select the view that will be opened as an item template.

>This setting appears after you enable the [Add Item On Dblclick](#add-item-on-dblclick) setting.
<!-- tooltip-end view_name -->
After the user fills in the item and applies it, the item's mark will appear on the map.
<!-- tooltip-start map_style -->
#### **Map Style**

The current setting is designed to customize the map. It allows you to set one of three available styles.

- **Standart** - looks like the classic Google map
- **Retro** - displays map in pink colors with a retro marks
- **Night** - allows the map to be displayed in dark colors
<!-- tooltip-end map_style -->
![Changing map style](gh-elements/google-map-style.gif "Map styles")

>After selecting the map will be constantly displayed in that style.
<!-- tooltip-start zoom -->
#### **Zoom On Select**

This is the range bar where you adjust the map zoom value. It zoom out of the map around the selected label by a certain amount. This allows you to focus on the map label.
<!-- tooltip-end zoom -->
The zoom value ranges from 0 to 23, where the last number is the maximum value. You can set the zoom amount using the slider or by typing in the field next to it. In general, this setting works like the [Range element](./range_element.md).

### Hover/Click Action and Marker Filter

The following three groups of settings allow you to configure marker actions and filter items for display on the map.

The first one configures the hover action. The second one configures the click actions. And the last one allows you to add filters to customize which objects will displayed on the map as markers and which will not.

![Hover action, click action and marker filter](gh-elements/google-map-hover-click-action-marker-filter.jpg "Three groups of settings")

Name|Description
:---|:---
Hover Action|allows you to select an action after hovering the cursor; [details...](#hover-action)
Popup Height|allows to set the height of the popup; [details...](#popup-height)
Popup Width|allows to set the width of the popup; [details...](#popup-width)
View For Hover|allows to select the view that will be displayed in the popup; [details...](#view-for-hover)
Click Action|allows to select the action after clicking on the marker; [details...](#click-action)
Dialog Position|allows to select the position in the browser window; [details...](#dialog-position)
View For Click|allows to select the view that will be displayed after clicking on the marker; [details...](#view-for-click)
<!-- tooltip-start hover_action -->
#### **Hover Action**

This is the main setting of the current block. It helps you determine how a mark on the map will react when user hovers over it. Currently, there are only two types of pop-ups or disabling action available.
<!-- tooltip-end hover_action -->
- **Show Popup** - enables a pop-up window, the size of which you can customize using the [following settings](#popup-height).
- **Google Popup** - enables the default Google pop-up window.
- **None** - allows you to disable the hovering action.

Depending on the selected value, other settings will differ.
<!-- tooltip-start height -->
#### **Popup Height**

The next setting allows you to configure the height of the custom pop-up. The size of the pop-up window is specified in pixels. So, here you can enter the height value in pixels.
<!-- tooltip-end height -->
>Along with the [following setting](#popup-width), the current setting is only available if **Show Popup** is selected in the [Hover Action](#hover-action).
<!-- tooltip-start width -->
#### **Popup Width**

Just like the previous setting, the current one allows you to configure the size of the custom pop-up. The height of this window depends on it.
<!-- tooltip-end width -->
>As noted above, this option is available only if the [Show Popup option](#hover-action) is selected.
<!-- tooltip-start view_name -->
#### **View For Hover**

If you selected a pop-up window as a hover action, you also need to select the view that will be displayed there. The current setting allows you to select a view option from the [application](#application).
<!-- tooltip-end view_name -->
>The view selected here is used when **Show Popup** or **Google Popup** is selected in the [Hover Action](#hover-action).
<!-- tooltip-start click_action -->
#### **Click Action**

The current setting allows you to select the action that will happen after clicking on the markers. There are three options, but only two of them are actions. The last one allows you to disable any action.
<!-- tooltip-end click_action -->
- **Open Item** - allows user to open certain items by clicking on any marker.
- **Show Dialog** - allows the user to open items in a large pop-up window, the location of which is determined by the [following setting](#dialog-position).
- **None** - allows you to disable any action after clicking on a map marker.
<!-- tooltip-start dialog_position -->
#### **Dialog Position**

The current setting is unique to the [**Show Dialog** action](#click-action). This lets you set the position of the dialog window in relation to the screen. Namely, you have to choose the side from which the pop-up window will open.
<!-- tooltip-end dialog_position -->
![Changing dialog position](gh-elements/google-map-dialog-position.gif "Dialog position")

There are three available positions for the dialog:

- **Left**
- **Right**
- **Bottom**

They indicate the location from which the pop-up will be opened. You can also open the dialog tab in full screen.
<!-- tooltip-start view_name -->
#### **View For Click**

Since the main action after clicking on the mark is aimed at opening items, you need to select a view. So, the current option where you have to select the view.
<!-- tooltip-end view_name -->
The selected view will open when [Open Item](#click-action) is selected and will be displayed in a pop-up window when [Show Dialog](#click-action) is selected.

#### **Markers Filter**

This is the block that consists of only one setting. This setting is a filter that allows you to determine which items are displayed on the map and which are not. It works as a [Filter element](./table_filter.md).

### Markers

This group of settings allows to create and customize different styles of markers.

![Markers for google map](gh-elements/google-map-markers.jpg "Google map markers")

Name|Description
:---|:---
Icon Color|allows to select the color of the marker icon; [details...](#icon-color)
Icon|allows to select the marker icon; [details...](#icon)
Background Color|allows to select the color of the marker background; [details...](#background-color)
Conditions|allows to set the conditions of the marker usability; [details...](#conditions)
Edit|allows to edit or delete the marker style; [details...](#edit)

#### **Icon Color**

The first customization setting allows you to set the color of the [icon on the map mark](#icon). It works in the same way as the [Color element](./color.md). Therefore, you can choose any color for the icons of a particular group of items.

#### **Icon**

Use the current setting to select the icon for the current item group. This is literally an [Icon](./icon_element.md) that opens a set of available icons. Like the rest of the settings in this block, icons are selected for each type of marker separately.

#### **Background Color**

Just like the [color of the icon](#icon-color) on the marker, you can change the color of the marker itself. The [Color](./color.md) allows you to choose an any color for the markers of the current option.

#### **Conditions**

This setting allows you to define which items will use the current style. Items are selected according to the conditions of the filters. **Conditions** consists of the Filter element, which provides tools for customizing filters.

#### **Edit**

The last setting is designed to allows you edit or delete the current option. It contains two buttons, each of which is responsible for different functions.

## Element Style

Despite the large number of settings and complex functionality, the google map has only [standard style settings](../Understanding_Gudhub/GH_Element/setting_overview.md). There is also not much variety in the [types of interpretation](#interpretation).

![Style of google map element](gh-elements/google-map-element-style.jpg "Google map element style")

## Filtration

You can filter the item by whether the map contains markers. Only the [Value](../Core_API/Filter/value.md) filter allows to do that.

## Interpretation

The google map has two interpretation types:

![Types of google map interpretation](gh-elements/google-map-interpretation-types.jpg "Google map interpretation types")

### Default

The first interpretation displays the element as an icon.

### Map

The second type is an interactive map that displays markers.

## Value Format

This element does not have any field value.

## Data Model

The google map data model contains all the configurations of the current item:

```json
{
    "data_model": {
        "app_id": "28752",
        "click": {
            "side": "bottom",
            "type": "open_item"
        },
        "click_view_id": "1528082",
        "field_id": "679192",
        "filters_list": [],
        "filters_list_color": [{
            "background": "#5e3987",
            "color": "#ee4eb9",
            "filters_list": [],
            "marker": "configuration"
        }],
        "hover": {
            "side": "",
            "type": "show_popup"
        },
        "hover_view_id": "1528082",
        "interpretation": [],
        "label_field_id": "679191",
        "map_style": "retro",
        "popup_height": "10",
        "popup_width": "10",
        "zoom": 0
    }
}
```

Name|Type|Description
:---|:---|:---
app_id|`string`|*contains ID of the source application*
click|`object`|*contains click action settings*
side|`string`|*contains the side of the browser window where the popup will be opened*
type|`string`|*contains the type of the action after clicking on the marker*
click_view_id|`string`|*contains ID of the view that will be opened after clicking on the marker*
field_id|`string`|*contains ID of the field containing the gps-coordinates to determine the location of the markers on the map*
filters_list|`array`|*contains all items filters and their settings*
filters_list_color|`array`|*contains all markers styles and their settings*
background|`string`|*contains the hex code of the marker background color*
color|`string`|*contains hex code of the color of the marker icon*
filters_list|`array`|*contains all filters that determine which items will use current marker style*
marker|`string`|*contains name of the marker icon*
hover|`object`|*contains hover action settings*
side|`string`|*contains the side where the popup window will be located after hovering over the marker*
type|`string`|*contains the type of the action after hovering over the marker*
hover_view_id|`string`|*contains ID of the view that will be displayed in the popup window after hovering over the marker*
interpretation|`array`|*contains all element interpretation types*
label_field_id|`string`|*contains ID of the field that will be used for markers labels*
map_style|`string`|*contains selected style of the map*
popup_height|`string`|*contains height of the popup window*
popup_width|`string`|*contains width of the popup window*
zoom|`number`|*contains value of the map zoom*
