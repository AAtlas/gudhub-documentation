# Filter Advanced

**Filter Advanced** is a gh-element that allows user filter items by the filters determined by its settings. This element also stands out because it allows you to display filters directly in the application, instead of opening an additional window, as in the [Table Filter](./table_filter.md).

![Using the filter advanced](gh-elements/filter-advanced-element.gif "Filter advanced")

This element consists of a button that opens a drop-down list of fields available for filtering and the filters themselves. After selecting a field, its filter is displayed in the element and can be used.

Like other filters, this [filter should be attached](#bind-to-table) to gh-elements such as [Table](./table.md) and [Cards](./cards.md). This will allow you to display the result of the filtering, namely the matched items.

## Functional Characteristics

This filter is mainly used to make the most of the filters. That is, you can put the most used filters on the main view next to the table, for example. Then, to filter out the items, the user will not have to constantly search for the necessary fields and select the filter type in the drop-down lists.

It also allows the user to set up personal filters that will constantly filter items. That is, when the user sets up filters in a given element, they are saved even after reloading the page and exiting the application. Thus, the user does not need to set up filters every time to find the necessary data. For example, the user can set everything up so that only his or her tasks are displayed.

## Element Options

These options allows to configure the element and displayed filters.

### Field Settings

The first group configures specifically the element field.

![Settings of ](gh-elements/filter-advanced-field-settings.jpg "Advanced filter field settings")

Name|Description
:---|:---
App|allows you to select an application to filter; [details...](#app)
Bind to table|allows to select the element that will be filtered out; [details...](#bind-to-table)
Disable filter auto save|allows you to disable the filter saving; [details...](#disable-filter-auto-save)
<!-- tooltip-start app -->
#### **App**

Since the current element filters items, you must select the application in which the items will be filtered. So the current setting is for application selection. This allows you to select one of the applications from your list of apps.
<!-- tooltip-end app -->
<!-- tooltip-start bind_to_table -->
#### **Bind To Table**

The items are usually displayed in the other gh-element like [Table](./table.md) or [Cards](./cards.md). Because of this, to filter elements in the application, you need to bind the filter to one of these elements, then in the current setting you can select it.
<!-- tooltip-end bind_to_table -->
>The element for the current setting is taken from the same application as the items. In other words, it is taken from the [App](#app).
<!-- tooltip-start disable_filter_auto_save -->
#### **Disable Filter Auto Save**

By default, the current element retains the filter values even after reloading the page or exiting the application. This allows users to avoid re-entering values for filtering. But the current settings allow you to block this function, so that the filter value is cleared every time.
<!-- tooltip-end disable_filter_auto_save -->
>In other words, if this option is enabled, filter values will **not be saved** after the page reloads.

### User Filter

These settings allows to set filters of which fields will be displayed in the application and configure the type of the filter.

![Filters added by users](gh-elements/filter-advanced-user-filter.jpg "User filter")

Name|Description
:---|:---
Column|shows the name of the field; [details...](#column)
Show|allows to show filter of the field to be displayed in current element; [details...](#show)
Filter type|allows to select the type of the filter; [details...](#filter-type)

#### **Column**

This column displays names of all of the application fields.

#### **Show**

Due to this function you can configure whether the filter of the field will be displayed in the current element.

#### **Filter type**

Allows you to select the type of filter to be used in the application.

## Element Style

Style of this element can be customized with the help of [standard style settings](../Understanding_Gudhub/GH_Element/setting_overview.md). The interpretation is described in the [end of the article](#interpretation).

![Style of advanced filter](gh-elements/filter-advanced-element-style.jpg "Advanced filter element style")

## Filtration

This is the element that filters out the other elements and no other filters can be applied to it.

## Interpretation

The only type of interpretation allows the item to display all added filters.

![Types of advanced filter interpretation](gh-elements/filter-advanced-interpretation-types.jpg "Filter advanced interpretation types")

## Value Format

The advanced filter does not have any field values.

## Data Model

The most part of the current data model is occupied by array of user filters.

```json
{
    "data_model": {
        "app_id": "28752",
        "field_id": "678842",
        "interpretation": [],
        "user_filter": [{
            "data_type": "close",
            "field_id": 678837,
            "filter_type": "phone_equal_or",
            "show": 0
        }]
    }
}
```

Name|Type|Description
:---|:---|:---
app_id|`string`|*contains ID of the application that is connected with the element*
field_id|`string`|*contains ID of the element that will be filtered using advanced filter*
interpretation|`array`|*contains all element interpretations*
user_filter|`array`|*contains all filters and their settings*
data_type|`string`|*contains type of the filter*
field_id|`number`|*contains ID of the field that will be filtered using this filter*
filter_type|`string`|*contains type of the filter*
show|`boolean`|*shows whether the filter of the certain element is shown in advanced filter or not*
