# Introduction Data Structure

GudHub is a cloud-based low-code platform, you can access it from web browsers. It is a simple resource for easy development. Due to an intuitive interface, you can build your web app piece by piece. This way speeds up the development process many times over.

The basic essence of the GudHub platform is application. You can add a large number of such objects, edit each of them, and delete them at any time.

![Example list of applications](data-structure/data-structure-main-page.png "Application list")

Just created application, called the standard case, has default values, such as name My Application.

![Example of just created application](data-structure/data-structure-just-created-app.png "Main view of just created application")

>From now on this application will be a constant example in this chapter.

My Application contains data, models, views, and all information about them.

Views are the ‘pages’ of the application. All other elements are placed in it. Earlier you could see the main view of My Application. If you tap ‘Add items’, you will see the second one.

![Example of form view](data-structure/data-structure-form.png "Form view of just created application")

In the picture, you can see empty fields that wait for you to enter data. As you may have gathered, that data is one of the application parts mentioned above.

By the way, all buttons, fields, filters, tables, and other blocks are data models. With the use of them, the application can understand what type of data was just been entered.

![Example outputs](data-structure/data-structure-outputs.png "Output table")

You just read about the overall scheme of the application work. The application is a bit more complicated inside than on the exterior. So, if you want to know what its development is based on, welcome to [Data Structure](Application_Data_Structure/data_structure.md) chapter!
