# Apps List

GudHub users can create and get access to shared applications. All of them, including shared ones, are contained in apps_list. And due to this, they are displayed on the main page.
When you start working with GudHub, apps_list is empty, and as you've already figured out, it starts filling up when you create applications or someone shared it with you.

**apps_list** has almost the same properties as [application](data_structure.md). Except for all arrays, they are empty.

Name|Type|Description
:---|:---|:------
app_id|`number`|*unique identification in the system*
app_name|`string`|*application name*
field_list|`array`|*this array contains all data type models*
file_list|`array`|*this array contains data about files loaded into the application*
icon|`object`|*contains your applications icon information*
items_list|`array`|*this array is information storage*
last_update|`number`|*shows last time of update in milliseconds*
permission|`number`|*shows the kind of access you have*
priority|`number`|*position in the list*
privacy|`number`|*shows application is public or not*
sharing_type|`number`|*shows how the application is shared*
show|`boolean`|*shows object is visible in list or not*
trash|`boolean`|*shows application is deleted or not*
view_init|`number`|*shows which view do we use to enter*
views_list|`array`|*this object array contains information about all program views*

## show

Thanks to this property, it is possible to hide your application. Users can customize it.

## privacy

There is one more thing available to users. The function called publish allows them to open access to their application. That means everyone would see a public app without login and password.
