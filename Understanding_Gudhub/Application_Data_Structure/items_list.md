# Items List

Every application has lots of information from inputs. For the application to be able to preserve and exploit that, it uses something like data storage called items_list. It is an object array with all information from inputs. It is empty by default. When you add an item with a need for inputs in application, new object will appear in item_list. It contains details from each field.  

For each input template will be created a new instance with such information:

Name|Type|Description
:---|:---|:------
fields|`array`|*contains information about data;* [*details...*](#fields)
index_number|`number`|*shows index number of the item in the list;* [*details...*](#index-number)
item_id|`number`|*saves unique identification number*
last_update|`number`|*shows last time of update in milliseconds; is used for showing updated file information after reloading the page*

## fields

This property is an object array. It is empty by default. It appears when new data is created. It consists of three properties. field_value saves data value, field_id shows what type of data is value, and data_id is the ID used by the server.

## index_number

Each field has an index number depending on its location in the list. Index starts from 1.
When we delete the item, the index_number of other ones will save their numbers. Despite this, when we add a new item, its index_number will get the value after the last number.

For example, we had 3 items.

![Example table for indexes](application-data-structure/app-data-structure-index-num-before.png "Original table")

```json
[
    {
    "fields": [{...
        "field_value": "first",...
        },...],
    "index_number": 1,
    "item_id": 2918286,
    "last_update": 1638969490135,
    },
    {
    "fields": [{...
        "field_value": "second",...
        },...],
    "index_number": 2,
    "item_id": 2919988,
    "last_update": 1638969479184,
    },
    {
    "fields": [{...
        "field_value": "third",...
        },...],
    "index_number": 3,
    "item_id": 2919989,
    "last_update": 1638969498385,
    }
]
```

If we delete the second one, the third item's index_number will remain 3. The first’s one will remain the same too. The next step is adding a new item that will get number 4.

![Example table after changing indexes](application-data-structure/app-data-structure-index-num-after.png "Table after changes")

```json
[
    {
    "fields": [{...
        "field_value": "first",...
        },...],
    "index_number": 1,
    "item_id": 2918286,
    "last_update": 1638969479184,
    },
    {
    "fields": [{...
        "field_value": "third",...
        },...],
    "index_number": 3,
    "item_id": ,
    "last_update": 1638969498385,
    },
    {
    "fields": [{...
        "field_value": "fourth",...
        },...],
    "index_number": 4,
    "item_id": ,
    "last_update": 1638970078808,
    }
]
```
