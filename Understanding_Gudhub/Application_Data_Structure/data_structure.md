# Overview

>Before reading this chapter, recommend you get acquainted with the application.

Data can have different forms and meanings, various types and values. The way of organizing this huge amount is a data structure. GudHub`s application has a separate object. It was called My Application by default but the user can change its name.
This chapter describes the data structure in the application and takes a detailed look at the main object and explains its properties.

My Application is the main object of application. It contains all application data.

```json
{
    "app_id": 26506, 
    "app_name": "My Application",
    "chunks": [],
    "field_list":[],
    "file_list": [],
    "group_id": 26477,
    "icon": {},
    "items_list":[],
    "last_update": 1637152291180,
    "permission": 4,
    "priority": 0,
    "privacy": 1,
    "show": true,
    "trash": false,
    "view_init": 1289961,
    "views_list":[]
}
```

Below you can see all application properties.

Name|Type|Description
:-----|:-------|:--------
app_id|`number`|*unique identification in the system*
app_name|`string`|*application name*
field_list|`array`|*contains all data type models; [details...](#3-field-list)*
file_list|`array`|*contains data about files loaded into the application; [details...](#5-file-list)*
*icon*|`object`|*contains your applications icon information; [details...](#1-icon)*
items_list|`array`|*this array is information storage; [details...](#4-items-list)*
last_update|`number`|*shows last time of update in milliseconds*
permission|`number`|*shows the kind of access you have*
priority|`number`|*position in the list*
privacy|`number`|*shows application is public or not*
show|`boolean`|*shows object is visible in list or not*
trash|`boolean`|*shows application is deleted or not*
view_init|`number`|*shows which view do we use to enter*
views_list|`array`|*this object array contains information about all program views; [details...](#2-views-list)*

## 1. icon

As you can guess from the title, this object contains your application’s icon information. For example background color, icon color, and what object is shown. [Read more about icon.](icon.md)

## 2. views_list

This object array contains information about all program views. For example, when you just created a new application, there are only two views: Main and Form. If you add new blocks to this application, information about the new view will appear in views_list. [Read more about views_list.](views_list.md)

## 3. field_list

This array contains all data type models. It could be inputs: fields for entry text, date, outputs: tables, task board, or actions: buttons, filter, etc. [Read more about field_list.](field_list.md)

## 4. items_list

This array is information storage. All information from inputs will be saved in the item_list array and you can look for all the data here. [Read more about items_list.](items_list.md)

## 5. file_list

This array contains data about files loaded into the application. When you add files to the application, information about them will appear in file_list. [Read more about file_list.](file_list.md)
