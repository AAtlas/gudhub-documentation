# Icon

The first thing we see in the application is an icon. This sweet tiny picture is a cover that draws attention. Therefore it is no less important part of the application and so it has its own properties. All of them are contained in the object called ‘icon’.

In this chapter, we will look at what information an application needs to know how its icon looks like.

Name|Type|Description
:---|:---|:------
gradient_down|`string`|*lower gradient color;* [*details...*](#gradient-down)
gradient_up|`string`|*upper gradient color;* [*details...*](#gradient-up)
icon_color|`string`|*color of the icon;* [*details...*](#icon-color)
icon_id|`string`|*shows what subject is depicted;* [*details...*](#icon-id)
id|`number`|*shows the icon identification number;* [*details...*](#id)

## gradient_down

Since the icon background has a gradient, we got two colors. This property shows a lower color. It saves HEX color code as string-type data.

## gradient_up

This property shows the upper color of the background gradient. As well as gradient_down, gradient_up saves HEX color code. It is a string type. If upper and lower colors are the same, background will be plain.

## icon_color

This property shows the icon color. It saves HEX color code as a string.

## icon_id

This property shows what is the kind of icon. When you choose the picture for the application icon, its ID will be saved in icon_id. This ID is a string that describes this picture. That means, if you choose picture of a pencil, icon’s ID will be “pencil”.

## id

This property saves unique identification number of the icon. It helps server to understand what icon is in this application.
