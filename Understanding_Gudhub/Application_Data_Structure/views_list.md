# Views List

Before starting this chapter, let’s talk about **views**. The view is the main container for all fields, elements, and blocks in the application. Roughly speaking, it is a space where we piece together application containers, and alternately place them. Every view contains containers that in return can contain other ones or elements.
Now we have a bit more understanding of what view is. So, as you already guessed, **views_list** contained information about all application containers.

All containers have the same structure. Their properties are described in this table :

Name|Type|Description
:-----|:-------|:--------
class|`string`|*CSS classes that will be added to the container*
container_id|`number`|*container ID*
content|`array`|*contains others containers;* [*details...*](#content)
icon_id|`string`|*which icon is displayed*
name|`string`|*view name; users can change it*
priority|`number`|*shows in which order containers are displayed*
template_id|`string`|*shows what type is the block*

>The excepts are the main views and elements. The difference will be described below.

## content

I would like to draw your attention to the only array here. Each container has property content that in turn has its own containers.

```json
{
    "content": [{
        "class": "gh_flex gh_col_height_1 gh_height_px",
        "container_id": 1289962,
        "content": [{
            "container_id": 1289963,…},…],
        "icon_id": "",
        "name": "",
        "priority": 0,
        "template_id": "gh_flex",
    },…]
}
```

As you could read above, containers contain other ones and elements. Since everything is clear with the containers, let’s look at the elements. Unlike other blocks, their content array is empty. Also, they have a special property called **field_id**. It saves field ID.

There is one more thing we should be looking at. The view is like a cover for other containers, therefore its structure has a few differences. The first container does not have container_id and template_id. Instead of that, it has **view_id**.
