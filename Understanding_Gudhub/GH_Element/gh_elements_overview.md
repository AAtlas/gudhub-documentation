# GH-Elements Overview

**gh-elements** are flexible UI elements with lots of options for useful and comfortable configuration. There are more than 100 different elements in GudHub. Each of them carries out its unique functions. This provides the ability to create multifunctional applications.

The most interesting feature of gh-elements is that they does not tied to only GudHub. That means, you are free to add all elements to other sites and use them there.

Despite such distinctions between gh-elements they can be divided into three types:

- Inputs
- Outputs
- Actions

**Inputs** aim to receive data from the user for storage and use in *outputs* and *actions*.

**Outputs** display the interpreted data in items.

**Actions** do not work with data at all. So, they cannot be filtered out. They perform some action to get certain result.

GH-elements can be placed anywhere within the views or even other elements. The simplest example is table which contains element and their outputs.

## Development

Every gh-element is stored in GudHub as an object in [field_list](../Application_Data_Structure/field_list.md) of the application. In turn, these objects contains element location, name, data model and interpretations.

```json
{
    "app_id": 24290,
    "data_model": {},
    "data_type": "text",
    "field_id": 625327,
    "field_id": 625327,
    "field_name": "Text",
    "field_priority": 0,
    "name_space": "text"
}
```

Name|Type|Description
:---|:---|:---
app_id|`number`|*ID of application where the gh-element is located*
data_model|`object`|*contains standard data properties; [details...](#data-model)*
data_type|`string`|*contains the type of data*
field_id|`number`|*ID of the field where the data type is located*
field_id|`number`|*ID of the field where the data type is located*
field_name|`string`|*the name field with this data type*
field_priority|`number`|*shows field priority for filtering*
name_space|`string`|*unique field ID in text format within current application*

### Data Model

Each gh-element has its own unique data model with different varieties of properties. The only one that is the same for everyone is an array of interpretations, about that you can read in [Interpretation chapter](../../Core_API/interpretation.md).
