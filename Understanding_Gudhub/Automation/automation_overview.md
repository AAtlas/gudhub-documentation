# Automation Overview

Most of gh-element has a special settings tab where you can configure additional functions that will be automatically triggered when the element is running. This is called **Automation**.

The work of some gh-elements, such as [Quiz](../../Gh_Elements/quiz.md), [Calculator](../../Gh_Elements/calculator.md), [Smart Input](../../Gh_Elements/smart_input.md), and [String Join](../../Gh_Elements/string_join.md), is based on automation. But for most elements automation can be added additionally. There are a special [nodes](#node) to configure such processes. Their work are based on the [Rete.js](https://rete.js.org/#/docs) modular framework.

## Node

An integral part of the element automation is the **node**. It is with the help of nodes that automation is configured.
Due to them you can configure the automatic operation of the any gh-element.

Besides names and icon, **Node** consists of:

![Appearance of the node](automation/node.jpg "Node")

- [Setting button](#node-settings)
- Delete button
- [Input and Output sockets](#connections)

Each of nodes is responsible for different functions such as creating items or messages, retrieving data from items, updating items, and others. Accordingly, each of them has unique sets of settings.

>Every gh-element has a default node that cannot be deleted. For almost all of them, the default node is the [GH Element Node](../../Automation_Nodes/gh_element_node.md). But there are four elements that have their own unique default nodes:
>
>- **API** that have [API node](../../Automation_Nodes/api_node.md) and [[Response](../../Automation_Nodes/response_node.md) node](../../Automation_Nodes/response_node.md)
>- **Smart Input** have [SmartInput node](../../Automation_Nodes/smart_input_node.md)
>- **Quiz** with [Quiz Node](../../Automation_Nodes/quiz_node.md)
>- **Trigger** contains [Trigger node](../../Automation_Nodes/trigger_node.md)

### Node Settings

Each of nodes has its own unique set of settings. But all of them have one standard option called **Notes**. It is designed to leave any notes in settings. That allows you to note the function of the node. This is very useful when you have a huge number of nodes, it helps not to get confused.

### List of Nodes

There are many nodes that will help you set up automation of elements:

- [API](../../Automation_Nodes/api_node.md)
- [Calculator](../../Automation_Nodes/calculator_node.md)
- Compare Items
- [Constants](../../Automation_Nodes/constants.md)
- [Create Items API](../../Automation_Nodes/create_items_api.md)
- [Delete Items](../../Automation_Nodes/delete_items_node.md)
- Face Detector
- [File Duplicate](../../Automation_Nodes/file_duplicate.md)
- [File Reader](../../Automation_Nodes/file_reader.md)
- [Filter](../../Automation_Nodes/filter_automation.md)
- [Fire Works](../../Automation_Nodes/fire_works.md)
- [Get Item By Item Reference](../../Automation_Nodes/get_item_by_item_ref.md)
- [Get Items](../../Automation_Nodes/get_items.md)
- [GH Element Node](../../Automation_Nodes/gh_element_node.md)
- [Go To Item](../../Automation_Nodes/go_to_item.md)
- [Google Calendar](../../Automation_Nodes/google_calendar.md)
- [If Condition](../../Automation_Nodes/if_condition.md)
- [Item Constructor](../../Automation_Nodes/item_constructor.md)
- [Item Destructor](../../Automation_Nodes/item_destructor.md)
- [Iterator](../../Automation_Nodes/iterator_node.md)
- [JSON Scheme](../../Automation_Nodes/json_scheme_node.md)
- Merge Items
- [Message Constructor](../../Automation_Nodes/message_constructor.md)
- [Object Constructor](../../Automation_Nodes/object_constructor.md)
- [Object Destructor](../../Automation_Nodes/object_destructor.md)
- Object To Item
- [Pop Up Form](../../Automation_Nodes/pop_up_form.md)
- [Populate Element](../../Automation_Nodes/populate_element.md)
- [Populate Items](../../Automation_Nodes/populate_items_node.md)
- [Populate With Date](../../Automation_Nodes/populate_with_date.md)
- Populate With Item Ref
- [Quiz Form](../../Automation_Nodes/quiz_form.md)
- [Quiz Node](../../Automation_Nodes/quiz_node.md)
- [Request](../../Automation_Nodes/request_automation.md)
- [Response](../../Automation_Nodes/response_node.md)
- [Send Email](../../Automation_Nodes/send_email_node.md)
- [Send Message](../../Automation_Nodes/send_message.md)
- [Smart Input Node](../../Automation_Nodes/smart_input_node.md)
- [Trigger](../../Automation_Nodes/trigger_node.md)
- [Turbo SMS](../../Automation_Nodes/turbo_sms_node.md)
- [Twilio](../../Automation_Nodes/twilio_node.md)
- [Update Items API](../../Automation_Nodes/update_items_api.md)
- [Verify Email](../../Automation_Nodes/verify_email_node.md)
- [Voice Machine Detection](../../Automation_Nodes/voice_machine_detection.md)
- [Website Checker](../../Automation_Nodes/website_checker.md)

Some of them, can be used only in the certain gh-elements:

- [Quiz](../../Gh_Elements/quiz.md)
- [API](../../Gh_Elements/api_element.md)
- [Trigger](../../Gh_Elements/trigger.md)
- [Smart Input](../../Gh_Elements/smart_input.md)
- [Task](../../Automation_Nodes/task_node.md)

## Connections

The connection between the nodes is carried out using **sockets**. Through them, you can transfer various data between nodes. More precisely, data is transmitted from the [output socket](#output-socket) of one node to the connected [input socket](#input-socket) of another node. Thus, a chain of nodes will be created.

There three types of data for sockets. Depending on them, the sockets may have different colors:

![Item socket](automation/item-socket.jpg "Item")
![Object socket](automation/object-socket.jpg "Object")
![Value socket](automation/value-socket.jpg "Value")

Due to that colors, you can determine which data the socket accepts or transmits.

As you have already read, **Sockets** also are divided into [Input](#input-socket) and [Output](#output-socket) sockets. They are divided depending on whether the socket accepts a value or transmits it.

>Every of node has unique set of sockets. This means that some nodes may have no input or output sockets, have unique sockets, and also sockets can be added by the user.

### Input Socket

Most nodes require certain data to work. Namely, you have to transfer data that you need to process. All that data is transmitted to the node through the **input sockets**. This socket can also be a trigger to start the node if any data is received.

### Output Socket

Every node processes data in a certain way. But all of them produce some result that can be used in other nodes. Thus, there are **output sockets** to transmit data to the following nodes.

## Node Sequence

As you already know, nodes must be connected to each other to perform certain actions. Sometimes nodes can build a complex structure. Then the question arises in what sequence the nodes work.

>The sequence of nodes work is determined by the [Rete algorithm](#rete-algorithm).

### Rete Algorithm

The **Rete Algorithm** is based on a dynamic data structure that automatically reorganizes itself to optimize the search during its use. It is very effective due to matching algorithm for implementing rule-based systems. The data do not need to be compared with each of the rule, because the static data can be ignored. Read more about [Rete Algorithm](https://rete.js.org/#/docs/engine).

In addition, there is one thing you should remember. The **work starts from the starting node**. That node takes data from the current field or item. And then passes it to the next nodes, which start the work of the next nodes and so on.

>In other words, each node, having finished its process, transmits the received data and thus starts the work of the next node that use the transmitted data.

## Data Model

All data of the element automation is contained in the object called automation. When the element automation is activated for the first time, this object will be added to the gh-element data model.

```json
{
    "automation": {
        "active": 1,
        "model": {
            "id": "trigger@0.1.0",
            "nodes": {
                "data": {},
                "id": 1,
                "inputs": {
                    "inpItems": {
                        "connections": []
                    }
                },
                "name": "Gh Element Node",
                "outputs": {
                    "newValue": {
                        "connections": []
                    },
                    "oldValue": {
                        "connections": []
                    },
                    "updatedItems": {
                        "connections": []
                    },
                },
                "position": [
                    -9.49564801367302, 
                    194.07235901146416
                ]
            }
        }
    }
}
```

Name|Type|Description
:---|:---|:---
active|`boolean`|*shows whether the automation is activated in the element*
model|`object`|*contains the model of the element automation*
id|`string`|*contains version of the trigger*
nodes|`object`|*contains all automation nodes*
data|`object`|*contains data of the node settings*
id|`number`|*contains ID of the node*
inputs|`object`|*contains data about node input sockets; sockets may differ depending on the node*
inpItems|`object`|*contains connections of the input socket; this property is unique for the certain node*
connections|`array`|*contains data about the node that is connected to this socket*
name|`string`|*contains name of the node*
outputs|`object`|*contains data of all output sockets; all sockets are unique for each node*
newValue|`object`|*contains data of the output socket connections; this socket is unique for the certain node*
connections|`array`|*contains data of the node to which the socket is connected*
position|`array`|*contains coordinates that determine the position of the node in the automation space in the element settings*
