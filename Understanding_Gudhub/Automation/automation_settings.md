# Automation Settings

**Automation** is part of the GudHub functionality. The essence of its work is to assemble nodes into a certain chain. In this way you can manually configure the automation functions. The process of setting up automation is described in [Automation Overview](automation_overview.md). And, like all other features, it can be customized in a specific tab.

That tab consists of:

- The [main workspace](#workspace) where you drag and drop nodes, assemble the automation chain, and debug it.
- The [list of nodes](#nodes-list)
- [Debugging](#debug-mode) switch
- Activation switch

The automation tab of some elements has a different look. For example, [API](../../Gh_Elements/api_element.md) has extra button and input field. Other element which have differences are [Trigger](../../Gh_Elements/trigger.md), [Smart Input](../../Gh_Elements/smart_input.md) and [Quiz](../../Gh_Elements/quiz.md).

## Workspace

As was mentioned above, **workspace** is a main area of work with automation. Here you can drag many nodes, configure them and make chains to implement your ideas.

![Automation workspace](automation/workspace.jpg "Workspace")

## Nodes List

This is the **list** where all available nodes are stored.

![List of nodes](automation/nodes-list.jpg "Nodes list")

It consists of a search field and blocks of nodes that can be dragged into the editing area.

![Dragging nodes](automation/drag-and-drop-node.gif "Drag and drop")

## Debug Mode

The **Debug Mode** allows you to check the operation of the chain of nodes before using it.

The process of debugging automation consists of [preparing test data](#mock-settings) and [debugging](#debugging-process) itself.

### Mock Settings

The debug process always needs the test data. Such debugging data are called **mocks**. The mock settings are designed to do the templates for the debugging. Just like the full automation settings, the mock settings are different for several elements.

#### **Standard Elements**

The current settings are typical for most gh-elements. Exceptions will be considered in the next section.

![Mocks settings of standard elements](automation/mocks-settings-standard.jpg "Standard mocks")

Name|Description
:--|:--
Name|allows to give a name to the template; [details...](#name)
Old Value|allows to enter the old value of the current element; [details...](#old-value)
New Value|allows to enter the new value of the current element; [details...](#new-value)
Field|allows to select the field by which the item will be selected; [details...](#field)
Item|allows to select the item; [details...](#item)
Edit|allows to edit or delete the template; [details...](#edit)

#### **Special Elements**

As noted above, only three gh elements have such mock settings.

![Mocks settings of special elements](automation/mocks-settings-special.jpg "Special mocks")

Name|Description
:--|:--
Name|allows to enter the name of the template; [details...](#name)
Output|allows to select the type of the output socket; [details...](#output)
Settings|allows to enter the mock data; [details...](#settings)
Edit|allows to edit or delete the template; [details...](#edit)

### Debugging Process

When the mock template is selected, you can start debugging. To do this, press the start button near the mock settings. Then the first node will be highlighted.

>The *debug process starts* from the **first node after the default node**.

![The beginning of debugging](automation/debug-start.gif "Start debugging")

After that, you can open the node settings and check the data that is transmitted to and from the node.

![Debug settings](automation/debugger-settings.jpg "Debugger")

Then click on the play icon on that node and debugger will switch to the next node. After switching to the next node you can check its data.

![Switching nodes](automation/debug-switching.gif "Nodes debugging")

### Settings Descriptions

This chapter contains the detailed description of the whole settings related to Debug Mode.

#### **Name**

This mock function is mandatory for all types of elements. Its main purpose is the naming of templates. Thus, you can enter any name for your template.

#### **Old Value**

This is a mock value that will be used as the old value of the current element. This setting is available in the automation settings [of almost all items](#standard-elements).

#### **New Value**

The value entered in this field will be used as the new value of the current element during debugging. You can do it in settings of the [standard elements](#standard-elements).

#### **Field**

The current setting works in conjunction with the [following](#item). It allows you to select the field of the current application whose values will be used as names of items. That helps to select item for the debugging.

#### **Item**

As was said above, this option works in pair with [Field](#field). When you select a field in the previous setting, all its values will be displayed in the drop-down list of the current setting. This allows you to select an item for debugging by the value of a particular field.

#### **Output**

This is the function typical for only a [few elements](#special-elements). It allows to select the output socket that determines the type of data for debugging. But [Smart Input](../../Gh_Elements/smart_input.md) has one nuance, in its settings, not the sockets themselves are specified, but the type of element that defines the whole group of sockets.

Each of these elements has its own default node with a unique set of sockets. And the data type depends on the selected socket. Nodes have a name that corresponds to the element:

- API -> [API](../../Automation_Nodes/api_node.md)
- Trigger -> [Trigger](../../Automation_Nodes/trigger_node.md)
- Smart Input -> [Smart Input](../../Automation_Nodes/smart_input_node.md)

So, there are three different sets for different elements:

1. [API](../../Gh_Elements/api_element.md):
   - GET
   - HEAD
   - POST
   - PUT
   - DELETE
2. [Trigger](../../Gh_Elements/trigger.md):
   - Created Items
   - Deleted Items
   - Updated Items
   - Updated File
   - Updated Value
3. [Smart Input](../../Gh_Elements/smart_input.md):
   - Button
   - Input
   - Form
   - List

#### **Settings**

The current setting is designed for large amounts of mock data. It opens a tab where you can enter various objects or arrays. Only [three elements](#special-elements) have this setting.

#### **Edit**

This is the function that consists of two buttons. The first button allows you to edit the template, and the second - to delete it.
