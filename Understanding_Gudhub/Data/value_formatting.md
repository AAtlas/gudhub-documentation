# Value Formatting

Lots of gh-element store some values. Despite of usual databases, GudHub has no column types.

> All values in GudHub are stored as a **string type** data.

That is why, we have introduced some standards of data recording. There are lots of gh-elements that contain some data called *field value*. The each value is formatted according to the element where it is stored.

## Data Presentation

Since all data is stored as a string type, its presentation based on dividers. That means, different types of elements has different dividers or at their absence.

### Single

Any single value is stored as a string data **without any dividers**.

```json
{
    "field_value": "John Dow"
}
```

### Value Range

There lots of element which has two values related to each other. They may circumscribe some range or denote coordinates. Such values are stored separated by a **colon**.

The current divider is inherent to such elements:

- [Range](../../Gh_Elements/range_element.md)
- [GPS Coords](../../Gh_Elements/gps_coords.md)
- [Date](../../Gh_Elements/date.md)

The example of values of such elements:

```json
{
    "field_value": "19.7633057:96.07851040000001"
}
```

### Multiple

Some of elements in GudHub contains the multiple value. This type of value has its own divider.
>All their values will be stored in one string separated by a **comma**.

```json
{
    "field_value": "John Dow, Johana Dow"
}
```

## Types Of Value

Besides of presentation, values can be divided by types. It is because every element has its own specification, so their data must be different.

### Numbers

All values are stored as a string type data. The number is not an exception.

```json
{
    "field_value": "3"
}
```

Generally speaking, the number does not have any features other than floating numbers.

#### **Float**

Floating point values are stored with a **point**. If user enters float value with the comma it will be saved as *NaN*.

```json
{
    "field_value": "5.00"
}
```

### Boolean

The boolean values in GudHub are stored as **1 or 0**. This is because all data is stored as string type data and numbers 1, 0 take less memory than words true, false.

```json
{
    "field_value": "1"
}
```

### Option

All options elements store the numbers of the selected option. Like the previous value type, the current one uses numbers to use less memory.

```json
{
    "field_value": "3"
}
```

Such type of value is used in these gh-elements:

- [Color List](../../Gh_Elements/color_list.md)
- [Text Options](../../Gh_Elements/options.md)
- [Radio Button](../../Gh_Elements/radio_button.md)
- [Radio Icon](../../Gh_Elements/radio_icon.md)

### Date and Time

The date and time values are stored in **milliseconds**. Milliseconds counted since January 1, 1970. All dates after this date are positive values, all dates before are negative values.

Date and time after 1970. `Jul 18 2022 21:00:29`:

```json
{
    "field_value": "1658178029973"
}
```

Date and time before 1970. `Jun 16 1917 02:59:30`:

```json
{
    "field_value": "-1658178029973"
}
```

### References

Items in GudHub can be linked using special generated references. It is the standard way to applications together. The reference consists of the source application ID and a specific item ID, separated by a **point**.

```json
{
    "field_value": "29000.3123100"
}
```

It can mostly be seen as the value of the [Item Reference](../../Gh_Elements/item_reference.md) parameter, but it is also used to configure different elements.

### Files

The files are contained in the file list. Each file has its own **ID**, and it is this ID that is contained in some elements. You can get the file itself using his it.

```json
{
    "field_value": "952766"
}
```

### Documents

The JSON documents in GudHub have their own IDs. It is this identifier that some elements store as a value:

```json
{
    "field_value": "62e0de10b3a26926b24136ef"
}
```

One of elements that contains such value is [Quiz](../../Gh_Elements/quiz.md).

### Color

The selected colors are contained in elements as a appropriate **hex code**.

```json
{
    "field_value": "#ffffff"
}
```

### Phone

The phone is an element whose value have a special divider. The value format of the phone depends of the availability of the country code and the extension. They can be added in [Phone element settings](../../Gh_Elements/phone.md).

>The country code and extension number are divided from the main number by **point**.

The full phone value is looks like this:

```json
{
    "field_value": "1.4842918926.456"
}
```

But the number without that additions have no dividers.
