# Data Philosophy

As you already know from Introduction, GudHub consists of applications that can be connected together. The applications are created by the users.
Each of them consists of three entities: data, model, and views.

## Data

Roughly speaking, data is entered information. It is contained in items. For example, in the picture, we can see the field called ‘Name’ and entered name John.

![Example of data](data-structure/data-structure-john.png "Data")

So, as you may have gathered, *John* is data.

## Models

In turn, items are the very models. In general, they could be divided into three groups: inputs, outputs, and actions.

### Inputs

Inputs are the fields, where you can enter any data. Depending on the model, you can enter different types of data. For example, the image field, where you can add image to the form.

![Example input](data-structure/data-structure-image.png "Input")

### Outputs

Outputs are UI components that displayed data on the screen. A case in point are table, map, charts, etc.

![Example output](data-structure/data-structure-text.png "Output")

In the picture, you can see the example of output text data in the table.

### Actions

All buttons or elements that run some processes by clicking are actions. For example, the add item button adds a new field, and the delete button deletes fields in the table.

![Example actions](data-structure/data-structure-del-add-but.png "Actions")

## Views

Earlier we mentioned the form. In fact, it is one of the application views. View is the space where all elements are placed in. It also allows displaying them and all required information.

![Example main view](data-structure/data-structure-views.png "Main view")
