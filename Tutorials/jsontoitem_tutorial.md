# Tutorial jsonToItems

You already familiar with code structure of the jsonToItems instrument from [the previous chapter](../Utils/jsonToItem/json_to_item.md). At the first sign, it is pretty easy to understand. But like any program code, it has its own nuances and some unclear moments. So, this tutorial will help you to fully understand and to consolidate the material.

> Before starting recommend to take a tutorial [GudHub Getting Started](../Getting_Started/getting_started_article.md) and to overview [jsonToItems](../Utils/jsonToItem/json_to_item.md), and [JSONPath](../Utils/jsonToItem/jsonpath.md). Also, you should have basic knowledge of JSON, and JavaScript or any object-oriented programming language.

## Preparation

In this tutorial we will overview the way of creating items with the help of jsonToItem method. As a result, three new items will be created simultaneously.

The first thing we have to do is to create a new application in GudHub. In the items should be at least 4 fields. It doesn't matter what you call them. But in this tutorial they are called like properties in JSON object.

![Example empty application](img-jsontoitems/utils-json-to-item-jsontoitem-tuto.png "New application")

It is needed to get relevant IDs and to view results.

## Step 0

Due to [GudHub Getting Started](../Getting_Started/getting_started_article.md) you already have launched project in Visual Studio Code and loaded Core Library.

So, create a constant for your authentication key. It is necessary for updating data in GudHub.

```js
import GudHub from '@gudhub/core';
const AUTHKEY = "ESFgfhtJsbjafskajlkmoiwuehyugT//ssefiooqw938u9jiuiOIsasdHIBSpwi20kd79uiohdwg2y81y";
const gudhub = new GudHub(AUTHKEY);
```

This code will grow step by step during this tutorial.

## Step 1

As you already know, jsonToItems method has two arguments: json and fieldsMap. Both are the objects.

At first, we need to create the JSON object which will be passed to the Items method. It can be any valid object. But now we will use a prepared Items:

```json
{
  "gallery": {
    "painting": [
        {
            "period": "baroque",
            "author": "Michelangelo Merisi da Caravaggio",
            "title": "The Calling of Saint Matthew",
            "year": 1600
        },
        {
            "period": "romantic",
            "author": "Joseph Mallord William Turner",
            "title": "The Fighting Temeraire",
            "year": 1839
        },
        {
            "period": "baroque",
            "author": "Diego Velázquez",
            "title": "Mars",
            "year": "",
        }
    ]
}
```

We have to  create a new variable called *json* and then assign the JSON object above to it:

```js
let json = {
  "gallery": {
    "painting": [
        {
            "period": "baroque",
            "author": "Michelangelo Merisi da Caravaggio",
            "title": "The Calling of Saint Matthew",
            "year": 1600
        },
        {
            "period": "romantic",
            "author": "Joseph Mallord William Turner",
            "title": "The Fighting Temeraire",
            "year": 1839
        },
        {
            "period": "baroque",
            "author": "Diego Velázquez",
            "title": "Mars",
            "year": "",
        }
    ]
}
```

## Step 2

The second step is to create a variable called fieldsMap and fill it in with the data. This is an array of objects, so we can fill it with multiple elements at once.

As we know from [jsonToItem chapter](../Utils/jsonToItem/json_to_item.md), every fieldsMap object has two arguments:

```js
{
  field_id: 43135,
  json_path : "$..painting[*].title"
}
```

So, the first step would be getting relevant fields IDs. You can read where you can get them in [How To Get App ID & Item ID & Field ID](../Getting_Started/how_to_get.md). After that we create a new constant array and fill it with fields IDs. Later we will take values from it.

```js
const FIELDID = ["645636", "594814", "594813", "645630"];
```

As for json_path, it has several options of queries which will be shown in [Items of queries](#items-of-queries). But now, we will use such type of queries:

```json
"$..painting[*].period"
```

Considering the number of items, we create four objects.
> Note. The number of objects corresponds to the number of filled fields in item. In turn, the number of items corresponds to the biggest number of values appropriates the one of JSONPath queries.

Depending of the data we need, we can change properties and values in square brackets. The first one shows from which object property the value will be taken. The second one show from which object in array the property will be taken.

For different fields we take different properties and relevant field_id:

```js
let fieldsMap = [
  {
    field_id: FIELDID[0],
    json_path : "$..painting[*].period",
  },
  {
    field_id: FIELDID[1],
    json_path : "$..painting[*].author",
  },
  {
    field_id: FIELDID[2],
    json_path : "$..painting[*].title",
  },
  {
    field_id: FIELDID[3],
    json_path : "$..painting[*].year",
}] 
```

After the first two steps we get the following code:

```js
import GudHub from '@gudhub/core';
const AUTHKEY =  "KJGdsa3JsbjafskajlkmoiwuehyugT//ssefiooqw938u9jiuiOIsasdHIBSpwi20kd79uiohdwg2y81y";
const gudhub = await new GudHub(AUTHKEY);
const FIELDID = ["645636", "594814", "594813", "645630"];

let json = {
  "gallery": {
    "painting": [
        {
            "period": "baroque",
            "author": "Michelangelo Merisi da Caravaggio",
            "title": "The Calling of Saint Matthew",
            "year": 1600
        },
        {
            "period": "romantic",
            "author": "Joseph Mallord William Turner",
            "title": "The Fighting Temeraire",
            "year": 1839,
        },
        {
            "period": "baroque",
            "author": "Diego Velázquez",
            "title": "Mars",
            "year": "",
        }
    ]
};

let fieldsMap = [{
    field_id: FIELDID[0],
    json_path : "$..painting[*].period",
  },
  {
    field_id: FIELDID[1],
    json_path : "$..painting[*].author",
  },
  {
    field_id: FIELDID[3],
    json_path : "$..painting[*].title",
  },
  {
    field_id: FIELDID[2],
    json_path : "$..painting[*].year",
}];
```

## Step 3

Congratulations! Half the job is done.

Actually, the last step is the shortest, but no less important. We create a new variable and call jsonToItem in it. More about this method and its arguments you can read in [jsonToItem](../Utils/jsonToItem/json_to_item.md).

```js
let items = gudhub.jsonToItems(json, fieldsMap);
```

>Now the **items** contains all generated items.

Create a new constant. This is needed to execute the following method. It is contained the current application ID. You can read where to get the app ID in [How To Get App ID & Item ID & Field ID](../Getting_Started/how_to_get.md).

```js
const APPID = "26506";
```

Finally, we can call [addNewItems](../Core_API/item_processor.md) method.

```js
gudhub.addNewItems(APPID, items);
```

>It adds all generated items in the current application.

After all, we can assemble the complete code:

```js
import GudHub from '@gudhub/core';
const AUTHKEY =  "KDJGJsbjafskajlkmoiwuehyugT//ssefiooqw938u9jiuiOIsasdHfsIBSpwi20kd79uiohdwg2y81y";
const gudhub = await new GudHub(AUTHKEY);
const APPID = "26506";
const FIELDID = ["645636", "594814", "594813", "645630"];

let json = {
  "gallery": {
    "painting": [
        {
            "period": "baroque",
            "author": "Michelangelo Merisi da Caravaggio",
            "title": "The Calling of Saint Matthew",
            "year": 1600
        },
        {
            "period": "romantic",
            "author": "Joseph Mallord William Turner",
            "title": "The Fighting Temeraire",
            "year": 1839,
        },
        {
            "period": "baroque",
            "author": "Diego Velázquez",
            "title": "Mars",
            "year": "",
        }
    ]
  }
};

let fieldsMap = [{
    field_id: FIELDID[0],
    json_path : "..painting[(@.length-1)].title",
  },
  {
    field_id: FIELDID[1],
    json_path : "$..painting[*].author",
  },
  {
    field_id: FIELDID[3],
    json_path : "$..painting[*].title",
  },
  {
    field_id: FIELDID[2],
    json_path : "$..painting[*].year",
}];


//item generation
let items = gudhub.jsonToItems(json, fieldsMap);


// add new generated items in application
gudhub.addNewItems(APPID, items);
```

And, as a result, in GudHub application we will see all generated items.

![Example table with data](img-jsontoitems/utils-json-to-item-jsontoitem-tuto-result.png "Result table")

-------------

## Items of queries

In the following Items we use the above code, but change the one part of it so that only one field is filled in generated items. Namely, it is field called **Title**. It has the field ID with index 2 in FIELDID array.

```js
let fieldsMap = [{
    field_id: FIELDID[2],
    json_path : "$..painting[*].title",
  },
]
```

From now on, we will only put different values in the json_path.

1\. The query chooses **all author values**: `$.gallery.painting[*].author`

Items:

![Result of example query 1](img-jsontoitems/utils-json-to-item-query-1.png "Result of query 1")

2\. The first query that chooses **all period values**: `$.gallery..period`

Items:

![Result of example query 2](img-jsontoitems/utils-json-to-item-query-2-3.png "Result of query 2")

3\. The second query that chooses **all period values**: `$..period`

Items:

![Result of example query 3](img-jsontoitems/utils-json-to-item-query-2-3.png "Result of query 3")

4\. The query chooses the **first period** value in list: `$..painting[0].period`

Items:

![Result of example query 4](img-jsontoitems/utils-json-to-item-query-4.png "Result of query 4")

5\. The first query that chooses the **first two period values** in list: `$..painting[0,1].title`

Items:

![Result of example query 5](img-jsontoitems/utils-json-to-item-query-5-6.png "Result of query 5")

6\. The second query that chooses the **first two period values** in list: `$..painting[:2].title`

Items:

![Result of example query 6](img-jsontoitems/utils-json-to-item-query-5-6.png "Result of query 7")

7\. The first query that chooses the **last period** value in list: `$..painting[-1:].title`

Items:

![Result of example query 7](img-jsontoitems/utils-json-to-item-query-7-8-9.png "Result of query 7")

8\. The second query that chooses the **last period** value in list: `$..painting[(@.length-1)].title`

Items:

![Result of example query 8](img-jsontoitems/utils-json-to-item-query-7-8-9.png "Result of query 8")

9\. The query that chooses the **title value** of the objects which has **author Diego Velázquez**: `$..painting[?(@.author=='Diego Velázquez')].title`

Items:

![Result of example query 9](img-jsontoitems/utils-json-to-item-query-7-8-9.png "Result of query 9")

10\. The query that chooses **year value** of the objects which **do not have title Mars**: `$..painting[?(@.title!='Mars')].year`

Items:

![Result of example query 10](img-jsontoitems/utils-json-to-item-query-10.png "Result of query 10")

11\. The query chooses **author values** of paintings with title Mars **or** period baroque: `$..painting[?(@.title=='Mars' || @.period=='baroque')].author`

Items:

![Result of example query 11](img-jsontoitems/utils-json-to-item-query-11.png "Result of query 11")

12\. The query chooses **author values** of paintings without title Mars **and** period romantic: `$..painting[?(@.title!='Mars' && @.period!='romantic')].title`

Items:

![Result of example query 12](img-jsontoitems/utils-json-to-item-query-12.png "Result of query 12")
