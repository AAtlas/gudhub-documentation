# Request Node

**Request node** - a node that allows you to automatically create requests and send them.

![Request Node](automation-nodes/request-node.jpg "Request")

## Inputs

There can be a lot of input sockets by adding new sockets

### Default Input Sockets

There are only one default input socket. It is responsible for starting the node work. Only when any value is received on this socket, a request is generated and sent. No matter what data comes in, the main thing is that the original socket was the same type as the current one.

Name|Data Type|Description
:---|:---|:---
RUN|`value`|*allows to send the request*

### Additional Input Sockets

The additional input sockets can be added using [Inputs Settings](#options). All added sockets can be divided by [Parameter Type](#params-type) and [Input Type](#input-type). These types can be combined in any way.

## Outputs

This node has only one output socket.

### Default Output Sockets

The only output socket of the node returns the request data as an object. This allows you to see the sent data and requests, as well as transmit them to the following nodes.

Name|Data Type|Description
:---|:---|:---
Object|`object`|*returns the sent request*

### Additional Output Sockets

There are no settings to add additional output sockets.

## Options

The current node has two groups of settings. The first one, called **Request Settings**, configures the request itself. And the second group, called **Inputs Settings**, allows you to add headers, parameters, and query parameters to the request. In parallel with new headers and parameters, sockets that accept data for them are created.

![Settings of request and inputs of request node](automation-nodes/request-inputs-settings.jpg "Request and Inputs settings")

Name|Description
:---|:---
URL address|allows to enter the destination address; [details...](#url-address)
Request Method|allows to select the request method of the request; [details...](#request-method)
Body Type|allows to select the type of the request body; [details...](#body-type)
Params Type|allows to determine the type of the custom request parameter; [details...](#params-type)
Input Name|allows to enter the name of input socket; [details...](#input-name)
Input Type|allows to select type of the input data; [details...](#input-type)
Edit|allows to edit or delete the input option; [details...](#edit)
Notes|allows to leave some notes in the node settings; [details...](#notes)

### URL address

Since this node sends requests, you must specify the address to which the request will be sent. This address is entered in the setting called **URL address**.

### Request Method

This function allows you to select the type of requests that will be sent from the current node. Different methods accept different data. There are four types available:

- GET - accepts Query Params
- POST - accepts JSON
- PUT - accepts JSON
- DELETE - accepts Query Params

### Body Type

This is a function that allows you to choose in which format the request body will be sent.

- JSON
- FORMDATA

### Params Type

The current function is created for selecting the type of the parameter:

- Headers
- Params
- Query

### Input Name

**Input Name** is a name of the input socket that accept the data for current parameter. You enter this name yourself, so you can enter any name.

### Input Type

The last option allows you to select the type of parameter data. It also defines the type of input socket. There are three existing types in total:

- Field Value
- Items
- Object

### Edit

This setting consists of two buttons to edit and delete the input option.

### Notes

This setting allows you to leave any notes in the node settings.
