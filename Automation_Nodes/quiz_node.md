# Quiz Node

**Quiz Node** is a special node that is available only for the [Quiz element](../Gh_Elements/quiz.md).  It is in this element by default. This node take data from the Quiz element and allows to transmit it to the next nodes. It is like a start trigger for the work of the [quiz element](../Gh_Elements/quiz.md).

![Node of quiz](automation-nodes/quiz-node.jpg "Quiz node")

## Inputs

This is one of that nodes which do not have any input sockets. This is because it takes data from the element in which the node is located, which is the current [Quiz element](../Gh_Elements/quiz.md).

## Outputs

The work of this node is mostly based on the output socket. They allows to start work of the element where the node is located and to transmit certain data.

### Default Output Sockets

The current node has three output sockets. The first one that is called *Start* usually connects with [Quiz Form](quiz_form.md) to move on to the first message of the [quiz](../Gh_Elements/quiz.md). In short, its purpose corresponds to its name. Other two sockets transmit the certain items.

Name|Data Type|Description
:---|:---|:---
Start|`value`|*allows to go to first message and start the quiz*
Quiz Item|`item`|*transmits the item of the application selected in [quiz element](../Gh_Elements/quiz.md) settings*
Current Item|`item`|*transmits the element that is open and contains [quiz item](../Gh_Elements/quiz.md)*

### Additional Output Sockets

Since this node has no settings, there is no way to add new outputs.

## Options

The current node have no settings and no delete button.
