# API Node

**API Node** is a node that allows to take data from HTTP requests. This data can be used to form a response in other nodes and then transmitted to the [Response node](response_node.md). More precisely, this node allows you to launch other nodes depending on a specific request.

>This node is available only in [API element](../Gh_Elements/api_element.md).

![API node](automation-nodes/api-node.jpg "API")

## Inputs

This node has no input sockets.

## Outputs

The current node has only default output sockets.

### Default Output Sockets

All output sockets of a node allow getting a response to different HTTP requests. More precisely, the response contains the same data as the request, so sockets transmit data of specific requests.

Name|Data Type|Description
:---|:---|:---
GET|`object`|*transmits data of the GET request*
HEAD|`object`|*transmits data of the HEAD request*
POST|`object`|*transmits data of the POST request*
PUT|`object`|*transmits data of the PUT request*
DELETE|`object`|*transmits data of the DELETE request*

### Additional Output Sockets

There are no way to add additional output sockets.

## Options

The current node has no settings.
