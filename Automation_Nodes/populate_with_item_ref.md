# Populate With Item Reference

**Populate With Item Reference** is a node that allows

![Populate with item reference node](automation-nodes/populate-with-item-ref-node.jpg "Populate with item reference")

## Inputs

### Default Input Sockets

Name|Data Type|Description
:---|:---|:---
Source items|`item`|
Destination Items|`item`|

### Additional Input Sockets

This is the node that has no additional input sockets.

## Outputs

### Default Output Sockets

Name|Data Type|Description
:---|:---|:---
New items|`item`|

### Additional Output Sockets

There no additional input sockets in this node.

## Options

**Populate With Item Ref** has only one group one settings.

![Settings of populate with item ref node](automation-nodes/populate-with-item-ref-settings.jpg "Populate with item reference settings")

Name|Description
:---|:---
Source application|
Source field|
Dest application|
Dest field|
Notes|allows to leave some note any notes in node settings

### Source Application

### Source Field

### Dest Application

### Dest Field

### Notes

The current function is designed to leave any notes in the node settings.
