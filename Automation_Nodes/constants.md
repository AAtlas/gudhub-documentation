# Constants

**Constants** is an automation node that allows to create static templates or constant values for using in other nodes. You can enter, save, and edit any data in constant settings. For example, you can use it in conjunction with the [Message Constructor](message_constructor.md). The *Constants* is usually used for saving the templates of messages. After transmitting, that templates will be filled with values in *Message Constructor*.

![Node for creating constants](automation-nodes/constans.jpg "Constants node")

## Inputs

The current node has only one input node, the accepted value of which is not used in the creation of templates.

### Default Input Sockets

The current node has no default input sockets.

### Additional Input Sockets

The current node has only one input socket. It is available when the [Send Constant By](#send-constant-by) is *PUSH*. The push socket is designed to delay the operation of the node until a certain value is transmitted to it.

Name|Data Type|Description
:---|:---|:---
PUSH|`value`|*accepted value will be the trigger to start the node operation*

## Outputs

The *Constants* node do not have any default output sockets, but it have settings which allows it to add unlimited number of additional socket.

### Default Output Sockets

As in the case of input sockets, there are no output sockets by default in this node.

### Additional Output Sockets

The current node can have many output sockets. The number of such socket is depended of the number of constants in [Value Outputs Table](#value-outputs-table). Each of these sockets transmits the corresponding constant value.

## Options

The current node has two block of settings.

### Value Outputs

The first block of settings only configures the start time of the node.

![Value Outputs](automation-nodes/constants-value-outputs.jpg "Constants value outputs")

Name|Description
:---|:---
Send Constant By|allows to select the type of node operation; [details...](#send-constant-by)
Notes|allows to note the needed info about settings; [details...](#notes)

#### **Send Constant By**

The current option allows you to configure the start of the node. Namely, you can delay the operation of the node by selecting the **PUSH** button until some value is transferred to the node. Or you can leave it to work in normal mode using the **PULL** button, that is, the node will start working immediately after the automation is activated.

#### **Notes**

Use this field to leave any notes in the settings of the node.

### Value Outputs Table

The second block is designed to add, update and delete constants.

![Settings of value outputs](automation-nodes/constants-values.jpg "Constants value outputs table")

Name|Description
:---|:---
Constant Name|allows to enter constant name; [details...](#constant-name)
Constant Value|allows to enter the value of the constants; [details...](#constant-value)
Edit|allows to edit or delete the constant; [details...](#edit)

#### **Constant Name**

This is the name of the constant that is used in other nodes to transmit the constant value. It is also used as a name of the output socket.

#### **Constant Value**

This function allows you to enter and view the value of a constant. After clicking on it, the working entry field opens. If the constant already has a value, it will be displayed there.

#### **Edit**

This option has two buttons to edit and delete the constant.
