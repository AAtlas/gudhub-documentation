# If Condition

**If Condition** is an automation node that allows to make some condition and, according to it, transfer data to different nodes. That is, there is a certain condition value that is passed to this node. When some data arrives at a node, it is transmitted to the different output sockets depending on the specified condition. This allows you to control what data will be transmitted to which nodes.

![If condition node](automation-nodes/if-condition-node.jpg "If condition")

## Inputs

The current node has one default socket and [settings](#default-and-inputs-settings) for adding lots of multiple input sockets.

### Default Input Sockets

The default input socket accepts the value that is used to make all conditions. Each output socket has a unique condition, but they are all based on the value from the **IF socket**. This value is compared to a certain constant. If the comparison returns `True`, the related value will be passed to the output socket with this condition.

Name|Data Type|Description
:---|:---|:---
IF|`value`, `item`|*accepts data that have to be compared with some condition*

### Additional Input Sockets

All additional input sockets accept data that can be transferred to any [output socket](#additional-output-sockets). They also can accept two different data types:

- Field Value
- Items

Their their types depends on their [settings](#default-and-inputs-settings). A new additional socket can be added using the [Inputs Settings](#default-and-inputs-settings).

## Outputs

The current node have only additional output settings.

### Default Output Sockets

The current node has no default output socket.

### Additional Output Sockets

Additional output sockets pass values from [input sockets](#additional-input-sockets) that satisfy the condition. A new output socket can be added in the [Outputs Settings](#outputs-settings). The type of socket depends on the type of the linked [input socket](#additional-input-sockets).

## Options

This node has three groups of options divided into two blocks. Two of them configures the additional sockets.

### Default and Inputs Settings

The first block contains settings for standard and additional input sockets.

![Inputs and default settings of if condition node](automation-nodes/if-condition-default-inputs-settings.jpg "Default settings and Inputs settings")

Name|Description
:---|:---
Application|allows to select the application in which the comparison will be performed; [details...](#application)
IF data type|allows to select the type of the [default input socket](#default-input-sockets); [details...](#if-data-type)
Type|allows to select the type of the [custom input socket](#additional-input-sockets); [details...](#type)
Field Name|allows to enter the name of the input socket; [details...](#field-name)
Edit|allows to edit or delete the input socket; [details...](#edit)
Notes|allows to leave some notes in the node setting; [details...](#notes)

#### **Application**

This is the setting where you need to select the application of the items that are passed to the [default input socket](#default-input-sockets). It allows to filter items. Namely, access to the fields and items of the selected application will be provided

>It is required to set up conditions when the [IF socket](#default-input-sockets) is of **type Items**.

#### **IF data type**

The current option allows you to select hte type of the default [input socket](#default-input-sockets). There are two existing types:

- Field Value
- Items

This function determines what type of data the node will accept for comparisons.

#### **Type**

This is the first function of the **Inputs Settings**. It determines the type of input sockets.

There are two available types:

- Field Value
- Items

>Since the output socket type depends on the linked input socket, this function also determines the output socket type.

#### **Field Name**

This is the setting where you can enter the name of the created socket. It will be displayed in the node.

#### **Edit**

This setting contains buttons for editing socket settings and deleting a socket.

#### **Notes**

The current setting allows to leave any notes in the node settings.

### Outputs Settings

The last group has two sets of settings for both types of [IF socket](#default-input-sockets).

- Field Value

    ![Settings of if condition outputs for IF field value type](automation-nodes/if-condition-outputs-field-value.jpg "Outputs for IF field value type")

- Items

    ![Settings of if condition outputs for IF items type](automation-nodes/if-condition-outputs-items.jpg "Outputs for IF items type")

Name|Description
:---|:---
Field Name|allows to enter the name of the custom output socket; [details...](#field-name)
Input to Output|allows to select the input socket that will be connected to the current; [details...](#input-to-output)
Value Condition|allows you to select the type of value comparison; [details...](#value-condition)
Value|allows to enter the value for comparison; [details...](#value)
Some/Every|allows to configure which elements will be compared; [details...](#someevery)
Item Condition|allows to add conditions of items comparing; [details...](#item-condition)
Edit|allows to edit or delete the socket; [details...](#edit)

#### **Output Field Name**

This is the function that allows you to enter the name of the created output socket. That name will be displayed in the node.

#### **Input to Output**

This is the setting that allows you to link the current output socket to one of the created input sockets. The **Input to Output** contains a list of [input sockets](#additional-input-sockets).

>Note that you must create an input socket before you start configuring this setting for the socket to appear in the list.

The data will be transferred to the current output from the selected input.

#### **Value Condition**

The current function allows you to select the sign of the condition. This is the **field value** comparison setting for the [IF socket](#default-input-sockets). It contains the drop down list of signs (>, <, <=, =>, =, !=)

#### **Value**

This is a field for entering a value for comparison. The value is constant for one output socket.

This is also the setting of the **field value** type for the [IF socket](#default-input-sockets).

#### **Some/Every**

This switcher determines whether all the passed values must match the condition or at least one.

>This function is useful when an array of values is passed to a node.

By default, the switch is in the **Some** position, meaning that there must be **at least one match**. When the switch is in the **Every** position, the condition requires that **all values in the array match**.

>This function is intended for the **Field Value** type of the [IF socket](#default-input-sockets).

#### **Item Condition**

This is the setting that allows to make conditions for items. It opens the filter settings and allows you to filter items by different fields of the [application](#application). **Item Condition** is available only for the [default socket](#default-input-sockets) of the **Items** type.
