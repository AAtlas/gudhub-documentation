# Get Item By Item Ref

**Get Item By Item Ref** is a node that allows you to get any element by its reference, indicated in some field. The work of this node based on the [Item Reference](../Gh_Elements/item_reference.md). It is the reference that consists of application ID and item ID. At this reference you can get access to the item.

![Get item  by ref node](automation-nodes/get-items-by-item-ref-node.jpg "Get item by ref")

## Inputs

The current node contains only default input sockets.

### Default Input Sockets

The current node can accept items in two ways:

- Accepts item
- Accept reference of item

This means that you can **pass the complete** item with the desired [Item Reference](../Gh_Elements/item_reference.md) to the node *or* **pass only its reference**, previously obtained in the [Item Destructor](item_destructor.md).

Name|Data Type|Description
:---|:---|:---
Items|`item`|*accepts items with needed reference field*
Item ID|`value`|*accepts item reference of the source item*

### Additional Input Sockets

The current node have no additional input sockets.

## Outputs

As with the input sockets, this node has only default output sockets.

### Default Output Sockets

The only output socket returns the item gotten by its reference.

Name|Data Type|Description
:---|:---|:---
Items|`item`|*returns item by reference*

### Additional Output Sockets

There are no settings to add any additional output sockets.

## Options

The current node has quite small number of settings. Almost all of them set up the source of the item reference.

![Default settings of get item by item ref node](automation-nodes/get-item-by-item-ref-default-settings.jpg "Get item by item ref settings")

Name|Description
:---|:---
Application|allows to select application with item reference; [details...](#application)
Field|allows to select the field with item reference; [details...](#field)
Notes|allows to leave any notes in the node settings; [details...](#notes)

### Application

For this node, you need to select an application that contains fields with references to the items you need.

### Field

This is a field that contains references to the required items. The field must be [**Item Reference** element](../Gh_Elements/item_reference.md).

### Notes

This field is created to leave some notes in the node settings.
