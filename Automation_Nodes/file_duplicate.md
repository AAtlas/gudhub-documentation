# File Duplicate

**File Duplicate** is a node that allows to duplicate different files to the selected applications. It can be used with files of various formats.

![File duplicate node](automation-nodes/file-duplicate-node.jpg "File duplicate")

## Inputs

There are two input sockets in this node.

### Default Input Sockets

By default, the input slots accept the data of the file to be copied and the item to which the file is to be copied. The second is additional information to that specified in the [settings](#options).

Name|Data Type|Description
:---|:---|:---
Items|`item`|*accepts items where the file should be duplicated*
Files ID|`value`|*accepts ID of the file that will be duplicated*

### Additional Input Sockets

The current node has no additional sockets.

## Outputs

The current node has no output sockets other than the default one.

### Default Output Sockets

The only output connector of the node allows you to get an item with a duplicated file.

Name|Data Type|Description
:---|:---|:---
Items|`item`|*transfers items with duplicate files*

### Additional Output Sockets

There are no settings for adding additional output sockets.

## Options

**File Duplicate** has only one block of settings.

![Default settings of file duplicate node](automation-nodes/file-duplicate-default-settings.jpg "File duplicate settings")

Name|Description
:---|:---
Dest Application|allows you to select the program into which the file will be duplicated; [details...](#dest-application)
Field for file|allows to select the field into which the file will be duplicated; [details...](#field-for-file)
Notes|allows to leave some notes in the node settings; [details...](#notes)

### Dest Application

The first function is responsible for selecting the destination program to which the file will be duplicated.

### Field For File

The current setting is necessary to select the field to which the file will be duplicated. This field is located in the [previously selected application](#dest-application).

### Notes

This field allows you to leave any notes in the node settings.
