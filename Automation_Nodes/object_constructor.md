# Object Constructor

**Object Constructor** is a node that allows you to automatically assemble an object from various transmitted data.

![Object constructor node](automation-nodes/object-constructor.jpg "Object constructor")

## Inputs

The current node has one default node and settings for adding additional input sockets.

### Default Input Sockets

The default socket accepts data of main object. It is the mandatory socket that accepts data of the HTTP request.

Name|Data Type|Description
:---|:---|:---
Object|`object`|*accepts the object for the construction of a new*

### Additional Input Sockets

All additional sockets are created to fill the object with some data. The type of socket can be selected in [Value Input settings](#options). They can also have an arbitrary name.

Name|Data Type|Description
:---|:---|:---
Value|`value`|*accepts values for new object*
Item|`item`|*accepts items for new objects*
Additional Object|`object`|*accepts object for generating a new object*

## Outputs

The current node has only one output socket.

### Default Output Sockets

A single output socket by default returns the created object and allows it to be passed to the following nodes.

Name|Data Type|Description
:---|:---|:---
Object|`object`|*transfers the newly created object*

### Additional Output Sockets

The additional output sockets cannot be added to this node.

## Options

The single block of node settings is designed to add properties to the future object. Also, a corresponding input socket will be created for each new property.

![Settings of value input of object constructor](automation-nodes/object-constructor-value-inputs.jpg "Object constructor value inputs")

Name|Description
:---|:---
Property Name|allows to enter the name of the property that will be displayed in the result object; [details...](#property-name)
Type|allows to select the type of the property; [details...](#type)
Input Socket Name|allows to enter the name of the property socket; [details...](#input-socket-name)
Edit|allows to edit or delete the property; [details...](#edit)
Notes|allows to leave notes in the node settings; [details...](#notes)

### Property Name

This is the name of the property that will be used in the result object.

### Type

This is the function that allows to select the type of the property. This function also defines the type of [socket](#input-socket-name) that will receive value of the property. There are three existing types:

- Value
- Item
- Object

### Input Socket Name

This is the name of the input node that accepts property values from other nodes.

### Edit

This is the column that contains two buttons for each of properties. One of them allows you to edit the property, and the other - to delete it.

### Notes

This field is used to leave any notes in the node settings.
