# Populate Items Node

**Populate Items** is a node that allows to fill items with the certain static data. Namely, due to it you can configure the filling of the certain fields of the destination item.

![Populate items node](automation-nodes/populate-items.jpg "Populate items")

## Inputs

*Populate Items* has only one input socket.

### Default Input Sockets

The node socket by default accepts elements that must be filled with some data.

Name|Data Type|Description
:---|:---|:---
Items|`item`|*accepts items which have to be filled*

### Additional Input Sockets

The current node has no additional input sockets.

## Outputs

There are the only one output socket in this node.

### Default Output Sockets

The output socket of the current node return items filled with the certain data.

Name|Data Type|Description
:---|:---|:---
Items|`item`|*transmits updated items*

### Additional Output Sockets

There are no settings for adding an additional output socket in this node.

## Options

The current node as two types of settings. One of them configures entire node. And the other configures which fields will be filled with which data.

### Default Settings

Default settings contain basic node settings, without which the node will not work.

![Default settings of populate items node](automation-nodes/populate-items-default-settings.jpg "Populate items settings")

Name|Description
:---|:---
Application|allows to select the application whose elements will be filled with data; [details...](#application)
Keep Whole Data|allows you to duplicate data from the source item to the items to be filled; [details...](#keep-whole-data)
Notes|allows to leave some notes in settings; [details...](#notes)

#### **Application**

Here you can select the destination application. It will be filled with all data from [Data To Populate](#data-to-populate) settings.

#### **Keep Whole Data**

This is a very useful function that allows duplicating data from the source item to the destination one. Namely, when this function is enabled, the fields of the destination item will be filled with data from the corresponding fields of the source item, if they are not filled with [data from the node settings](#data-to-populate).

#### **Notes**

Use this function to leave any notes in the settings of the current node.

### Data To Populate

The last block of settings allows you to enter data to fill in. However, these settings are optional.

![Setting of populate items data](automation-nodes/populate-items-data-to-populate.jpg "Data to populate settings")

Name|Description
:---|:---
Field|allows to select the field what will be filled; [details...](#field)
Value|allows to enter the data for filling the selected field; [details...](#value)
Edit|allows to edit of delete the option; [details...](#edit)

#### **Field**

This is an option where you can select a field to fill with data from the next setting.

#### **Value**

This is the data that will be the data that will fill the field. You enter the value yourself. It will be the same for all items passing through the node.

#### **Edit**

This is the column that contains buttons for editing and deleting the option.
