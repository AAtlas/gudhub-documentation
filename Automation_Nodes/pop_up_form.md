# Pop Up Form

**Pop Up Form** is a node that displays a pop-up window with message and answer for it. When updating an item, this node will open a pop-up window with a [pre-entered message](#message) and [answers](#answers) below. You can connect any nodes to these answers. This will allow users to choose the function that will work after their click.

>This node is available only in the automation settings of the [Smart Input](../Gh_Elements/smart_input.md) element.

![Pop up form node](automation-nodes/pop-up-form-node.jpg "Pop up form")

## Inputs

The current node has no input sockets other than the default one.

### Default Input Sockets

The current node does not accepts any data. Although the single input socket has a value type, it only starts the node.

Name|Data Type|Description
:---|:---|:---
Start|`value`|*starts the work of the node*

### Additional Input Sockets

There are no additional input sockets.

## Outputs

Unlike input sockets, the number of output sockets is unlimited due to the [settings](#answers).

### Default Output Sockets

The default output socket of the current node allows you to pass the message displayed in the pop-up window to the following nodes.

Name|Data Type|Description
:---|:---|:---
Message|`value`|*transmits the message from settings*

### Additional Output Sockets

An additional socket will be added when you add a new answer in [settings](#answers). All of them allow the automation process to jump to certain nodes depending on the user's response.

## Options

The node options consist of two block of settings.

### Default Settings

![Default settings of pop up form node](automation-nodes/pop-up-form-default-settings.jpg "Pop up form settings")

Name|Description
:---|:---
Message|allows to enter the message of the pop-up window; [details...](#message)
Notes|allows to leave some notes in the node; [details...](#notes)

#### **Message**

The first function of the block allows you to enter text that will be displayed in a pop-up window.

#### **Notes**

Current setting is a field where you can leave some notes about the node or any other text you wish.

### Answers

![Settings of pop up form answers](automation-nodes/pop-up-form-answers.jpg "Answer settings")

Name|Description
:---|:---
Text|allows to enter the text of the button; [details...](#text)
Output Name|allows to assign a name to the output socket; [details...](#output-name)
Edit|allows to edit or delete the answer; [details...](#edit)

#### **Text**

Each of the answers is displayed as a button in a pop-up window. The current setting allows you to enter the text of this button.

#### **Output Name**

When you create a new answer, a new output socket will be created for it. **Output Name** allows you to name this socket anything you want.

#### **Edit**

This is a setting that consists of two buttons to edit and delete the answer.
