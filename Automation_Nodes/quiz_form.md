# Quiz Form

**Quiz Form** is a special node that is available only for [Quiz element](../Gh_Elements/quiz.md). It allows you to generate a message with any number of responses.

![Quiz Form Node](automation-nodes/quiz-form.jpg "Quiz Form")

## Inputs

The input sockets take all needed data for generating messages and allows to start the node work.

### Default Input Sockets

The *Quiz Form* cannot start until the single default input socket is connected to the previous node.

Name|Data Type|Description
:---|:---|:---
Start|`value`|*due to this socket the work of the node starts*

### Additional Input Sockets

The Quiz Form has only one additional input socket called **Message**. It accepts any messages from other nodes, most often from the [Message Constructor](message_constructor.md).

>This socket is only available if [Dynamic Message](#dynamic-message) is enabled.

Name|Data Type|Description
:---|:---|:---
Message|`value`|*receives messages from other nodes*

## Outputs

The outputs of the current node are the ramification. Thanks to the answers, the user can receive different messages.

### Default Output Sockets

Name|Data Type|Description
:---|:---|:---
Message|`value`|*transmit message of the current node*

### Additional Output Sockets

The Quiz Form has all settings to create new output sockets. They represent variants of responses to messages from the previous node. These sockets also allow you to jump to one of the following nodes, depending on the answer you select.

>Despite the fact that these sockets are additional, they are the most used.

### Default Settings

The default settings are for configuring the node message. That message will be displayed in the [Quiz element](../Gh_Elements/quiz.md).

![Default settings of quiz form node](automation-nodes/quiz-form-default-settings.jpg "Quiz form settings")

Name|Description
:---|:---
Dynamic Message|allows to generate the dynamic messages; [details...](#dynamic-message)
Message|allows to enter the static message; [details...](#message)
Notes|allows to leave notes in settings; [details...](#notes)

#### **Dynamic Message**

When this function is enable the nodes can accept the dynamic messages. These are message templates that are automatically populated with data from items. They are transmitted through an [additional input socket](#additional-input-sockets).

#### **Message**

This is the function that contains a static message that will be displayed in the [Quiz element](../Gh_Elements/quiz.md). It is available only if the [Dynamic Message](#dynamic-message) is turned off.

#### **Notes**

This feature allows you to leave any notes exactly in the node settings. For example, you can note here the purpose of the current node.

### Answers

The second block of settings is for configuring the answers to the message in the current node. Each of the answer will be displayed below the message as a green button with text.

![Answer settings of quiz form](automation-nodes/quiz-form-answers.jpg "Quiz form answer")

Name|Description
:---|:---
Text|allows you to enter the answer to the message; [details...](#text)
Output Name|allows to enter the name of the output socket; [details...](#output-name)
Edit|allows to edit or delete the answer; [details...](#edit)

#### **Text**

This field is for the entering the text of the answer. This text will be displayed on the answer button.

#### **Output Name**

The current function allows to customize the name of the output socket.

#### **Edit**

Use this column to delete or edit any answer option.
