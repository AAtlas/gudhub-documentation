# File Reader

**File Reader** is a node that allows you to get data from different files. Since the gh-element stores only file identifiers, it is difficult to use data from them. That's why this node exists specifically to receive data in text format.

![File reader node](automation-nodes/file-reader-node.jpg "File reader")

## Inputs

The only input socket in this node is the default socket.

### Default Input Sockets

Using the current socket, you can pass the ID of the desired file to the node to get the data stored in it.

Name|Data Type|Description
:---|:---|:---
File ID|`value`|*accepts the identifier of the file whose data is to be retrieved*

### Additional Input Sockets

This node has no additional input sockets.

## Outputs

As with the input socket, this node has only one output socket.

### Default Output Sockets

From this output socket, you receive data from a file in the form of text data.

Name|Data Type|Description
:---|:---|:---
Data|`value`|*returns data from a file*

### Additional Output Sockets

The current node also has no additional output sockets.

## Options

The current node has only two configuration parameters that determine the location and type of the output file.

![Default settings of file reader node](automation-nodes/file-reader-default-settings.jpg "Default settings")

Name|Description
:---|:---
Application|allows you to select the application from which files will be taken; [details...](#application)
File Type|allows to select the type of the file from which the data will be retrieved; [details...](#file-type)
Notes|allows to leave any notes in the node settings; [details...](#notes)

### Application

Here you have to select the application there the files for reading are stored. This allows the node to retrieve file identifiers from some field in this application.

### File Type

The **File Reader** can retrieve data not only from files but also from documents. To choose the one you need, select the appropriate type:

- **Text** for files
- **Document** for documents

### Notes

This field is for your personal notes. Here you can specify the purpose of the node.
