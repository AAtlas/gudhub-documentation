# Filter Node

**Filter** is a node that allows to automatically filter the transmitted items. It allows you to compare items from different applications or filter by values of the transmitted items.

![Filter automation node](automation-nodes/filter-node.jpg "Filter node")

## Inputs

The current node has one input socket by default and has settings for adding new sockets.

### Default Input Sockets

A single socket by default accepts items of the selected application to filter them.

Name|Data Type|Description
:---|:---|:---
Items|`item`|*accepts items that will be filtered*

### Additional Input Sockets

There are a separated block of settings that allows you to add additional sockets. Those sockets accept the data

## Outputs

Two output sockets are provided. Both of them are default.

### Default Output Sockets

The current node has two default output sockets. Due to them, the node returns and transmits the filtered elements and their quantity to the next node.

Name|Data Type|Description
:---|:---|:---
Items|`item`|*transmits items which have been filtered*
Quantity|`value`|*transmits the number of filtered elements*

### Additional Output Sockets

The current node has no additional output sockets.

## Options

The current node has its own unique settings, which are divided into two blocks.

### Default Settings

The first block contains the basic settings of the node. They configure the operation of the node.

![Default settings of filter node](automation-nodes/filter-default-settings.jpg "Filter default settings")

Name|Description
:---|:---
Application|allows to select the application whose elements will be filtered; [details...](#application)
Filter Variable|allows to block variable filters; [details...](#filter-variable)
Add Filter|allows to add different filters by which items will be filtered; [details...](#add-filter)
Notes|allows to leave notes in settings; [details...](#notes)

#### **Application**

Allows you to select the application that will be used to filter the transferred items. Namely, the items of this application will be compared with the items passed to the node.

#### **Filter Variable**

When this option is enabled, the variable filters in the [following setting](#add-filter) will not work. That allows you to block some filters in one click without deleting them.

#### **Add Filter**

This is the standard filter setting. Using it you can add a lot of filters to the node. They allows to filter out items different values, variables, and fields.

#### **Notes**

The current field is designed to leave any notes in settings of the current node.

### Value Inputs

The second block allows you to accept certain values for the filtering elements. When adding a new option, a new input box will be created.

![Settings of filter value inputs](automation-nodes/filter-value-inputs.jpg "Filter value inputs")

Name|Description
:---|:---
Field|allows to select field by value of which items will be filtered; [details...](#field)
Field Name|allows to enter the name of input socket; [details...](#field-name)
Search Type|allows to select the type of the filter; [details...](#search-type)
Edit|allows to edit or delete the option; [details...](#edit)

#### **Field**

In this setting you can select the field by which the accepted positions will be filtered. More precisely, here you can create a new input socket to compare specific values.

#### **Field Name**

This function allows you to enter any name for the new socket. Due to this, you can note what data this socket receives.

#### **Search Type**

This setting is designed for selecting the filter type for the [field](#field). That allows to filter elements in different ways. Each [field](#field) has a unique set of search types.

#### **Edit**

This is the column that contains two buttons. One of them allows you to edit the case, and the other - to delete it.
