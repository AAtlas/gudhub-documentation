# Object Destructor

**Object Destructor** - a node that allows you to decompose an object into parts. This is useful for getting some data from the object.

>If an array of items is passed to this node, it returns only one item.

![Object destructor node](automation-nodes/object-destructor.jpg "Object destructor")

## Inputs

This node has only one input socket.

### Default Input Sockets

A single input socket accepts the object to be disassembled.

Name|Data Type|Description
:---|:---|:---
Object|`object`|*accepts the source object to be disassembled*

### Additional Input Sockets

There are no settings to add additional sockets to this node.

## Outputs

The number of output sockets depends on the number of values to be obtained.

### Default Output Sockets

There are no default output sockets in this node.

### Additional Output Sockets

This node provides the ability to add many additional output sockets. After you configure what data should be received from the source object in [Value Outputs](#value-outputs), the output socket will be created.

Name|Data Type|Description
:---|:---|:---
Result Object|`object`|*return the gotten object*
Result Value|`value`|*returns the gotten value*

## Options

The current node has three blocks of settings. Only [Value Outputs](#value-outputs) configures node. The other two blocks allow you to test the operation of the node with your settings.

### Mock

This block of settings is used for testing the node work. It is used as support tool for settings.

![Mock settings of object destructor](automation-nodes/object-destructor-mock.jpg "Object destructor mock")

Name|Description
:---|:---
Code Editor|allows to enter the mock object; [details...](#editor)
Notes|allows to leave any notes in settings; [details...](#notes)

#### **Editor**

This is the space in settings for entering the mock object. Enter a mock object here to check if the correct data will be retrieved from the source object. The data obtained by the [Value Outputs](#value-outputs) will be displayed in [Output Result](#output-result).

#### **Notes**

This setting is created for notes. This gives you the opportunity to note the purpose of the node.

### Value Outputs

This block of settings allows you to configure the output values.

![Settings of node value outputs](automation-nodes/object-destructor-value-outputs.jpg "Value outputs")

Name|Description
:---|:---
Search Type|allows to select the type of data that will be searched in the source object; [details...](#search-type)
Property Name|allows to enter the name of the property that you need to get from the object; [details...](#property-name)
Type|allows to select the type of the output data; [details...](#type)
Output Socket Name|allows to enter the name of the output socket; [details...](#output-socket-name)
Edit|allows to edit or delete the output option; [details...](#edit)

#### **Search Type**

This function allows you to select the type of data that will be obtained from the source object. More precisely, you choose how to search for data: by *Value* or *Query*.

#### **Property Name**

Use this field to enter the name of the property that you need to get out of the source object. The property whose name you enter will be displayed in the [Output Result](#output-result). Access to properties is carried out using a special query language - [JSON Path](../Utils/jsonToItem/jsonpath.md). It means that with the help of queries you can get data even from multi-level objects and filter which data will be taken.

#### **Type**

This is the function that allows you to select in which format the value will be returned. There are two types of the result:

- Object
- Value

#### **Output Socket Name**

This setting allows you to enter the name of the output socket that transmits the value or object received from the source object.

#### **Edit**

This is the column that contains two buttons for each of options. One of them allows you to edit the option, and the other - to delete it.

### Output Result

This option is used in combination with [Mock](#mock). It displays the data searched for in the [Value Outputs](#value-outputs). This will allow you to check the settings and correct operation of the node.

![Block with output results](automation-nodes/object-destructor-output-result.jpg "Output result")
