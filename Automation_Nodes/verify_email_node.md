# Verify Email Node

**Verify Email** is a node that allows you to automatically verify email addresses. This means that it checks whether the email address exists and whether it is possible to send emails to it or not. This node has two different modes for working with different types of data:

- Value

    ![Node in value mode](automation-nodes/verify-email-node.jpg "Verify email value node")

- Items

    ![Node in items mode](automation-nodes/verify-email-node-items.jpg "Verify email items node")

## Inputs

The **Verify Email** has two sockets in total but they are interchangeable. That is, the available socket depends on the [node settings](#mode).

### Default Input Sockets

The current node accepts email addresses in different formats. Depending on the node [mode](#mode), emails can be transmitted as separate values or as parts of some items.

Name|Data Type|Description
:---|:---|:---
Email|`value`|*accepts emails that require verification*
Items|`items`|*accepts items that contain email addresses for verification*

### Additional Input Sockets

The current node has no additional input sockets.

## Outputs

The current node has two groups of output sockets. Any group can be used only with certain [settings](#mode).

### Default Output Sockets

As you can see, this node has five default output sockets. All of them allows to get the status of the email address. There are four existed statuses:

- Deliverable
- Risky
- Undeliverable
- Unknown

There is a separate socket for each of them in the Value mode. Namely, each socket returns the specific status. In the case of the Items mode, unlike the previous ones, the Items socket returns an item whose status is stored in the [selected field](#result-field-id).

Name|Data Type|Description
:---|:---|:---
Deliverable|`value`|*if email address is deliverable, it returns the appropriate status*
Risky|`value`|*if email is risky, this socket returns this status*
Undeliverable|`value`|*if email has undeliverable status, the current socket returns this status*
Unknown|`value`|*if the node cannot determine the status of the email address, this socket will return an unknown status*
Items|`item`|*returns items with the field filled with the checking status*

### Additional Output Sockets

There are no additional output sockets in this node.

## Options

The settings of the current node depend on the [Mode](#mode) option. Depending on the selected mode, the node settings will change:

- Value

    ![Value mode settings](automation-nodes/verify-email-value.jpg "Value default settings")

- Items

    ![Items mode settings](automation-nodes/verify-email-items.jpg "Items default settings")

Name|Description
:---|:---
Mode|allows to select the mode of the node work; [details...](#mode)
App id|allows to select application where the email and checking results stored; [details...](#app-id)
Email field id|allows you to select the field from which the email will be taken for checking; [details...](#email-field-id)
Result field id|allows to select the field where the results of the check will be stored; [details...](#result-field-id)
Notes|allows you to leave any notes; [details...](#notes)

### Mode

Node modes determine what type of data this node can work with. Depending on the selected mode, the node settings will change.

- **Value** - there are no additional settings for this mode, only defined sockets.
- **Items** - all following settings customize this mode.

### App Id

The current option allows you to select the source application. The email addresses for verification are taken from items of the selected app. Also, the results of the verification will be stored in items of the selected application.

### Email Field Id

Here you have to select the field where the email addresses are stored. The value of this field will be verified via a **Verify Email**.

### Result Field Id

This is the field where the result of the email verifying will be stored. This means that the items will be returned with this field filled in.

### Notes

The last setting allows you to leave any notes in the node settings. You can use it to specify the purpose of a node.
