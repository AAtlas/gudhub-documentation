# Populate Element

**Populate Element** - a node that allows you to fill the element in which the node is located with some data. The element can be filled with data from another node or an entered static value.

- Node with item socket
  
  ![Item socket of populate element node](automation-nodes/populate-element.jpg "Populate element with item socket")

- Node with value socket
  
  ![Value socket of populate element node](automation-nodes/populate-element-value.jpg "Populate element with value socket")

## Inputs

The current node has two default sockets.

### Default Input Sockets

The current node has two default input sockets but they interchangeable. Namely, if one of them is used, the other is not available. The change of sockets is configured by the function [Value Input](#value-input).

Name|Data Type|Description
:---|:---|:---
Items|`item`|*starts the work of the node when the item is transferred to it*
Value|`value`|*accept value that will fill the current element*

### Additional Input Sockets

There are no settings to add additional input sockets to this node.

## Outputs

The current node has no output sockets and they cannot be added in the settings.

## Options

The current node has only one group of settings. They allow you to adjust the operation of the node.

![Settings of populate element node](automation-nodes/populate-element-settings.jpg "Node settings")

Name|Description
:---|:---
Value Input|allows you to determine what type of value will fill the field; [details...](#value-input)
Output Value|allows you to enter a static value to fill the field; [details...](#output-value)
Notes|allows to leave notes in settings; [details...](#notes)

### Value Input

This is the function that allows to change the type of value that fills the element. Depending on the type, the [input socket will vary](#inputs). When the feature is enabled, you can pass some value to the node to populate the element. Otherwise, you enter a static value in [special setting](#output-value).

### Output Value

This function is created to enter the static value, that will fill the element if some item will be transmitted to start the current node. This option is available if the value [Value Input](#value-input) is disabled.

### Notes

Here you can leave any notes. They will be displayed every time you open the node settings.
