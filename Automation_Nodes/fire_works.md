# Fire Works

**Fire Works** is a node that allows you to add fireworks after a certain action. They consist of the **fireworks animations** themselves, a [text message](#text), and, in some cases, a [darkened background](#background). The action is triggered after another action that is configured in the [automation chain](../Understanding_Gudhub/Automation/automation_overview.md) and lasts for a [certain period of time](#animation-duration).

![Fire works node](automation-nodes/fire-works-node.jpg "Fire works")

## Inputs

This node has only one input socket.

### Default Input Sockets

The input node of the current node does not allow any data to be transmitted to the node. Instead, it acts as a trigger for the node to operate. that is, as soon as any data is transmitted to the input socket, fireworks are triggered.

Name|Data Type|Description
:---|:---|:---
Start|`value`|*accepts any value and triggers the node action*

### Additional Input Sockets

This element have no additional sockets.

## Outputs

The current node has only one output socket that returns a specific value.

### Default Output Sockets

The single output socket of the node works as an end-of-action indicator. When the fireworks end, the node returns the word **finish**.

Name|Data Type|Description
:---|:---|:---
Finish|`value`|*returns `finish` when the action is completed*

### Additional Output Sockets

There are no settings for adding additional sockets.

## Options

The current node has fairly simple settings that allow you to customize fireworks.

![Default settings of the fire works node](automation-nodes/fire-works-default-settings.jpg "Fire works default settings")

Name|Description
:---|:---
Text|allows you to enter the title of the fireworks; [details...](#text)
Text Color|allows you to set the color of the fireworks title; [details...](#text-color)
Animation Duration|allows you to set the duration of the fireworks; [details...](#animation-duration)
Background|allows you to determine whether the fireworks background will be used or not; [details...](#background)
Notes|allows you to leave any notes; [details...](#notes)

### Text

The first setting of the current node allows you to enter the title of fireworks. This is a message for users that appears in the middle of the screens.

### Text Color

The next setting is responsible for the color of the [fireworks text](#text). Here you can choose any color using the **color picker**. It works like the [Color](../Gh_Elements/color.md).

### Animation Duration

The current setting allows you to set the duration of the fireworks. The **duration** can be **up to five seconds**. To set the duration time, choose one of the following options:

- 1 second
- 2 seconds
- 3 seconds
- 4 seconds
- 5 seconds

As you have already understood, there are only 5 options with a difference of one second.

### Background

During fireworks, the background becomes inactive. That is:

> The user **cannot use any of the GudHub functions while the fireworks are running**.

So, the current setting only allows you to determine whether the background of the fireworks will be darkened or not.

>By default, this setting is enabled.

If the setting is enabled, the background will be darkened until the fireworks end. Otherwise, the background will be the same as without fireworks. Consider this when choosing a [color for your text](#text-color).

### Notes

The purpose of the current setting is to leave any notes that will be displayed on the node above the input sockets. With its help, you can mark what you use this or that node for or, in this case, write down the title of fireworks. Thus, even without opening the settings, you can understand what you are using this node for in your [automation](../Understanding_Gudhub/Automation/automation_overview.md).
