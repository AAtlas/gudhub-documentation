# Trigger Node

**Trigger** - a node that listens to the application. It passes the results of all application events to the following nodes. This node is available only in the [Trigger element](../Gh_Elements/trigger.md) where the source application can be selected.

![Trigger node](automation-nodes/trigger-node.jpg "Trigger")

## Inputs

*Trigger* is one of those nodes that automatically receive data from the certain application. Therefore it do not have any input sockets.

## Outputs

Current node has a quite big set of output sockets.

### Default Output Sockets

All data is transmitted to the node from the certain application that can be selected in the settings of the [Trigger element](../Gh_Elements/trigger.md).

Name|Data Type|Description
:---|:---|:---
Created Items|`item`|*transmits just created items of the selected application*
Deleted Items|`item`|*transmits deleted items of the selected application*
Updated Items|`item`|*transmits items which are updated of the selected application*
Updated File|`value`|*transmits files from the selected application which are updated*
Updated Value|`item`|*transmits updated values from the selected application*

### Additional Output Sockets

Since this node has no settings, there is no possibility to add additional output sockets.

## Options

As mentioned above, the current node has no settings.
