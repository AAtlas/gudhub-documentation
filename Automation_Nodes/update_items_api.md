# Update Items API

**Update Items API** is a node that allows to automatically update items. Namely, when some item, that is transferred to the input socket, is updated this node will send the update item request to the server.

The current node is consists of edit and delete button, input and output sockets.

![Node of update items API](automation-nodes/update-items-api.jpg "Update items API")

## Inputs

The update items API has only one input socket and new sockets cannot be added.

### Default Input Sockets

For the most part, updated elements are transferred, but the socket accepts any elements.

Name|Data Type|Description
:---|:---|:---
Items|`item`|*socket that accept any items*

### Additional Input Sockets

There are no additional input sockets.

## Outputs

This node has no settings to add additional sockets. Therefore, it only has a default output socket.

### Default Output Sockets

The default socket allows you to transfer items that have been used in the current node to the following nodes. That allows you to move on to the next nodes only after the **Update Items API** has worked.

Name|Data Type|Description
:---|:---|:---
Items|`item`|*transmits the items that have been updated*

### Additional Output Sockets

The node does not contains the additional output sockets.

## Options

The current node has only one setting that is important to the element, which is in a single settings block.

![Default settings of update items API node](automation-nodes/update-items-api-default-settings.jpg "Update items API settings")

Name|Description
:---|:---
Application|allows to select the application were the updates will be done; [details...](#application)
Notes|allows to save some notes in settings; [details...](#notes)

### Application

This is the selected application, the elements of which will be transferred to the node and updated.

### Notes

This notes will be displayed only in the current node settings. You can leave any comments here.
