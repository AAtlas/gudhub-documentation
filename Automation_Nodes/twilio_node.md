# Twilio SMS

**Twilio SMS** - a node that allows you to automatically send SMS using the [Twilio service](https://www.twilio.com/). You can configure it so that messages will be automatically sent, for example, to your customers after a certain action.

![Twilio SMS node](automation-nodes/twilio-sms-node.jpg "Twilio SMS")

## Inputs

The **Twilio SMS** has only default sockets.

### Default Input Sockets

To send messages, three types of data must be transmitted to the node sockets. These are the contacts of the caller and the recipient of the call, as well as the content of the message.

Name|Data Type|Description
:---|:---|:---
Caller ID|`value`|*accepts Twilio phone numbers from which messages will be sent*
Recipient|`value`|*accepts number of the message recipients*
Message|`value`|*accept the message text*
Image|`value`|*accept images for messages*

### Additional Input Sockets

There are no additional input sockets in this node.

## Outputs

The current node has no output sockets.

## Options

![Settings of twilio SMS node](automation-nodes/twilio-sms-settings.jpg "Twilio SMS settings")

Name|Description
:---|:---
Account SID|allows to enter the SID of your Twilio account; [details...](#account-sid)
Auth Token|allows to enter the authentication token of your Twilio account; [details...](#auth-token)
Notes|allows to leave some notes in the node settings; [details...](#notes)

### Account SID

This is the field where you must enter the 34-digit [SID of your Twilio account](https://www.twilio.com/docs/sms/tutorials/how-to-send-sms-messages-particle-photon#retrieve-your-twilio-account-sid-and-api-secret). It is used in conjunction with the [Auth Token](#auth-token) to authenticate your app's API requests.

### Auth Token

The second important thing that you have in your Twilio account is [Auth Token](https://www.twilio.com/docs/iam/api/authtoken). Enter it in the appropriate setting to send requests.

### Notes

This is the setting where you can enter any notes about the current node or anything else.
