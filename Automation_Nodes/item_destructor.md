# Item Destructor

**Item Destructor** is a node that allows to take items apart. That allows user to use any data from item. There the default data that are available:

- Items
- Item Ref
- Item ID
- App ID

Besides those, you can pull data from any field of an item.

The node consists of setting button, delete button, input and output default sockets.

![Item destructor node with additional output socket](automation-nodes/item-destructor-additional-output.jpg "Item destructor configured node")

>The new sockets are for the value of the selected field of the application. They are located under the last slot by default.

## Inputs

According to the name of the node, the destructor accepts items of the selected [application](#application). For this reason, there could not be added any other sockets.

### Default Input Sockets

This node has only one input socket.

Name|Data Type|Description
:---|:---|:---
Items|`item`|*socket that accepts any items*

### Additional Input Sockets

There are no setting to add an additional input sockets in this node.

## Outputs

Unlike inputs, the item destructor has many output sockets.By default, there are four outputs, and using the [node settings](#options), new outputs can be added.

### Default Output Sockets

The default sockets are named according to the data they return.

Name|Data Type|Description
:---|:---|:---
Items|`item`|*returns items that can be updated or not*
Item Ref|`value`|*returns [reference](../Gh_Elements/item_reference.md) of the certain item from the selected application*
Item Id|`value`|*returns the ID of the element that is passed to the node*
App Id|`value`|*returns ID of the application where the items is located*

### Additional Output Sockets

The item destructor settings allows to create a new output sockets that allows to output the values of the certain fields.

## Options

The current node has two groups of settings.

### Default Settings

The default settings of the node mostly configures the source data.

![Default settings of item destructor](automation-nodes/item-destructor-default-settings.jpg "Item destructor default settings")

Name|Description
:---|:---
Application|allows to select the source application; [details...](#application)
Notes|allows to note something in settings; [details...](#notes)

#### **Application**

This is an application whose elements will be taken apart.

#### **Notes**

This function is designed to note the important things. For example, you can note destination of this node.

### Value Outputs

This group of settings that allows to configure the node outputs.

![Value output settings of item destructor](automation-nodes/item-destructor-value-outputs.jpg "Item destructor value outputs")

Name|Description
:---|:---
Field|allows to select the source field; [details...](#field)
Field Name|allows to enter the name of the field; [details...](#field-name)
Edit|allows to edit or delete the option of output; [details...](#edit)

#### **Field**

This is the field from which data will be pulled.

#### **Field Name**

This is the name of the value that is entered by user.

#### **Edit**

This column contains two buttons that allows to edit or delete the option.
