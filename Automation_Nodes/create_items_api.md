# Create Items API

**Create Items API** is a node that automatically creates a new item in the selected [application](#application). It also allows you to create duplicates of existing items.

![Appearance of create items API node](automation-nodes/create-items-api.jpg "Create items API node")

## Inputs

The current node has only one default input socket, and no additional sockets can be added.

### Default Input Sockets

This means that each update of the items that are passed in creates a new item. This means that each update of the items to be transmitted leads to the creation of a new item. If the item is to be created in the same application where updates are tracked, the created item will be a duplicate of the updated item..

Name|Data Type|Description
:---|:---|:---
Items|`item`|*accept the template of the item that will be created or the item those updates will be tracked*

### Additional Input Sockets

This node has no additional input sockets.

## Outputs

Like the input socket, there is only one output socket.

### Default Output Sockets

Created items from this node can be transferred to other nodes, due to this socket.

Name|Data Type|Description
:---|:---|:---
Items|`item`|*transmits the created item to the next node*

### Additional Output Sockets

This node has no settings for creating an additional output socket.

## Options

The current node has a rather small set of settings. There are only Default Settings.

![Default settings of create items API node](automation-nodes/create-items-api-default-settings.jpg  "Create items API settings")

Name|Description
:---|:---
Application|allows to select the application where the item will be created; [details...](#application)
Notes|allows to leave some notes in the node settings; [details...](#notes)

### Application

This is the application in which a new item or a duplicate of an item will be created.

### Notes

This is the field for notes. The entered text will be saved in the settings of the current node.
