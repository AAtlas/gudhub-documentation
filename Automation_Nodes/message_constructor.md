# Message Constructor

**Message Constructor** is a node that allows to generate different dynamic messages. Namely, those are the messages which are automatically filled with the certain data. The node takes the values of the created [variables](#variable-name) and inserts them into the [message templates](#default-input-sockets).

![Node called Message Constructor](automation-nodes/message-constructor.jpg "Message constructor")

As you may have understood, the text of the message template can contain some variables instead of which the values will be substituted for. In order to distinguish variable from plain text, it must be highlighted with `*||*`.

For example, *name* is a variable:

```json
Hello, *|name|*!
```

Templates with all variables are usually stored in the [Constants](constants.md) node and the corresponding values are substituted only in the Message Constructor node.

## Inputs

The input sockets of **Message Constructor** accept parts of the message, namely the template and the variables.

### Default Input Sockets

Name|Data Type|Description
:---|:---|:---
Message Template|`value`|*accepts the template for the dynamic message*

### Additional Input Sockets

The additional input sockets of the current element accepts values of the variables created in [constructor settings](#variable-name).

## Outputs

This node has only one output socket.

### Default Output Sockets

The current socket returns the result of the value substitution. That result is a ready-made message.

Name|Data Type|Description
:---|:---|:---
Message|`value`|*transmits the full generated message*

### Additional Output Sockets

The current node has no additional sockets.

## Options

### Value Outputs

![Settings of value outputs](automation-nodes/message-constructor-value-inputs.jpg "Message constructor settings")

Name|Description
:---|:---
Input Socket Name|allows to enter the name of the new additional socket; [details...](#input-socket-name)
Variable Name|allows to enter the name of the variable; [details...](#variable-name)
Edit|allows to edit or delete the variable; [details...](#edit)
Notes|allows to leave some notes in settings; [details...](#notes)

#### **Input Socket Name**

As you can guess from the title, this function allows to enter the name of the socket of the each of created variables.

#### **Variable Name**

This is the name of the variable that have to be listed in the message template. Thanks to this, the value of any field from the element can be dynamically substituted in the template.

#### **Edit**

This is a regular column that contains two buttons for editing and deleting a variable variant.

#### **Notes**

This settings is designed to leave any notes in the settings.
