# Smart Input Node

**Smart input** is a special node that is used exclusively in an [element with the same name](../Gh_Elements/smart_input.md). The sockets of this node and the [smart input element](../Gh_Elements/smart_input.md) types are closely related:
> Depending on the selected type in the element settings, different output sockets are used.

Thus, if the element settings do not match the socket used, the [smart input element](../Gh_Elements/smart_input.md) will not work.

![Appearance of the smart input node](automation-nodes/smart-input-node.jpg "Smart input node")

> This node is available only in the [Smart Input](../Gh_Elements/smart_input.md) settings.

## Inputs

The current node accepts data only from the current [smart input element](../Gh_Elements/smart_input.md), so it has no input sockets.

## Outputs

Since the current node has no other parts than the output sockets, they play an important role in the operation of the node.

### Default Output Sockets

Node sockets by default transmit data from the application in which a particular smart input item is located. Names of these sockets basically describes what data will be taken for subsequent operations and updates. Namely, they show what data will be transmitted to the next nodes.

Name|Data Type|Description
:---|:---|:---
Current Item|`item`|*returns item where the [smart input element](../Gh_Elements/smart_input.md) is located and being updated*; [details...](#current-item)
Selected Item|`item`|*return selected items that is selected using [smart input element](../Gh_Elements/smart_input.md)*; [details...](#selected-item)
New Item|`item`|*returns new items of the application where the current [smart input element](../Gh_Elements/smart_input.md) is located*; [details...](#new-item)
Value|`value`|*returns the value of the current [smart input element](../Gh_Elements/smart_input.md) is located, if it can be entered*; [details...](#value)

### Dependence On Type

Each of sockets is used for the certain types of the [smart input element](../Gh_Elements/smart_input.md). That types are configures in settings of that element.

![Types of smart input element](automation-nodes/smart-input-element-types.jpg "Smart input element settings")

#### **Current Item**

The data of current item is used for two types of [Smart Input](../Gh_Elements/smart_input.md):

- Button
- Input

As a result, the user will be able to fill out the fields or change the data in the fields remotely.

> Reminder. The current element is the element where [smart input element](../Gh_Elements/smart_input.md) is located and is currently open or being updated.

#### **Selected Item**

The node returns selected items only when the type is:

- List

Because, the items are selected in the [smart input element](../Gh_Elements/smart_input.md). More precisely, due to this type, you can make a drop-down list of items, and then select one of them by clicking on the desired item in the list. Then it is this element that will be passed to the next nodes.

#### **New Item**

The new items of the current application are used when this type is on:

- Form

Due to this, after clicking on [smart input element](../Gh_Elements/smart_input.md) a new empty element of the current application will be opened and simultaneously passed to the next node to perform the following operations.

#### **Value**

A node transmits a value in only one case:

- Input

The transmitted value is the data entered in the field of the [smart input element](../Gh_Elements/smart_input.md).

### Additional Output Sockets

The smart input node has no settings. Thus, there are no settings for adding new sockets.

## Options

The current node belongs to those nodes that have no settings and cannot be deleted.
