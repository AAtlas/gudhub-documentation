# Response Node

**Response** is a node that allows to return the response of the HTTP requests called in [API element](../Gh_Elements/api_element.md). This response can be generated using different [nodes](../Understanding_Gudhub/Automation/automation_overview.md) or can return request data from an [API node](api_node.md). The received response can be viewed at the link specified in [API element](../Gh_Elements/api_element.md).

>The current node is standard for the [API element](../Gh_Elements/api_element.md) and is available only in it.

![Response node](automation-nodes/responce-node.jpg "Response")

This node is usually paired with several nodes depending on the socket that is used.

- When you need to return the **object**, use [Object Constructor](object_constructor.md) or create an object in [JSON Scheme](json_scheme_node.md).
- The response in **HTML** and **XML** formats can be created in [Message Constructor](message_constructor.md).

## Inputs

This node has only a few default input sockets.

### Default Input Sockets

Input sockets accept different types of data to display them as responses in a specific format.

Name|Data Type|Description
:---|:---|:---
Object|`object`|*accepts an object to return it in response*
HTML|`value`|*accepts data to convert it to an HTML file*
XML|`value`|*accepts data to convert it to an XML file*

### Additional Input Sockets

## Outputs

The *Response* is a node that only receives data, so it has no output sockets.

## Options

The current node has no settings.
