# Iterator Node

**Iterator** is a node that allows you to iterate the part of the automation. Most nodes can process only one item at a time. On the other hand, Iterator allows you to go through an array and process each element accordingly.

![Iterator node](automation-nodes/iterator-node.jpg "Iterator")

## Inputs

There are three input sockets with three different types.

### Default Input Sockets

All the input sockets have the similar purpose. They accept arrays of elements of different data types that will be processed in the iterator.

Name|Data Type|Description
:---|:---|:---
Items|`item`|*accepts an array of items*
Object|`object`|*accepts an array of objects*
Value|`value`|*accepts an array of values*

### Additional Input Sockets

The additional input sockets can be added using the [Options tab](#options-tab). They are mostly used to pass any constants or additional values to the iterator. This allows you to use these values in node operations and thus complement your automation.

Additional input sockets are created in pairs with the [output sockets](#additional-output-sockets). This allows you to **change values within one iteration**. Therefore, for each subsequent iteration, the value from the corresponding output socket will be passed instead of the one that was originally passed to the **Iterator**.

## Outputs

The output sockets of the iterator have the same types as the input sockets.

### Default Output Sockets

This node returns arrays of elements of three different types. There are three different sockets for that. In turn, each element of the returned array is processed by the iterator.

Name|Data Type|Description
:---|:---|:---
Items|`item`|*returns an array of processed items*
Object|`object`|*returns an array of processed objects*
Value|`value`|*returns an array of processed values*

### Additional Output Sockets

Just like input additional sockets, output sockets can be added using [Options Tab](#options-tab) settings. More precisely, you create an [input socket](#additional-input-sockets) and a corresponding output socket at the same time. This allows you to **change the value** in the iteration and **pass the already changed value to the next iteration**. Also, with the help of these sockets, you can pull the values of variables from the **Iterator**.

## Options

The main part of Iterator settings is its individual automation settings. But there is another tab that contains additional settings for the current node.

### Iterator

As was mentioned above, the first tab consists of the automation settings. Here we have standard options, such as a list of nodes, copy and paste buttons, default nodes, and the *Apply* button.

![Settings of iterator node](automation-nodes/iterator-settings.jpg "Settings")

Name|Description
:---|:---
Iterator Input|special node that transmits every element into the iterator; [details...](#iterator-input)
Iterator Output|unique node that transmits elements from the iterator; [details...](#iterator-output)

#### **Iterator Input**

This is the node through which the array elements are transferred inside the iterator. Due to this, each element will be processed by a chain of nodes inside the iterator.

>Its output sockets correspond to the input sockets of the **iterator**. There are three sockets by default and an unlimited number of [additional sockets](#additional-input-sockets) can be added.

#### **Iterator Output**

The second node does the opposite of the previous one by default - it outputs the processed elements from the iterator.

>It contains three input sockets which correspond to the output sockets of the **iterator**.

### Options Tab

The second tab contains settings for adding new input sockets. The created socket will be available in the **iterator** itself and in the [Iterator Input](#iterator-input) node.

>Their main purpose is to pass various constants inside the iterator to use them for operations in nodes.

![Options of iterator node](automation-nodes/iterator-options.jpg "Iterator options")

Name|Description
:---|:---
Sync iteration|allows to change the type of iteration
Type|allows to select the type of a new socket; [details...](#type)
Socket Name|allows to enter the name of a new socket; [details...](#socket-name)
Edit|allows to edit or delete a custom socket; [details...](#edit)

#### **Sync Iteration**

This is a rather unusual setting for the current node. This is a switch that allows you to set the type of iteration. That is, synchronous and asynchronous iterations are available in the current node. This setting allows you to switch between them as needed.

>**Asynchronous iteration** is used **by default**.

If this option is **enabled**, **asynchronous iteration** is used. Otherwise, the iterator works **synchronously**.

#### **Type**

The first option allows you to specify the type of data that the created socket will accept. There are three types available:

- Items
- Object
- Value

#### **Socket Name**

This is the field where you have to enter the name of a new socket. This name will be displayed in the **Iterator node** and at the [Iterator Input](#iterator-input) after the default sockets.

#### **Edit**

This setting consists of two buttons. One of them allows you to edit a new socket, and the other allows you to delete it.
