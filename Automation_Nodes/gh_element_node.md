# GH Element Node

**GH Element Node** is the initial automation node for most gh-elements. It allows you to track updates of the current item to run some processes in the connected nodes. You can also get the values of the element in which the node is located and pass them to the following nodes.

![Base node of GH elements](automation-nodes/gh-element-node.jpg "GH element node")

## Inputs

The current node does not have any input socket. It takes data from the current item or element.

## Outputs

The GH element node contains only default output sockets.

### Default Output Sockets

All output sockets track different data from the current item. Two of them also transmits data to the next nodes.

Name|Data Type|Description
:---|:---|:---
Updated Items|`item`|*monitors items updates*
New Value|`value`|*transmits new element value*
Old Value|`value`|*transmits old element value*

### Additional Output Sockets

Since this node has no settings, it is not possible to add additional sockets.

## Options

The current node has no settings for its configuration.
