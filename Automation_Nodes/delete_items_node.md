# Delete Items Node

**Delete Items** is a node that allows you to remotely remove items from a specific program. That is, using this node in automation, you can configure the conditions under which certain items will be deleted.

![Appearance of the delete items node](automation-nodes/delete-items-node.png "Delete items node")

## Inputs

There is only one input socket, and it is used by default.

### Default Input Sockets

The current node accepts items which have to be deleted. This allows you to create certain conditions under which the items will be deleted. For example, you can filter data by a certain status and delete an item by transferring it to this node.

Name|Data Type|Description
:---|:---|:---
Items|`items`|*accepts items that will be deleted*

### Additional Input Sockets

This node is not configured to add additional sockets.

## Outputs

This node has only default sockets.

### Default Output Sockets

The single output socket of the current node allows you to transfer the data of the just deleted items to the next nodes in the chain. This ensures that the data is saved and can be used in further work.

Name|Data Type|Description
:---|:---|:---
Items|`items`|*returns deleted items*

### Additional Output Sockets

The current node has no additional sockets.

## Options

This is a node that has a small number of settings.

![Default settings of the delete items node](automation-nodes/delete-items-default-settings.png "Default settings")

Name|Description
:---|:---
Application|allows to select the application from which items will be deleted; [details...](#application)
Notes|allows to leave any notes in the node settings; [details...](#notes)

### Application

The first parameter is the main and most important for configuring the current node. Here you should select the program from which you want to delete items.

### Notes

This is the standard setting for every node. It allows you to describe the purpose of the node or leave any other notes in its settings.
