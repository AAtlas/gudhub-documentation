# Turbo SMS

**Turbo SMS** is a node that allows you to send SMS messages using the [TurboSMS](https://turbosms.ua/ua/) service. The fee for sending a message corresponds to the service's rates. This element is recommended to use with [Phone](../Gh_Elements/phone.md) and [Text Area](../Gh_Elements/text_area.md). The data will be pulled from these fields. This element is quite similar to the [Send Message](./send_message.md) and [Twilio SMS](./twilio_node.md) nodes.

![Turbo SMS node](automation-nodes/turbo-sms-node.jpg "Turbo SMS")

This node can also be used for mass messaging. You can do this by using **Turbo SMS** inside [Iterator](./iterator_node.md).

## Inputs

There are not so many input sockets.

### Default Input Sockets

Current sockets are responsible for sending SMS messages. To be more precise, you have to send the recipient's phone number and message content here.

Name|Data Type|Description
:---|:---|:---
Phone|`value`|*accepts the phone number to which the SMS will be sent*
Message|`value`|*accepts the contents of the SMS message*

### Additional Input Sockets

This node has no settings for adding additional sockets.

## Outputs

The current node has no additional output sockets.

### Default Output Sockets

The output sockets of the current node are responsible for responding to requests. That is, these sockets return information about the status of sending an SMS message.

Name|Data Type|Description
:---|:---|:---
Error|`value`|*returns a message when an error occurred while sending and the message was not sent*
Sent|`value`|*returns a message if the SMS message was successfully sent*

### Additional Output Sockets

There are no additional output sockets in this node.

## Options

This node has only one small block of settings called **Default Settings**. It contains only two settings.

![Default settings of turbo sms node](automation-nodes/turbo-sms-default-settings.jpg)

Name|Description
:---|:---
Token|allows you to enter the token of the TurboSMS account; [details...](#token)
Notes|allows you to leave any notes in the node; [details...](#notes)

### **Token**

This setting is the most important for configuring the node. It accepts the token HTTP API of the [TurboSMS](https://turbosms.ua/ua/) account. This token provides authorization in the service through which SMS messages will be sent.

### **Notes**

This is the default setting for each node. It allows you to write any text that will be displayed in the node above the input slots.
