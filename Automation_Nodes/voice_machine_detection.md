# Voice Machine Detection

**Voice Machine Detection** is a node that allows you to check the certain phone numbers. Specifically, it determines who answers the call. This is useful for checking some contacts.

![Voice machine detection node](automation-nodes/voice_machine_detection-node.jpg "Voice machine detection")

## Inputs

The current node has only default input sockets.

### Default Input Sockets

There are two default input sockets. Both receive data that is important for making a call. The recipient's phone number will be checked.

Name|Data Type|Description
:---|:---|:---
From|`value`|*accept the phone number from which the call will be made*
To|`value`|*accepts the phone number to be checked*

### Additional Input Sockets

The current node has no additional input sockets.

## Outputs

The voice detector  has only one output socket.

### Default Output Sockets

The single output socket returns the result of the recipient checking. Namely, it shows who answers the call: machine_start, person, fax, or unknown in cases where the detector cannot determine who called.

Name|Data Type|Description
:---|:---|:---
Result|`value`|*returns the result of the check*

### Additional Output Sockets

There are no settings for adding additional output sockets.

## Options

![Default settings of voice machine detection node](automation-nodes/voice-machine-detection-default-settings.jpg "Default settings")

Name|Description
:---|:---
Account SID|allows you to enter your Twilio account SID; [details...](#account-sid)
Auth Token|allows you to enter a Twilio authentication token; [details...](#auth-token)
Notes|allows to leave any notes in settings; [details...](#notes)

### Account SID

To use the current node, you must have a Twilio account, where you can get the credentials for this and the following settings. In the first **Voice machine detection** setting, you must enter the [account SID](https://www.twilio.com/docs/glossary/what-is-a-sid) that will allow the node to access your Twilio account.

### Auth Token

This is the second option where you need to enter the [Auth Token](https://www.twilio.com/docs/iam/api/authtoken) from your Twilio account, which is used to authenticate the API request.

### Notes

The last option is used by default. This is the field where you can enter any necessary notes.
