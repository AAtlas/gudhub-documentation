# Item Constructor

**Item Constructor** is an automation node that allows to construct items and fill them with data. It takes an empty item passed to it and fills its fields with certain data or duplicates them from the original. This will allow you to update an existing item or create a new one.

![Item constructor node](automation-nodes/item-constructor-node.jpg "Item Constructor")

## Inputs

The input sockets of this node accept the item template and data to fill the item fields.

### Default Input Sockets

The current node creates new items based on the transmitted items from the [selected application](#application). The item template is filled with other transmitted data. The default data that can be accepts is the ID of the transmitted item.

Name|Data Type|Description
:---|:---|:---
Items|`item`|*accepts items of the selected application*
ItemID|`value`|*accepts ID of a certain item selected in the previous node*

### Additional Input Sockets

The item constructor has a separate block of settings for adding new input sockets. They are intended to fill in the fields of the future item.

Also, there are the Push socket that is available when the [Send Constant By](#send-constant-by) is *PUSH*. This socket allows to delay work of the node. Used when some data does not have time to be processed before being used in the current node.

Name|Data Type|Description
:---|:---|:---
PUSH|`value`|*accepts the value that will be the trigger for the node to start working*

## Outputs

The current node has only one output socket.

### Default Output Sockets

The singly output socket returns the created item.

Name|Data Type|Description
:---|:---|:---
Items|`item`|*at the exit you will receive the item with the certain data*

### Additional Output Sockets

This node has no additional output slots and they cannot be added.

## Options

The current node has many settings which are divided in two groups.

### Default Settings

The first block of settings allows to configure the base node options.

![Default settings of item constructor node](automation-nodes/item-constructor-default-settings.jpg "Item constructor settings")

Name|Description
:---|:---
Application|allows to select the application to which items will be added; [details...](#application)
Send Constant By|allows to control the start of the node; [details...](#send-constant-by)
Fill Empty Fields|allows to fill the fields of the new item with data from the source item; [details...](#fill-empty-fields)
Notes|allows to leave some notes in settings; [details...](#notes)

#### **Application**

This is the application from which data will be taken to create a new item. These include item template, item ID, and data from any its field.

#### **Send Constant By**

The current function is created to control the node. It has two variants of work:

- PULL
- PUSH

The first one allows the node to work as all others. In turn, the second variant allows the node to delay its operation until certain data is transferred to it.

#### **Fill Empty Fields**

If this function is enabled, the data from the fields of the transferred item will be saved in the new item.  Otherwise, the created element will be empty, excluding fields from [Value Inputs](#value-inputs).

#### **Notes**

This field is designed to leave some notes in settings of the node.

### Value Inputs

The second block of settings is designed to add new input sockets and edit their settings.

![Value inputs of item constructor node](automation-nodes/item-constructor-value-inputs.jpg "Item constructor value inputs")

Name|Description
:---|:---
Field|allows to select the field for socket; [details...](#field)
Field Name|allows to enter the name of the new socket; [details...](#field-name)
Edit|allows to edit or delete the socket; [details...](#edit)

#### **Field**

This is the field that is connected with the new socket. It will be filled with the data that is transmitted through the input socket.

#### **Field Name**

This is the name of the new input socket. You can enter any name you want.

#### **Edit**

The current column contains two buttons for each of the additional sockets. One of them allows you to edit the socket settings, and the other one allows you to delete it.
