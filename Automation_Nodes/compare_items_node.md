# Compare Items Node

![Compare items node](automation-nodes/compare-items-node.jpg "Compare items")

## Inputs

### Default Input Sockets

Name|Data Type|Description
:---|:---|:---
Source items|`item`|
Destination Items|`item`|

### Additional Input Sockets

## Outputs

### Default Output Sockets

Name|Data Type|Description
:---|:---|:---
New items|`item`|
Difference items|`item`|
Same items|`item`|

### Additional Output Sockets

## Options
