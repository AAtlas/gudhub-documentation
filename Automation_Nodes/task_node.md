# Task Node

**Task Node** is a default node of the [Task element](../Gh_Elements/task_element.md). The only purpose of this node is to transmit data from the item where the corresponding gh-element is located to the automation chain.

![Appearance of the task node](automation-nodes/task-node.jpg "Task node")

## Inputs

The current node has no input sockets. It accepts data from the element in which the [Task element](../Gh_Elements/task_element.md) is located.

## Outputs

The current node has only one socket.

### Default Output Sockets

A single socket of a node passes the current item to the following nodes for operation. This means that the only data that a node can retrieve is the data of the item in which the [Task element](../Gh_Elements/task_element.md) is located.

Name|Data Type|Description
:---|:---|:---
Current Item|`items`|*transmits data of the current item*

### Additional Output Sockets

There is no additional output sockets in this node.

## Options

The **Task** is one of those nodes that has no settings.
