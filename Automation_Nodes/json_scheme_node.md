# JSON Scheme

**JSON Scheme** is a node that allows you to generate a specific schema after a certain event and automatically pass it to the following nodes.

![JSON scheme node](automation-nodes/json-scheme-node.jpg "JSON scheme")

## Inputs

Since all node input slots are optional, a JSON schema can have an unlimited number of input sockets.

### Default Input Sockets

There are no default input settings in this node.

### Additional Input Sockets

The current node has separate settings that add new input jacks. Their main purpose is to accept values to filter some array in the scheme.

## Outputs

### Default Output Sockets

The current node returns the created JSON scheme as an object format data.

Name|Data Type|Description
:---|:---|:---
Object|`object`|*returns the object created in settings*

### Additional Output Sockets

No additional output sockets can be added to this node.

## Options

Current node has two blocks of settings.

### Scheme Settings

The first block of settings is designed to create JSON schemes.

![Settings of JSON scheme](automation-nodes/json-scheme-settings.jpg "JSON scheme settings")

Name|Description
:---|:---
Scheme|allows to open the space for creating the JSON scheme; [details...](#scheme)
Notes|allows to leave some notes in the node settings; [details...](#notes)

#### **Scheme**

This button opens the [JSON constructor](../Utils/JSON_Conctructor/overview.md), in which you can create the required scheme.

#### **Notes**

This option is created to leave any notes in the node settings.

### Input Variables

This set of settings allows you to create the customized variables in the array filter.

![Settings of JSON scheme variable inputs](automation-nodes/json-scheme-inputs-variables.jpg "JSON scheme variable settings")

Name|Description
:---|:---
Input Socket Name|allows to enter the name of variable and input socket; [details...](#input-socket-name)
Edit|allows to edit or delete the variable; [details...](#edit)

#### **Input Socket Name**

This is field for entering the name of variable that you create. All created variables will be used in the array filter. They will be displayed in list of variable filters.

#### **Edit**

This is the function that contains edit button and delete button for each of variables.
