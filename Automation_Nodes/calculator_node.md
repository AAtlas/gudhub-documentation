# Calculator

**Calculator** is a node that allows you to create and configure some automatic calculations. These calculations will be performed in parallel to the operation of the elements but will start after a certain trigger.

![Calculator Node](automation-nodes/calculator-node.jpg "Calculator")

## Inputs

A node can have a huge number of input sockets depending on the complexity of the calculations.

### Default Input Sockets

The current value have no default input sockets.

### Additional Input Sockets

All input sockets accept values of the expression variables in the node. Those sockets can be added in [default settings](#default-settings).

## Outputs

Unlike the input sockets, the set of output sockets in the current node is small. Namely, the *calculation* has only one output socket.

### Default Output Sockets

The default output socket allows you to pass a value to next sockets. That value is the result of a calculation in the node.

Name|Data Type|Description
:---|:---|:---
Value|`value`|*transmits the result of the calculation*

### Additional Output Sockets

There are no way to add additional output sockets.

## Options

This node has two groups of setting which configures the expression and its arguments.

### Default Settings

The first group of settings is designed to add different additional sockets.

![Default settings of calculator](automation-nodes/calculator-default-settings.jpg "Calculator settings")

Name|Description
:---|:---
Field Name|allows to enter the name of the new variable; [details...](#field-name)
Edit|allows to edit or delete the option; [details...](#edit)
Notes|allows to leave some notes in the node settings; [details...](#notes)

#### **Field Name**

This is the name of the created variable. It is used in the [calculation](#calculation) and as a name of the [input socket](#additional-input-sockets). This name is entered by you.

#### **Edit**

This column contains buttons for editing and deleting the variable.

#### **Notes**

This is the field for notes that will be saved in current settings. For example, you can enter here the destination of the current node.

### Calculation Settings

These settings are responsible for the expression itself and some additional values.

![Settings of the node calculation](automation-nodes/calculator-calculation.jpg "Calculation settings")

Name|Description
:---|:---
Additional values|allows to add some static value to the calculation; [details...](#additional-values)
Calculation|allows to select the type of the calculation; [details...](#calculation)
<!-- tooltip-start additional_values -->
#### **Additional Values**

This function allows you to add an unlimited number of static values for the following expression.
<!-- tooltip-end additional_values -->
<!-- tooltip-start calculation -->
#### **Calculation**

As you may have guessed, here you can create the expression that will be used to calculate the value in the node. More precisely, you can assemble an expression from added [variables](#default-settings), created [constants](#additional-values) and suggested mathematical symbols.
<!-- tooltip-end calculation -->