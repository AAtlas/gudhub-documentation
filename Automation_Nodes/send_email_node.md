# Send Email Node

**Send Email** is a node that allows you to set up automatic email sending. It is useful for making send newsletters or contact customers immediately after they leave a request. It is also easy to use. All you need to do is pass your regular email data to the node.

![The send email node](automation-nodes/send-email-node.jpg "Send email")

## Inputs

The **Send Email** node accepts only email data.

### Default Input Sockets

The main purpose of input sockets is to receive all the data needed to create and send an email. This includes the sender's and recipient's email addresses, the sender's name, the content, and the subject line. This is all the data you would normally enter to send a regular email.

Name|Data Type|Description
:---|:---|:---
From|`value`|*accepts email address from which an email will be sent*
From Name|`value`|*accepts a name that will be displayed in the email instead of the sender's address*
To|`value`|*accepts the recipient's email address*
Subject|`value`|*accepts text of an email subject*
Content|`value`|*accepts text of an email content; it also can be HTML code*

### Additional Input Sockets

There are no additional input sockets.

## Outputs

The current node has two output sockets, and both are the default output sockets.

### Default Output Sockets

The output sockets of the current node do not returns any data. They can only be used to trigger specific nodes in case of successful or unsuccessful email sending.

Name|Data Type|Description
:---|:---|:---
Sent|`value`|*allows to trigger a certain node in case of successful email sending*
Error|`value`|*allows to trigger a certain node in case there was an error while sending the email*

### Additional Output Sockets

This node has no additional output sockets.

## Options

The current node has no configuring settings. It contains only the **Notes** option.

### Default Settings

![Default settings of send email node](automation-nodes/send-email-default-settings.jpg "Default settings")

Name|Description
:---|:---
Notes|allows you to leave any notes in the node settings. It is useful for specifying the purpose of the node

#### **Notes**

This is the field in settings that allows you to leave any notes. These notes will be displayed in the node under its name.

### SMTP Credentials

![Default settings of send email node](automation-nodes/send-email-smtp-credentials.jpg "Default settings")

Name|Description
:---|:---
Host|allows you to enter the mail host; [details...](#host)
Username|allows you to enter the login of the SMTP account; [details...](#username)
Password|allows you to enter the password of the SMTP account; [details...](#password)
Port|allows you to enter the port of the SMTP server; [details...](#port)

#### **Host**

The first setting is for entering the address of the SMTP server. Every email service has its own address.

#### **Username**

This is the setting where you enter your SMTP account login. For a private account, the login is usually an email address. However, since some SMTP accounts may contain multiple email addresses, the login valid for the account must be something else, such as the account name.

#### **Password**

The next setting requires you to enter a password for your SMTP account. This will allow the node to access the account and send emails on behalf of the connected email accounts.

#### **Port**

The last setting is responsible for the port of the SMTP service. The exact port is usually specified in the service documentation.
