# Face Detector

**Face detector** is a node that recognizes faces on a webcam.

![Face detector node](automation-nodes/face-detector-node.jpg "Face detector")

## Inputs

### Default Input Sockets

Name|Data Type|Description
:---|:---|:---
Images id|`value`|

### Additional Input Sockets

This node has no additional input sockets.

## Outputs

### Default Output Sockets

Name|Data Type|Description
:---|:---|:---
Not Detected|`item`|
Detected|`item`|

### Additional Output Sockets

There are no settings to add additional sockets to this node.

## Options

![Default settings of face detector node](automation-nodes/face-detector-default-settings.jpg "Face detector settings")

Name|Description
:---|:---
Application|
Field for Trained Data|
Single/Multiple Recognition|
Training Mode|
Application For Recognize|
Notes|

### Application

### Field for Trained Data

### Single/Multiple Recognition

### Training Mode

### Application For Recognize

### Notes
