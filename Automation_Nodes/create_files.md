# Create Files

**Create Files** - a node that allows you to create a file and save it in the selected field.

![Create files node](automation-nodes/create-files-node.jpg "Create files")

## Inputs

There are only two default input sockets in this node.

### Default Input Sockets

The input sockets of the node accept the data of the file to be created and some elements that will be the trigger for the node to work.

Name|Data Type|Description
:---|:---|:---
Items|`item`|*accepts items as a trigger for starting work of the node*
Files|`value`|*accepts files in  two formats*

### Additional Input Sockets

This node has no additional input sockets.

## Outputs

The current node has only one output socket.

### Default Output Sockets

The only output socket transmits the items that contain the created files. That means, it returns the file in the [file list](../Understanding_Gudhub/Application_Data_Structure/file_list.md) of the certain item.

Name|Data Type|Description
:---|:---|:---
Item|`item`|*returns items with the file field filled with the data of the created file*

### Additional Output Sockets

There are no settings to add any additional output sockets.

## Options

Create Files node has only one block of settings that configures destination of the file.

![Default settings of create files node](automation-nodes/create-files-default-settings.jpg "Create files settings")

Name|Description
:---|:---
Dest Application|allows to select the application where file will be saved; [details...](#dest-application)
Field for file|allows to select the destination application; [details...](#field-for-file)
File types|allows to select the type of file that will be transmitted to the node; [details...](#file-types)
Notes|allows to leave some notes in node settings; [details...](#notes)

### Dest Application

This is the application where the created file will be created saved.

### Field For File

This is the destination field where the created file will be saved.

### File Types

The current function allows you to select the type of file that can be transmitted to the node:

- base64
- URL

### Notes

This is an option in the node settings that allows you to leave any notes there.
