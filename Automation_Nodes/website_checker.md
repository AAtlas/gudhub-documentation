# Website Checker

**Website Checker** is a node that allows you to check what technologies were used to develop a website. The checking is performed by the website URL. As a result, it returns an array consisting of site technologies.

![Website checker node](automation-nodes/website-checker-node.jpg "Website checker")

## Inputs

The **Website Checker** has only one default input socket.

### Default Input Sockets

The current node accepts items that perform two roles - source and destination of the check. This means that the [links](#url-field) to the sites to be checked are taken from these items, and the [result](#result-field) and [status](#status-field) of the check will be saved in the same items.

Name|Data Type|Description
:---|:---|:---
Items|`items`|*accepts applications that specify the sites to be checked*

### Additional Input Sockets

There are no additional input sockets.

## Outputs

The only output socket of the current node is the default one.

### Default Output Sockets

The node returns an items with the filled [Status Field](#status-field) and [Result Field](#result-field). That is, after checking the desired sites, certain fields of the transferred elements will be filled with the results of the check.

Name|Data Type|Description
:---|:---|:---
Items|`items`|*returns elements with the checked fields filled with the results of the check*

### Additional Output Sockets

This node has no additional output sockets.

## Options

In the settings of the current node, you set the source of websites to be checked and the destination fields for saving the check results.

![Default settings of website checker node](automation-nodes/website-checker-default-settings.jpg "Default settings")

Name|Description
:---|:---
Application|allows to select the application where sites are stored; [details...](#application)
Url field|allows to select the field where links are stored; [details...](#url-field)
Result field|allows you to select the field in which the result of the site check will be saved; [details...](#result-field)
Status field|allows you to select the field in which the site status will be stored; [details...](#status-field)
Notes|allows you to leave any notes in the node settings; [details...](#notes)

### Application

This is the application from which the links for checking will be taken and the results of the check will be stored here as well.

### Url Field

The current setting allows you to select the field where links to websites are stored. That field should be a [Link](../Gh_Elements/link.md) element.

### Result Field

This is the option where you have to select the field where an array of technologies of the checked website will be saved. It is recommended to select the [Text](../Gh_Elements/text.md) element with a multiple mode here.

![Website checking result](automation-nodes/website-checker-result-field.jpg "Result example")

>In cases where the node cannot identify the technologies used or cannot open the site, it returns nothing. Accordingly, the field will remain empty.

### Status Field

In this setting, you must select the field in which the check status will be stored. There are two existing statuses:

- **ok** - checker has accessed the site
- **error** - website is unavailable for some reason

### Notes

The purpose of this setting is to provide you with a place to make notes about the node.
