# Google Calendar Node

**Google Calendar** is a node that allows you to create events in your Google calendar. This is integration with [Google products](https://about.google/products/). Using it you can create events in your calendar directly from **GudHub** application.

![Google calendar node](automation-nodes/google-calendar-node.jpg "Google calendar")

## Inputs

All node sockets are default sockets.

### Default Input Sockets

All input sockets of this node receive the data required to create events and meetings. An event will be created only when a summary, description, start date, and end date are passed to the node. In turn, you don't have to send anything to the Attendees socket.

Name|Data Type|Description
:---|:---|:---
Summary|`value`|*accepts the value that will be used as a summary of the event*
Description|`value`|*accepts the value that will be the description of the event in the calendar*
Start|`value`|*accepts the start date and time of the event*
End|`value`|*accepts the end date and time of the event or meeting*
Attendees|`value`|*accepts the comma separated emails of people invited to the meeting*

### Additional Input Sockets

The current node has no additional sockets.

## Outputs

The current node has only one output socket.

### Default Output Sockets

The single output socket returns the response of the request sent to the Google API.

Name|Data Type|Description
:---|:---|:---
Object|`object`|*returns a Google API response*

### Additional Output Sockets

There are no settings to add additional sockets to this node.

## Options

Node options consist of settings that customize how the node interacts with the user's Google calendar.

![Default settings of google calendar node](automation-nodes/google-calendar-default-settings.jpg "Google calendar settings")

After [accessing your Google account](#request-access-to-calendar), one of the default settings will change.

![Default settings after authorization](automation-nodes/google-calendar-default-setting-authorized.jpg "Default settings")

Name|Description
:---|:---
Request access to calendar|allows GudHub to get access to your Google Calendar; [details...](#request-access-to-calendar)
Calendar|allows you to select one of your calendars; [details...](#calendar)
Create meeting|allows node to create meetings in your calendar; [details...](#create-meeting)
Send notifications|allows node to send you notifications; [details...](#send-notifications)
Notes|allows you to leave any notes in the node settings; [details...](#notes)

### Request Access To Calendar

The first calendar setting allows you to connect you google account ot the node. In other words using it you provide access to your Google calendar. Only after authorization will the current node be able to create events and meetings.

### Calendar

This setting contains the list of all calendars available in your Google account. Here you can select the desired calendar in which the event will be created.

### Create Meeting

This is the special setting that allows you to create meeting based on the created event. That is the created event will contain the link to google meet. In this case you have to transfer the emails of the invited people to the node. All attendees will receive an invitation to the meeting.

### Send Notifications

The current setting determines whether notifications about event creation are sent to the event creator and all attendees. The message about event will be sent to their email addresses.

### Notes

This is a standard setting that allows you to specify the purpose of a node in its settings or leave any other notes.
