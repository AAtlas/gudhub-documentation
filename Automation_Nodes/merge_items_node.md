# Merge Items Node

**Merge Items** is

![Merge items node](automation-nodes/merge-items-node.jpg "Merge items")

## Inputs

### Default Input Sockets

Name|Data Type|Description
:---|:---|:---
Source items|`item`|
Destination Items|`item`|

### Additional Input Sockets

## Outputs

### Default Output Sockets

Name|Data Type|Description
:---|:---|:---
New Items|`item`|

### Additional Output Sockets

## Options
