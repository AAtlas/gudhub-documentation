# Populate With Date

**Populate with date** - a node that allows you to fill any field with a specific date. This is useful, for example, if you need to record the date when any data was updated.

>This node can fill any field regardless of its data type.

![Populate with date node](automation-nodes/populate-with-date.jpg "Populate with date")

## Inputs

The current element accepts only one type of data.

### Default Input Sockets

By default, the input socket accepts items. More precisely, when an event occurs with certain item and it is transmitted to the current node, it will start working.

Name|Data Type|Description
:---|:---|:---
Items|`item`|*accepts elements in which the field will be filled*

### Additional Input Sockets

The current node has no additional input sockets.

## Outputs

There is only one output socket in this node.

### Default Output Sockets

The default socket transmits an updated element in which the selected fields are filled with certain dates.

Name|Data Type|Description
:---|:---|:---
Items|`item`|*transmits updated items with populated fields*

### Additional Output Sockets

The current node has no additional output sockets.

## Options

The *Populate with date* node has its own unique settings. They are divided into two groups.

### Default Settings

The first group of settings contains only two options.

![Default settings of populate with date node](automation-nodes/populate-with-date-default-settings.jpg "Populate with date settings")

Name|Description
:---|:---
Application|allows to select the application where the item will be populated with some date; [details...](#application)
Notes|allows to leave some notes in the node settings; [details...](#notes)

#### **Application**

Use this function to select the application in which field data in the certain item should be updated after a specific event.

#### **Notes**

This field is created to leave any notes in the settings of the current node.

### Data To Populate

The second block of settings allows you to configure which fields will be filled in and with what data.

![Settings of data to populate](automation-nodes/populate-with-date-data-to-populate.jpg "Data to populate")

Name|Description
:---|:---
Field|allows to select field that will be populated; [details...](#field)
Date|allows to select the date that will be specified in the appropriate field; [details...](#date)
Edit|allows to edit or delete the data option; [details...](#edit)

#### **Field**

This function is created for selecting the field from the [certain application](#application) that will be filled with data.

#### **Date**

This function contains a list of values that can be entered in the [selected field](#field):

- Now
- Next Day
- Two Days After
- Three Days After
- Four Days After
- Next Week
- Two Weeks After
- Three Weeks After
- This Sunday
- This Monday
- This Tuesday
- This Wednesday
- This Thursday
- This Saturday

>The exact date is determined automatically. It is calculated relative to the current date.

#### **Edit**

This is the column that contains two buttons for each of options. The first button allows you to edit the option, and the second - to delete it.
