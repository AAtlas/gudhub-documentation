# Get Items

**Get Items** is a node that allows you to get items from a specific application. This is usually useful if you need to use data from another application.

![Get items node](automation-nodes/get-items-node.jpg "Get items")

## Inputs

The current node has only one input socket.

### Default Input Sockets

The single input socket of the node does not transmit any data to it. It starts node processes only after a certain event arrives in the socket.

Name|Data Type|Description
:---|:---|:---
Items|`item`|*triggers the start of the node operation*

### Additional Input Sockets

The node has no settings for adding additional input sockets.

## Outputs

As with the input, this node has only one output socket.

### Default Output Sockets

Name|Data Type|Description
:---|:---|:---
Items|`item`|*transmits items from the selected application*

### Additional Output Sockets

Additional output sockets cannot be added to the node.

## Options

*Get Items* has one configuring setting and auxiliary option.

![Default settings of get items node](automation-nodes/get-items-default-settings.jpg "Get items settings")

Name|Description
:---|:---
Application|allows to select the application from which items will be gotten; [details...](#application)
Notes|allows to leave any notes in settings; [details...](#notes)

### Application

This is the source application whose items will be gotten using the current node.

### Notes

This field is created for setting notes. That is, with its help you can enter and save some notes in the settings of the current node.
