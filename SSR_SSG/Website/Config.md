# Config

Every website should have `config.mjs` in its root folder. This file will help you to configure your website properly.

There is a set of properties that allow you to customize the configuration exactly as you need it. Some of them are mandatory, some are used as needed. In total, there are 6 such properties:

---

## Content
- [Config](#config)
  - [Content](#content)
  - [Config structure](#config-structure)
  - [Server config VS client config](#server-config-vs-client-config)
  - [Config properties](#config-properties)
    - [Routes](#routes)
    - [Auth Key](#auth-key)
    - [Build Folder](#build-folder)
    - [Configuration Object for GudHub Initialization](#configuration-object-for-gudhub-initialization)
    - [Query Params Cache Whitelist](#query-params-cache-whitelist)
    - [Index Sitemap](#index-sitemap)
    - [Website Name](#website-name)
    - [Website Description](#website-description)
    - [Chapters](#chapters)
      - [**Sitemap**](#sitemap)
      - [**Feed**](#feed)
    - [Redirects](#redirects)
---

The template config file contains only these three: **Auth key**, **Build folder**, **Chapters**.

> **Note:** All properties from the config file will be passed to the server.  
> This allows you to securely transfer data to the server and then use it in development. All those properties will be stored in `window.constants` on the server.

---

## Config structure

Config structure in divided in two types:
- `Server config` : store confidential data that is available only in server scope.
- `Client config` : store data that used for client render or logic (will be passed to client side, available through `window.getConfig()`).

But access to server config or client config works in the same way: 
```javascript
window.getConfig()
```

Let`s see config examples.

`./config.mjs` file content:
```javascript
    export { chapters } from './config/chapters.mjs';
    export { routes } from './config/routes.mjs';
    export { components_list } from './config/components-list.mjs';
    export { blog_config } from './config/blog-config.mjs';

    export const build_folder = 'dist';
    export const auth_key = "hVwVL/lcfeDRZcqGkfTapGx9X6QpVbQ/szyK/JWydkekVFmF0gZBQy7KhjtLRnz5bZ";
    export const index_sitemap = true;

    // ClientConfig
    export * as clientConfig from './client-config.mjs';
```

`./client-config.mjs` file content:
```javascript
    export { chapters } from './config/chapters.mjs';

    import { blog_config } from './config/blog-config.mjs';
    import { formConfig } from './config/form-config.mjs';
    import { generalInfo } from './config/general-info.mjs';
    import { menuConfig } from './config/menu-config.mjs';
    import { breadcrumbsConfig } from './config/breadcrumbs-config.mjs';

    export const componentsConfigs = {
        blog_config,
        formConfig,
        generalInfo,
        menuConfig,
        breadcrumbsConfig
    };

    export const multiLanguage = true;
    export const languageList = [
        'uk'
    ];
    export const defaultLanguage = 'uk';
```

Client-config is imported to main file `./config.mjs` ().

This code will destructure clientConfig into serverConfig (so you have access to clientConfig in server environment) :
```javascript
    const serverConfig = {
        ...config,
        ...config.clientConfig
    }
```

## Server config VS client config

Text

---

## Config properties

This section provides detailed information about all available configuration properties that can be used for a GudHub DXP-based project.

---

  ### Routes

  The first property in this list allows you to configure routes for your website:

  ```js
  export const routes = [
      {
          route: '/example/',
          index: '/example/example.htm'
      },
      {
          route: '/blog/:category/',
          index: '/blog/category.html'
      },
      ...
  ]
  ```

  > This **must be an array of objects**, each of which **contain route and index properties**.  

  More detailed information can be found in the chapter [Routing](routing.md).

---

  ### Auth Key

  This is a property that is mostly used to securely transmit the authentication key to the server:

  ```js
  export const auth_key = 'kjjjHKSKDJSIOUIO-kJSCNKSJIU';
  ```

  This auth key will be used while accessing applications and their data on the server side while rendering.

  > It also sometimes can be used in the [sitemap](#sitemap).

---

  ### Build Folder

  This property is used to set the folder for your build:

  ```js
  export const build_folder = 'dist';
  ```

  If you have a website with a bundler (e.g., webpack), you need to pass your build folder name here. That is, the built files of the site will be stored in this folder.

  > If you have a simple website, just remove this variable or leave it empty.

---

  ### Configuration Object for GudHub Initialization

  This object is used as a configuration for initializing GudHub.

  ```js
  export const gudhub_db_settings_config = {
      server_url : 'https://example.com/GudHub', 
      wss_url : 'wss://example.com/GudHub/ws/app/', 
      initWebsocket : false
  };
  ```

  > If it is not provided, the default configuration will be applied.

---

  ### Query Params Cache Whitelist

  The array stores the keys for query parameters whose corresponding pages will be cached on the server.

  ```js
  export const query_params_cache_whitelist = ['example-type', 'example-status', 'example-color', 'example-price-level'];
  ```

  This whitelist is a security feature used to control which query parameters are allowed for caching. Only the specified query parameters will be stored in the cache, ensuring efficient retrieval and reducing load times for repeated queries.

  > If this configuration is not provided, query parameters will not be cached by default. Only parameters listed in this array will be cached, so adding or removing parameters allows you to control what pages are stored on the server for improved performance and data control.

---

  ### Index Sitemap

  The current property allows you to determine whether a sitemap index will be created or not:

  ```js
  export const index_sitemap = true;
  ```

  If `index_sitemap` is set to **true**, the server will generate an index sitemap and child sitemaps based on the [chapter sitemap configuration](#chapters).

  > If this property is **false**, the server will generate a single sitemap with all pages.

---

  ### Website Name

  This property is designed to set the name of the website:

  ```js
  export const website_name = 'Atlasiko';
  ```

  This value will be **used as the title in feeds (RSS, Atom, JSON)**. To learn more about feeds, [see **chapters** configuration](#chapters).

---

  ### Website Description

  This is the property that allows you to use descriptions:

  ```js
  export const website_description = 'Example of website description.'
  ```

  When you set a specific description, it can be used as a title in feeds (RSS, Atom, JSON). For more information about channels, see [Chapters](#chapters).

---

  ### Chapters

  The current property contains all chapters of the site with their properties.

  > It is one of the mandatory properties.

  Here you can add new chapters or delete existing ones:

  ```js
  export const chapters = {
      pages: {
          app_id: 00000,
          slug_field_id: 000000,
          element_id: 000000,
          title_field_id: 000000,
          description_field_id: 000000,
          thumbnail_field_id: 000000,
          created_at_field_id: 000000,
          author_reference_field_id: 000000,
          sitemap: {
              frequency: 'weekly',
              priority: 0.8,
              sitemapName: 'pages',
              cases: [
                  {
                      case: '/',
                      priority: 1,
                      frequency: 'daily'
                  },
                  {
                      case: /example/g,
                      priority: 0.2,
                      frequency: 'weekly',
                      sitemapName: 'examples'
                  }
              ],
              filter: (items) => {
                  return items.filter(item => {
                      const field = item.fields.find(field => field.field_id == 699138);
                      if(field) {
                          return field.field_value == 1;
                      }
                      return false;
                  });
              }
          },
          feed: {
              google_news: {
                  filter: (items) => {
                      let filteredItems = items.filter(item => {
                          const field = item.fields.find(field => field.field_id == 697704);
                          if(field) {
                              return field.field_value == 1;
                          }
                          return false;
                      });

                      filteredItems = filteredItems.filter(item => {
                          const field = item.fields.find(field => field.field_id == 698599);
                          if(field) {
                              return field.field_value == 3;
                          }
                          return true;
                      });

                      return filteredItems;
                  }
              }
          }
      }
  }
  ```

  A **Chapter** is an object that stores chapters as properties. Each chapter has its own set of properties, which are stored in the object as well. There are only two mandatory properties:

  Property Name|Type|Description
  :--|:--|:--
  `app_id`|`number`|*app id of application for this chapter*
  `slug_field_id`|`number`|*field id of field with slug (url) of page*

  All other properties are optional:

  Property Name|Type|Description
  :--|:--|:--
  `json_field_id`|`number`|*field id of field with data (json editor data type)*
  `title_field_id`|`number`|*field id of field with page title (text). Will be used in feed generation*
  `description_field_id`|`number`|*field id of field with page description (text editor). Will be used in feed generation*
  `thumbnail_field_id`|`number`|*field id of field with thumbnail path (text). Will be used in feed generation*
  `created_at_field_id`|`number`|*field id of field with created at date (date). Will be used in feed generation*
  `author_reference-field_id`|`number`|*field id with reference to page author. Author item should be in the same application as pages. Will be used in feed generation*
  `sitemap`|`object`|*configuration object for sitemap*
  `feed`|`object`|*if you want to generate feed (rss, atom, json), you should add this property to chapter, you want to generate feed for. Currently, we support only one feed for one chapter per website*

  >If you want some sort of CMS, you need to create GudHub application for your website.

  It will be better, to create separate application for every logical part of website (simple pages, blog, products, etc.). Every item in these applications will be page in future. For every application you need to create object in **chapters object** like above.

 #### **Sitemap**

  The sitemap is a list of information about pages, video, and other file on your site. In the config file, this sitemap is specified as a property of [chapters](#chapters). But it also has its own properties:

  ```js
  export const chapters = {
      pages: {
          app_id: 00000,
          slug_field_id: 000000,
          sitemap: {
              frequency: 'weekly',
              priority: 0.8,
              sitemapName: 'pages',
              cases: [
                  {
                      case: '/',
                      priority: 1,
                      frequency: 'daily'
                  },
                  {
                      case: /example/g,
                      priorite: 0.2,
                      frequency: 'weekly',
                      sitemapName: 'examples'
                  }
              ],
              filter: (items) => {
                  return items.filter(item => {
                      const field = item.fields.find(field => field.field_id == 699138);
                      if(field) {
                          return field.field_value == 1;
                      }
                      return false;
                  });
              }
          }
      }
  }
  ```

  Property Name|Type|Description
  :--|:--|:--
  `frequency`|`string`|*default frequency for all pages*
  `priority`|`number`|*default priority for all pages*
  `sitemapName`|`string`|*if you set index_sitemap to true, this property will generate separate sitemap for pages from this chapter and adds link to it in index sitemap*
  `cases`|`array`|*array of special cases. If some pages must have different properties in sitemap from other in this chapter, you should create cases for this pages. In example above we change priority and frequency for homepage and example page*
  `filter`|`function`|*if you want to filter some pages, to not add them to sitemap, you can create filter. Filter property receive javascript function and run it. In example above, we are filtering all pages, that have status property equal to 'draft' in gudhub*

  As mentioned above, an object called **cases** has its own set of properties:

  Property Name|Type|Description
  :--|:--|:--
  `case`|`string`|*for which page this case should work. Cat receive string with slug or regular expression*
  `priority`|`number`|*value of priority for this case*
  `frequency`|`string`|*value of frequency for this case*
  `sitemapName`|`string`|*if you want to create separate sitemap for this case(s), you can pass it's name here. Then, separate sitemap with given name will be generated, and all pages that meet the condition will be added in it. Also, this pages will be removed from parent sitemap, to avoid the duplicates*

  #### **Feed**

  This property allows you to create RSS for the specific chapter.

  >It **can be used for only one chapter** within an [array](#chapters).

  **To create a feed you need** the `google_news` inside the `feed` object, [chapter properties](#chapters):

  - `app_id`
  - `slug_field_id`
  - `element_id`
  - `title_field_id`
  - `description_field_id`
  - `thumbnail_field_id`

  And two config properties: [Website Name](#website-name) and [Website Description](#website-description). The feed object has its own properties:

  ```js
  export const chapters = {
      pages: {
          app_id: 00000,
          slug_field_id: 000000,
          element_id: 000000,
          title_field_id: 000000,
          description_field_id: 000000,
          thumbnail_field_id: 000000,
          created_at_field_id: 000000,
          feed: {
              google_news: {
                  filter: (items) => {
                      let filteredItems = items.filter(item => {
                          const field = item.fields.find(field => field.field_id == 697704);
                          if(field) {
                              return field.field_value == 1;
                          }
                          return false;
                      });

                      filteredItems = filteredItems.filter(item => {
                          const field = item.fields.find(field => field.field_id == 698599);
                          if(field) {
                              return field.field_value == 3;
                          }
                          return true;
                      });

                      return filteredItems;
                  }
              }
          }
      }
  }
  ```

  Property Name|Type|Description
  :--|:--|:--
  `google_news`|`object`|*name of service to generate feed for. Currently, only google news is supported, so if you want to generate feed, you should add this property*
  `filter`|`function`|*if you want to filter some pages, to not add them to feed, you can create filter. Filter property receive javascript function and run it. In example above, we are filtering all pages, that have status property equal to 'draft' and in next step filtering all pages, that have status not equal no "News" in gudhub app*

  Usually, you only need to create a `google_news`. The `filter` only further filters the pages. When you create a feed, you create three types of feeds at the same time: **RSS**, **Atom**, and **JSON**.

  ### Redirects

  The last property allows you to create redirects to any page:

  ```js
  export const redirects = [
      {
          from: '/example/',
          to: '/example-2/'
      }
  ]
  ```

  Property Name|Type|Description
  :--|:--|:--
  `from`|`string`|*the route from which redirects will be made*
  `to`|`string`|*the route to which redirects will be made*

  Here you can create redirects for your website. Redirects property is an array of objects. Each object should has from and to properties. All redirects will have 300 status (moved permanently).
