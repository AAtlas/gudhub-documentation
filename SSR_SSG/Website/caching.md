# Caching

The cache on the GudHub server has several different uses. Namely, a few modes of server operation use the cache differently. Each of them makes the [server](../Server/Server.md) work a little differently.

## Production Mode

The current mode is intended for the site to **return cached pages**. This mode is used mostly for a ready-made website. To enable this mode, open the `.env` file and enter:

```json
    MODE=production
```

Originally, the page has no cache, but when you open it for the first time, it generates one. That's why the first user may have to wait quite a long time for the page to be generated, but all subsequent times it will be much faster due to the cache that was obtained for the first time. After that, every time any user opens the page, that cache will be used.

## SSR Mode

Due to this mode, the server do not use the existing cache. It accesses the dist folder where the built files are located and generates a new cache based on them. So, this mode is useful when you need to update the content of the page.

To use it, take the page you wanna regenerate and add `?mode=ssr` in the end of it, like here:

```json
    gudhub.com/news/?mode=ssr
```

>Please note that this mode is not activated in the `.env` file.

The second mode works in conjunction with the [previous one](#production-mode). This means that you need to set the [production](#production-mode) mode in `.env` and only then activate the current.

### Image Generation

As you know, it is hard for server to work with images. If your site has a large number of images, it can take a long time to generate them. So, the server has a feature that allows you to prevent repeated image generation.

To use this feature enter the `disable-images-regeneration=true` string after the `mode=ssr`:

```json
    https://:www.gudhub.com/news/?mode=ssr&disable-images-regeneration=true
```

>That is, this function can be used **only in combination with the** [SSR mode](#ssr-mode).

## Multiple SSR

When you have a large number of pages that need to be re-generated using [SSR mode](#ssr-mode), it is inconvenient and time-consuming to do it one page at a time. To solve this problem, the GudHub server has a special request. It allows you to re-generate an array of pages at once.

To use this method you have to send the request to your local server that looks like this:

```json
    http://127.0.0.1:7777/generate-pages/
```

The request body contains two properties:

```json
{
    "pages": "/news/",
    "website": "gudhub.com"
}
```

Property Name|Type|Description
:--|:--|:--
pages|`string`|*allows you to **filter pages.** That is, those pages that **meet the conditions specified here will be re-generated**. For example, you can specify a category name like `/news/` or even a word that appears in the links of the desired pages*
website|`string`|*transfer the **site whose pages will be re-generated***

>This request does not re-generate images that are not updated. This is because the [feature described in the previous chapter](#image-generation) is **active here by default**.

This mode takes pages from all sitemaps and creates an array. If the `pages` property is empty, it processes all pages from the sitemaps. Otherwise, it filters the pages and processes those that satisfy the conditions.

## Development Mode

The last mode always returns the latest version of the site.

>In this case, the **cache is saved but not used**.

To use this mode, open the `.env` and enter this here:

```json
    MODE=development
```

It is mainly used in development, but if you are using the latest version of the server, then the simplest site can be raised in this mode in production.
