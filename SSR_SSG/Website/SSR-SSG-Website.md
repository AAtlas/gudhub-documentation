# Website

This part of documentation is about websites and configuration. Here you can find list of pages about it:

1. [Getting started](../SSR-SSG-Getting-started.md).
2. [Config](./Config.md).

The website template consists of lots of different files. Their structure is specially configured to work with GudHub packages.

## Project Folder

This is the main folder in which the site template was installed.  It is created by you at the beginning of the [package installation](../SSR-SSG-Getting-started.md). All other folders and files are stored here.

>For the most part, the project structure is standard.

To work with the GudHub server and packages, your project must meet certain requirements. Therefore, we will consider all the files using the example of a template that already meets all the requirements.

Folders are first in the list, followed by files. There are four folder at all:

- [Website](#website)
  - [Project Folder](#project-folder)
    - [Dist](#dist)
    - [Node Modules](#node-modules)
    - [Source](#source)
    - [Config](#config)

Each of them is described in a separate chapters below. So, now  we will consider the purpose of other files:

- `config.mjs` - allows you to configure the site, set a list of initial parameters required to start the project. Also have export of clientConfig. More information can be found in the article [Config](./Config.md).
- `client-config.mjs` - store configuration of components or data that need to be accessible is both of SSR and CSR (client side render) [Config](./Config.md). 
- `package-lock.json` and `package.json` - both package files contain all the libraries included in the project.
- `README.md` - a basic readme file that contains introductory information about the [technology](../Intro.md), how to [set it up and work with it](../SSR-SSG-Getting-started.md), and explanations of how it works.
- `webpack.bundle.js` - webpack config, responsible for bundling all components in one `bundle.js` and `bundle.css` (used in SSG-SSR puppeteer). Read more about website bundling here [Bundling](./Bundling.md)..
- `webpack.common.js`, `webpack.dev.js`, and `webpack.prod.js` - webpack configіs, which is responsible for copying all html, css and js to dist, also packs each component into a separate package (the server uses these component packages to separately build an optimized bundle for each page).  Read more about website bundling here [Bundling](./Bundling.md).
- `gitignore` files - contain files and folders that Git should ignore and not track.

>Among all these files, only **contents of the `scr` folder** and **`config` file** can be freely edited.

### Dist

Webpack builds all files in the **dist folder**. So, it contains only the **builded files**.

### Node Modules

This is the folder where the node modules will be installed.

### Source

The **source** folder is where your main work will take place. Here we have a standard set of template files and folders:

- assets
  - css
  - images
  - js
- html files

So, it is a folder that stores all the styles, images, components, HTML files, and other files of the site.

>Each of them can be edited at your discretion.

### Config

Contains files "***.mjs", they store data that you need in render process or user interaction, for example you can use it to store vocabulary map (static content which depends on user selected language or something else). Those files must be exported in `./config.mjs` (store data that accessible only in SSR) or `./client-config.mjs` (accessible in both SSR and CSR).