# Routing

This article describes the way to add routes to your website. You can do this by adding objects to the routes array in the [config file](Config.md). In each object must be **route** and **index properties**:

```js
export const routes = [
    {
        route: '/example/',
        index: '/example/example.htm'
    },
    {
        route: '/blog/:category/',
        index: '/blog/category.html'
    }
]
```

Property Name|Type|Description
:--|:--|:--
Route property|`string`|*the url of page you want to create.*
Index property|`string`|*a path to html file, which should be opened on this route.*

## Route Types

There are two types of routes:

- **Static routes**. This is the standard route where you enter the name of a specific category.
- **Dynamic routes**. You probably noticed this route - `/blog/:category/`. This is dynamic route. It can be used for dynamic pages (articles, categories, authors for blog e.x.). This route will work in cases like this: `/blog/web-development`/ and will return `/blog/category.html` html file.

You can combine several dynamic parts in one route, for example, like this:

    /blog/:category/:article/

## Order of Routes

If different types of routes are stored in the same array, pay special attention to the order of routers:

```js
export const routes = [
    {
        route: '/blog/example/',
        index: '/blog/example.html'
    },
    {
        route: '/blog/:category/',
        index: '/blog/category.html'
    },
    {
        route: '/blog/authors/:author/',
        index: '/blog/author.html'
    },
    {
        route: '/blog/:category/:article/',
        index: '/blog/article.html'
    }
]
```

>In case like above, **order of routes is important**. Because, route, for example, `/blog/example/` match both `/blog/example/` and '/blog/:category/'. That's why, **standard routes must be above dynamic**.
