# Development

This part of documentation is about GHComponent development.
<br><br>

## Get started

To develop GHComponent you need to clone it's repository. Then, pick the right node version from package.json, install dependencies, and run

```shell
npm run dev
```
This will start build of GHComponent.
<br><br>
After this, you have 2 options. If you use any type of bundler on your website, run in GHComponent project

```shell
npm link
```

Go to website project, pick same version of node, and run 

```shell
npm link @gudhub/gh-component
```

After this step, website will pick code for GHCopmponent not from node_modules, but from your local build.

<br><br>

If you don't have any build, you need to to server current folder. To do this, for now, we recomend you to use **Live Server** extendison for VS Studio Code (or any similar extension to server code from current folder). In the future, we will add server feature to GHComponent.
<br>
Next, you need to include GHComponent to your website page as script

```html
<script src="http://localhost:5500/dist/main.js"></script>
```