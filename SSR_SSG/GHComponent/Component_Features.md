# GHComponent Features

Every component inherited from `GHComponent` includes these features:

## Content
- [GHComponent Features](#ghcomponent-features)
  - [Content](#content)
  - [Environment Variable](#environment-variable)
  - [GudHub Library](#gudhub-library)
  - [Server-Side Attribute Handling](#server-side-attribute-handling)
  - [Text Replacement with `gh-id`](#text-replacement-with-gh-id)
  - [Template Rendering](#template-rendering)
  - [Event Handling](#event-handling)
  - [GudHub Data Handling](#gudhub-data-handling)
  - [Utility Methods](#utility-methods)
  - [Error Handling](#error-handling)

## Environment Variable

Each component determines its environment (either `client` or `server`) based on the current execution context. This can be used to execute code specific to the client or server:

```javascript
this.environment === 'client' || 'server';
```

- **Server**: Suitable for HTML rendering or initializing GudHub.
- **Client**: Suitable for event listeners, sliders, and other browser-specific actions.

## GudHub Library

- On the **server**, GudHub is automatically initialized using the auth-key from the site configuration.
- On the **client**, you can create an instance of GudHub as needed.

## Server-Side Attribute Handling

- Adding the `to-be-removed` attribute to a component will cause it to be stripped on the server, leaving only the inner HTML on the client. **Note:** This will break the component's functionality and remove its `<script>` tag. If the component appears multiple times on a page, only the first instance will work.

## Text Replacement with `gh-id`

- Elements inside the component with a `gh-id` (path to data in json object) attribute will have their inner text replaced by corresponding data from GudHub's JSON document.
- This replacement happens during server-side rendering or as the component loads on the client.

Example:

```html
<span gh-id="example.text"></span>
```

- To fetch data, ensure a JSON editor is configured in the GudHub application for the relevant page.

## Template Rendering

- Components can use the `render` method to dynamically generate their content based on a provided template and the component's data:

```javascript
this.render(`<div>${this.someData}</div>`);
```

- This method supports:
  - Looping via `gh-for` attributes.
  - Binding component properties to the template.

## Event Handling

- Automatically binds event listeners specified in attributes (e.g., `onclick`, `onchange`) to their corresponding methods in the component.
- Arguments passed to these methods can include:
  - Evaluated JavaScript expressions.
  - Component properties.

## GudHub Data Handling

- **Fetching Data**: Use `getGhData(ghId, chapter, slug)` to retrieve data from GudHub's JSON document by `gh-id`.
- **Adding Data**: Use `addGhData(ghId, data, chapter, slug)` to insert new data into GudHub's JSON document.
- **Default Data**: Define a default template for a component's data using `setDefaultData(data)`.

## Utility Methods

- **replaceText(slug)**: Replaces all `gh-id` elements' inner HTML with data from GudHub JSON documents.
- **findValueByPath(json, path)**: Fetches a nested value in an object using a dot-separated string path.
- **findIds(application, slug)**: Finds the `appId` and `itemId` for the corresponding GudHub application or slug.
- **activateListeners()**: Binds events to elements inside the component.

## Error Handling

- Use the `error(err)` method to reject the component's rendering promise if an error occurs during rendering.

By leveraging these features, `GHComponent` provides a powerful foundation for creating GudHub-integrated components with robust client-server functionality.