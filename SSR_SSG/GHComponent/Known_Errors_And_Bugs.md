# Known errors and bugs

Here you can find common errors and bugs.

## TO-BE-REMOVED attribute breaks component or page

TO-BE-REMOVED attribute often breaks component or even whole page on generation. If page with your component works in debugger, but empty on regular open, try to remove 'to-be-removed' attributes from component on this page. This bug, probably, will be fixed in the future, or we will completly remove 'to-be-removed' feature support.