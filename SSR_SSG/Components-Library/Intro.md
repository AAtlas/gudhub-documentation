# Intro

Library "ssg-webcomponents-library" stores components which can be used in any site project, first of all they were collected in one lib to speed up the development time.
Library is already imported to all our CLI's

## Content
- [Intro](#intro)
  - [Content](#content)
  - [Purpose](#purpose)
  - [Why it doesnt use bundler](#why-it-doesnt-use-bundler)

## Purpose

This lib collects all components which are reusable, they doesnt have any "hardcoded" data, they have documentation to help you configure them or use.
  
## Why it doesnt use bundler

Since the site and server projects build components independently, we do not need any build in the component library.