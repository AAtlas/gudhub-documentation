# How components-library works

## Content
- [How components-library works](#how-components-library-works)
  - [Content](#content)
  - [Component Files Structure](#component-files-structure)
  - [General Config](#general-config)
  - [How to use components](#how-to-use-components)

## Component Files Structure

Each separate component has folder named as tagName of component, and files:

  - `config.js`: file that store object with tagName and path of component;
  - `component-name.html` : layout file;
  - `component-name.js` : js file;
  - `component-name.scss` : styles;
  - `component-name.json` : template of data from gudhub; 
  - `component-name.readme.md` : documentation;

## General Config

  Config `./src/config.js`  contains a list of exports of configurations of all components. Used to connect all lib components in one time.

## How to use components

Just use as usual ssg-ssr component, write its tagName in page html and add correspond data-gh-id attribute (if component needs it).

Example:
  ```html
     <custom-component data-gh-id="test-component"></custom-component>
  ```