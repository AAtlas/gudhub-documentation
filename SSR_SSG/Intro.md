# Intro

**GudHub Digital Experience Platform** is a complete set of tools that allows you to design, create, manage and optimize digital customer experiences. Our *DXP* provides you with all the necessary tools for **website management** and **business analytics**. It is a custom system with data output to the website. Based on this data, the site will be rendered.

The operation of our *DXP* is based on **SSR/SSG technologies**. First, let's briefly describe them.

- **Server-Side Rendering (SSR)** approach allows you to generate a web page on the first request and at each subsequent reload, a request is made to the server, which allows you to always get up-to-date data.

- **Static site generation (SSG)** is slightly different from the previous approach. It generates all pages at once and then returns on request.

Both of them are stand out by the speed of loading and displaying content. These rendering technologies complement the set of tools in **GudHub DXP** well.

>So, the **GudHub DXP** is a full-fledged digital experience platform. Using it you can manage the digital interaction with users and update the content of your website.

This chapter will help you to get acquainted with this platform, understand how it works and learn how to use it.

## How it's work

It's based on native web components and puppeteer library. Basically, we just start a page on server with puppeteer (which is just headless chrome), preparing sources (images e.x), waiting for all components to resolve and cache ready HTML. Then, we just send this cached HTML documents to user on request.

**N.B.** For better experience, you should use it in a pair with GudHub.

### Parts of technology

For now, it consists of 3 parts:

1. [GUDHUB SSR/SSG Server](./Server/Server.md). Responsive for server side rendering, sources preparing, caching and cache serving.
2. [GH Component](./GHComponent/GHComponent.md). GHComponent is using for all other components creation. Every component should be inherited from GHComponent.
3. [Website](./Website/SSR-SSG-Website.md). Website itself. Here you can find info about website configuration.
