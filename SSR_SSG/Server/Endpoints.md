# API Endpoints Documentation

## Overview

This document describes the API endpoints available in the SSR-SSG server, their purposes, and expected behaviors. Each section outlines the HTTP method, endpoint path, description, and details of the expected request and response.

---

## Content
- [API Endpoints Documentation](#api-endpoints-documentation)
  - [Overview](#overview)
  - [Content](#content)
  - [Sitemap and Feed Endpoints](#sitemap-and-feed-endpoints)
    - [`/sitemap.xml`](#sitemapxml)
    - [`/:type-sitemap.xml`](#type-sitemapxml)
  - [Page and File Endpoint](#page-and-file-endpoint)
    - [`*`](#)
  - [SSR Menu Endpoints](#ssr-menu-endpoints)
    - [`/auth`](#auth)
    - [`/ssr-menu`](#ssr-menu)
  - [Utility Endpoints](#utility-endpoints)
    - [`/version`](#version)

---

## Sitemap and Feed Endpoints

### `/sitemap.xml`
- **Method**: `GET`
- **Description**: Generates a general sitemap for the website.
- **Behavior**:
  - Uses website sitemap configurations to determine sitemap structure.
  - Returns xml file with list of typed-sitemap with link for it ( `/:type-sitemap.xml` ).

### `/:type-sitemap.xml`
- **Method**: `GET`
- **Description**: Generates a type-specific sitemap (e.g., articles, categories, pages).
- **Behavior**:
  - Uses website sitemap configurations for type-specific generation.

## Page and File Endpoint

### `*`
- **Method**: `GET`
- **Description**: Catches all unspecified requests and serves the appropriate content or error page.
- **Behavior**:
  - Resolves requests to static files or generates pages dynamically (SSR/SSG mode).
  - Handles redirects, trailing slash enforcement, and query parameter sanitization.
  - Attempts to serve a `404.html` page or defaults to a "404" response if no page or file is found.

---

## SSR Menu Endpoints

### `/auth`
- **Method**: `GET`
- **Description**: Serves the authentication page.

### `/ssr-menu`
- **Method**: `GET`
- **Description**: Provides indexed site pages with opportunity to update them ( SSR ).
- **Middleware**: `authenticateToken` (Validates user authentication).

---

## Utility Endpoints

### `/version`
- **Method**: `GET`
- **Description**: Returns the current version of the package.
- **Response**: Plain text containing the version number.
