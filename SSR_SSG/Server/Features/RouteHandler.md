# RouteHandler.js

## Overview

This document describes the `RouteHandler` methods, and their respective functionalities. The `RouteHandler` is responsible for routing and handling URLs, finding matching route templates, and managing static file retrieval in SSR or SSG context.

---

## Content 
- [RouteHandler.js](#routehandlerjs)
  - [Overview](#overview)
  - [Content](#content)
  - [Methods](#methods)
    - [`findSlugTemplate`](#findslugtemplate)
    - [`findStaticFile`](#findstaticfile)

---

## Methods

  ### `findSlugTemplate`

  - **Purpose**: Finds a matching route template for a given URL and extracts path parameters.
  - **Parameters**:

    - `routes` (Array): An array of route objects, each containing a route string and associated properties.
    - `url` (string): The URL to be matched against the provided routes.
    - `formattedQueryParams` (string, optional): Query parameters to append to the resulting path.

  - **Returns**: An object with the following structure:

      - `found`: (boolean): Indicates if a matching route was found.
      - `paramsObject`: (object): Contains extracted path parameters.
      - `queryParams`: (string): Query string with extracted parameters.

  - **Behavior**: Splits the URL and route templates into segments. Matches the URL against routes, including dynamic parameters (e.g., :id).

---

  ### `findStaticFile`
  - **Purpose**: Resolves a given URL to a static file or an HTML file in the build directory.

  - **Parameters**:

    - `root` (string): The root directory of site.
    - `website` (string): A unique identifier for the website (used for caching).
    - `url` (string): The requested URL.
    - `mode` ('ssg' | 'ssr'): The server's mode of operation ('ssg' or 'ssr').
    - `urlParams` (object): Additional parameters for resource generation (e.g., image dimensions).
    - `buildFolder` (string): The directory containing the built files for the website.

  - **Returns**: An object with the following structure:

    - `type`: ('asset' | 'html'): Indicates whether the resolved file is an asset or HTML.
    - `path`: (string): The file path relative to the root directory.

  - **Behavior**: Detects whether the url refers to a file (based on the presence of an extension). If the URL points to an image, invokes the ImageHandler to get image from cache (SSG) or generation (SSR). Checks for .html files or index.html in the specified build folder if no specific file is found.