# SSG.js

## Overview

This document describes the `SSG` class, which provides methods for handling static site generation (SSG) caching and error management. It includes functionality for retrieving cached pages, adding error logs to the cache, and handling timeouts during page generation.

---

## Content 
- [SSG.js](#ssgjs)
  - [Overview](#overview)
  - [Content](#content)
  - [Methods](#methods)
    - [`getCache`](#getcache)
    - [`addTimeoutErrorToCache`](#addtimeouterrortocache)

---

## Methods

### `getCache`

- **Purpose**: Retrieves the cached HTML file for a given route and query parameters.
- **Parameters**:
  - `website` (string): The identifier of the website whose cache is being accessed.
  - `route` (string): The route path for the requested page.
  - `formattedQueryParams` (string, optional): Query parameters formatted as part of the request.
- **Returns**:
  - The cached HTML content as a Buffer if the cache file exists.
  - `false` if the cache file is not found.
- **Behavior**:
  - Constructs the path to the cache file based on the provided route and query parameters.
  - Attempts to read the file at the constructed path.
  - Returns the file content or `false` if an error occurs (e.g., file not found).

---

### `addTimeoutErrorToCache`

- **Purpose**: Modifies the cached HTML file to include error messages and logs when a page rendering timeout occurs.
- **Parameters**:
  - `website` (string): The identifier of the website whose cache is being updated.
  - `route` (string): The route path for the page with the timeout error.
  - `log` (Array): An array of log entries to include in the updated cache.
- **Returns**:
  - The modified HTML content as a string.
  - `'Cache not found'` if the cache file does not exist.
- **Behavior**:
  - Constructs the path to the cached HTML file for the given route.
  - Checks if the cache file exists; returns `'Cache not found'` if it does not.
  - Opens the cached page in a Puppeteer browser instance.
  - Injects error logs and timeout messages into the page's HTML.
  - Saves the modified HTML back to the cache file.
  - Returns the updated HTML content.

---