# RedirectsHandler.js

## Overview
The RedirectsHandler class is responsible for validating whether a given URL matches any routes configured for redirection. The list of routes is provided during the initialization of the class instance, which occurs for every incoming request.

---

## Content 
- [RedirectsHandler.js](#redirectshandlerjs)
  - [Overview](#overview)
  - [Content](#content)
  - [Methods](#methods)
    - [`checkRedirects`](#checkredirects)

---

## Methods

  ### `checkRedirects`

  - **Purpose**: Checks if url match with redirects "from"

  - **Parameters**:
    - `url`: (string): url .

  - **Returns**: An object contain urls in "from" (string) and "to" (string).

  - **Behavior**: Checks if url match with redirects "from".