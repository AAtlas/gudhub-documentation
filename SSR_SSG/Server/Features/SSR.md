# SSR.js

## Overview

This document describes the SSR class, which is responsible for rendering and caching server-side rendered (SSR) pages using Puppeteer. The class manages the rendering process, including logging, bundling, and caching of HTML and JSON data.

## Content 


## Methods

  ### `generate`

  - **Purpose**: Renders an SSR page using Puppeteer, processes the output, and caches the result.

  - **Parameters**:

    - **root** (string): Root directory of the website.
    - **website** (string): Website identifier for caching and resource management.
    - **route** (object): Contains route-specific information like path and query parameters.
    - **chapters** (object): Site-specific chapters data.
    - **auth_key** (string): Authorization key for accessing gudhub resources.
    - **buildFolder** (string): Path to the website folder containing built files.
    - **disableImagesRegeneration** (boolean): If true, prevents regeneration of images.
    - **preview** (boolean): Indicates whether the rendering is for preview purposes.
    - **disableBundling** (boolean): If true, skips bundling of components.
    - **config** (object): Configuration data for SSR.
    - **gudhubDBSettingsConfig** (object): configurations of gudhub instance used to get data.
    - **clientSortedAndFilteredQueryParams** (string, optional): Pre-sorted query parameters to pass them in puppeteer.

  - **Returns**:
    The HTML content of the rendered page.
    '404' if the page is not found.
    Error object if SSR fails.

  - **Behavior**:
    Launches a Puppeteer page and sets up logging.
    Injects client configuration and language-specific data.
    Loads the page and processes the resulting HTML and assets.
    Bundles components if not disabled.
    Caches HTML and JSON data unless in preview mode.

  ### `cacheHTML`

  - **Purpose**: Caches the rendered HTML content of an SSR page.

  - **Parameters**:

    - `content` (string): The HTML content to be cached.
    - `website` (string): Website identifier for caching.
    - `route` (object): Contains route object.
    - `formattedQueryString` (string, optional): if page needs to be cached by queryParams, pass formatted (filtered by website queryParams whitelist and sort by alphabet) query string to append to the cache path.

  - **Returns**: true upon successful caching.

  - **Behavior**:
    Constructs the cache path using the route and query parameters.
    Creates directories if they do not exist.
    Writes the HTML content to an index.html file in the cache directory.

  ### `cacheJsonData`

  - **Purpose**: Caches JSON data associated with an SSR page.

  - **Parameters**:

    - `jsonData` (object): The JSON data to be cached.
    - `website` (string): Website identifier for caching.
    - `route` (object): Contains route-specific information like path and query parameters.

  - **Returns**: true upon successful caching, false otherwise.

  - **Behavior**:
    Constructs the cache path using the route and query parameters.
    Creates directories if they do not exist.
    Writes the JSON data to a cachedJsonData.json file in the cache directory.