# SSG-menu.js

## Overview

The `SSG-menu.js` file defines two primary routes for a static site generator (SSG) management system:

1. **Authentication Routes (`/auth`)**: Handles user authentication to secure access to the SSG menu.
2. **SSR Menu Routes (`/ssr-menu`)**: Facilitates listing website pages and regenerating them via server-side rendering (SSR).

These routes ensure controlled and secure access to SSG tools, while enabling efficient page management.

---

## Content

- [SSG-menu.js](#ssg-menujs)
  - [Overview](#overview)
  - [Content](#content)
  - [Routes](#routes)
    - [`/auth`](#auth)
      - [Endpoints](#endpoints)
    - [`/ssr-menu`](#ssr-menu)
      - [Endpoints](#endpoints-1)
      - [Middleware](#middleware)

---

## Routes

### `/auth`

#### Endpoints

- **`/`**:
  - **Behavior**: 
    - Returns the login page as an HTML file.
  - **Example**:
    ```http
    GET /auth/
    ```
    **Response**:
    - **200**: HTML content of the login page.

- **`/login`**:
  - **Behavior**:
    - Authenticates the user using the GudHub API.
    - Validates the user's `auth_key` against the website's `auth_key`.
    - Sets an `accesstoken` cookie if authentication is successful.
  - **Example**:
    ```http
    POST /auth/login
    Content-Type: application/json

    {
        "username": "admin",
        "password": "password123"
    }
    ```
    **Response**:
    - **200**: `{ "message": "Login successful" }` (on success).
    - **401**: `Unauthorized: Invalid credentials`.
    - **403**: `Forbidden: Invalid authentication key`.

---

### `/ssr-menu`

#### Endpoints

- **`/`**:
  - **Behavior**:
    - Serves the SSR menu page as an HTML file.
  - **Example**:
    ```http
    GET /ssr-menu/
    ```
    **Response**:
    - **200**: HTML content of the SSR menu page.

- **`/pages-list`**:
  - **Behavior**:
    - Fetches the list of pages from the website’s sitemap located at `<protocol>://<host>/pages-sitemap.xml`.
    - Returns the list of page URLs as JSON.
  - **Example**:
    ```http
    GET /ssr-menu/pages-list
    ```
    **Response**:
    - **200**: JSON array of page URLs.
      ```json
      [
          "https://example.com/page1",
          "https://example.com/page2"
      ]
      ```
    - **500**: `{ "error": "Internal server error" }`.

- **`/generate-pages`**:
  - **Behavior**:
    - Initiates the SSR process to regenerate pages specified in the request body.
    - Prevents concurrent SSR processes.
    - Returns the results of the SSR process.
  - **Example**:
    ```http
    POST /ssr-menu/generate-pages
    Content-Type: application/json

    {
        "items": [
            "https://example.com/page1",
            "https://example.com/page2"
        ]
    }
    ```
    **Response**:
    - **200**: JSON with SSR results.
      ```json
      [
        {
          "site": "https://example.com/page",
          "status": "Done",
          "duration": "2.1s",
          "valid": "true",
          "time": "finish date"
        }
      ]
      ```
    - **400**: `{ "message": "Invalid input" }`.
    - **409**: `{ "message": "Another SSR process is running" }`.
    - **500**: `{ "message": "An error occurred during the SSR process" }`.

---

#### Middleware

- **`authenticateToken`**:
  - **Behavior**:
    - Ensures users are authenticated before accessing sensitive SSR menu endpoints.
  - **Usage**:
    ```javascript
    router.use('/generate-pages', authenticateToken);
    ```
    **Expected Behavior**:
    - If the user is authenticated, the request proceeds to the endpoint.
    - If the user is unauthenticated, the server responds with `401 Unauthorized`.
