# SitemapGenerator.js

## Overview
The `SitemapGenerator` class is responsible for creating sitemaps for websites hosted on the platform. It includes methods for generating simple sitemaps, index sitemaps, and type-specific sitemaps. The class interacts with the website's configuration files and uses the GudHub API to gather and organize page data for sitemap generation.

---

## Content 
- [SitemapGenerator.js](#sitemapgeneratorjs)
  - [Overview](#overview)
  - [Content](#content)
  - [Methods](#methods)
    - [`generateSimpleSitemap`](#generatesimplesitemap)
    - [`generateIndexSitemap`](#generateindexsitemap)
    - [`generateTypeSitemap`](#generatetypesitemap)
    - [`getPages`](#getpages)

---

## Methods

  ### `generateSimpleSitemap`
  - **Purpose**: Generates a simple sitemap containing all pages from the website's chapters.

  - **Parameters**:
    - `req`: HTTP request object.
    - `res`: HTTP response object.

  - **Respond**: gzip-compressed XML sitemap.

  - **Behavior**:
    - Fetches the website configuration and chapter details.
    - Uses GudHub to retrieve pages for each chapter.
    - Writes URLs with priority and frequency based on the configuration.

---

  ### `generateIndexSitemap`
  - **Purpose**: Creates an index sitemap linking to multiple type-specific sitemaps.

  - **Parameters**:
    - `req`: HTTP request object.
    - `res`: HTTP response object.
    - `config`: Website configuration object.

  - **Respond**: gzip-compressed XML index sitemap.

  - **Behavior**:
    - Iterates through chapters and sitemap configurations to find type-specific sitemaps.
    - Links each type-specific sitemap in the index sitemap.

---

  ### `generateTypeSitemap`
  - **Purpose**: Generates a sitemap for a specific type (e.g., news, images).

  - **Parameters**:
    - `req`: HTTP request object.
    - `res`: HTTP response object.
    - `config`: Website configuration object.

  - **Respond**: gzip-compressed XML sitemap for the requested type.

  - **Behavior**:
    - Filters chapters and pages based on the requested type.
    - Writes URLs to the sitemap with additional metadata (e.g., images, news information).

---

  ### `getPages`
  - **Purpose**: Retrieves pages from GudHub for a specific chapter.

  - **Parameters**:
    - `gudhub`: GudHub API client.
    - `chapter`: Chapter configuration object.

  - **Returns**: An array of page objects, each containing:
    - `slug`: The page URL slug.
    - `title`: The page title (if available).
    - `created_at`: The page creation date (if available).
    - `images`: Associated images (if available).

  - **Behavior**:
    - Filters out deleted or draft pages.
    - Parses fields for slug, title, created date, and images.
    - Applies chapter-specific filters and transformations if defined.