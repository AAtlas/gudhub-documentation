# Bundler.js

## Overview
The `Bundler` class is a utility designed to generate optimized bundles of assets for specific web pages. Unlike a full site bundler that compiles all components for an entire site, this bundler focuses on creating bundles for individual pages, including only the components required for those pages. It is used to optimize assets for rendering with tools like Puppeteer, ensuring performance and minimal resource usage.

---

## Content 
- [Bundler.js](#bundlerjs)
  - [Overview](#overview)
  - [Content](#content)
  - [Methods](#methods)
    - [`bundle`](#bundle)

---

## Methods

  ### `bundle`

  - **Purpose**: Generates an optimized bundle of JavaScript and CSS assets for a specific page by compiling only the components present on that page.

  - **Parameters**:
    - `options`: An object containing:
      - `website`: (string): host of the website.
      - `root`: (string): Root directory path of the website.
      - `components`: (Array of components configs) Array of component configs required for the page.
      - `route`: (object): An route object representing the page route (used as path to save bundle).

  - **Returns**: A promise that resolves when the bundling process completes successfully.

  - **Behavior**:

    1. Fetches the website configuration using getWebsiteConfig to access components list.

    2. Identifies components used on the page by matching options.components (components list from page) against the components_list (all components list of site) from the configuration. Filters out components marked as serverOnly to exclude them from the client-side bundle.
     
    3. Prepares a Webpack configuration to bundle JavaScript files for the required components.

    4. Executes Webpack to compile JavaScript assets into a single bundle.js file.
    Handles errors gracefully, logging issues if encountered.

    5. Identifies and processes CSS files corresponding to the filtered components.
    Adjusts paths for components located in node_modules/@gudhub.
    Reads and combines CSS content into a single bundle.css file.

    6. Saves the compiled JavaScript and CSS bundles to the appropriate cache folder.
    Ensures that all assets are correctly linked to the corresponding page route.

    7. Catches and logs errors encountered during Webpack compilation or file operations.
    Rejects the promise in case of failures, ensuring robust error reporting.
    Completion:

    8. Resolves the promise upon successful bundling, allowing further processing or rendering to proceed.