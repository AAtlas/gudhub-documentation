# Configure

## .env

- `SERVER_PORT`: Port
- `HOST`: host of your web-site ( in dev its your local ip, if its production, then write your site host: example.com )
- `PATH_TO_WEBSITES_FOLDER`: absolute path to the folder which contains your web-sites folders
- `PATH_TO_CACHE_FOLDER`: absolute path to the folder where all cached data (html, assets, data) will be stored
- `MODE`: "development" or "production"