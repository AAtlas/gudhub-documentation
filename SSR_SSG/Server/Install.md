# Install

## Process

1. **Clone**: Clone project to your local folder
2. **install packages**: Enter `npm i` to your terminal
3. **Configure**: Write `.env` file by `.env.example` ( explained in [Configure](./Configure.md) )
4. **Start web-site**: Rename your web-site folder like **HOST:PORT** in server `.env`
5. **Start server**: Do `npm run dev` in server terminal
6. **Open site**: Go to `http://<HOST:PORT>`