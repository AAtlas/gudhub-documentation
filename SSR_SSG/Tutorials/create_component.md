# How to create component

## Component creation requirements

- Every component should be inherited from GHComponent.
- Component should use `super.render()` method.
- As mush as possible html rendering should run on server side.

## Tutorial

1. Create component folder.
2. Create html, scss, json (template of component data that stored in gudhub) and js files (better to name them in one way, for example: "custom-component.html", "custom-component.scss", "custom-component-data.json", "custom-component.js").
3. Add template data to json file to work with:
   ```json
      {
       "items": [
         {
           "number": "X",
           "symbol": "&nbsp years",
           "text": "Pellentesque habitant morbi tristique"
         },
         {
           "number": "98",
           "symbol": "%",
           "text": "Nam pulvinar quis erat"
         },
         {
           "number": "X",
           "symbol": " ",
           "text": "Nulla ac accumsan arcu, eget"
         },
         {
           "number": "10/7",
           "symbol": " ",
           "text": "Nam bibendum mi a erat tincidunt"
         }
       ]
     }
   ```
4. Add base code of component:
   
   ```javascript
   import html from './custom-component.html';
   import './custom-component.scss';
   import jsonTemplate from './custom-component-data.json';

   class CustomComponent extends GHComponent {

       constructor() {
           super();
           // set your component default template (if you doesnt have data for component in gudhub, component willnot find it and decide to paste it (template) by himself, to help you, you'll need only to edit that template)
           super.setDefaultData(jsonTemplate);
       }

       async onServerRender() {
            // get ghId from attributes
           this.ghId = this.getAttribute('data-gh-id') || null;

            // retrieve data from gudhub
           this.json = await super.getGhData(this.ghId);
           this.items = this.json.items;

           super.render(html);
       }

   }
    // define your web-component in window
   window.customElements.define('custom-component', CustomComponent);
   ```
5. Add html layout and scss ([layout principles](../Tutorials/component_layout.md)).
   
   For example, we will use layout of an already made component "service-counter":

   ```html
   <section class="service_counter">
       <div class="container">
           <div class="counters">
               ${
                   items.reduce((acc, item, index) => {
                       return acc + `
                       <div class="counter_item">
                           <div class="counter_top">
                               <span class="counter_num" gh-id="${ghId}.items.${index}.number">44</span>
                               <span class="counter_symbol" gh-id="${ghId}.items.${index}.symbol">+</span>
                           </div>
                           <div class="counter_bottom" gh-id="${ghId}.items.${index}.text">Custom Software Development</div>
                       </div>
                       `
                   }, '')
               }
           </div>
       </div>
   </section>
   ```

   Explanation:
    - `${}` usage: html will be imported in to js like **Template string**, so `${}` can be processed like in js.
    - `JS Context in html`: context of execution your code in `${}` is your component, so you can use your methods and variables in layout like this:
    
      Example : your component has "this.items = [...]"

      ```html
        ${
            items.reduce((acc, item) => {
                return acc + `
                  ...
                  //layout based on your item data
                  ...
                `
            }, '')
        }
      ```
    - `Why using ${index} in gh-id attribute`: as `ghId` is path to property, if array used in path (items are array of objects) we need index to iterate through objects.
  
6. Connect component to your `components-list` of your web-site:
  
    Add object to a list
     ```javascript
       {
         tag: "custom-component",
         src: "/src/assets/js/components/custom-component/custom-components.js"
       }
     ```

    Explanation: why do we need this list of components? To bundle optimized assets of page components, but server [Bundler](../Server/Features/Bundler.md) needs to know where component code and assets are located.

7. Your component is ready to use, lets add it to the page:

    ```html
       <custom-component data-gh-id="test-component"></custom-component>
    ```

    `data-gh-id`: attribute value is key of gudhub json object correspond to this component.