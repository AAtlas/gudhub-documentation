# How to Add a Component to Components-Library

## Content
- [How to Add a Component to Components-Library](#how-to-add-a-component-to-components-library)
  - [Content](#content)
  - [Create Component Files](#create-component-files)
  - [Update General Config](#update-general-config)

## Create Component Files

1. **Create a folder for your component**: 
   - The folder name should match the `tagName` of your component. For example, if your component's tagName is `custom-component`, create a folder named `custom-component`.

2. **Add required files to the folder**:
   - `config.js`: Define the component’s configuration here. The file should export an object with the following structure:
     ```javascript
     export default {
         tagName: 'custom-component',
         path: './src/components/custom-component/custom-component.js',
     };
     ```
   - `custom-component.html`: Create the layout of the component using HTML.
   - `custom-component.js`: Write the JavaScript logic for the component.
   - `custom-component.scss`: Add styles for the component.
   - `custom-component.json`: Provide a template of data from gudhub that the component will use.
   - `custom-component.readme.md`: Document the component’s functionality, usage, and any additional notes.

## Update General Config

1. Open the general configuration file: `./src/config.js`.

2. Export your component’s `config.js` file from general config:
   ```javascript
   export customComponent from './components/custom-component/config.js';
   ```

3. Save the changes.