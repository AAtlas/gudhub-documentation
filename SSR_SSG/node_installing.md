# Node installing

This article explains how to install the node js for using in the development.

## Step 1

Open the terminal on your devise or press Ctrl+Alt+T.

![The opened terminal](ssr-ssg-images/opened-terminal.jpg "Terminal")

## Step 2

Then enter the following command in the opened terminal:

```js
   sudo apt install nodejs
```

![Command to install the Node JS](ssr-ssg-images/install-node-js.jpg "Installing Node JS")

After that check the installation using command:

```js
    node -v
```

![Command for checking the node version](ssr-ssg-images/node-version.jpg "Node version checking")

## Step 3

After that you have to install the NPM. To do that enter the command in terminal:

```js
    sudo apt install npm
```

![Command for installing the npm](ssr-ssg-images/install-npm.jpg "NPM installation")

To check its version use command:

```js
    npm -v
```

![Command for checking the NPM version](ssr-ssg-images/npm-version.jpg "NPM version")
