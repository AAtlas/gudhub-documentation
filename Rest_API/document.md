# Documents

Documents are the JSON arrays of objects and related to NoSQL databases. The document requests allow working with such data, creating a new one, and deleting it:

1. [document/insert-one](#document-insert-one)
2. [document/find-one](#document-find-one)

## document/insert-one

The current request is called to update the document or add a new one if it does not exist.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub_Test/api/new/document/insert-one`

The current request consists of token and the document object.

Name|Type|Description
:---|:---|:---
document|`object`|*contains data about document location*
token|`string`|*allows to get access*

#### **Example:**

```json
  document: {
    "app_id":"28877",
    "item_id":"3118314",
    "element_id":693593,
    "data":"{}"
  }
  token: thog-tayjf-.k-cbeapsebnqawkphhiny-ursyqsn
```

### Response

As a result, it returns the full document object.

```json
{
  "app_id": 28877,
  "data": "{}",
  "element_id": 693593,
  "item_id": 3118314,
  "last_update": 1660053018469,
  "document_id": "62f2661a90089904e853c9b9"
}
```

### Status Code

Name|Description
:---|:---
200 Ok|document is deleted

## document/find-one

This is hte request that allows to find the document. If such a document does not exist, it will be created.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub_Test/api/new/document/find-one`

A query consists of a token and the document itself.

Name|Type|Description
:---|:---|:---
document|`object`|*contains the partial data of the document*
token|`string`|*allows to get the access*

#### **Example:**

```json
  document: {
    "app_id":"28877",
    "item_id":"3118314",
    "element_id":693593
  }
  token: uz-ewes.fmrfjrfiyprcjny_iimoleix_hmusbni
```

### Response

In response, it will return the full document object

```json
{
  "app_id": 28877,
  "data": "{}",
  "element_id": 693593,
  "item_id": 3118314,
  "last_update": 1660053018469,
  "document_id": "62f2661a90089904e853c9b9"
}
```

### Status Code

Name|Description
:---|:---
200 Ok|document is deleted
