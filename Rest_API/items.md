# Item

These requests are aimed at working with items. Since elements are contained in items, deleting item request leads to removing elements and their data too.

There are only three item request:

1. [item/add](#item-add)
2. [item/update](#item-update)
3. [item/delete](#item-delete)

## item/add

Request for adding a new item to the application.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub/api/items/add`

It contains an array of items with added data, application ID, and token.

Name|Type|Description
:---|:---|:---
token|`string`|*using for getting access*
app_id|`object`|*unique application ID*
items|`array`|*array of added data*

***Example:***

```json
      items:[{
         "item_id":0,
         "fields":[]
      }]
      app_id: 3733

      token:
      beyewulqzxnbd.tifrvcgdulrnmovvasdyizwbdjykn
```

### Response

In return will be an array of item objects.

```json
[{
  "item_id":81907,
  "index_number":8,
  "last_update":1502099832000,
  "fields":[{ }]
}]
```

### Status Code

Name|Description
:---|:---
200 Ok|item is added
710|user have no right to add items

## item/update

Request for data updating in the existed item.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub/api/items/update`

It contains an array of updated items, application ID, and token.

Name|Type|Description
:---|:---|:---
token|`string`|*using for getting access*
app_id|`number`|*contains ID of the application whose items will be updated*
items|`array`|*contains items with updated data*

***Example:***

```json
      items:[{
         "item_id":"81905",
         "fields":[{
               "field_id":"42530",
               "field_value":"test"
            }]
      }]
      app_id: 3733


      token:beyewulqzxnbd.tifrvcgdulrnmovvasdyizwbdjykn
```

### Response

As a result, we will see updated file information.

```json
{
 "item_id":81905,
 "index_number":6,
 "last_update":1502099396000,
 "fields":[{
      "field_id":42530,
      "field_value":"test",
      "data_id":467973
   }]
}
```

### Status Code

Name|Description
:---|:---
000 Unupdate|wrong properties (expansion, size, ...)
200 Ok|item is updated
710|user have no right to update items

## item/delete

Request for deleting an item. When we do it all documents in item will be deleted too.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub/api/items/delete`

It contains an array of item IDs and token.

Name|Type|Description
:---|:---|:---
items_ids|`array`|*array of deleted data*

***Example:***

```json
      items_ids: [81904]

      token:beyewulqzxnbd.tifrvcgdulrnmovvasdyizwbdjykn
```

### Response

In response, you will receive all the updated items.

```json
[{
      "fields": [{
            "field_id": 684731, 
            "element_id": 684731, 
            "field_value": "1659365564518", 
            "data_id": 23432835
      }],
      "index_number": 6,
      "item_id": 81904,
      "last_update": 1659508824000
}]
```

### Status Code

Name|Description
:---|:---
200 Ok|item/items with specified ID is deleted
710|user have no right to delete items
