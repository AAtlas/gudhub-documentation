# Introduction Rest API

GudHub`s API is a comfortable tool to integrate applications. It helps you to write applications in any programming language without any problems.

API works by tokens. That means the requests are sent with tokens. That is how the server knows that user tries to send a request. Every token is generated for 10 hours only.

API has its own structure in GudHub. It is divided into:

- authentication
- application
- items
- elements
- documents
- files
- sharing

There are existing different requests for them. They are sent as string-type data.

For the actions in web-services are used API requests. In GudHub there are several groups of such requests:

Name|Description
:---|:---
auth|requests carry out authentification; [details…](auth.md)
app|requests for work with the application; [details…](app.md)
items|requests for working with items; [details…](items.md)
elements|requests for working with elements; [details…](element.md)
documents|requests for working with documents; [details…](document.md)
file|request for working with uploaded files; [details…](file.md)
sharing|request for sharing the application; [details…](sharing.md)
clear_cache|request for cleaning cache; [details…](clear_cache.md)

The request body contains needed server information in a text format instead of JSON.
