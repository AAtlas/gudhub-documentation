# File

GudHub has interesting file generation. Files are saved in a file list. In turn, file ID is saved in an item. When we need to use a file in the application, we calls the method that finds the file in file_list by its ID.

For working with different file, there are existing file requests:

1. [file/upload](#file-upload)
2. [file/update](#file-update)
3. [file/duplicate](#file-duplicate)
4. [file/delete](#file-delete)

## file/upload

Request for loading the image to the application.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|https://gudhub.com/GudHub/file/upload

It contains token and file object that contains all information about the file.

Name|Type|Description
:---|:---|:---
token|`string`|*using for getting access*
file|`object`|*uploaded file object*

***Example:***

```json
    token:.nemdoznk_qmb-qmohhs_-uwdhdzrhevjlhbdole

    file:{
    "format":"base64",
    "source":"...",
    "file_name":"149847809617",
    "extension":"jpg",
    "app_id":"3733",
    "item_id":"81906"
    }
```

### Response

In reply will be file data.

```json
{  
    "file_id":23609,
    "app_id":3733,
    "item_id":81906,
    "file_name":"149847809617",
    "url":"http://gudhub.com/userdata/3733/23609.jpg",
    "extension":"jpg"
}
```

### Status Code

Name|Description
:---|:---
200 Ok|file is downloaded
710|user have no access rights

## file/update

This is a request that allows you to update a file uploaded to GudHub.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|https://gudhub.com/GudHub/file/update

It contains token and file object. The file object contains the main data of the file. If any property has an empty value, the corresponding file property will be overwritten.

>So, if you don't want to change the value of the property, you shouldn't write the property in the query.

Name|Type|Description
:---|:---|:---
token|`string`|*using for getting access*
file|`object`|*uploaded object file*

***Example:***

```json
    file: {
        "format":"html",
        "source":"",
        "file_name":"document",
        "extension":"html",
        "file_id":24404,
        "title": "file name",
        "alt": "test example"
    }

    token: pzubxbfeqxl.vyccbfzwdwourloqhm-_babfozs-xs.
```

### Response

In return we will see updated file data.

```json
{
    "file_id":24404,
    "extension": "html",
    "app_id":4164,
    "item_id":271648,
    "file_name":"document",
    "url":"http://gudhub.com/userdata/4164/24404.html",
    "extension":"html",
    "title": "file name",
    "alt": "test example"
}
```

### Status Code

Name|Description
:---|:---
000 Unupdate|wrong properties (expansion, size, ...)
200 Ok|file is updated
710|user have no access rights

## file/duplicate

Request for creating a duplicate of the current file.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub_Test/api/new/file/duplicate`

It contains token, source object that in turn contains file ID and destination object.

Name|Type|Description
:---|:---|:---
token|`string`|*using for getting access*
source|`number`|*shows where the file duplicates from*
destination|`object`|*shows where the file duplicates to*

***Example:***

```json
    token:.nemdoznk_qmb-qmohhs_-uwdhdzrhevjlhbdole

    source: {
        "file_id":23609
    }
    destination: {
        "app_id":3733,
        "item_id":81906,
        "field_id":42637
    }
```

### Response

In response will be shown object of a file copy.

```json
{  
   "file_id":23700,
   "app_id":3733,
   "item_id":81906,
   "file_name":"149847809617",
   "url":"http://gudhub.com/userdata/3733/23609.jpg",
   "extension":"jpg"
}
```

### Status Code

Name|Description
:---|:---
000 Unupdate|wrong properties (expansion, size, ...)
200 Ok|file duplicated
710|user have no access rights

## file/delete

Request for file deleting, but for real it just gets trash status true.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`GET`|`Query String Parameters`|https://gudhub.com/GudHub_Test/file/delete

It contains token and file ID.

Name|Type|Description
:---|:---|:---
token|`string`|*using for getting access*
file_id|`number`|*ID given to the uploaded file*

***Example:***

```json
    token:.nemdoznk_qmb-qmohhs_-uwdhdzrhevjlhbdole

    file_id:23609
```

### Response

In response will be file data.

```json
{
    "app_id": 12345,
    "extension": "ipeg",
    "item_id": 798231,
    "file_name": "download",
    "item_id": 8782319,
    "last_update": 2837821,
    "url": "https://gudhub.com/userdata/12345/798231.jpeg"
}
```

### Status Code

Name|Description
:---|:---
200 Ok|file is deleted
710|user have no access rights
