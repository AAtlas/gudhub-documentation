# App Rest API

App requests are responsible for operations with the application. They allow you to create, update, and delete the application.

There are 4 requests in total:

1. [applist/get](#applist-get)
2. [app/create](#app-create)
3. [app/get](#app-get)
4. [app/update](#app-update)
5. [app/delete](#app-delete)
6. [app/users-get](#app-users-get)

## applist/get

The current request is created to get a list of applications to which the user has access.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`GET`|`Query String Parameters`|`https://gudhub.com/GudHub_Test/api/applist/get`

In order to call this request, it is enough to pass the token of the required user to it.

Property|Type|Description
:----|:----|:----
token|`string`|*used for getting access*

***Example:***

```json
token: bfpbqed_z_tgqb.fbdsgej.kyhfhkwlvopafjc_nfl
```

### Response

In response, you will get an object with an array **apps_list**, which contains all user applications.

```json
{
   "apps_list": [{
      "app_id": 3765,
      "app_name": "Team",
      "chunks": [],
      "field_list": [],
      "file_list": [],
      "group_id": 38,
      "icon": {...},
      "items_list": [],
      "keys_list": [],
      "last_update": 1665762169086,
      "permission": 1,
      "priority": 0,
      "privacy": 0,
      "sharing_type": 1,
      "show": true,
      "trash": false,
      "view_init": 188643,
      "views_list": []
   },{
      "app_id": 3766,
      "app_name": "Tasks",
      "chunks": [],
      "field_list": [],
      "file_list": [],
      "group_id": 39,
      "icon": {...},
      "items_list": [],
      "keys_list": [],
      "last_update": 1665389958082,
      "permission": 2,
      "priority": 0,
      "privacy": 0,
      "sharing_type": 1,
      "show": false,
      "trash": false,
      "view_init": 188647,
      "views_list": []
   }]
}
```

### Status Code

Name|Description
:---|:---
200 Ok|applist successfully received
710|user does not have rights to get applist

## app/create

Request for creating a new application.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Query String Parameters`|`https://gudhub.com/GudHub_Test/api/app/create`

It contains a user token and app object that in turn contains information about the application in a text format.

Property|Type|Description
:----|:----|:----
app|`object`|*application object, which is created*
token|`string`|*using for getting access*

***Example:***

```json
   token:opkqqgo_lbvugkfu_cibljwozbsxishsentbyhrr

   app:{
      "app_name":"Template New",
      "icon":{  
         "icon_id":"box",
         "icon_color":"ffffff",
         "gradient_up":"00CCF8",
         "gradient_down":"00F8B2"
      },
      "views_list":[...],
      "view_list":[...],
      "items_list":[...],
      "field_list":[...] 
   }
```

### Response

In result, you will get created application object.

```json
{  
   "app_id":3751,
   "app_name":"Test views",
   "icon":{  
      "id":3689,
      "icon_id":"box",
      "icon_color":"ffffff",
      "gradient_up":"00CCF8",
       "gradient_down":"00F8B2"
   },
   "group_id":3761,
   "trash":false,
   "last_update":1502266215410,
   "view_init":6876,
   "privacy":1,
   "sharing_type": 1,
   "views_list":[...],
   "view_list":[...],
   "items_list":[...],
   "field_list":[...],
   "file_list":[]
}
```

### Status Code

Name|Description
:---|:---
200 Ok|application is created
710|user does not have rights to create application

## app/get

Request for updating and saving changes of the application and its data.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`GET`|`Query String Parameters`|`https://gudhub.com/GudHub_Test/api/app/get`

It contains user token and app object.

Property|Type|Description
:----|:----|:----
app_id|`object`|*application object, which is created*
token|`string`|*using for getting access*

***Example:***

```json
   token:opkqqgo_lbvugkfu_cibljwozbsxishsentbyhrr

   app_id:3751
```

### Response

In reply will be the existing application object.

```json
{  
   "app_id":3751,
   "app_name":"Test views",
   "icon":{  
      "id":3689,
      "icon_id":"box",
      "icon_color":"ffffff",
      "gradient_up":"00CCF8",
      "gradient_down":"00F8B2"
   },
   "group_id":3761,
   "trash":false,
   "last_update":1502266215410,
   "view_init":6876,
   "privacy":1,
   "sharing_type": 1,
   "views_list":[...],
   "view_list":[...],
   "items_list":[...],
   "field_list":[...],
   "file_list":[],
   "keys_list":[
      {
      "key_id": 32,
      "keychain_id": 123,
      "parent_key": 125,
      "fields":[{...}],
      "filter_list": [{...}],
      "rules": 1111
   }]
}
```

### Status Code

Name|Description
:---|:---
200 Ok|application received
710|user does not have rights to preview application

## app/update

Request for updating the application. That means it gives the possibility to update application data. The current request updates only those properties that have been passed. All others remain unchanged.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub/api/app/update`

It contains in the body user token and app object. In an application object, it passes only those properties that can be updated. These are at least two properties: the app ID and any other property that can be updated.

>Note that if you pass an **empty array** of type field_list, you **erase all the data** in this array.

Property|Type|Description
:----|:----|:----
app|`object`|*application object, which is created*
token|`string`|*using for getting access*

***Example:***

An example of a request with the **minimum set of properties** in an application object.

```json
   token:opkqqgo_lbvugkfu_cibljwozbsxishsentbyhrr

   app:{

      "app_id":3751,
      "app_name":"Test views"
   }
```

An example request with **all the properties available for updating** in the application object.

```json
   token:opkqqgo_lbvugkfu_cibljwozbsxishsentbyhrr

   app:{

      "app_id":3751,
      "app_name":"Test views",
      "icon":{ 
         "icon_id":"box",
         "icon_color":"ffffff",
         "gradient_up":"00CCF8",
         "gradient_down":"00F8B2"
      },
      "trash":false,
      "view_init":6876,
      "privacy":1,
      "sharing_type": 1,
      "views_list":[...],
      "view_list":[...],
      "field_list":[...]
   }
```

### Response

As a result it returns updated application object.

```json
{  
   "app_id":3751,
   "app_name":"Test",
   "icon":{  
      "id":3689,
      "icon_id":"box",
      "icon_color":"ffffff",
      "gradient_up":"00CCF8",
      "gradient_down":"00F8B2"
   },
   "group_id":3761,
   "trash":false,
   "last_update":1502266215410,
   "view_init":6876,
   "privacy":1,
   "sharing_type": 1,
   "views_list":[...],
   "view_list":[...],
   "items_list":[...],
   "field_list":[...],
   "file_list":[]
}
```

### Status Code

Name|Description
:---|:---
200 Ok|application update
710|user does not have rights to update application

## app/delete

Request for deleting an application. In fact, this data cannot be deleted, it just **gets status trash: true**.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub_Test/api/app/delete`

The request contains the user token and app object.

Property|Type|Description
:----|:----|:----
app|`object`|*application object, which is created*
token|`string`|*using for getting access*

***Example:***

```json
   token:opkqqgo_lbvugkfu_cibljwozbsxishsentbyhrr

   app_id:3751
```

### Response

In reply will be the deleted application.

```json
{  
   "app_id":3751,
   "app_name":"Test",
   "icon":{  
      "id":3689,
      "icon_id":"box",
      "icon_color":"ffffff",
      "gradient_up":"00CCF8",
      "gradient_down":"00F8B2"
   },
   "group_id":3761,
   "trash":false,
   "last_update":1502266215410,
   "view_init":6876,
   "privacy":1,
   "sharing_type": 1,
   "views_list":[...],
   "view_list":[...],
   "items_list":[...],
   "field_list":[...],
   "file_list":[]
}
```

### Status Code

Name|Description
:---|:---
200 Ok|application is removed
710|user does not have rights to delete application

## app/users-get

The purpose of this request is to get a list of the users who are allowed to have access to a specific program. It also allows you to see what permissions users have and how they were granted access to the program.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`GET`|`Query String Parameters`|`https://gudhub.com/GudHub_Temp/api/app/users-get`

The token and ID of the application whose users you want to receive must be passed in this request.

Property|Type|Description
:----|:----|:----
app_id|`object`|*ID of the application, users with access to which will be received*
token|`string`|*using for getting access*

***Example:***

```json
   token:opkqqgo_lbvugkfu_cibljwozbsxishsentbyhrr

   app_id:3751
```

### Response

In response, you will receive an array of user objects.

```json
[{
   "app_id": 28877,
   "avatar_128": "https://gudhub.com/avatars/123.jpg",
   "avatar_512": "https://gudhub.com/avatars/345.jpg",
   "fullname": "John Dow",
   "sharing_permission": 2,
   "user_id": 1578,
   "sharing_type": 2
}]
```

### Status Code

Name|Description
:---|:---
200 Ok|request is success
