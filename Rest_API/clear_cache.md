# Clear Cache

This is the only request that the user does not have access to. As you may have gathered, it clears the application cache.

## new/app/clearCache

This is the only request for clearing the cache.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`GET`|`Query String Parameters`|`https://gudhub.com/GudHub_Temp/api/new/app/clearCache`

It contains token and application ID.

Name|Type|Description
:---|:---|:---
app|`object`|*application object, which is created*
token|`string`|*using for getting access*

***Example:***

```json
token:midj_xxlu-x-.odhkxrlh_cokrlroe_cyhymgwgi
app_id:3733
```

### Response

In the result will be a message about successful cleaning.

```json
Cache for application with id=3733 is cleared

https://gudhub.com/GudHub_Temp/api/new/app/clearCache?app_id=30&token=kaqqvbddvebwcvjkupdovnpolpbdhjs-pacr-.mwje
```

### Status Code

Name|Description
:---|:---
200 Ok|cache is cleaned
