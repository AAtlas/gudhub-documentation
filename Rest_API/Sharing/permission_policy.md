# Permission Policy

GudHub has a specific access system. There are different type of permission that are used in different cases. Some of them intersect and have certain priorities in use.

So, there are two classes of permissions:

- [Application permission](#app-sharing)
- [Group permission](#group-sharing)

The different between them is that they provide access to different entities. Their purpose can be understood from their names.

## App Sharing

The **App Sharing** is responsible for the access to the applications. Permissions of this class determine what the user can do with the application:

Value|Meaning|Description
:---|:---|:---
0 |Block|user have no access to the application
1 |Read|allows user to only to read the data in the app
2 |Write|allows user to change data in the application
3 |Admin|allows user to update the application, share it and add to groups
4 |Owner|user is owner of the application

There are two ways to get access to the application:

- Through the group
- Through personal sharing

The access can be provided simultaneously through group and personal sharing.

>The highest priority belongs to the **personal sharing**.

This means that if a **user in a group** has been granted **Admin permission** to a particular application, but through **personal sharing**, he or she **has Read-only permission** to this application, the ***user will not be able to edit it***.

The sharing through the group allows you to share the app with a big number of people at a time. In turn, personal sharing is more beneficial for a small group or to give special permission to some person in the group.

There is an extra property that allows you to understand which type of sharing is used. It is called [Sharing Type](#sharing-type).

>You can read more about sharing apps through the personal sharing [here](sharing.md) and about sharing through a group [here](./group_sharing/group_overview.md).

### Sharing Type

The current property shows how the application was accessed. This means that each property value means a specific type of access.

Value|Meaning|Message
:---|:---|:---
0|Public App|the application is public
1|Shared by App Sharing|app sharing is used to share an app to the current user
2|Shared by Group|the application is shared with the current user through the group
3|Shared both by App Sharing and Group|both group sharing and shared application sharing provide access to the application for the current user
4|Owner|means that the current user is a creator of the application

Since the application can be shared in different ways, and they can be mixed, the **sharing type will differ in different cases**:

`Case`|Owner|App Sharing|Group|Public|***`Sharing Type`***
:--|:--|:--|:--|:--|:--
**Case 1**|X||||***`4`***
**Case 2**||X|||***`1`***
**Case 3**|||X||***`2`***
**Case 4**||||X|***`0`***
**Case 5**||X|X||***`3`***
**Case 6**|X||X||***`4`***
**Case 7**|X|||X|***`4`***
**Case 8**||X||X|***`1`***
**Case 9**|||X|X|***`2`***
**Case 10**||X|X|X|***`3`***
**Case 11**|X||X|X|***`4`***

This table shows all available cases of permission usage. Using it, you can visually see what the share type will be in a particular situation. For example, despite the fact that an **app is public**, if it is **shared using app sharing**, the s**haring type is 1**.

## Group Sharing

The **Group Sharing** allows you to configure access permissions to groups. Depending on this permission, the user can access programs in the group, add and remove them, or even change the rights of users in the group. What capabilities the user will have depends on the permission granted:

Value|Meaning|Message
:---|:---|:---
0|blocked|user cannot access the group; the group will not be displayed in this user`s group list
1|member|user that can read and write data in a certain group of applications
2|admin|user who has the rights to add, delete and update applications in the group
3|owner|default permission for creator of the group; owner can all what can admin

>More details about group sharing you can find [here](./group_sharing/group_overview.md).
