# User Sharing

The sharing requests allows not only share the application with other users. Due to them you can literally manage accesses to the application. Each user can have one of the [permissions](#description-of-sharing_permission-value) to access the application.

The sharing request allows the user to share the app, assign access permission and invite people to GudHub:

1. [sharing/add](#sharing-add)
2. [sharing/get-app-users](#sharing-get-app-users)
3. [sharing/update](#sharing-update)
4. [sharing/delete](#sharing-delete)
5. [invitation/add](#invitation-add)

## Description of sharing_permission Value

Value|Meaning|Description
:---|:---|:---
0 |Block|ban on having any rights to the certain application(it will not be displayed in this user`s app_list)
1 |Read|right only to read data of a certain application
2 |Write|right to read and write in a certain application
3 |Admin|right to add, delete, and update fields in application
4 |Owner|default permission for application creating; is given to user that create this application; can all what can admin, delete and share application

## sharing/add

Due to this request, you can expand an application with other users. It contains token, user ID, application ID, and a number of sharing permission.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub/sharing/add`

It contains token, user ID, application ID, and a number of sharing permission.

Property|Type|Description
:---|:---|:---
token|`string`|*using for getting access*
app_id|`object`|*application object, which is created*
user_id|`number`|*personal user ID*
sharing_permision|`number`|*rights to application which are  granted to user*

#### **Example:**

```json
        app_id: 178
        sharing_permission: 0
        token: bbcvqhzgr.swms-_-.oj_vdia_bert.gkanyfww_cg
        user_id: 28
```

### Response

In response will be data about user and his permission.

```json
{
        "user_id":28,
        "app_id":178,
        "permission":0
}
```

### Status Code

Name|Description
:---|:---
200|user has accessed the application
710|user have no rights for sharing

## sharing/get-app-users

The current request is called to get the list of users who has permission to the application. It is automatically called when the user clicks [Sharing](../Gh_Elements/sharing_element.md).

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub_Test/sharing/get-app-users`

This request send the token and ID of the application.

Property|Type|Description
:---|:---|:---
app_id|`number`|*The ID of the application from which the users will be gotten*
token|`string`|*allows to get access*

#### **Example:**

```json
app_id: 28877
token: thog-tayjf-.k-cbeapsebnqawkphhiny-ursyqsn
```

### Response

It returns an array of all users that have access rights to this application.

```json
[{
        "app_id": 28877,
        "avatar_128": "https://gudhub.com/avatars/123.jpg",
        "avatar_512": "https://gudhub.com/avatars/345.jpg",
        "fullname": "John Dow",
        "sharing_permission": 2,
        "user_id": 1578
}]
```

### Status Code

Name|Description
:---|:---
200|user is registered
400 Bad Request|incorrect request syntax

## sharing/update

This is the request that allows to update the user permission.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub/sharing/update`

This request contains all the same token, user ID, application ID, and a number of the new user permission.

Property|Type|Description
:---|:---|:---
app_id|`number`|*contains ID of the application*
sharing_permission|`number`|*contains number of the permission*
token|`string`|*contains working token*
user_id|`number`|*contains ID of user whose access will be update*

#### **Example:**

```json
        app_id: 28753
        sharing_permission: 3
        token: lrxvzikg-mij-_szdgipxtjeybd.-v._amkdbdsxjk
        user_id: 1571
```

### Response

In response will be updated data about user and his new permission.

```json
{
        "user_id":1571,
        "app_id":28753,
        "permission":3
}
```

### Status Code

Name|Description
:---|:---
200|user has accessed the application
710|user have no rights for updating

## sharing/delete

The current request is invoked when the [owner or administrator](#description-of-sharing-permission-value) of an application deletes a user from that application. It is not the same as the [blocked permission](#description-of-sharing-permission-value) that. The deleted user can open that app if it is public. If permission is [Blocked](#description-of-sharing-permission-value), the blocked user will not be able to open this application anyway.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub/sharing/delete`

The request sends the token, the ID of the user to be deleted, and the ID of the application from which the user will be deleted.

Property|Type|Description
:---|:---|:---
app_id|`number`|*ID of the application in which the operation is performed*
token|`string`|*used to get the access*
user_id|`number`|*ID of the user that will be deleted*

#### **Example:**

```json
app_id: 22208
token: bgikl-szbeaqivhj.fdqu/trwbwqbgh..ffjqwb
user_id: 1534
```

### Response

The response will be a remote user object.

```json
{
        "app_id": 22208,
        "permission": 1,
        "user_id": 1534
}
```

### Status Code

Name|Description
:---|:---
200|user has accessed the application
710|user have no rights for deleting other users

## invitation/add

This is the request that is sent to invite the user to the GudHub. With this invitation, the user sends access to some selected applications. The user sending invitations can give any permission of the shared application to the invitee.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub_Test/api/invitation/add`

The current request sends an array with invitees emails, an array of app objects, and token.

Property|Type|Description
:---|:---|:---
guests_emails|`array`|*contains all emails addresses to which the invitations will be sent*
apps|`array`|*contains ID and permission of the application*
token|`string`|*allows to get access to the apps*

#### **Example:**

```json
guests_emails: ["john@dow.com","johana@dow.com"]
apps: [{"app_id":27781,"permission":1},{"app_id":31080,"permission":2}]
token: jezyusdfmbzvqn-un._fk.ydnvamdoph.ybbdameo
```

### Response

In response, we will get an array with objects containing the email to which the invitation will be sent, the application ID and the permission given to the invitee.

```json
[{
        "guest_email": "john@dow.com", 
        "app_id": 27781, 
        "permission": 1
},{
        "guest_email": "johana@dow.com", 
        "app_id": 27781, 
        "permission": 1
},{
        "guest_email": "john@dow.com", 
        "app_id": 31080, 
        "permission": 2
},{
        "guest_email": "johana@dow.com",
        "app_id": 31080, 
        "permission": 2
}]
```

### Status Code

Name|Description
:---|:---
200|user has accessed the application
