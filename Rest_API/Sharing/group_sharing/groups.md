# Managing Sharing Groups

Each of group has its own name and generated ID.

>Maximum number of signs in the name of the group is *40*.

Requests to work directly with the group:

- [sharing-group/create-group](#sharing-group-create-group)
- [sharing-group/update-group](#sharing-group-update-group)
- [sharing-group/delete-group](#sharing-group-delete-group)

## sharing-group/create-group

At first, you have to create the group. The current request is used for this.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub/api/sharing-group/create-group`

It transfers user token and name of the group.

Property|Type|Description
:---|:---|:---
token|`string`|*using to getting the access to the group*
group_name|`string`|*name of the group that will be created*

#### **Example Request:**

```json
    token: gvydsjtynywadogvejv_bawqlztqfjabdyuhrsnref
    group_name: Test
```

### Response

In response, an object with the generated ID and the entered name of the new group is returned.

```json
{
    "group_id": 5,
    "group_name": "Test"
}
```

### Status Code

Status Code|Message
:---|:---
200|group is created
600|data too long for column 'group_name' at row 1
601|incorrect string value: '\xF0\x9F\x98\x83' for column 'group_name' at row 1

## sharing-group/update-group

When a user wants to change the name of the group, an update request is sent.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub/api/sharing-group/update-group`

This request contains the user token, ID and new name of the group to be updated.

Property|Type|Description
:---|:---|:---
token|`string`|*using to getting the access to the group*
group_name|`string`|*new name of the group that will be updated*
group_id|`number`|*ID of the group that will be updated*

#### **Example Request:**

```json
    token: gvydsjtynywadogvejv_bawqlztqfjabdyuhrsnref
    group_id: 5
    group_name: Updated group name
```

### Response

This request returns the name and ID of the group just updated.

```json
{
    "group_id": 5,
    "group_name": "Updated group name"
}
```

### Status Code

Status Code|Message
:---|:---
200|group is updated
600|incorrect string value: '\xF0\x9F\x98\x83' for column 'group_name' at row 1
601|data too long for column 'group_name' at row 1
710|access is not allowed

## sharing-group/delete-group

The current request is to delete the existed group.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub/api/sharing-group/delete-group`

The request consists of the user token and the identifier of the group to be deleted.

Property|Type|Description
:---|:---|:---
token|`string`|*using to getting the access to the group*
group_id|`number`|*ID of the group that will be deleted*

#### **Example Request:**

```json
    token: gvydsjtynywadogvejv_bawqlztqfjabdyuhrsnref
    group_id: 5
```

### Response

In response, the user will get the ID and name of the just deleted group.

```json
{
    "group_id": 5,
    "group_name": "Updated group name"
}
```

### Status Code

Status Code|Message
:---|:---
200|group is deleted
600|attempt to create delete event with null entity
710|access is not allowed
