# Group Sharing Overview

*GudHub* provides a lot of functions to make connections between application. Such networks usually have many applications with different permissions. To share one application, you must share all others. With [Sharing](../sharing.md) the user can share and set permissions for only one application at a time. In the case of a large number of applications, it is too difficult to control all the permissions.

**Group Sharing** is created to resolve that problem. This feature allows you to group different applications together and set the same permission for all of them. And all this in just a couple of clicks. It saves a lot of time and effort.

Three groups of requests are used for the work with groups:

- [Managing Sharing Groups](groups.md)
- [Managing Users In Groups](user_group.md)
- [Managing Apps In Groups](app_group.md)

## Work Logic

*Group Sharing* has its own work logic. The most important thing is that permission to the certain application is provided in accordance with a certain hierarchy. The higher the permission, the higher the priority of access.

This means that if you have administrator rights for an application, but you are just a member of the group (where this application is located), you still have administrator access to that application.

In addition, the group has some rules related to [apps](app_group.md) and [users](user_group.md).

## Full List Of Requests

- [sharing-group/create-group](groups.md)
- [sharing-group/update-group](groups.md)
- [sharing-group/delete-group](groups.md)
- [sharing-group/add-user-to-group](user_group.md)
- [sharing-group/get-users-by-group](user_group.md)
- [sharing-group/update-user-in-group](user_group.md)
- [sharing-group/delete-user-from-group](user_group.md)
- [sharing-group/get-groups-by-user](user_group.md)
- [sharing-group/add-app-to-group](app_group.md)
- [sharing-group/get-apps-by-group](app_group.md)
- [sharing-group/delete-app-from-group](app_group.md)
- [sharing-group/get-groups-by-app](app_group.md)
