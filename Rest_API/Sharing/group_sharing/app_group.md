# Managing Apps In Groups

Like group users, applications have separate requests to work in groups. Each of them is described below. You can also find the [rules](#apps-rules) for applications here.

>Each of application in group has its own [permission](#app-permissions).

The app groups have a few requests to work with applications in it:

- [sharing-group/add-app-to-group](#sharing-group-add-app-to-group)
- [sharing-group/get-apps-by-group](#sharing-group-get-apps-by-group)
- [sharing-group/update-app-in-group](#sharing-group-update-app-in-group)
- [sharing-group/delete-app-from-group](#sharing-group-delete-app-from-group)
- [sharing-group/get-groups-by-app](#sharing-group-get-groups-by-app)

## Apps Rules

There is an important nuance:

- Group Owner/Admin can add new application to the group (in case when that user is the application Owner/Admin).

## App Permissions

Value|Meaning|Message
:---|:---|:---
0 |Block|ban on having any rights to the certain application(it will not be displayed in this user`s app_list)
1 |Read|right only to read data of a certain application
2 |Write|right to read and write in a certain application
3 |Admin|right to add, delete, and update fields, items, and the application
4 |Owner|default permission for application creator; is given to user that create this application; can all what can admin

## sharing-group/add-app-to-group

This request is invoked to add a new application to the group.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub/api/sharing-group/add-app-to-group`

This request consists of the token, the group and application IDs, and the permission that is given to the application.

Property|Type|Description
:---|:---|:---
token|`string`|*using to getting the access to the group*
group_id|`number`|*ID of the group to which the application will be added*
app_id|`number`|*ID of application that will be added to the certain group*
app_permission|`number`|*ID of the permission of application in group*

#### **Example Request:**

```json
    token: gvydsjtynywadogvejv_bawqlztqfjabdyuhrsnref
    group_id: 4
    app_id: 45
    app_permission: 1
```

### Response

The response contains the group and application IDs, and the specified permission for the new application.

```json
{
    "app_id": 56,
    "group_id": 4,
    "app_permission": 1
}
```

### Status Code

Status Code|Message
:---|:---
200|response
600|attempt to create delete event with null entity
710|access is not allowed

## sharing-group/get-apps-by-group

The current request allows to get a list of applications which belong to gr
This chapter describes each of the queries related to a user who belongs to a group.  of users in the group has his/her own [permission](#user-permissions).
to the certain group.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub/api/sharing-group/get-apps-by-group`

It contains the token and the group ID.

Property|Type|Description
:---|:---|:---
token|`string`|*using to getting the access to the group*
group_id|`number`|*ID of the group from which the list of applications will be obtained*

#### **Example Request:**

```json
    token: gvydsjtynywadogvejv_bawqlztqfjabdyuhrsnref
    group_id: 5
```

### Response

In response, you will get an array of objects which contains data about applications: group and application IDs, and application permission.

```json
[{
    "app_id": 11,
    "group_id": 4,
    "app_permission": 3
},{
    "app_id": 21,
    "group_id": 4,
    "app_permission": 1
},{
    "app_id": 31,
    "group_id": 4,
    "app_permission": 1
},{
    "app_id": 41,
    "group_id": 4,
    "app_permission": 4
}]
```

### Status Code

Status Code|Message
:---|:---
200|response
600|attempt to create delete event with null entity
710|access is not allowed

## sharing-group/update-app-in-group

This is the request that allows to change the permission of the application.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub/api/sharing-group/update-app-in-group`

The request consists of token, ID of the application that will be updated, ID of the group to which the application belong and its new permission.

Property|Type|Description
:---|:---|:---
token|`string`|*using to getting the access to the group*
group_id|`number`|*ID of the group where the application permission will be changed*
app_id|`number`|*ID of application whose permission will be changed*
app_permission|`number`|*new ID of the permission of the application*

#### **Example Request:**

```json
    token: gvydsjtynywadogvejv_bawqlztqfjabdyuhrsnref
    group_id: 4
    app_id: 45
    app_permission: 2
```

### Response

In response, we get an object of the updated application, but with a new permission:

```json
{
    "app_id": 56,
    "group_id": 4,
    "app_permission": 2
}
```

### Status Code

Status Code|Message
:---|:---
200|response
600|attempt to create delete event with null entity
710|access is not allowed

## sharing-group/delete-app-from-group

This request allows to delete the application from group.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub/api/sharing-group/delete-app-from-group`

The current request contains the token, group and application IDs, and the app permission.

Property|Type|Description
:---|:---|:---
token|`string`|*using to getting the access to the group*
group_id|`number`|*ID of the group from which the application will be deleted*
app_id|`number`|*ID of the application that will be deleted from group*
app_permission|`number`|*ID of the permission of application in group*

#### **Example Request:**

```json
    token: gvydsjtynywadogvejv_bawqlztqfjabdyuhrsnref
    group_id: 4
    app_id: 45
    app_permission: 2
```

### Response

In response, you will get the data of the deleted application: group ID, application ID, and its permission.

```json
{
    "app_id": 45,
    "group_id": 4,
    "app_permission": 2
}
```

### Status Code

Status Code|Message
:---|:---
200|response
600|attempt to create delete event with null entity
710|access is not allowed

## sharing-group/get-groups-by-app

The current request is called to get the list of groups to which the requested application belongs.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub/api/sharing-group/get-groups-by-app`

The request contains only the token and the application ID.

Property|Type|Description
:---|:---|:---
token|`string`|*using to getting the access to the group*
app_id|`number`|*ID of the application*

#### **Example Request:**

```json
    token: gvydsjtynywadogvejv_bawqlztqfjabdyuhrsnref
    app_id: 34
```

### Response

The array of group objects will be returned. Each of object contains the ID and the name of the certain group.

```json
[{
    "group_id": 1,
    "group_name": "Management",
    "app_permission": 2
},{
    "group_id": 2,
    "group_name": "Developing",
    "app_permission": 3
},{
    "group_id": 3,
    "group_name": "Sales",
    "app_permission": 0
}]
```

### Status Code

Status Code|Message
:---|:---
200|response
600|attempt to create delete event with null entity
710|access is not allowed
