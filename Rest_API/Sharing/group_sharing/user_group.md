# Managing Users In Groups

This chapter describes each of the requests related to a user who belongs to a group. They all have a certain logic of operation and a few important [rules](#users-rules).

>Each of users in the group has his/her own [permission](#user-permissions).

Each of the groups uses these requests to work with users:

- [sharing-group/add-user-to-group](#sharing-group-add-user-to-group)
- [sharing-group/get-users-by-group](#sharing-group-get-users-by-group)
- [sharing-group/update-user-in-group](#sharing-group-update-user-in-group)
- [sharing-group/delete-user-from-group](#sharing-group-delete-user-from-group)
- [sharing-group/get-groups-by-user](#sharing-group-get-groups-by-user)

## Users Rules

There are two important rules for users:

- Group Owner/Admin can add User to group and set his/her permission.
- User can delete himself from group. In other words, user can leave the group.

## User Permissions

Value|Meaning|Message
:---|:---|:---
0|blocked|user who is not allowed to have any rights for a certain group of applications(the group will not be displayed in this user`s app_list)
1|member|user that can read and write data in a certain group of applications
2|admin|user who has the rights to add, delete and update applications in the group
3|owner|default permission for creator of the group; owner can all what can admin

## sharing-group/add-user-to-group

This request allows to add a new user to the group.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub/api/sharing-group/add-user-to-group`

This request sent token, user and group IDs, and user permission to the group.

Property|Type|Description
:---|:---|:---
token|`string`|*using to getting the access to the group*
group_id|`number`|*ID of the destination group*
user_id|`number`|*ID of the user that will be added*
group_permission|`number`|*ID of the permission of the user*

#### **Example Request:**

```json
    token: gvydsjtynywadogvejv_bawqlztqfjabdyuhrsnref 
    group_id: 3
    user_id: 23456
    group_permission: 2
```

### Response

In response, you will get the ID of the added user and his/her permission to the group.

```json
{
    "user_id": 74872,
    "group_permission": 2,
    "group_id": 3
}
```

### Status Code

Status Code|Message
:---|:---
200|response
600|attempt to create delete event with null entity
601|such permission does not exist
710|access is not allowed

## sharing-group/get-users-by-group

This chapter describes each of the queries related to a user who belongs to a group.  get a list of users who belong to group.

>Each of users in the group has his/her own [permission](#user-permissions).
to a certain group.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub/api/sharing-group/get-users-by-group`

It only accepts the token and the group ID.

Property|Type|Description
:---|:---|:---
token|`string`|*using to getting the access to the group*
group_id|`number`|*ID of the group from which you want to pull the list of users*

#### **Example Request:**

```json
    token: gvydsjtynywadogvejv_bawqlztqfjabdyuhrsnref
    group_id: 5
```

### Response

The current response request returns an array of user objects. Each of them contains the group ID, the user ID, and his/her rights for that group.

```json
[{
    "user_id": 24381,
    "group_permission": 4,
    "group_id": 5
},{
    "user_id": 24382,
    "group_permission": 1,
    "group_id": 5
},{
    "user_id": 24383,
    "group_permission": 1,
    "group_id": 5
},{
    "user_id": 24384,
    "group_permission": 0,
    "group_id": 5
}]
```

### Status Code

Status Code|Message
:---|:---
200|response
600|attempt to create delete event with null entity
710|access is not allowed

## sharing-group/update-user-in-group

The current request is called to change the user permission to the group.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub/api/sharing-group/update-user-in-group`

The request contains group and user IDs, and a new permission.

Property|Type|Description
:---|:---|:---
token|`string`|*using to getting the access to the group*
group_id|`number`|*ID of the group in which the permission of the certain user will be updated*
user_id|`number`|*ID of the user whose permission will be updated*
group_permission|`number`|*ID of permission that is granted to the user*

#### **Example Request:**

```json
    token: gvydsjtynywadogvejv_bawqlztqfjabdyuhrsnref
    group_id: 4
    user_id: 32424
    group_permission: 0
```

### Response

It returns the ID of the group and the user, and, accordingly, the new permission of the user in the group.

```json
{
    "group_id": 4,
    "user_id": 23456,
    "group_permission": 0
}
```

### Status Code

Status Code|Message
:---|:---
200|response
600|attempt to create delete event with null entity
601|such permission does not exist
710|access is not allowed

## sharing-group/delete-user-from-group

To delete other users and leave the group, the user deletion request is used.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub/api/sharing-group/delete-user-from-group`

This request sends the token, group and user IDs.

Property|Type|Description
:---|:---|:---
token|`string`|*using to getting the access to the group*
group_id|`number`|*ID of the group from which the user will be deleted*
user_id|`number`|*ID of the user that will be deleted*

#### **Example Request:**

```json
    token: gvydsjtynywadogvejv_bawqlztqfjabdyuhrsnref
    group_id: 6
    user_id: 52738
```

### Response

In response, you will get the data about deleted user: ID of the group, his/her user ID and permission.

```json
{
    "user_id": 34453,
    "group_permission": 2,
    "group_id": 6
}
```

### Status Code

Status Code|Message
:---|:---
200|response
600|attempt to create delete event with null entity
710|access is not allowed

## sharing-group/get-groups-by-user

This request allows to get the full list of the groups the certain user belongs to.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub/api/sharing-group/get-groups-by-user`

It contains only the token and user ID.

Property|Type|Description
:---|:---|:---
token|`string`|*using to getting the access to the group*
user_id|`number`|*ID of the user that is used for searching*

#### **Example Request:**

```json
    token: gvydsjtynywadogvejv_bawqlztqfjabdyuhrsnref
    user_id: 15432
```

### Response

In response, you get an array of groups objects to which the user belongs.

```json
[{
    "group_id": 1,
    "group_name": "Management",
    "user_id": 15432,
    "group_permission": 1
},{
    "group_id": 2,
    "group_name": "Developing",
    "user_id": 15432,
    "group_permission": 0
},{
    "group_id": 3,
    "group_name": "Sales",
    "user_id": 15432,
    "group_permission": 3
}]
```

### Status Code

Status Code|Message
:---|:---
200|response
600|attempt to create delete event with null entity
710|access is not allowed
