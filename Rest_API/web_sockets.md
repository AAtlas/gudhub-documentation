# Web Sockets

Request of the current type are aimed to sending messages to users.They perform similar actions - sending any messages, but are used in different cases. They also have a specific feature. After using these requests, in addition to the response, there will also be web socket messages.

There are two such requests:

1. [ws/emit-to-user](#ws-emit-to-user)
2. [ws/broadcast-to-app-subscribers](#ws-broadcast-to-app-subscribers)

## ws/emit-to-user

The first request allows you to send various messages to a **specific person**, or rather, a user.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub/ws/emit-to-user`

It consist of the data object that contains the message, an access token and the user ID.

Property|Type|Description
:---|:---|:---
data|`object`|*any data that is transmitted*
token|`string`|*sender's token for access to the service*
user_id|`number`|*accepts ID of the recipient*

***Example:***

```json
data: {some_data: ‘here’,…}
token: "xDDdP3FhfYamgo8XEqdrtgfhyjq4hg2A443zTL6fyRNvGSvuQoxXGCS"
user_id: 22
```

### Response

In response, we receive all the data that was passed in the request, the API, and the sender's default application ID, which is created when the user has just registered.

```json
api: "/ws/emit-to-user"
app_id: 178
response: {some_data: ‘here’,…}
token: "xDDdP3FhfYamgo8XEqdrtgfhyjq4hg2A443zTL6fyRNvGSvuQoxXGCS"
user_id: 22
```

### WebSocket Message

The **recipient user** will receive the following WebSocket message. It is very similar to the request response, but the **`user_id` is an ID of the message sender**.

```json
api: "/ws/emit-to-user"
app_id: 178
response: {some_data: ‘here’,…}
token: "xDDdP3FhfYamgo8XEqdrtgfhyjq4hg2A443zTL6fyRNvGSvuQoxXGCS"
user_id: 1
```

### Status Code

Name|Description
:---|:---
200|user received a message

## ws/broadcast-to-app-subscribers

This request is different from the first one, which sends a message to **all users subscribed to one specific app**.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub/ws/broadcast-to-app-subscribers`

The request contains a data object with a message inside, an access token, and an application with recipient users.

Property|Type|Description
:---|:---|:---
data|`object`|*any data that needs to be transferred*
token|`string`|*sender's token for access to the service*
app_id|`number`|*application whose subscribers are to be notified*

***Example:***

```json
app_id: 178
data: {some_data: ‘here’,…}
token: "xDDdP3FhfYamgo8XEqdrtgfhyjq4hg2A443zTL6fyRNvGSvuQoxXGCS"
```

### Response

It returns all the data that was sent, the AP, and the sender ID in the initialized app that was created when that user first signed up.

```json
api: "/ws/broadcast-to-app-user"
app_id: 178
response: {some_data: ‘here’,…}
token: "xxDDdP3FhfYamgo8XEqdrtgfhyjq4hg2A443zTL6fyRNvGSvuQoxXGCS"
```

### WebSocket Message

In case of this request, **all users which subscribed on the specific application** will receive the web socket message. It contains all the same data as the response to the request, as well as the sender's ID.

```json
api: "/ws/broadcast-to-app-user"
app_id: 178
response: {some_data: ‘here’,…}
token: "xDDdP3FhfYamgo8XEqdrtgfhyjq4hg2A443zTL6fyRNvGSvuQoxXGCS"
user_id: 1
```

### Status Code

Name|Description
:---|:---
200|all users received the message
