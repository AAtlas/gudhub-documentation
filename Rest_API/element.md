# Elements

Element is a part of the user interface, more precisely, that is displayed on the screen. As usual, it contains data, that will be deleted too if someone removes an element.

All actions with files are performed using these requests:

1. [element/add](#element-add)
2. [element/update](#element-update)
3. [element/get](#element-get)
4. [element/delete](#element-delete)
5. [value-increase](#value-increase)

## element/add

Request for creating new elements.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`Post`|`Form Data`|`https://gudhub.com/GudHub_Test/api/new/element/add`

It contains token, application ID, and element object, that in turn contains information about it.

Property|Type|Description
:---|:---|:---
token|`string`|*using for getting access*
app_id|`object`|*unique application ID*
element|`object`|*object which consists of element data*

***Example:***

```json
  token:midj_xxlu-x-.odhkxrlh_cokrlroe_cyhymgwgi
  app_id:3733
  element:{
        "field_id":1,
        "create_field":"newfield1",
        "field_name":"First Name2",
        "data_id":0,
        "data_type":"text",
        "file_name":"0"
  }
```

### Response

In response will be information about created element.

```json
{
  "field_id":42637,
  "field_priority":0,
  "field_name":"First Name2",
  "data_type":"text"
}
```

### Status Code

Name|Description
:---|:---
000|element is created
200 Ok|user have no rights to create a new element

## element/update

Request for updating element data.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub_Test/api/new/element/update`

It contains token and element object.

Name|Type|Description
:---|:---|:---
token|`string`|*using for getting access*
element|`object`|*object which consists of element data*

***Example:***

```json
  Token:midj_xxlu-x-.odhkxrlh_cokrlroe_cyhymgwgi

  element:{
      "field_id":42637,
      "element_id":42637,
      "field_priority":0,
      "field_name":"TESTNAME2",
      "data_type":"text"
  }
```

### Response

In return will be updated element information.

```json
{
  "field_id":42637,
  "element_id":42637,
  "field_priority":0,
  "field_name":"TESTNAME2",
  "data_type":"text"
}
```

### Status Code

Name|Description
:---|:---
000|user have no right to update elements
000|wong data (format or the number of symbols)
200 Ok|element is updated

## element/get

Request for getting an element.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub_Test/api/new/element/get`

It contains token and element ID.

Name|Type|Description
:---|:---|:---
token|`string`|*using for getting access*
element_id|`number`|*unique element ID*

***Example:***

```json
  token:midj_xxlu-x-.odhkxrlh_cokrlroe_cyhymgwgi

  element_id:42637
```

### Response

In return will be an element object.

```json
{
  "field_id":42637,
  "element_id":42637,
  "field_priority":0,
  "field_name":"TESTNAME2",
  "data_type":"text"
}
```

### Status Code

Name|Description
:---|:---
200 Ok|successful operation

## element/delete

Request for deleting elements.

### Request

Request Method|Content Type|Request URL
:---|:---|:---
`POST`|`Form Data`|`https://gudhub.com/GudHub_Test/api/new/element/delete`

It contains token and element ID.

Name|Type|Description
:---|:---|:---
token|`string`|*using for getting access*
element_id|`number`|*unique element ID*

***Example:***

```json
  token:midj_xxlu-x-.odhkxrlh_cokrlroe_cyhymgwgi

  element_id:42637
```

### Response

As a result, you will see a message about successful deteling.

```json
delete field with id = 42637. Ok
```

### Status Code

Name|Description
:---|:---
000|user have no right to delete element
200 Ok|element with a specified ID is deleted

## value-increase

This request helps to increase field value by the specified value It works both with integers and decimals (for example 1, 1.25), with positive and negative numbers.

### Request

It contains token, data ID, and increase value.

Name|Type|Description
:---|:---|:---
token|`string`|*using for getting access*
data_id|`number`|*unique data ID*
increase_on|`number`|*value on which a current value will be increased*

This request helps to increase field value by the specified value.
Sending **token** and parameters **data_id** and **increase_on**.
The method works both with integers and decimals (for example 1, 1.25), with positive and negative numbers.

***Example:***

```json
  increase_on: 5
  data_id: 2115545

  token:.bbzmagbgc_qsbfitdbrl.psqcenmdvqdjbakwyojm.cj
```

### Response

In return will be gotten a result of increasing.

```json
new_value = 13.0
```
